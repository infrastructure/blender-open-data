# Benchmark script development set up

Start a shell in `/benchmark_script` and issue:

 1. `poetry install`
 2. Ensure `./test.sh` succeeds.

To debug the script use something like:
```
blender --background \
        --factory-startup \
        -noaudio \
        --debug-cycles \
        --enable-autoexec \
        --engine \
        CYCLES \
        --python \
        main.py \
        <arguments>
```
