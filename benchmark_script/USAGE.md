# Benchmark script usage
To print the usage of the benchmark script run the following with a supported
Blender version:
```
blender --background \
        --factory-startup \
        -noaudio \
        --debug-cycles \
        --enable-autoexec \
        --engine \
        CYCLES \
        --python \
        main.py \
        -- \
        --help
```
Then to benchmark the `scene.blend` file run something akin to:
```
blender --background \
        --factory-startup \
        -noaudio \
        --debug-cycles \
        --enable-autoexec \
        --engine \
        CYCLES \
        scene.blend \
        --python \
        main.py \
        -- \
        --device-type CPU
```
