.PHONY: all
all: benchmark_script launcher

.PHONY: benchmark_script
benchmark_script:
	$(MAKE) -C benchmark_script
	mkdir -p dist/
	cp benchmark_script/dist/*.tar.gz dist/

.PHONY: launcher
launcher:
	$(MAKE) -C launcher package
	mkdir -p dist/
	cp launcher/dist/* dist/

.PHONY: clean
clean:
	rm -rf dist/
	$(MAKE) -C benchmark_script clean
	$(MAKE) -C launcher clean

.PHONY: test
test:
	cd benchmark_script/ && ./test.sh
	cd launcher/ && ./test.sh
	cd launcher_authenticator/ && ./test.sh
	cd website/ && ./test.sh
