# Releasing

Make an annotated tag of the format `launcher/<major>.<minor>.<patch>` using
```
git tag --annotate launcher/<major>.<minor>.<patch>
git push --tags
```
Make sure you can build the launcher normally, see [BUILD.md](BUILD.md), and
then follow the steps below.

## Linux
`./build_container.sh make package`

## Windows
`make package`

## macOS
`make package`
