#!/usr/bin/env sh
set -eux

docker build \
  --build-arg UID="$(id -u)" \
  --build-arg USERNAME="$(id -un)" \
  --tag blender-open-data-launcher-build-build_container \
  - <build_container.Dockerfile
docker run -ti --user "$(id -u)" --mount "type=bind,source=$(realpath ..),target=/repo" blender-open-data-launcher-build-build_container "$@"
