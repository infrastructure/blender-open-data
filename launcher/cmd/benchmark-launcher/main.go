package main

import (
	"git.blender.org/blender-open-data/launcher/internal"
	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/ui"
)

func main() {
	core.InitLogging()

	config.CLI = false

	err := ui.Execute()
	if err != nil {
		internal.ExitWithError(err)
	}
}
