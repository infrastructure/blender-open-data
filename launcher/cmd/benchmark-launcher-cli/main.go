package main

import (
	"git.blender.org/blender-open-data/launcher/internal"
	"git.blender.org/blender-open-data/launcher/internal/cli"
	"git.blender.org/blender-open-data/launcher/internal/config"
)

func main() {
	config.CLI = true

	err := cli.Execute()
	if err != nil {
		internal.ExitWithError(err)
	}
}
