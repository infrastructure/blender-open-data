package core

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var sampleRegex = regexp.MustCompile(`Sample ([0-9]+)/([0-9]+)`)
var cyclesMemoryRegex = regexp.MustCompile(`\| Mem:([0-9.]+[KM]?), Peak:([0-9.]+[KM]?) \|.*Finished`)

var renderedSamplesInTimeRegex = regexp.MustCompile(`Rendered ([0-9]+) samples in (-?[0-9]+(\.[0-9]+)?) seconds`)
var renderTimeRegex = regexp.MustCompile(`Total render time: ([0-9]+(\.[0-9]+)?)`)
var renderTimeNoSyncRegex = regexp.MustCompile(`Render time \(without synchronization\): (-?[0-9]+(\.[0-9]+)?)`)

var configuredTimeLimitRegex = regexp.MustCompile(`Time limit is set to (-?[0-9]+(\.[0-9]+)?)`)

var (
	errNoData = errors.New("no data available")
)

func IsNoData(err error) bool {
	return err == errNoData
}

type ParsedSamplesInTime struct {
	numberOfSamples int64
	renderTime      float64
	isSet           bool
}

type ParsedRenderTime struct {
	renderTime float64
	isSet      bool
}

type ParsedRenderTimeNoSync struct {
	renderTimeNoSync float64
	isSet            bool
}

type ParsedMemoryUsage struct {
	peakMemoryInMegabytes float64
	isSet                 bool
}

type ParsedConfiguredTimeLimit struct {
	timeLimit float64
	isSet     bool
}

func NewParsedSamplesInTime() ParsedSamplesInTime {
	return ParsedSamplesInTime{0, 0, false}
}

func NewParsedRenderTime() ParsedRenderTime {
	return ParsedRenderTime{0, false}
}

func NewParsedRenderTimeNoSync() ParsedRenderTimeNoSync {
	return ParsedRenderTimeNoSync{0, false}
}

func NewParsedMemoryUsage() ParsedMemoryUsage {
	return ParsedMemoryUsage{0, false}
}

func NewParsedConfiguredTimeLimit() ParsedConfiguredTimeLimit {
	return ParsedConfiguredTimeLimit{0, false}
}

func parseSamplesInTime(line []byte, parsedSamplesInTime *ParsedSamplesInTime) error {
	samplesInTimeMatch := renderedSamplesInTimeRegex.FindSubmatch(line)
	if samplesInTimeMatch == nil {
		return errNoData
	}

	numberOfSamplesBytes := samplesInTimeMatch[1]
	timeBytes := samplesInTimeMatch[2]
	if numberOfSamplesBytes == nil || timeBytes == nil {
		return fmt.Errorf("cannot parse parsedSamplesInTime: %s", string(line))
	}

	numberOfSamples, err := strconv.ParseInt(string(numberOfSamplesBytes), 10, 64)
	if err != nil {
		return err
	}

	renderTime, err := strconv.ParseFloat(string(timeBytes), 64)
	if err != nil {
		return err
	}

	parsedSamplesInTime.numberOfSamples = numberOfSamples
	parsedSamplesInTime.renderTime = renderTime
	parsedSamplesInTime.isSet = true

	return nil
}

func parseUpdateRenderTime(line []byte, parsedRenderTime *ParsedRenderTime) error {
	renderTimeMatch := renderTimeRegex.FindSubmatch(line)
	if renderTimeMatch == nil {
		return errNoData
	}

	renderTimeBytes := renderTimeMatch[1]
	if renderTimeBytes == nil {
		return fmt.Errorf("cannot parse totalRenderTime: %s", string(line))
	}

	renderTime, err := strconv.ParseFloat(string(renderTimeBytes), 64)
	if err != nil {
		return err
	}

	parsedRenderTime.renderTime = renderTime
	parsedRenderTime.isSet = true

	return nil
}

func parseUpdateRenderTimeNoSync(line []byte, parsedRenderTimeNoSync *ParsedRenderTimeNoSync) error {
	renderTime := renderTimeNoSyncRegex.FindSubmatch(line)
	if renderTime == nil {
		return errNoData
	}

	renderTimeNoSyncBytes := renderTime[1]
	if renderTimeNoSyncBytes == nil {
		return fmt.Errorf("cannot parse renderTimeNoSync: %s", string(line))
	}

	renderTimeNoSync, err := strconv.ParseFloat(string(renderTimeNoSyncBytes), 64)
	if err != nil {
		return err
	}

	parsedRenderTimeNoSync.renderTimeNoSync = renderTimeNoSync
	parsedRenderTimeNoSync.isSet = true

	return nil
}

func parsePeakMemoryUsage(line []byte, parsedMemoryUsage *ParsedMemoryUsage) error {
	cyclesMemory := cyclesMemoryRegex.FindSubmatch(line)
	if cyclesMemory == nil {
		return errNoData
	}

	peakMemoryBytes := cyclesMemory[2]
	peakMemoryBytesLen := len(peakMemoryBytes)
	if peakMemoryBytes == nil || peakMemoryBytesLen <= 1 {
		return fmt.Errorf("cannot parse peakMemory: %s", string(line))
	}

	var denominator float64
	switch peakMemoryBytes[peakMemoryBytesLen-1] {
	case 'K':
		denominator = 1024.0
	case 'M':
		denominator = 1.0
	default:
		return fmt.Errorf("cannot parse peakMemory: %s", string(line))
	}

	peakMemory, err := strconv.ParseFloat(string(peakMemoryBytes[:peakMemoryBytesLen-1]), 64)
	if err != nil {
		return err
	}

	parsedMemoryUsage.peakMemoryInMegabytes = peakMemory / denominator
	parsedMemoryUsage.isSet = true

	return nil
}

func parseConfiguredTimeLimit(line []byte, parsedConfiguredTimeLimit *ParsedConfiguredTimeLimit) error {
	configuredTimeLimitMatch := configuredTimeLimitRegex.FindSubmatch(line)
	if configuredTimeLimitMatch == nil {
		return errNoData
	}

	configuredTimeLimitBytes := configuredTimeLimitMatch[1]
	if configuredTimeLimitBytes == nil {
		return fmt.Errorf("Missing configuredTimeLimit: %s", string(line))
	}

	configuredTimeLimitBytesLen := len(configuredTimeLimitBytes)
	if configuredTimeLimitBytesLen < 1 {
		return fmt.Errorf("Empty configuredTimeLimit: %s", string(line))
	}

	configuredTimeLimit, err := strconv.ParseFloat(string(configuredTimeLimitBytes[:configuredTimeLimitBytesLen]), 64)
	if err != nil {
		return err
	}

	parsedConfiguredTimeLimit.timeLimit = configuredTimeLimit
	parsedConfiguredTimeLimit.isSet = true

	return nil
}

func parseProgress(line []byte) (Progress, error) {
	sampleMatch := sampleRegex.FindSubmatch(line)
	if sampleMatch == nil {
		return Progress{}, errNoData
	}

	numRenderedSamplesBytes := sampleMatch[1]
	numTotalSamplesBytes := sampleMatch[2]

	if numRenderedSamplesBytes == nil || numTotalSamplesBytes == nil {
		return Progress{}, fmt.Errorf("missing samples information")
	}

	numRenderedSamples, err := strconv.ParseInt(string(numRenderedSamplesBytes), 10, 32)
	if err != nil {
		return Progress{}, err
	}
	numTotalSamples, err := strconv.ParseInt(string(numTotalSamplesBytes), 10, 32)
	if err != nil {
		return Progress{}, err
	}

	return Progress{
		NumRenderedSamples: int(numRenderedSamples),
		NumTotalSamples:    int(numTotalSamples),
	}, nil
}

func parseDevices(messages []json.RawMessage) ([]Device, error) {
	type deviceEnvelop struct {
		Type string `json:"type"`
	}

	var devices []Device
	for _, message := range messages {
		var deviceEnv deviceEnvelop
		if err := json.Unmarshal(message, &deviceEnv); err != nil {
			return nil, err
		}

		deviceType := deviceEnv.Type
		if deviceType == string(CPUType) {
			var cpuDevice CPUDevice
			decoder := json.NewDecoder(bytes.NewReader(message))
			decoder.DisallowUnknownFields()
			if err := decoder.Decode(&cpuDevice); err != nil {
				return nil, err
			}
			devices = append(devices, cpuDevice)
		} else {
			var gpuDevice GPUDevice
			decoder := json.NewDecoder(bytes.NewReader(message))
			decoder.DisallowUnknownFields()
			if err := decoder.Decode(&gpuDevice); err != nil {
				return nil, err
			}
			devices = append(devices, gpuDevice)
		}
	}

	return devices, nil
}

func (systemInfo *RenderSystemInfo) UnmarshalJSON(data []byte) error {
	var env struct {
		Bitness       string            `json:"bitness"`
		Machine       string            `json:"machine"`
		System        string            `json:"system"`
		DistName      string            `json:"dist_name"`
		DistVersion   string            `json:"dist_version"`
		Devices       []json.RawMessage `json:"devices"`
		NumCPUSockets int               `json:"num_cpu_sockets"`
		NumCPUCores   int               `json:"num_cpu_cores"`
		NumCPUThreads int               `json:"num_cpu_threads"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}
	devices, err := parseDevices(env.Devices)
	if err != nil {
		return err
	}

	systemInfo.Bitness = env.Bitness
	systemInfo.Machine = env.Machine
	systemInfo.System = env.System
	systemInfo.DistName = env.DistName
	systemInfo.DistVersion = env.DistVersion
	systemInfo.NumCPUSockets = env.NumCPUSockets
	systemInfo.NumCPUCores = env.NumCPUCores
	systemInfo.NumCPUThreads = env.NumCPUThreads
	systemInfo.Devices = devices
	return nil
}

func (deviceInfo *RenderDeviceInfo) UnmarshalJSON(data []byte) error {
	var env struct {
		DeviceType     string            `json:"device_type"`
		ComputeDevices []json.RawMessage `json:"compute_devices"`
		NumCPUThreads  int               `json:"num_cpu_threads"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}
	computeDevices, err := parseDevices(env.ComputeDevices)
	if err != nil {
		return err
	}

	deviceInfo.DeviceType = env.DeviceType
	deviceInfo.NumCPUThreads = env.NumCPUThreads
	deviceInfo.ComputeDevices = computeDevices
	return nil
}

func parseRenderInfo(text string) (*RenderInfo, error) {
	var renderInfo RenderInfo
	decoder := json.NewDecoder(strings.NewReader(text))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&renderInfo); err != nil {
		return nil, err
	}
	return &renderInfo, nil
}

func parseDeviceList(text string) ([]Device, error) {
	var messages []json.RawMessage
	decoder := json.NewDecoder(strings.NewReader(text))
	if err := decoder.Decode(&messages); err != nil {
		return nil, err
	}
	return parseDevices(messages)
}

func (device *CPUDevice) UnmarshalJSON(data []byte) error {
	var env struct {
		Type string `json:"type"`
		Name string `json:"name"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}
	switch env.Type {
	case string(CPUType):
		device.Type = CPUType
	default:
		return fmt.Errorf("type should be CPU")
	}
	device.Name = env.Name

	return nil
}

func (device *GPUDevice) UnmarshalJSON(data []byte) error {
	var env struct {
		Type      string `json:"type"`
		Name      string `json:"name"`
		IsDisplay bool   `json:"is_display"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}
	switch env.Type {
	case string(CUDAType):
		device.Type = CUDAType
	case string(HIPType):
		device.Type = HIPType
	case string(OptiXType):
		device.Type = OptiXType
	case string(MetalType):
		device.Type = MetalType
	case string(OneAPIType):
		device.Type = OneAPIType
	default:
		return fmt.Errorf("type should be OPTIX, CUDA, HIP, METAL, or ONEAPI")
	}
	device.Name = env.Name
	device.IsDisplay = env.IsDisplay

	return nil
}
