package core

import (
	"bufio"
	"context"
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"
)

func ListDevices(blenderExecutable BlenderExecutable, benchmarkScript BenchmarkScript, ctx context.Context) ([]Device, error) {
	DebugLogger.Print("Listing devices.")
	DebugLogger.Printf("blenderExecutable: %#v", blenderExecutable)

	arguments := []string{
		"--background",
		"--factory-startup",
		"-noaudio",
		"--debug-cycles",
		"--enable-autoexec",
		"--engine",
		"CYCLES",
		"--python",
		string(benchmarkScript),
		"--",
		"--list-devices",
	}

	DebugLogger.Printf("arguments: %v", arguments)

	cmdCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	cmd := exec.CommandContext(cmdCtx, string(blenderExecutable), arguments...) // nolint:gosec
	cmd.Dir = filepath.Dir(string(blenderExecutable))
	DecorateCommand(cmd)

	outputPipe, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	cmd.Stderr = cmd.Stdout

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	var devices *[]Device = nil

	lineScanner := bufio.NewScanner(outputPipe)
	for lineScanner.Scan() {
		blenderOutputLine := lineScanner.Bytes()

		BlenderOutputLogger.Println(string(blenderOutputLine))

		if strings.HasPrefix(string(blenderOutputLine), "Benchmark JSON Device List: ") {
			trimmedLine := strings.TrimSpace(strings.TrimPrefix(string(blenderOutputLine), "Benchmark JSON Device List: "))
			parsedDevices, err := parseDeviceList(trimmedLine)
			if err != nil {
				return nil, err
			} else {
				devices = &parsedDevices
			}
		}
	}

	err = cmd.Wait()
	// Blender tends to exit with segfaults, so if the Devices are available
	// we consider the run a success.
	if devices == nil && err != nil {
		return nil, err
	}

	if devices == nil {
		return nil, fmt.Errorf("missing Benchmark JSON Device List")
	}

	DebugLogger.Print("Finished listing devices.")
	return *devices, nil
}
