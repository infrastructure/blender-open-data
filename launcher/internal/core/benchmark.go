package core

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

func scanBenchmarkOutput(
	output io.ReadCloser,
	progressChannel chan Progress,
) (*RenderInfo, error) {
	var renderInfo *RenderInfo = nil

	var parsedSamplesInTime = NewParsedSamplesInTime()
	var parsedRenderTime = NewParsedRenderTime()
	var parsedRenderTimeNoSync = NewParsedRenderTimeNoSync()
	var parsedMemoryUsage = NewParsedMemoryUsage()
	var parsedConfiguredTimeLimit = NewParsedConfiguredTimeLimit()

	lineScanner := bufio.NewScanner(output)

	for lineScanner.Scan() {
		blenderOutputLine := lineScanner.Bytes()

		BlenderOutputLogger.Println(string(blenderOutputLine))

		if strings.HasPrefix(string(blenderOutputLine), "Benchmark JSON Data: ") {
			trimmedLine := strings.TrimSpace(strings.TrimPrefix(string(blenderOutputLine), "Benchmark JSON Data: "))
			parsedRenderInfo, err := parseRenderInfo(trimmedLine)
			if err != nil {
				return nil, err
			}
			renderInfo = parsedRenderInfo
			continue
		}

		progress, err := parseProgress(blenderOutputLine)
		if err != nil && IsNoData(err) {
			// Pass: ignore lines which do not contain progress report.
		} else if err == nil {
			progress.TimeLimit = parsedConfiguredTimeLimit.timeLimit
			progressChannel <- progress
		} else {
			return nil, err
		}

		err = parseSamplesInTime(blenderOutputLine, &parsedSamplesInTime)
		if err != nil && !IsNoData(err) {
			return nil, err
		}

		err = parseUpdateRenderTime(blenderOutputLine, &parsedRenderTime)
		if err != nil && !IsNoData(err) {
			return nil, err
		}

		err = parseUpdateRenderTimeNoSync(blenderOutputLine, &parsedRenderTimeNoSync)
		if err != nil && !IsNoData(err) {
			return nil, err
		}

		err = parsePeakMemoryUsage(blenderOutputLine, &parsedMemoryUsage)
		if err != nil && !IsNoData(err) {
			return nil, err
		}

		err = parseConfiguredTimeLimit(blenderOutputLine, &parsedConfiguredTimeLimit)
		if err != nil && !IsNoData(err) {
			return nil, err
		}
	}

	if renderInfo == nil {
		return nil, fmt.Errorf("Did not receive Benchmark JSON Data.")
	}
	if !parsedSamplesInTime.isSet {
		return nil, fmt.Errorf("Missing value for \"number_of_samples\".")
	}
	if !parsedRenderTime.isSet {
		return nil, fmt.Errorf("Missing value for \"total_render_time\".")
	}
	if !parsedRenderTimeNoSync.isSet {
		return nil, fmt.Errorf("Missing value for \"render_time_no_sync\".")
	}
	if !parsedMemoryUsage.isSet {
		return nil, fmt.Errorf("Missing value for \"device_peak_memory\".")
	}

	secondsPerSample := parsedSamplesInTime.renderTime / float64(parsedSamplesInTime.numberOfSamples)
	samplesPerMinute := 60 / secondsPerSample

	renderInfo.Stats = RenderStats{
		DevicePeakMemory: parsedMemoryUsage.peakMemoryInMegabytes,
		NumberOfSamples:  parsedSamplesInTime.numberOfSamples,
		TimeForSamples:   parsedSamplesInTime.renderTime,
		SamplesPerMinute: samplesPerMinute,
		TotalRenderTime:  parsedRenderTime.renderTime,
		RenderTimeNoSync: parsedRenderTimeNoSync.renderTimeNoSync,
		TimeLimit:        parsedConfiguredTimeLimit.timeLimit,
	}
	return renderInfo, nil
}

type BlenderExecutable string
type BlendFile string
type BenchmarkScript string

func RunBlenderBenchmark(
	blenderExecutable BlenderExecutable,
	benchmarkScript BenchmarkScript,
	requestedDevice RequestedDevice,
	blendFile BlendFile,
	warmUp WarmUp,
	ctx context.Context,
	progressChannel chan Progress,
) (*RenderInfo, error) {

	DebugLogger.Print("Starting benchmark.")
	DebugLogger.Printf("blenderExecutable: %#v", blenderExecutable)

	arguments := []string{
		"--background",
		"--factory-startup",
		"-noaudio",
		"--debug-cycles",
		"--enable-autoexec",
		"--engine",
		"CYCLES",
		string(blendFile),
		"--python",
		string(benchmarkScript),
		"--",
	}
	if requestedDevice.Type != NotSpecifiedType {
		arguments = append(arguments, "--device-type")
		arguments = append(arguments, string(requestedDevice.Type))
	}
	if requestedDevice.Name != "" {
		arguments = append(arguments, "--device")
		arguments = append(arguments, requestedDevice.Name)
	}
	if warmUp {
		arguments = append(arguments, "--warm-up")
	}

	DebugLogger.Printf("arguments: %v", arguments)

	cmdCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	cmd := exec.CommandContext(cmdCtx, string(blenderExecutable), arguments...) // nolint:gosec
	cmd.Dir = filepath.Dir(string(blenderExecutable))
	DecorateCommand(cmd)

	outputPipe, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	cmd.Stderr = cmd.Stdout

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	renderInfo, err := scanBenchmarkOutput(outputPipe, progressChannel)
	if err != nil {
		return nil, err
	}

	if err := cmd.Wait(); err != nil {
		return renderInfo, err
	}

	DebugLogger.Print("Finished benchmark.")
	return renderInfo, nil
}

func RunBenchmark(
	blenderExecutable BlenderExecutable,
	benchmarkScript BenchmarkScript,
	requestedDevice RequestedDevice,
	blendFile BlendFile,
	warmUp WarmUp,
	ctx context.Context,
	progressChannel chan Progress,
) (*RenderInfo, error) {
	heartCtx, ctxCancel := context.WithCancel(ctx)
	wg := new(sync.WaitGroup)
	wg.Add(1)

	go func() {
		defer wg.Done()

		for {
			select {
			case <-heartCtx.Done():
				return
			case <-time.After(200 * time.Millisecond):
				progressChannel <- Progress{IsHeartbeat: true}
			}
		}
	}()

	info, err := RunBlenderBenchmark(
		blenderExecutable,
		benchmarkScript,
		requestedDevice,
		blendFile,
		warmUp,
		ctx,
		progressChannel,
	)

	ctxCancel()
	wg.Wait()

	return info, err
}
