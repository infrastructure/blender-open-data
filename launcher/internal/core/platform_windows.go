//go:build windows
// +build windows

package core

import (
	"os/exec"
	"syscall"
)

const BlenderExecutableName = "blender.exe"
const CurrentOperatingSystem = OperatingSystemWindows

func platformSpecificBrowserCommands() [][]string {
	return [][]string{{"cmd", "/c", "start"}}
}

func DecorateCommand(cmd *exec.Cmd) {
	cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
}
