//go:build darwin
// +build darwin

package core

import "os/exec"

const BlenderExecutableName = "Blender"
const CurrentOperatingSystem = OperatingSystemMacOS

func platformSpecificBrowserCommands() [][]string {
	return [][]string{{"open"}}
}

func DecorateCommand(cmd *exec.Cmd) {
}
