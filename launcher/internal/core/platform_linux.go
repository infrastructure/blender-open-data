//go:build linux
// +build linux

package core

import (
	"os"
	"os/exec"
)

const BlenderExecutableName = "blender"
const CurrentOperatingSystem = OperatingSystemLinux

func platformSpecificBrowserCommands() [][]string {
	var commands [][]string = nil
	if os.Getenv("DISPLAY") != "" {
		commands = append(commands, []string{"xdg-open"})
	}
	commands = append(commands,
		[]string{"firefox"},
		[]string{"chromium"},
		[]string{"google-chrome"},
		[]string{"chrome"},
	)
	return commands
}

func DecorateCommand(cmd *exec.Cmd) {
}
