package core

import (
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"time"
	"strings"
)

func OpenBrowser(URL url.URL) bool {
	DebugLogger.Printf("Trying to open URL in browser: %s.", URL.String())

	for _, command := range browserCommands() {
		process := exec.Command(command[0], append(command[1:], URL.String())...) // nolint:gosec
		DecorateCommand(process)

		err := process.Start()
		if err != nil {
			return false
		}

		errors := make(chan error, 1)
		go func() {
			errors <- process.Wait()
		}()

		// We consider the browser to have opened successfully if it exits
		// cleanly or has been open for three seconds.
		select {
		case <-time.After(3 * time.Second):
			DebugLogger.Printf("Browser was open for more than 3 seconds, assuming everything is okay.")
			return true
		case err := <-errors:
			if err == nil {
				return true
			}
		}
	}

	DebugLogger.Printf("Failed to open browser.")

	return false
}

func browserCommands() [][]string {
	var commands [][]string = nil

	browser := os.Getenv("BROWSER")
	if browser != "" {
		commands = append(commands, []string{browser})
		return commands
	}

	commands = append(commands, platformSpecificBrowserCommands()...)

	DebugLogger.Printf("Possible browser commands: %v", commands)

	return commands
}

func ConfigDirectory() (string, error) {
	platformConfigDir, err := os.UserConfigDir()
	if err != nil {
		return "", err
	}

	configDir := filepath.Join(platformConfigDir, "blender-benchmark-launcher")

	err = os.MkdirAll(configDir, 0700)
	if err != nil {
		return "", err
	}

	return configDir, nil
}

func CacheDirectory() (string, error) {
	platformCacheDir, err := os.UserCacheDir()
	if err != nil {
		return "", err
	}

	cacheDir := filepath.Join(platformCacheDir, "blender-benchmark-launcher")

	err = os.MkdirAll(cacheDir, 0755)
	if err != nil {
		return "", err
	}

	return cacheDir, nil
}

func GetCurrentArchitecture() Architecture {
    if runtime.GOARCH == "arm64" {
        return ArchitectureARM64
    }
    processIdentifier := os.Getenv("PROCESSOR_IDENTIFIER")
    if strings.Contains(processIdentifier, "ARM") {
        return ArchitectureARM64
    }
    return ArchitectureX64
}
