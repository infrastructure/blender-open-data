package core

type DeviceType string

const (
	NotSpecifiedType DeviceType = "NONE"
	CPUType          DeviceType = "CPU"
	HIPType          DeviceType = "HIP"
	CUDAType         DeviceType = "CUDA"
	OptiXType        DeviceType = "OPTIX"
	MetalType        DeviceType = "METAL"
	OneAPIType       DeviceType = "ONEAPI"
)

type WarmUp bool

type RequestedDevice struct {
	Type DeviceType
	Name string
}

type Device interface {
	GetType() DeviceType
	GetName() string
}

type CPUDevice struct {
	Type DeviceType `json:"type"`
	Name string     `json:"name"`
}

func (device CPUDevice) GetType() DeviceType {
	return device.Type
}

func (device CPUDevice) GetName() string {
	return device.Name
}

type GPUDevice struct {
	Type      DeviceType `json:"type"`
	Name      string     `json:"name"`
	IsDisplay bool       `json:"is_display"`
}

func (device GPUDevice) GetType() DeviceType {
	return device.Type
}

func (device GPUDevice) GetName() string {
	return device.Name
}

type RenderStats struct {
	DevicePeakMemory float64 `json:"device_peak_memory"`
	NumberOfSamples  int64   `json:"number_of_samples"`
	TimeForSamples   float64 `json:"time_for_samples"`
	SamplesPerMinute float64 `json:"samples_per_minute"`
	TotalRenderTime  float64 `json:"total_render_time"`
	RenderTimeNoSync float64 `json:"render_time_no_sync"`
	TimeLimit        float64 `json:"time_limit"`
}

type RenderSystemInfo struct {
	Bitness       string   `json:"bitness"`
	Machine       string   `json:"machine"`
	System        string   `json:"system"`
	DistName      string   `json:"dist_name"`
	DistVersion   string   `json:"dist_version"`
	Devices       []Device `json:"devices"`
	NumCPUSockets int      `json:"num_cpu_sockets"`
	NumCPUCores   int      `json:"num_cpu_cores"`
	NumCPUThreads int      `json:"num_cpu_threads"`
}

type RenderDeviceInfo struct {
	DeviceType     string   `json:"device_type"`
	ComputeDevices []Device `json:"compute_devices"`
	NumCPUThreads  int      `json:"num_cpu_threads"`
}

type RenderInfoBlenderVersion struct {
	Version         string `json:"version"`
	BuildDate       string `json:"build_date"`
	BuildTime       string `json:"build_time"`
	BuildCommitDate string `json:"build_commit_date"`
	BuildCommitTime string `json:"build_commit_time"`
	BuildHash       string `json:"build_hash"`
}

type RenderInfo struct {
	Timestamp      string                   `json:"timestamp"`
	BlenderVersion RenderInfoBlenderVersion `json:"blender_version"`
	SystemInfo     RenderSystemInfo         `json:"system_info"`
	DeviceInfo     RenderDeviceInfo         `json:"device_info"`
	Stats          RenderStats              `json:"stats"`
}

type Progress struct {
	NumRenderedSamples int
	NumTotalSamples    int
	TimeLimit          float64

	IsHeartbeat bool
}

type OperatingSystem string

const (
	OperatingSystemLinux   = OperatingSystem("linux")
	OperatingSystemMacOS   = OperatingSystem("darwin")
	OperatingSystemWindows = OperatingSystem("windows")
)

type Architecture string

const (
	ArchitectureARM64 = Architecture("arm64")
	ArchitectureX64   = Architecture("x64")
)
