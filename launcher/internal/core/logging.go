package core

import (
	"io"
	"log"
	"os"
	"path/filepath"
)

var VerbosityFlag uint8 = 1

var BlenderOutputLogger *log.Logger = nil
var DebugLogger *log.Logger = nil
var ErrorLogger *log.Logger = nil
var OutputLogger *log.Logger = nil
var DisplayLogger *log.Logger = nil

func LogFilePath() (string, error) {
	cacheDir, err := CacheDirectory()
	if err != nil {
		return "", err
	}
	return filepath.Join(cacheDir, "benchmark-launcher.log"), nil
}

func InitLogging() {
	blenderOutputWriters := make([]io.Writer, 0)
	debugWriters := make([]io.Writer, 0)
	errorWriters := make([]io.Writer, 0)
	outputWriters := make([]io.Writer, 0)
	displayWriters := make([]io.Writer, 0)

	logFilePath, err := LogFilePath()
	if err == nil {
		logFile, err := os.OpenFile(logFilePath, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, os.FileMode(0644))
		if err == nil {
			blenderOutputWriters = append(blenderOutputWriters, logFile)
			debugWriters = append(debugWriters, logFile)
			errorWriters = append(errorWriters, logFile)
			outputWriters = append(outputWriters, logFile)
			displayWriters = append(displayWriters, logFile)
		}
	}

	if VerbosityFlag >= 3 {
		blenderOutputWriters = append(blenderOutputWriters, os.Stderr)
	}

	if VerbosityFlag >= 2 {
		debugWriters = append(debugWriters, os.Stderr)
	}

	if VerbosityFlag >= 1 {
		errorWriters = append(errorWriters, os.Stderr)
	}

	outputWriters = append(outputWriters, os.Stdout)
	displayWriters = append(displayWriters, os.Stderr)

	BlenderOutputLogger = log.New(io.MultiWriter(blenderOutputWriters...), "BLENDER: ", 0)
	DebugLogger = log.New(io.MultiWriter(debugWriters...), "", log.LstdFlags|log.LUTC|log.Lshortfile)
	ErrorLogger = log.New(io.MultiWriter(errorWriters...), "ERROR: ", 0)
	OutputLogger = log.New(io.MultiWriter(outputWriters...), "", 0)
	DisplayLogger = log.New(io.MultiWriter(displayWriters...), "", 0)
}
