package rpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/core"
)

type Config struct {
	UserAgent         string
	Client            http.Client
	MetadataURL       url.URL
	AuthenticationURL url.URL
	SubmitURL         url.URL
	LogsURL           url.URL
}

type MessageType string
type ResponseType MessageType
type RequestType MessageType

type ResponseEnvelope struct {
	Message Response
}

func (envelope *ResponseEnvelope) UnmarshalJSON(data []byte) error {
	var genericEnvelope struct {
		MessageType string `json:"message_type"`
		Message     json.RawMessage
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&genericEnvelope); err != nil {
		return err
	}

	switch genericEnvelope.MessageType {
	case string(TokenResponseType):
		message := TokenResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(MetadataResponseType):
		message := MetadataResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(VerifiedResponseType):
		message := VerifiedResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(RejectedResponseType):
		message := RejectedResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(SubmissionSuccessfulResponseType):
		message := SubmissionSuccessfulResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(VerificationTimedOutResponseType):
		message := VerificationTimedOutResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(ParseErrorResponseType):
		message := ParseErrorResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(UnhandledRequestResponseType):
		message := UnhandledRequestResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(InvalidLauncherErrorResponseType):
		message := InvalidLauncherErrorResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(InvalidTokenErrorResponseType):
		message := InvalidTokenErrorResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(NonVerifiedTokenErrorResponseType):
		message := NonVerifiedTokenErrorResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	case string(SchemaValidationErrorResponseType):
		message := SchemaValidationErrorResponse{}
		if err := json.Unmarshal(genericEnvelope.Message, &message); err != nil {
			return err
		}
		envelope.Message = message
	default:
		return fmt.Errorf("unhandled response type: %s", genericEnvelope.MessageType)
	}

	return nil
}

type RequestEnvelope struct {
	Message Request
}

func (envelope RequestEnvelope) MarshalJSON() ([]byte, error) {
	switch message := envelope.Message.(type) {
	case AuthenticationRequest:
		var specializedEnvelope struct {
			MessageType string                `json:"message_type"`
			Message     AuthenticationRequest `json:"message"`
		}
		specializedEnvelope.MessageType = string(AuthenticationRequestType)
		specializedEnvelope.Message = message
		jsonEncodedEnvelope, err := json.Marshal(specializedEnvelope)
		if err != nil {
			return nil, err
		} else {
			return jsonEncodedEnvelope, nil
		}
	case SubmissionRequest:
		var specializedEnvelope struct {
			MessageType string            `json:"message_type"`
			Message     SubmissionRequest `json:"message"`
		}
		specializedEnvelope.MessageType = string(SubmissionRequestType)
		specializedEnvelope.Message = message
		jsonEncodedEnvelope, err := json.Marshal(specializedEnvelope)
		if err != nil {
			return nil, err
		} else {
			return jsonEncodedEnvelope, nil
		}
	default:
		return nil, fmt.Errorf("unkown request type of message: %s", message)
	}
}

type Response interface{}

type Request interface{}

type TokenResponse struct {
	VerificationURL url.URL
	Token           string
}

func (tokenResponse *TokenResponse) UnmarshalJSON(data []byte) error {
	var env struct {
		VerificationUrl string `json:"verification_url"`
		Token           string `json:"token"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	VerificationUrl, err := url.Parse(env.VerificationUrl)
	if err != nil {
		return err
	}
	tokenResponse.VerificationURL = *VerificationUrl
	tokenResponse.Token = env.Token

	return nil
}

const TokenResponseType = ResponseType("token_response")

type SizeInBytes int64

type LauncherSHA256Checksum SHA256Checksum
type LauncherLabel string

type Launcher struct {
	Label           LauncherLabel
	URL             url.URL
	Checksum        LauncherSHA256Checksum
	Size            SizeInBytes
	OperatingSystem core.OperatingSystem
	Supported       bool
	Latest          bool
	CLI             bool
}

func (launcher *Launcher) UnmarshalJSON(data []byte) error {
	var env struct {
		Label           string `json:"label"`
		URL             string `json:"url"`
		Checksum        string `json:"checksum"`
		Size            int    `json:"size"`
		OperatingSystem string `json:"operating_system"`
		Supported       bool   `json:"supported"`
		Latest          bool   `json:"latest"`
		CLI             bool   `json:"cli"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	launcher.Label = LauncherLabel(env.Label)
	URL, err := url.Parse(env.URL)
	if err != nil {
		return err
	}
	launcher.URL = *URL
	launcher.Checksum = LauncherSHA256Checksum(env.Checksum)
	launcher.Size = SizeInBytes(env.Size)
	switch env.OperatingSystem {
	case string(core.OperatingSystemLinux):
		launcher.OperatingSystem = core.OperatingSystemLinux
	case string(core.OperatingSystemWindows):
		launcher.OperatingSystem = core.OperatingSystemWindows
	case string(core.OperatingSystemMacOS):
		launcher.OperatingSystem = core.OperatingSystemMacOS
	default:
		return fmt.Errorf("invalid value for operating_system")
	}
	launcher.Supported = env.Supported
	launcher.Latest = env.Latest
	launcher.CLI = env.CLI

	return nil
}

type SceneSHA256Checksum SHA256Checksum
type SceneLabel string

type Scene struct {
	Label    SceneLabel
	URL      url.URL
	Checksum SceneSHA256Checksum
	Size     SizeInBytes
}

func (scene *Scene) UnmarshalJSON(data []byte) error {
	var env struct {
		Label    string `json:"label"`
		URL      string `json:"url"`
		Checksum string `json:"checksum"`
		Size     int    `json:"size"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	scene.Label = SceneLabel(env.Label)
	URL, err := url.Parse(env.URL)
	if err != nil {
		return err
	}
	scene.URL = *URL
	scene.Checksum = SceneSHA256Checksum(env.Checksum)
	scene.Size = SizeInBytes(env.Size)

	return nil
}

type BenchmarkScriptSHA256Checksum SHA256Checksum
type BenchmarkScriptLabel string

type BenchmarkScript struct {
	Label    BenchmarkScriptLabel
	URL      url.URL
	Checksum BenchmarkScriptSHA256Checksum
	Size     SizeInBytes
}

func (benchmarkScript *BenchmarkScript) UnmarshalJSON(data []byte) error {
	var env struct {
		Label    string `json:"label"`
		URL      string `json:"url"`
		Checksum string `json:"checksum"`
		Size     int    `json:"size"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	benchmarkScript.Label = BenchmarkScriptLabel(env.Label)
	URL, err := url.Parse(env.URL)
	if err != nil {
		return err
	}
	benchmarkScript.URL = *URL
	benchmarkScript.Checksum = BenchmarkScriptSHA256Checksum(env.Checksum)
	benchmarkScript.Size = SizeInBytes(env.Size)

	return nil
}

type BlenderVersionSHA256Checksum SHA256Checksum
type BlenderVersionLabel string

type BlenderVersion struct {
	Label           BlenderVersionLabel
	URL             url.URL
	Checksum        BlenderVersionSHA256Checksum
	Size            SizeInBytes
	OperatingSystem core.OperatingSystem
	Architecture    core.Architecture
	BenchmarkScript BenchmarkScript
	AvailableScenes []Scene
}

func (blenderVersion *BlenderVersion) UnmarshalJSON(data []byte) error {
	var env struct {
		Label           string          `json:"label"`
		URL             string          `json:"url"`
		Checksum        string          `json:"checksum"`
		Size            int             `json:"size"`
		OperatingSystem string          `json:"operating_system"`
		Architecture    string          `json:"architecture"`
		BenchmarkScript BenchmarkScript `json:"benchmark_script"`
		AvailableScenes []Scene         `json:"available_scenes"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	blenderVersion.Label = BlenderVersionLabel(env.Label)
	URL, err := url.Parse(env.URL)
	if err != nil {
		return err
	}
	blenderVersion.URL = *URL
	blenderVersion.Checksum = BlenderVersionSHA256Checksum(env.Checksum)
	blenderVersion.Size = SizeInBytes(env.Size)

	switch env.OperatingSystem {
	case string(core.OperatingSystemLinux):
		blenderVersion.OperatingSystem = core.OperatingSystemLinux
	case string(core.OperatingSystemWindows):
		blenderVersion.OperatingSystem = core.OperatingSystemWindows
	case string(core.OperatingSystemMacOS):
		blenderVersion.OperatingSystem = core.OperatingSystemMacOS
	default:
		return fmt.Errorf("invalid value for operating_system")
	}

	switch env.Architecture {
	case string(core.ArchitectureARM64):
		blenderVersion.Architecture = core.Architecture(core.ArchitectureARM64)
	case string(core.ArchitectureX64):
		blenderVersion.Architecture = core.Architecture(core.ArchitectureX64)
	default:
		return fmt.Errorf("invalid value for architecture: '%s'", env.Architecture)
	}

	blenderVersion.BenchmarkScript = env.BenchmarkScript
	blenderVersion.AvailableScenes = env.AvailableScenes
	return nil
}

type MetadataResponse struct {
	Launchers       []Launcher       `json:"launchers"`
	BlenderVersions []BlenderVersion `json:"blender_versions"`
}

const MetadataResponseType = ResponseType("metadata_response")

type VerifiedResponse struct {
	UserId   int    `json:"user_id"`
	UserName string `json:"username"`
	Email    string `json:"email"`
}

const VerifiedResponseType = ResponseType("verified_response")

type RejectedResponse struct {
}

const RejectedResponseType = ResponseType("rejected_response")

type VerificationTimedOutResponse struct {
}

const VerificationTimedOutResponseType = ResponseType("verification_timed_out_response")

type ErrorResponse interface {
	GetMessage() string
}

type ParseErrorResponse struct {
	Message string `json:"message"`
}

const ParseErrorResponseType = ResponseType("parse_error_response")

type UnhandledRequestResponse struct {
	Message     string `json:"message"`
	MessageType string `json:"message_type"`
}

const UnhandledRequestResponseType = ResponseType("unhandled_request_response")

type AuthenticationRequest struct {
	Hostname string `json:"hostname"`
}

const AuthenticationRequestType = RequestType("authentication_request")

type BenchmarkResults struct {
	SchemaVersion string            `json:"schema_version"`
	Data          []BenchmarkResult `json:"data"`
}

type SubmissionRequest struct {
	Token            Token                  `json:"token"`
	LauncherChecksum LauncherSHA256Checksum `json:"launcher_checksum"`
	BenchmarkResults BenchmarkResults       `json:"benchmark"`
}

const SubmissionRequestType = RequestType("submission_request")

type SubmissionSuccessfulResponse struct {
	BenchmarkURL url.URL
}

const SubmissionSuccessfulResponseType = ResponseType("submission_successful_response")

func (response *SubmissionSuccessfulResponse) UnmarshalJSON(data []byte) error {
	var env struct {
		BenchmarkURL string `json:"benchmark_url"`
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&env); err != nil {
		return err
	}

	BenchmarkURL, err := url.Parse(env.BenchmarkURL)
	if err != nil {
		return err
	}
	response.BenchmarkURL = *BenchmarkURL

	return nil
}

type InvalidLauncherErrorResponse struct {
	Message string `json:"message"`
}

const InvalidLauncherErrorResponseType = ResponseType("invalid_launcher_error_response")

type InvalidTokenErrorResponse struct {
	Message string `json:"message"`
}

const InvalidTokenErrorResponseType = ResponseType("invalid_token_error_response")

type NonVerifiedTokenErrorResponse struct {
	Message string `json:"message"`
}

const NonVerifiedTokenErrorResponseType = ResponseType("non_verified_token_error_response")

type SchemaValidationErrorResponse struct {
	Message string `json:"message"`
}

const SchemaValidationErrorResponseType = ResponseType("schema_validation_error_response")

type SHA256Checksum string

type Token string

type BenchmarkResult struct {
	Launcher       Launcher
	BlenderVersion BlenderVersion
	Scene          Scene
	RenderInfo     core.RenderInfo
}

type BenchmarkResultJSON struct {
	Timestamp         string                `json:"timestamp"`
	BlenderVersion    BlenderVersionJSON    `json:"blender_version"`
	BenchmarkLauncher BenchmarkLauncherJSON `json:"benchmark_launcher"`
	BenchmarkScript   BenchmarkScriptJSON   `json:"benchmark_script"`
	Scene             SceneJSON             `json:"scene"`
	SystemInfo        core.RenderSystemInfo `json:"system_info"`
	DeviceInfo        core.RenderDeviceInfo `json:"device_info"`
	Stats             core.RenderStats      `json:"stats"`
}

type BlenderVersionJSON struct {
	core.RenderInfoBlenderVersion
	Label    string `json:"label"`
	Checksum string `json:"checksum"`
}

type BenchmarkLauncherJSON struct {
	Label    string `json:"label"`
	Checksum string `json:"checksum"`
}

type BenchmarkScriptJSON struct {
	Label    string `json:"label"`
	Checksum string `json:"checksum"`
}

type SceneJSON struct {
	Label    string `json:"label"`
	Checksum string `json:"checksum"`
}

func (result *BenchmarkResult) MarshalJSON() ([]byte, error) {
	return json.Marshal(BenchmarkResultJSON{
		Timestamp: result.RenderInfo.Timestamp,
		BlenderVersion: BlenderVersionJSON{
			RenderInfoBlenderVersion: result.RenderInfo.BlenderVersion,
			Label:                    string(result.BlenderVersion.Label),
			Checksum:                 string(result.BlenderVersion.Checksum),
		},
		BenchmarkLauncher: BenchmarkLauncherJSON{
			Label:    string(result.Launcher.Label),
			Checksum: string(result.Launcher.Checksum),
		},
		BenchmarkScript: BenchmarkScriptJSON{
			Label:    string(result.BlenderVersion.BenchmarkScript.Label),
			Checksum: string(result.BlenderVersion.BenchmarkScript.Checksum),
		},
		Scene: SceneJSON{
			Label:    string(result.Scene.Label),
			Checksum: string(result.Scene.Checksum),
		},
		SystemInfo: result.RenderInfo.SystemInfo,
		DeviceInfo: result.RenderInfo.DeviceInfo,
		Stats:      result.RenderInfo.Stats,
	})
}

type DownloadProgress struct {
	CurrentBytes int64
	TotalBytes   int64
}
