package rpc

import (
	"fmt"
	"net/url"
)

type CommunicationError struct {
	err error
}

func (err *CommunicationError) Error() string {
	return fmt.Sprintf("communication error: %s", err.err.Error())
}

var (
	_ error = &CommunicationError{}
)

type DownloadError struct {
	err error
}

func (err *DownloadError) Error() string {
	return fmt.Sprintf("download error: %s", err.err.Error())
}

var (
	_ error = &DownloadError{}
)

type CurrentLauncherNotSupportedError struct {
	NewLauncherURL *url.URL
}

func (err *CurrentLauncherNotSupportedError) Error() string {
	return "current launcher not supported"
}

var (
	_ error = &CurrentLauncherNotSupportedError{}
)

type InvalidChecksumError struct {
	ActualChecksum   SHA256Checksum
	ExpectedChecksum SHA256Checksum
}

func (err *InvalidChecksumError) Error() string {
	return fmt.Sprintf("invalid checksum, found: %s, expected: %s", string(err.ActualChecksum), string(err.ExpectedChecksum))
}

var (
	_ error = &InvalidChecksumError{}
)

type BrokenCacheError interface {
	error
	isBrokenCacheError()
}

type BrokenBlenderVersionError struct {
	BlenderVersion BlenderVersion
	err            error
}

func (err *BrokenBlenderVersionError) isBrokenCacheError() {}

func (err *BrokenBlenderVersionError) Error() string {
	return fmt.Sprintf("Blender version %s with checksum %s is broken: %s", err.BlenderVersion.Label, err.BlenderVersion.Checksum, err.err.Error())
}

var (
	_ BrokenCacheError = &BrokenBlenderVersionError{}
)

type BrokenBenchmarkScriptError struct {
	BenchmarkScript BenchmarkScript
	err             error
}

func (err *BrokenBenchmarkScriptError) isBrokenCacheError() {}

func (err *BrokenBenchmarkScriptError) Error() string {
	return fmt.Sprintf("benchmark script %s with checksum %s is broken: %s", err.BenchmarkScript.Label, err.BenchmarkScript.Checksum, err.err.Error())
}

var (
	_ BrokenCacheError = &BrokenBenchmarkScriptError{}
)

type BrokenSceneError struct {
	Scene Scene
	err   error
}

func (err *BrokenSceneError) isBrokenCacheError() {}

func (err *BrokenSceneError) Error() string {
	return fmt.Sprintf("scene %s with checksum %s is broken: %s", err.Scene.Label, err.Scene.Checksum, err.err.Error())
}

var (
	_ BrokenCacheError = &BrokenSceneError{}
)

type InvalidTokenError struct{}

func (err *InvalidTokenError) Error() string {
	return "the authentication token is broken"
}

var (
	_ error = &InvalidTokenError{}
)

type NonVerifiedTokenError struct{}

func (err *NonVerifiedTokenError) Error() string {
	return "the authentication token is invalid"
}

var (
	_ error = &NonVerifiedTokenError{}
)

type SchemaValidationError struct{}

func (err *SchemaValidationError) Error() string {
	return "the benchmark results do not match the schema"
}

var (
	_ error = &SchemaValidationError{}
)
