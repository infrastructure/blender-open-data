//go:build !dev
// +build !dev

package rpc

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"

	"git.blender.org/blender-open-data/launcher/internal/core"
)

func GetLauncherChecksum() (*LauncherSHA256Checksum, error) {
	core.DebugLogger.Print("Calculating checksum of current launcher.")

	executable, err := os.Executable()
	if err != nil {
		return nil, err
	}

	file, err := os.Open(executable)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	hasher := sha256.New()
	if _, err := io.Copy(hasher, file); err != nil {
		return nil, err
	}
	checksum := LauncherSHA256Checksum(SHA256Checksum(hex.EncodeToString(hasher.Sum(nil))))

	core.DebugLogger.Printf("Launcher checksum: %s.", string(checksum))

	return &checksum, nil
}
