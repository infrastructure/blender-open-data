package rpc

import (
	"fmt"
	"net/http"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/config"
)

var DefaultConfig = Config{
	UserAgent: fmt.Sprintf("Blender Benchmark Launcher %s", config.Version),
	Client: http.Client{
		Timeout: 20 * time.Second,
	},
	MetadataURL:       config.MetadataURL,
	AuthenticationURL: config.AuthenticationURL,
	SubmitURL:         config.SubmitURL,
	LogsURL:           config.LogsURL,
}
