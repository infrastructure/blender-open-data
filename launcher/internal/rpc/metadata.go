package rpc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
)

func GetMetaData(config Config) ([]Launcher, []BlenderVersion, error) {
	core.DebugLogger.Printf("Fetching Metadata: %s", config.MetadataURL.String())

	request, err := http.NewRequest(http.MethodGet, config.MetadataURL.String(), nil)
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}

	request.Header.Set("User-Agent", config.UserAgent)

	response, err := config.Client.Do(request)
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}

	if !(200 <= response.StatusCode && response.StatusCode < 300) {
		return nil, nil, &CommunicationError{fmt.Errorf("non-success status code returned: %d", response.StatusCode)}
	}

	envelope := ResponseEnvelope{}
	err = json.Unmarshal(body, &envelope)
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}

	metadataResponse, ok := envelope.Message.(MetadataResponse)
	if !ok {
		return nil, nil, &CommunicationError{fmt.Errorf("expected MetadataResponse found %T", envelope.Message)}
	}

	core.DebugLogger.Printf("Successfully fetched metadata")

	return metadataResponse.Launchers, metadataResponse.BlenderVersions, nil
}

func GetLatestSupportedLauncher(launchers []Launcher) *Launcher {
	for _, launcher := range launchers {
		if launcher.CLI == config.CLI && launcher.Latest && launcher.OperatingSystem == core.CurrentOperatingSystem {
			return &launcher
		}
	}

	for _, launcher := range launchers {
		if launcher.Latest && launcher.OperatingSystem == core.CurrentOperatingSystem {
			return &launcher
		}
	}

	return nil
}

func GetCurrentLauncher(launchers []Launcher) (*Launcher, error) {
	core.DebugLogger.Print("Getting current launcher.")

	checksum, err := GetLauncherChecksum()
	if err != nil {
		return nil, err
	}

	for _, launcher := range launchers {
		if launcher.Supported && launcher.Checksum == *checksum {
			return &launcher, nil
		}
	}

	latestSupportedLauncher := GetLatestSupportedLauncher(launchers)
	if latestSupportedLauncher != nil {
		return nil, &CurrentLauncherNotSupportedError{NewLauncherURL: &latestSupportedLauncher.URL}
	}

	return nil, &CurrentLauncherNotSupportedError{NewLauncherURL: nil}
}

func FilterSupportedBlenderVersions(blenderVersions []BlenderVersion) []BlenderVersion {
	currentArchitecture := core.GetCurrentArchitecture()
	var supportedBlenderVersions []BlenderVersion = nil
	for _, blenderVersion := range blenderVersions {
		if blenderVersion.OperatingSystem == core.CurrentOperatingSystem &&
			blenderVersion.Architecture == currentArchitecture {
			supportedBlenderVersions = append(supportedBlenderVersions, blenderVersion)
		}
	}
	return supportedBlenderVersions
}
