package rpc

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/gorilla/websocket"
)

func GetExistingToken() (*Token, error) {
	core.DebugLogger.Print("Getting existing token.")

	configDir, err := core.ConfigDirectory()
	if err != nil {
		return nil, err
	}

	tokenPath := filepath.Join(configDir, "token.v1.bin")

	core.DebugLogger.Printf("Trying to read token from %s", tokenPath)

	tokenFile, err := os.Open(tokenPath)
	if err != nil {
		core.DebugLogger.Printf("while opening token: %s", err)
		return nil, err
	}
	defer tokenFile.Close()

	tokenBytes, err := ioutil.ReadAll(tokenFile)
	if err != nil {
		core.DebugLogger.Printf("while reading token: %s", err)
		return nil, err
	}

	core.DebugLogger.Print("Successfully read token")

	token := Token(tokenBytes)
	return &token, nil
}

func SaveToken(token Token) error {
	core.DebugLogger.Print("Saving new token.")

	configDir, err := core.ConfigDirectory()
	if err != nil {
		core.DebugLogger.Printf("error getting config directory%s", err)
		return err
	}
	tokenPath := filepath.Join(configDir, "token.v1.bin")

	core.DebugLogger.Printf("Trying to save token to %s", tokenPath)

	err = ioutil.WriteFile(tokenPath, []byte(token), 0600)
	if err != nil {
		core.DebugLogger.Printf("error saving token: %s", err)
		return err
	}

	core.DebugLogger.Print("Successfully saved token")

	return nil
}

func ConnectToAuthenticator(config Config) (*websocket.Conn, error) {
	core.DebugLogger.Printf("Connecting to the authenticator: %s", config.AuthenticationURL.String())

	connection, response, err := websocket.DefaultDialer.Dial(config.AuthenticationURL.String(), nil)
	if response != nil {
		response.Body.Close()
	}

	if err != nil {
		return nil, &CommunicationError{err}
	}
	return connection, nil
}

func RequestNewToken(connection *websocket.Conn) (*Token, *url.URL, error) {
	core.DebugLogger.Printf("Requesting a new token")

	hostname, err := os.Hostname()
	if err != nil {
		core.DebugLogger.Printf("error getting hostname, using unknown instead: %s", err)
		hostname = "unknown"
	}

	err = connection.WriteJSON(RequestEnvelope{
		Message: AuthenticationRequest{
			Hostname: hostname,
		},
	})
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}

	envelope := ResponseEnvelope{}

	err = connection.ReadJSON(&envelope)
	if err != nil {
		return nil, nil, &CommunicationError{err}
	}
	tokenResponse, ok := envelope.Message.(TokenResponse)
	if !ok {
		return nil, nil, &CommunicationError{fmt.Errorf("expected TokenResponse got %T", envelope)}
	}

	token := Token(tokenResponse.Token)
	verificationURL := tokenResponse.VerificationURL
	return &token, &verificationURL, nil
}

func WaitForVerification(connection *websocket.Conn) (*VerifiedResponse, bool, error) {
	core.DebugLogger.Print("Waiting for token verification.")

	envelope := ResponseEnvelope{}

	err := connection.ReadJSON(&envelope)
	if err != nil {
		return nil, false, &CommunicationError{err}
	}

	switch v := envelope.Message.(type) {
	case VerifiedResponse:
		core.DebugLogger.Print("Token verified successfully.")
		return &v, true, nil
	case RejectedResponse:
		core.DebugLogger.Print("Token rejected.")
		return nil, false, nil
	default:
		return nil, false, &CommunicationError{fmt.Errorf("expected VerifiedResponse or RejectedResponse got %T", envelope)}
	}
}
