package rpc

import (
	"time"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
)

func InitSentry() error {
	if //noinspection ALL
	config.SentryDSN == "" {
		return nil
	}

	core.DebugLogger.Print("Initializing Sentry.")

	err := sentry.Init(sentry.ClientOptions{
		Dsn: config.SentryDSN,
	})
	if err != nil {
		return err
	}

	return nil
}

func CaptureWithSentry(err error) (*sentry.EventID, bool) {
	if //noinspection ALL
	config.SentryDSN == "" {
		core.DebugLogger.Printf("would have sent error to Sentry: %#v", err.Error())
		if config.Debug {
			devEventID, err := uuid.NewRandom()
			if err != nil {
				return nil, false
			}
			eventID := sentry.EventID("dev_event_id_" + devEventID.String())
			return &eventID, true
		} else {
			return nil, true
		}
	}

	core.DebugLogger.Printf("Sending error to Sentry: %s", err.Error())

	eventID := sentry.CaptureException(err)
	flushed := sentry.Flush(5 * time.Second)
	if flushed {
		core.DebugLogger.Printf("sent error (%s) to Sentry.", string(*eventID))
	} else {
		core.ErrorLogger.Printf("tried to send error (%s) to Sentry but might have failed.", string(*eventID))
	}
	return eventID, flushed
}
