//go:build dev
// +build dev

package rpc

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
)

func GetLauncherChecksum() (*LauncherSHA256Checksum, error) {
	var checksum LauncherSHA256Checksum
	if config.CLI {
		checksum = LauncherSHA256Checksum(SHA256Checksum(fmt.Sprintf("dev_checksum_%s_cli", core.CurrentOperatingSystem)))
	} else {
		checksum = LauncherSHA256Checksum(SHA256Checksum(fmt.Sprintf("dev_checksum_%s_gui", core.CurrentOperatingSystem)))
	}
	return &checksum, nil
}
