package rpc

import (
	"archive/tar"
	"archive/zip"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/machinebox/progress"
	"github.com/mholt/archiver"
)

func checkValidFlagFile(directory string) (bool, error) {
	_, err := os.Stat(filepath.Join(directory, "valid.launcher"))
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		} else {
			return false, err
		}
	}
	return true, nil
}

func createValidFlagFile(directory string) error {
	validFlagFile, err := os.Create(filepath.Join(directory, "valid.launcher"))
	if err != nil {
		return err
	}
	err = validFlagFile.Close()
	if err != nil {
		return err
	}
	return nil
}

func GetBlenderVersion(config Config, blenderVersion BlenderVersion, progressChannel chan DownloadProgress) (*core.BlenderExecutable, error) {
	var err error
	defer close(progressChannel)

	core.DebugLogger.Printf("Getting Blender version: %s", blenderVersion.Label)

	blenderExecutable, err := FindValidBlenderExecutable(blenderVersion)
	if err == nil {
		core.DebugLogger.Printf("Blender available locally: %s.", string(*blenderExecutable))
		return blenderExecutable, nil
	}

	core.DebugLogger.Print("Blender version not available locally, downloading.")

	blenderVersionDir, err := blenderVersionDirectory(blenderVersion)
	if err != nil {
		return nil, err
	}

	err = RemoveBlenderVersion(blenderVersion)
	if err != nil {
		return nil, err
	}
	err = os.MkdirAll(blenderVersionDir, 0755)
	if err != nil {
		return nil, err
	}

	checksum, err := DownloadAndExtract(config, blenderVersion.URL, blenderVersionDir, progressChannel)
	if err != nil {
		_ = RemoveBlenderVersion(blenderVersion)
		return nil, err
	}
	if SHA256Checksum(blenderVersion.Checksum) != *checksum {
		_ = RemoveBlenderVersion(blenderVersion)
		return nil, &InvalidChecksumError{
			ActualChecksum:   *checksum,
			ExpectedChecksum: SHA256Checksum(blenderVersion.Checksum),
		}
	}

	blenderExecutable, err = findBlenderExecutable(blenderVersion)
	if err != nil {
		_ = RemoveBlenderVersion(blenderVersion)
		return nil, err
	}

	err = createValidFlagFile(blenderVersionDir)
	if err != nil {
		_ = RemoveBlenderVersion(blenderVersion)
		return nil, err
	}

	core.DebugLogger.Printf("Blender downloaded: %s.", string(*blenderExecutable))

	return blenderExecutable, nil
}

func blenderVersionDirectory(blenderVersion BlenderVersion) (string, error) {
	cacheDir, err := core.CacheDirectory()
	if err != nil {
		return "", err
	}

	// IMPORTANT NOTE: Windows has a 260 character limit on paths. When trying
	// to access files with paths longer than that the system call will just
	// return that the file does not exist. Since some paths in the Blender
	// directory tree are quite long we only use the first 8 characters of the
	// checksum on Windows.
	var checksumPart = string(blenderVersion.Checksum)
	if runtime.GOOS == "windows" && len(checksumPart) > 8 {
		checksumPart = checksumPart[:8]
	}

	blenderVersionDir := filepath.Join(cacheDir, "blender-versions", checksumPart)

	return blenderVersionDir, nil
}

func RemoveBlenderVersion(blenderVersion BlenderVersion) error {
	core.DebugLogger.Printf("Removing Blender version: %s", blenderVersion.Label)

	blenderVersionDir, err := blenderVersionDirectory(blenderVersion)
	if err != nil {
		return err
	}

	err = os.RemoveAll(blenderVersionDir)
	if err != nil {
		return err
	}

	return nil
}

func FindValidBlenderExecutable(blenderVersion BlenderVersion) (*core.BlenderExecutable, error) {
	blenderVersionDir, err := blenderVersionDirectory(blenderVersion)
	if err != nil {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: err}
	}

	ok, err := checkValidFlagFile(blenderVersionDir)
	if err != nil {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: err}
	} else if !ok {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: fmt.Errorf("missing valid flag file")}
	}

	return findBlenderExecutable(blenderVersion)
}

func findBlenderExecutable(blenderVersion BlenderVersion) (*core.BlenderExecutable, error) {
	blenderVersionDir, err := blenderVersionDirectory(blenderVersion)
	if err != nil {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: err}
	}

	var blenderExecutablePattern string
	switch core.CurrentOperatingSystem {
	case core.OperatingSystemLinux:
		blenderExecutablePattern = filepath.Join(blenderVersionDir, "*", core.BlenderExecutableName)
	case core.OperatingSystemWindows:
		blenderExecutablePattern = filepath.Join(blenderVersionDir, "*", core.BlenderExecutableName)
	case core.OperatingSystemMacOS:
		blenderExecutablePattern = filepath.Join(blenderVersionDir, "Blender.app", "Contents", "MacOS", core.BlenderExecutableName)
	}

	matches, err := filepath.Glob(blenderExecutablePattern)
	if err != nil {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: err}
	}
	if len(matches) != 1 {
		return nil, &BrokenBlenderVersionError{BlenderVersion: blenderVersion, err: fmt.Errorf("did not find precisely one blender executable: %v", matches)}
	}

	blenderExecutable := core.BlenderExecutable(matches[0])

	return &blenderExecutable, nil
}

func GetBenchmarkScript(config Config, benchmarkScript BenchmarkScript, progressChannel chan DownloadProgress) (*core.BenchmarkScript, error) {
	defer close(progressChannel)
	var err error

	core.DebugLogger.Printf("Getting benchmark script: %s", benchmarkScript.Label)

	benchmarkScriptPath, err := FindValidBenchmarkScript(benchmarkScript)
	if err == nil {
		core.DebugLogger.Printf("Benchmark script available locally: %s.", string(*benchmarkScriptPath))
		return benchmarkScriptPath, nil
	}

	core.DebugLogger.Print("Benchmark script not available locally, downloading.")

	benchmarkScriptDir, err := benchmarkScriptDirectory(benchmarkScript)
	if err != nil {
		return nil, err
	}

	err = RemoveBenchmarkScript(benchmarkScript)
	if err != nil {
		return nil, err
	}
	err = os.MkdirAll(benchmarkScriptDir, 0755)
	if err != nil {
		return nil, err
	}

	checksum, err := DownloadAndExtract(config, benchmarkScript.URL, benchmarkScriptDir, progressChannel)
	if err != nil {
		_ = RemoveBenchmarkScript(benchmarkScript)
		return nil, err
	}
	if SHA256Checksum(benchmarkScript.Checksum) != *checksum {
		_ = RemoveBenchmarkScript(benchmarkScript)
		return nil, &InvalidChecksumError{
			ActualChecksum:   *checksum,
			ExpectedChecksum: SHA256Checksum(benchmarkScript.Checksum),
		}
	}

	benchmarkScriptPath, err = findBenchmarkScript(benchmarkScript)
	if err != nil {
		_ = RemoveBenchmarkScript(benchmarkScript)
		return nil, err
	}

	err = createValidFlagFile(benchmarkScriptDir)
	if err != nil {
		_ = RemoveBenchmarkScript(benchmarkScript)
		return nil, err
	}

	core.DebugLogger.Printf("Benchmark script downloaded: %s.", string(*benchmarkScriptPath))

	return benchmarkScriptPath, nil
}

func benchmarkScriptDirectory(benchmarkScript BenchmarkScript) (string, error) {
	cacheDir, err := core.CacheDirectory()
	if err != nil {
		return "", err
	}
	benchmarkScriptDir := filepath.Join(cacheDir, "benchmark-scripts", string(benchmarkScript.Checksum))

	return benchmarkScriptDir, nil
}

func RemoveBenchmarkScript(benchmarkScript BenchmarkScript) error {
	core.DebugLogger.Printf("Removing benchmark script: %s", benchmarkScript.Label)

	benchmarkScriptDir, err := benchmarkScriptDirectory(benchmarkScript)
	if err != nil {
		return err
	}

	err = os.RemoveAll(benchmarkScriptDir)
	if err != nil {
		return err
	}

	return nil
}

func FindValidBenchmarkScript(benchmarkScript BenchmarkScript) (*core.BenchmarkScript, error) {
	benchmarkScriptDir, err := benchmarkScriptDirectory(benchmarkScript)
	if err != nil {
		return nil, &BrokenBenchmarkScriptError{BenchmarkScript: benchmarkScript, err: err}
	}

	ok, err := checkValidFlagFile(benchmarkScriptDir)
	if err != nil {
		return nil, &BrokenBenchmarkScriptError{BenchmarkScript: benchmarkScript, err: err}
	} else if !ok {
		return nil, &BrokenBenchmarkScriptError{BenchmarkScript: benchmarkScript, err: fmt.Errorf("missing valid flag file")}
	}

	return findBenchmarkScript(benchmarkScript)
}

func findBenchmarkScript(benchmarkScript BenchmarkScript) (*core.BenchmarkScript, error) {
	benchmarkScriptDir, err := benchmarkScriptDirectory(benchmarkScript)
	if err != nil {
		return nil, &BrokenBenchmarkScriptError{BenchmarkScript: benchmarkScript, err: err}
	}

	matches, err := filepath.Glob(filepath.Join(benchmarkScriptDir, "*", "main.py"))
	if err != nil {
		return nil, &BrokenBenchmarkScriptError{BenchmarkScript: benchmarkScript, err: err}
	}
	if len(matches) != 1 {
		return nil, &BrokenBenchmarkScriptError{
			BenchmarkScript: benchmarkScript,
			err:             fmt.Errorf("did not find precisely one benchmark script: %v", matches),
		}
	}

	benchmarkScriptPath := core.BenchmarkScript(matches[0])

	return &benchmarkScriptPath, nil
}

func GetScene(config Config, scene Scene, progressChannel chan DownloadProgress) (*core.BlendFile, error) {
	defer close(progressChannel)
	var err error

	core.DebugLogger.Printf("Getting scene: %s", scene.Label)

	blendFile, err := FindValidBlendFile(scene)
	if err == nil {
		core.DebugLogger.Printf("Scene available locally: %s.", string(*blendFile))
		return blendFile, nil
	}

	core.DebugLogger.Print("Scene not available locally, downloading.")

	sceneDir, err := sceneDirectory(scene)
	if err != nil {
		return nil, err
	}

	err = RemoveScene(scene)
	if err != nil {
		return nil, err
	}
	err = os.MkdirAll(sceneDir, 0755)
	if err != nil {
		return nil, err
	}

	checksum, err := DownloadAndExtract(config, scene.URL, sceneDir, progressChannel)
	if err != nil {
		_ = RemoveScene(scene)
		return nil, err
	}
	if SHA256Checksum(scene.Checksum) != *checksum {
		_ = RemoveScene(scene)
		return nil, &InvalidChecksumError{
			ActualChecksum:   *checksum,
			ExpectedChecksum: SHA256Checksum(scene.Checksum),
		}
	}

	blendFile, err = findBlendFile(scene)
	if err != nil {
		_ = RemoveScene(scene)
		return nil, err
	}

	err = createValidFlagFile(sceneDir)
	if err != nil {
		_ = RemoveScene(scene)
		return nil, err
	}

	core.DebugLogger.Printf("Scene downloaded: %s.", string(*blendFile))

	return blendFile, nil
}

func sceneDirectory(scene Scene) (string, error) {
	cacheDir, err := core.CacheDirectory()
	if err != nil {
		return "", err
	}
	sceneDir := filepath.Join(cacheDir, "scenes", string(scene.Checksum))

	return sceneDir, nil
}

func RemoveScene(scene Scene) error {
	core.DebugLogger.Printf("Removing scene: %s", scene.Label)

	sceneDir, err := sceneDirectory(scene)
	if err != nil {
		return err
	}

	err = os.RemoveAll(sceneDir)
	if err != nil {
		return err
	}

	return nil
}

func FindValidBlendFile(scene Scene) (*core.BlendFile, error) {
	sceneDir, err := sceneDirectory(scene)
	if err != nil {
		return nil, &BrokenSceneError{scene, err}
	}

	ok, err := checkValidFlagFile(sceneDir)
	if err != nil {
		return nil, &BrokenSceneError{scene, err}
	} else if !ok {
		return nil, &BrokenSceneError{scene, fmt.Errorf("missing valid flag file")}
	}

	return findBlendFile(scene)
}

func findBlendFile(scene Scene) (*core.BlendFile, error) {
	sceneDir, err := sceneDirectory(scene)
	if err != nil {
		return nil, &BrokenSceneError{scene, err}
	}

	matches, err := filepath.Glob(filepath.Join(sceneDir, "*", "main.blend"))
	if err != nil {
		return nil, &BrokenSceneError{scene, err}
	}
	if len(matches) != 1 {
		return nil, &BrokenSceneError{scene, fmt.Errorf("did not find precisely one blend file: %v", matches)}
	}

	blendFile := core.BlendFile(matches[0])

	return &blendFile, nil
}

func downloadAndExtractArchiveTar(
	config Config,
	URL url.URL,
	destinationDirectory string,
	progressChannel chan DownloadProgress,
) (*SHA256Checksum, error) {
	core.DebugLogger.Print("Downloading as '.tar'.")
	core.DebugLogger.Printf("URL: %s", URL.String())
	core.DebugLogger.Printf("destinationDirectory: %s", destinationDirectory)

	request, err := http.NewRequest(http.MethodGet, URL.String(), nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("User-Agent", config.UserAgent)
	request.Header.Add("Accept-Encoding", "gzip")

	client := http.Client{}
	response, err := client.Do(request) // nolint:bodyclose
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if !(200 <= response.StatusCode && response.StatusCode < 300) {
		return nil, fmt.Errorf("non-success status code returned: %d", response.StatusCode)
	}

	progressReader := progress.NewReader(response.Body)
	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(500 * time.Millisecond):
				select {
				case progressChannel <- DownloadProgress{
					CurrentBytes: progressReader.N(),
					TotalBytes:   response.ContentLength,
				}:
				default:
				}
			}
		}
	}()

	hasher := sha256.New()
	teedReader := io.TeeReader(progressReader, hasher)

	var archiveReader archiver.Reader
	if strings.HasSuffix(URL.Path, ".tar.bz2") {
		archiveReader = archiver.NewTarBz2()
	} else if strings.HasSuffix(URL.Path, ".tar.gz") {
		archiveReader = archiver.NewTarGz()
	} else if strings.HasSuffix(URL.Path, ".tar.xz") {
		archiveReader = archiver.NewTarXz()
	} else {
		return nil, fmt.Errorf("unsupported file format: %s", URL.String())
	}

	err = archiveReader.Open(teedReader, response.ContentLength)
	if err != nil {
		return nil, err
	}
	defer archiveReader.Close()

	for {
		archiveFile, err := archiveReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		var fullPath string
		var linkPath string
		switch header := archiveFile.Header.(type) {
		case *tar.Header:
			fullPath = header.Name
			linkPath = header.Linkname
		default:
			archiveFile.Close()
			return nil, fmt.Errorf("unknown archive header: %T", header)
		}
		destinationPath := filepath.Join(destinationDirectory, filepath.FromSlash(fullPath))

		if _, err := os.Stat(filepath.Dir(destinationPath)); os.IsNotExist(err) {
			err = os.MkdirAll(filepath.Dir(destinationPath), 0755)
			if err != nil {
				return nil, err
			}
		}

		switch mode := archiveFile.Mode(); {
		case mode.IsDir():
			err := os.Mkdir(destinationPath, archiveFile.Mode().Perm())
			if err != nil {
				return nil, err
			}
		case mode.IsRegular():
			destinationFile, err := os.OpenFile(destinationPath, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, archiveFile.Mode().Perm())
			if err != nil {
				return nil, err
			}

			_, err = io.Copy(destinationFile, archiveFile)
			if err != nil {
				_ = destinationFile.Close()
				return nil, err
			}

			err = archiveFile.Close()
			if err != nil {
				_ = destinationFile.Close()
				return nil, err
			}

			err = destinationFile.Close()
			if err != nil {
				return nil, err
			}
		case mode&os.ModeSymlink != 0:
			if linkPath == "" {
				return nil, fmt.Errorf("file is a symlink but linkPath is empty: %s", fullPath)
			}
			err := os.Symlink(linkPath, destinationPath)
			if err != nil {
				return nil, err
			}
		default:
			return nil, fmt.Errorf("file is not regular, a directory, or a symlink: %s (%v)", fullPath, archiveFile.Mode())
		}

		err = archiveFile.Close()
		if err != nil {
			return nil, err
		}
	}

	checksum := SHA256Checksum(hex.EncodeToString(hasher.Sum(nil)))

	return &checksum, nil
}

func downloadAndExtractArchiveZip(
	config Config,
	URL url.URL,
	destinationDirectory string,
	progressChannel chan DownloadProgress,
) (*SHA256Checksum, error) {
	core.DebugLogger.Print("Downloading as '.zip'.")
	core.DebugLogger.Printf("URL: %s", URL.String())
	core.DebugLogger.Printf("destinationDirectory: %s", destinationDirectory)

	request, err := http.NewRequest(http.MethodGet, URL.String(), nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("User-Agent", config.UserAgent)

	client := http.Client{}
	response, err := client.Do(request) // nolint: bodyclose
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if !(200 <= response.StatusCode && response.StatusCode < 300) {
		return nil, fmt.Errorf("non-success status code returned: %d", response.StatusCode)
	}

	progressReader := progress.NewReader(response.Body)
	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(500 * time.Millisecond):
				select {
				case progressChannel <- DownloadProgress{
					CurrentBytes: progressReader.N(),
					TotalBytes:   response.ContentLength,
				}:
				default:
				}
			}
		}
	}()

	hasher := sha256.New()
	teedReader := io.TeeReader(progressReader, hasher)

	zipFilePath := filepath.Join(destinationDirectory, "archive.zip")
	zipFile, err := os.OpenFile(zipFilePath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	defer os.Remove(zipFilePath)
	if err != nil {
		return nil, err
	}
	defer zipFile.Close()

	_, err = io.Copy(zipFile, teedReader)
	if err != nil {
		return nil, err
	}

	archiveReader := archiver.NewZip()
	err = archiveReader.Open(zipFile, response.ContentLength)
	if err != nil {
		return nil, err
	}
	defer archiveReader.Close()

	for {
		archiveFile, err := archiveReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		var fullPath string
		var linkPath string
		switch header := archiveFile.Header.(type) {
		case zip.FileHeader:
			fullPath = header.Name
		default:
			archiveFile.Close()
			return nil, fmt.Errorf("unknown archive header: %T", header)
		}
		destinationPath := filepath.Join(destinationDirectory, filepath.FromSlash(fullPath))

		if _, err := os.Stat(filepath.Dir(destinationPath)); os.IsNotExist(err) {
			err = os.MkdirAll(filepath.Dir(destinationPath), 0755)
			if err != nil {
				return nil, err
			}
		}

		switch mode := archiveFile.Mode(); {
		case mode.IsDir():
			err := os.Mkdir(destinationPath, archiveFile.Mode().Perm())
			if err != nil {
				return nil, err
			}
		case mode.IsRegular():
			destinationFile, err := os.OpenFile(destinationPath, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, archiveFile.Mode().Perm())
			if err != nil {
				return nil, err
			}

			_, err = io.Copy(destinationFile, archiveFile)
			if err != nil {
				_ = destinationFile.Close()
				return nil, err
			}

			err = archiveFile.Close()
			if err != nil {
				_ = destinationFile.Close()
				return nil, err
			}

			err = destinationFile.Close()
			if err != nil {
				return nil, err
			}
		case mode&os.ModeSymlink != 0:
			if linkPath == "" {
				return nil, fmt.Errorf("file is a symlink but linkPath is empty: %s", fullPath)
			}
			err := os.Symlink(linkPath, destinationPath)
			if err != nil {
				return nil, err
			}
		default:
			return nil, fmt.Errorf("file is not regular, a directory, or a symlink: %s (%v)", fullPath, archiveFile.Mode())
		}

		err = archiveFile.Close()
		if err != nil {
			return nil, err
		}
	}

	checksum := SHA256Checksum(hex.EncodeToString(hasher.Sum(nil)))

	return &checksum, nil
}

func DownloadSizeBlenderVersion(blenderVersion BlenderVersion) SizeInBytes {
	totalDownloadSize := SizeInBytes(0)

	if _, err := FindValidBlenderExecutable(blenderVersion); err != nil {
		totalDownloadSize += blenderVersion.Size
	}

	if _, err := FindValidBenchmarkScript(blenderVersion.BenchmarkScript); err != nil {
		totalDownloadSize += blenderVersion.BenchmarkScript.Size
	}

	return totalDownloadSize
}

func DownloadSizeScene(scene Scene) SizeInBytes {
	if _, err := FindValidBlendFile(scene); err != nil {
		return scene.Size
	}
	return 0
}

func TotalDownloadSize(blenderVersion BlenderVersion, scenes []Scene) SizeInBytes {
	totalDownloadSize := SizeInBytes(0)

	totalDownloadSize += DownloadSizeBlenderVersion(blenderVersion)

	for _, scene := range scenes {
		totalDownloadSize += DownloadSizeScene(scene)
	}

	return totalDownloadSize
}
