package rpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/core"
)

func PostBenchmarkResults(config Config, token Token, launcher Launcher, benchmarkResults []BenchmarkResult) (*url.URL, error) {
	core.DebugLogger.Printf("Submitting results: %s", config.SubmitURL.String())
	core.DebugLogger.Printf("benchmarkResults: %#v", benchmarkResults)

	message, err := json.Marshal(RequestEnvelope{SubmissionRequest{
		Token:            token,
		LauncherChecksum: launcher.Checksum,
		BenchmarkResults: BenchmarkResults{
			SchemaVersion: "v4",
			Data:          benchmarkResults,
		},
	}})

	if err != nil {
		return nil, &CommunicationError{err}
	}

	request, err := http.NewRequest(http.MethodPost, config.SubmitURL.String(), bytes.NewBuffer(message))
	if err != nil {
		return nil, &CommunicationError{err}
	}

	request.Header.Set("User-Agent", config.UserAgent)
	request.Header.Set("Content-Type", "application/json")

	response, err := config.Client.Do(request)
	if err != nil {
		return nil, &CommunicationError{err}
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, &CommunicationError{err}
	}

	envelope := ResponseEnvelope{}
	err = json.Unmarshal(body, &envelope)
	if err != nil {
		return nil, &CommunicationError{err}
	}

	switch message := envelope.Message.(type) {
	case SubmissionSuccessfulResponse:
		core.DebugLogger.Print("Submission successful.")
		return &message.BenchmarkURL, nil
	case InvalidLauncherErrorResponse:
		launchers, _, err := GetMetaData(config)
		if err != nil {
			return nil, fmt.Errorf("while handling an InvalidLauncherResponse: %s", err)
		}
		latestSupportedLauncher := GetLatestSupportedLauncher(launchers)
		if latestSupportedLauncher != nil {
			return nil, &CurrentLauncherNotSupportedError{NewLauncherURL: &latestSupportedLauncher.URL}
		}
		return nil, &CurrentLauncherNotSupportedError{NewLauncherURL: nil}
	case InvalidTokenErrorResponse:
		return nil, &InvalidTokenError{}
	case NonVerifiedTokenErrorResponse:
		return nil, &NonVerifiedTokenError{}
	case SchemaValidationErrorResponse:
		return nil, &SchemaValidationError{}
	default:
		return nil, &CommunicationError{fmt.Errorf("unexpected message type found %T", message)}
	}
}
