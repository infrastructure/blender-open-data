//go:build darwin
// +build darwin

package rpc

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/machinebox/progress"
)

func DownloadAndExtract(
	config Config,
	URL url.URL,
	destinationDirectory string,
	progressChannel chan DownloadProgress,
) (*SHA256Checksum, error) {
	var checksum *SHA256Checksum
	var err error

	if strings.HasSuffix(URL.Path, ".dmg") {
		checksum, err = downloadAndExtractDMG(config, URL, destinationDirectory, progressChannel)
	} else if strings.HasSuffix(URL.Path, ".zip") {
		checksum, err = downloadAndExtractArchiveZip(config, URL, destinationDirectory, progressChannel)
	} else if strings.HasSuffix(URL.Path, ".tar.bz2") || strings.HasSuffix(URL.Path, ".tar.gz") || strings.HasSuffix(URL.Path, ".tar.xz") {
		checksum, err = downloadAndExtractArchiveTar(config, URL, destinationDirectory, progressChannel)
	} else {
		return nil, &DownloadError{fmt.Errorf("unsupported file format: %s", URL.String())}
	}

	if err != nil {
		return nil, &DownloadError{err}
	}

	return checksum, nil
}

func downloadAndExtractDMG(
	config Config,
	URL url.URL,
	destinationDirectory string,
	progressChannel chan DownloadProgress,
) (*SHA256Checksum, error) {
	core.DebugLogger.Print("Downloading as '.dmg'.")
	core.DebugLogger.Printf("URL: %#s", URL.String())
	core.DebugLogger.Printf("destinationDirectory: %#s", destinationDirectory)

	request, err := http.NewRequest(http.MethodGet, URL.String(), nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("User-Agent", config.UserAgent)

	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if !(200 <= response.StatusCode && response.StatusCode < 300) {
		return nil, fmt.Errorf("non-success status code returned: %d", response.StatusCode)
	}

	progressReader := progress.NewReader(response.Body)
	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(500 * time.Millisecond):
				select {
				case progressChannel <- DownloadProgress{
					CurrentBytes: progressReader.N(),
					TotalBytes:   response.ContentLength,
				}:
				default:
				}
			}
		}
	}()

	hasher := sha256.New()
	teedReader := io.TeeReader(progressReader, hasher)

	dmgFilePath := filepath.Join(destinationDirectory, "archive.dmg")
	dmgFile, err := os.OpenFile(dmgFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	defer os.Remove(dmgFilePath)
	defer dmgFile.Close()

	_, err = io.Copy(dmgFile, teedReader)
	if err != nil {
		return nil, err
	}

	mountPoint := filepath.Join(destinationDirectory, "mountpoint")
	err = os.Mkdir(mountPoint, 0755)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mountCommand := exec.CommandContext(ctx, "hdiutil", "attach", dmgFilePath, "-mountpoint", mountPoint, "-nobrowse")
	core.DecorateCommand(mountCommand)
	err = mountCommand.Run()
	if err != nil {
		return nil, err
	}
	unmountCommand := exec.CommandContext(ctx, "hdiutil", "detach", mountPoint, "-force")
	core.DecorateCommand(unmountCommand)
	defer unmountCommand.Run()
	defer os.Remove(mountPoint)

	copyCommand := exec.CommandContext(ctx, "cp", "-R", "-p", mountPoint+string(filepath.Separator), destinationDirectory)
	core.DecorateCommand(copyCommand)
	err = copyCommand.Run()
	if err != nil {
		return nil, err
	}

	checksum := SHA256Checksum(hex.EncodeToString(hasher.Sum(nil)))

	return &checksum, nil
}
