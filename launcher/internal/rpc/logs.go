package rpc

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/getsentry/sentry-go"
)

func UploadLogFile(config Config, sentryEventId sentry.EventID) error {
	logFilePath, err := core.LogFilePath()
	if err != nil {
		return err
	}

	file, err := os.Open(logFilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return err
	}

	logURL := config.LogsURL
	logURL.Path = path.Join(config.LogsURL.Path, string(sentryEventId))
	req, err := http.NewRequest("POST", logURL.String()+"/", file)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/octet-stream")
	req.ContentLength = stat.Size()
	response, err := config.Client.Do(req)

	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 201 {
		return fmt.Errorf("unexpected status code: %d", response.StatusCode)
	}

	_, err = io.Copy(ioutil.Discard, response.Body)
	if err != nil {
		return err
	}

	return nil
}
