//go:build windows || linux
// +build windows linux

package rpc

import (
	"fmt"
	"net/url"
	"strings"
)

func DownloadAndExtract(
	config Config,
	URL url.URL,
	destinationDirectory string,
	progressChannel chan DownloadProgress,
) (*SHA256Checksum, error) {
	var checksum *SHA256Checksum
	var err error

	if strings.HasSuffix(URL.Path, ".zip") {
		checksum, err = downloadAndExtractArchiveZip(config, URL, destinationDirectory, progressChannel)
	} else if strings.HasSuffix(URL.Path, ".tar.bz2") || strings.HasSuffix(URL.Path, ".tar.gz") || strings.HasSuffix(URL.Path, ".tar.xz") {
		checksum, err = downloadAndExtractArchiveTar(config, URL, destinationDirectory, progressChannel)
	} else {
		return nil, &DownloadError{fmt.Errorf("unsupported file format: %s", URL.String())}
	}

	if err != nil {
		return nil, &DownloadError{err}
	}

	return checksum, nil
}
