package themes

import (
	"errors"
	"io/ioutil"

	"git.blender.org/blender-open-data/launcher/assets"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type fonts struct {
	texture *imgui.TextureID
	medium  imgui.Font
	large   imgui.Font
}

func loadFonts(fonts *fonts, io imgui.IO, platform backend.Platform, renderer backend.Renderer) error {
	// If the window is minimised we do not reload the fonts.
	if platform.FramebufferSize()[0] <= 0.0 || platform.UISize()[0] <= 0.0 {
		return nil
	}

	if fonts.texture != nil {
		renderer.DestroyTexture(*fonts.texture)
	}

	fontFile, err := assets.Assets.Open("/Heebo-Regular.ttf")
	if err != nil {
		return err
	}
	defer fontFile.Close()
	fontData, err := ioutil.ReadAll(fontFile)
	if err != nil {
		return err
	}

	// We supersample the font to make it nice and crispy on HiDPI displays.
	io.SetFontGlobalScale(platform.UISize()[0] / platform.FramebufferSize()[0])
	superSampleScale := platform.FramebufferSize()[0] / platform.UISize()[0]
	fontMedium := io.Fonts().AddFontFromMemoryTTF(fontData, 20.0*superSampleScale)
	fontLarge := io.Fonts().AddFontFromMemoryTTF(fontData, 32.0*superSampleScale)

	if !io.Fonts().Build() {
		return errors.New("ImGui font atlas build did fail. Reason is not given :(")
	}

	rasterized := io.Fonts().TextureDataRGBA32()
	texture := renderer.RegisterTextureImGUI(*rasterized)
	io.Fonts().SetTextureID(texture)

	fonts.texture = &texture
	fonts.medium = fontMedium
	fonts.large = fontLarge

	return nil
}
