package themes

import (
	"fmt"
	"image"
	"io"

	"git.blender.org/blender-open-data/launcher/assets"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type Theme struct {
	fonts              fonts
	spacing            imgui.Vec2
	buttonPadding      imgui.Vec2
	MainBackground     imgui.TextureID
	ProgressBackground imgui.TextureID
	platform           backend.Platform
	renderer           backend.Renderer
}

func NewTheme(platform backend.Platform, renderer backend.Renderer) (*Theme, error) {
	file, err := assets.Assets.Open("/background.png")
	if err != nil {
		return nil, err
	}
	mainBackground, err := registerBackground(file, renderer)
	if err != nil {
		return nil, err
	}

	file, err = assets.Assets.Open("/progress.png")
	if err != nil {
		return nil, err
	}
	progressBackground, err := registerBackground(file, renderer)
	if err != nil {
		return nil, err
	}

	theme := Theme{
		fonts:              fonts{},
		spacing:            imgui.Vec2{X: 10, Y: 10},
		buttonPadding:      imgui.Vec2{X: 15, Y: 8},
		MainBackground:     *mainBackground,
		ProgressBackground: *progressBackground,
		platform:           platform,
		renderer:           renderer,
	}

	err = loadFonts(&theme.fonts, imgui.CurrentIO(), platform, renderer)
	if err != nil {
		return nil, err
	}

	return &theme, nil
}

func (theme *Theme) ReloadFonts() error {
	err := loadFonts(&theme.fonts, imgui.CurrentIO(), theme.platform, theme.renderer)
	if err != nil {
		return err
	}

	return nil
}

func (theme *Theme) Dispose() {
	theme.renderer.DestroyTexture(theme.MainBackground)
	theme.renderer.DestroyTexture(theme.ProgressBackground)
	if theme.fonts.texture != nil {
		theme.renderer.DestroyTexture(*theme.fonts.texture)
	}
}

func registerBackground(file io.Reader, renderer backend.Renderer) (*imgui.TextureID, error) {
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	imgNRGBA, ok := img.(*image.NRGBA)
	if !ok {
		return nil, fmt.Errorf("images should be of RGBA type without alpha pre-multiplication")
	}

	textureId := renderer.RegisterTexture(*imgNRGBA)

	return &textureId, nil
}

func (theme Theme) HeaderMedium(text string) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 65})

	imgui.PushFont(theme.fonts.medium)
	defer imgui.PopFont()

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.8, Y: 0.8, Z: 0.8, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.7, Y: 0.7, Z: 0.7, W: 1.0})
	defer imgui.PopStyleColorV(3)

	imgui.Text(text)
}

func (theme Theme) HeaderLarge(text string) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 90})

	imgui.PushFont(theme.fonts.large)
	defer imgui.PopFont()

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.8, Y: 0.8, Z: 0.8, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.7, Y: 0.7, Z: 0.7, W: 1.0})
	defer imgui.PopStyleColorV(3)

	imgui.Text(text)
}

func (theme Theme) PushDefaults() {
	theme.PushText()
	theme.PushSpacing()
}

func (theme Theme) PopDefaults() {
	theme.PopSpacing()
	theme.PopText()
}

func (theme Theme) PushSpacing() {
	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, theme.spacing)
}

func (theme Theme) PopSpacing() {
	imgui.PopStyleVarV(1)
}

func (theme Theme) PushText() {
	imgui.PushFont(theme.fonts.medium)

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 0.82, Y: 0.84, Z: 0.85, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.481250, Y: 0.487500, Z: 0.518750, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.937255, Y: 0.937255, Z: 0.937255, W: 1.0})
}

func (theme Theme) PopText() {
	imgui.PopStyleColorV(3)
	imgui.PopFont()
}

func (theme Theme) PushLargeText() {
	imgui.PushFont(theme.fonts.large)

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.481250, Y: 0.487500, Z: 0.518750, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.937255, Y: 0.937255, Z: 0.937255, W: 1.0})
}

func (theme Theme) PopLargeText() {
	imgui.PopStyleColorV(3)
	imgui.PopFont()
}

func (theme Theme) CalcButtonSize(label string) imgui.Vec2 {
	return imgui.CalcTextSize(label, false, 0).Plus(theme.buttonPadding.Times(2))
}

func (theme Theme) PushPrimaryButton() {
	imgui.PushFont(theme.fonts.medium)

	imgui.PushStyleColor(imgui.StyleColorButton, imgui.Vec4{X: 0.0, Y: 0.64, Z: 0.92, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonHovered, imgui.Vec4{X: 0.166, Y: 0.746, Z: 1.0, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonActive, imgui.Vec4{X: 0.188, Y: 0.756, Z: 1.0, W: 1.0})

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.8, Y: 0.8, Z: 0.8, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.7, Y: 0.7, Z: 0.7, W: 1.0})

	imgui.PushStyleVarVec2(imgui.StyleVarFramePadding, theme.buttonPadding)
	imgui.PushStyleVarFloat(imgui.StyleVarFrameRounding, 4)
}

func (theme Theme) PopPrimaryButton() {
	imgui.PopStyleVarV(2)
	imgui.PopStyleColorV(6)
	imgui.PopFont()
}

func (theme Theme) PushSecondaryButton() {
	imgui.PushFont(theme.fonts.medium)

	imgui.PushStyleColor(imgui.StyleColorButton, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonHovered, imgui.Vec4{X: 0.29, Y: 0.31, Z: 0.33, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonActive, imgui.Vec4{X: 0.39, Y: 0.41, Z: 0.43, W: 1.0})

	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 0.82, Y: 0.84, Z: 0.85, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextDisabled, imgui.Vec4{X: 0.42, Y: 0.44, Z: 0.45, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorTextSelectedBg, imgui.Vec4{X: 0.9, Y: 0.9, Z: 0.9, W: 1.0})

	imgui.PushStyleVarVec2(imgui.StyleVarFramePadding, theme.buttonPadding)
	imgui.PushStyleVarFloat(imgui.StyleVarFrameRounding, 4)
}

func (theme Theme) PopSecondaryButton() {
	imgui.PopStyleVarV(2)
	imgui.PopStyleColorV(6)
	imgui.PopFont()
}

func (theme Theme) PushCheckbox() {
	imgui.PushStyleColor(imgui.StyleColorFrameBg, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorFrameBgHovered, imgui.Vec4{X: 0.29, Y: 0.31, Z: 0.33, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorFrameBgActive, imgui.Vec4{X: 0.39, Y: 0.41, Z: 0.43, W: 1.0})

	imgui.PushStyleColor(imgui.StyleColorCheckMark, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})

	imgui.PushStyleVarFloat(imgui.StyleVarFrameRounding, 4)
}

func (theme Theme) PopCheckbox() {
	imgui.PopStyleColorV(4)
	imgui.PopStyleVarV(1)
}

func (theme Theme) PushCombo() {
	imgui.PushStyleVarFloat(imgui.StyleVarFrameBorderSize, 0)
	imgui.PushStyleVarFloat(imgui.StyleVarPopupBorderSize, 0)

	/* The items in the dropdown. */
	imgui.PushStyleColor(imgui.StyleColorHeader, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorHeaderHovered, imgui.Vec4{X: 0.29, Y: 0.31, Z: 0.33, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorHeaderActive, imgui.Vec4{X: 0.39, Y: 0.41, Z: 0.43, W: 1.0})

	/* Left side of the selector, has text on it. */
	imgui.PushStyleColor(imgui.StyleColorFrameBg, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorFrameBgHovered, imgui.Vec4{X: 0.29, Y: 0.31, Z: 0.33, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorFrameBgActive, imgui.Vec4{X: 0.39, Y: 0.41, Z: 0.43, W: 1.0})

	/* Right side where the arrow is. */
	imgui.PushStyleColor(imgui.StyleColorButton, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonHovered, imgui.Vec4{X: 0.29, Y: 0.31, Z: 0.33, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorButtonActive, imgui.Vec4{X: 0.39, Y: 0.41, Z: 0.43, W: 1.0})

	/* Dropdown. */
	imgui.PushStyleColor(imgui.StyleColorPopupBg, imgui.Vec4{X: 0.19, Y: 0.21, Z: 0.23, W: 1.0})

	imgui.PushStyleVarFloat(imgui.StyleVarFrameRounding, 4)
}

func (theme Theme) PopCombo() {
	imgui.PopStyleColorV(10)
	imgui.PopStyleVarV(3)
}

func (theme Theme) PushProgressBar() {
	imgui.PushFont(theme.fonts.medium)
	imgui.PushStyleVarFloat(imgui.StyleVarFrameBorderSize, 0)

	imgui.PushStyleColor(imgui.StyleColorFrameBg, imgui.Vec4{X: 0.0, Y: 0.0, Z: 0.0, W: 0.5})
	imgui.PushStyleColor(imgui.StyleColorPlotHistogram, imgui.Vec4{X: 0.0, Y: 0.64, Z: 0.92, W: 1.0})
	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 1.0, Y: 1.0, Z: 1.0, W: 1.0})

	imgui.PushStyleVarFloat(imgui.StyleVarFrameRounding, 4)
}

func (theme Theme) PopProgressBar() {
	imgui.PopStyleColorV(3)
	imgui.PopStyleVarV(2)
	imgui.PopFont()
}

func (theme Theme) PushHyperLink() {
	imgui.PushStyleColor(imgui.StyleColorText, imgui.Vec4{X: 0.0, Y: 0.72, Z: 1.0, W: 1.0})
}

func (theme Theme) PopHyperLink() {
	imgui.PopStyleColorV(1)
}

func (theme Theme) TopLeft(size imgui.Vec2) CursorHelper {
	return CursorHelper{
		theme:        theme,
		CursorPos:    imgui.Vec2{X: 0, Y: 150},
		previousSize: size,
	}
}

func (theme Theme) BottomRight(size imgui.Vec2) CursorHelper {
	return CursorHelper{
		theme:        theme,
		CursorPos:    imgui.Vec2{X: imgui.WindowWidth() - size.X, Y: imgui.WindowHeight() - size.Y},
		previousSize: size,
	}
}

func (theme Theme) BottomLeft(size imgui.Vec2) CursorHelper {
	return CursorHelper{
		theme:        theme,
		CursorPos:    imgui.Vec2{X: 0, Y: imgui.WindowHeight() - size.Y},
		previousSize: size,
	}
}
