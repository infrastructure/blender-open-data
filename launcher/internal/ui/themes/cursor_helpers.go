package themes

import "github.com/inkyblackness/imgui-go/v4"

type CursorHelper struct {
	CursorPos    imgui.Vec2
	theme        Theme
	previousSize imgui.Vec2
}

func (helper *CursorHelper) Up(size imgui.Vec2) imgui.Vec2 {
	helper.CursorPos.Y -= helper.theme.spacing.Y + size.Y
	helper.previousSize = size
	return helper.CursorPos
}

func (helper *CursorHelper) Down(size imgui.Vec2) imgui.Vec2 {
	helper.CursorPos.Y += helper.theme.spacing.Y + helper.previousSize.Y
	helper.previousSize = size
	return helper.CursorPos
}

func (helper *CursorHelper) Left(size imgui.Vec2) imgui.Vec2 {
	helper.CursorPos.X -= helper.theme.spacing.X + size.X
	helper.previousSize = size
	return helper.CursorPos
}

func (helper *CursorHelper) Right(size imgui.Vec2) imgui.Vec2 {
	helper.CursorPos.X += helper.theme.spacing.X + helper.previousSize.X
	helper.previousSize = size
	return helper.CursorPos
}
