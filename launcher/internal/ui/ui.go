package ui

import (
	"context"
	"runtime"
	"sync"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"git.blender.org/blender-open-data/launcher/internal/ui/states"
	"git.blender.org/blender-open-data/launcher/internal/ui/themes"
	"github.com/inkyblackness/imgui-go/v4"

	_ "image/png"
)

func Execute() error {
	if runtime.GOMAXPROCS(0) <= 1 {
		panic("GOMAXPROCS needs to be bigger than one.")
	}

	err := rpc.InitSentry()
	if err != nil {
		core.DebugLogger.Printf("could not initialize Sentry: %s", err)
	}

	imguiContext := imgui.CreateContext(nil)
	defer imguiContext.Destroy()
	io := imgui.CurrentIO()

	platform, err := backend.NewGLFW(io, backend.GLFWClientAPIOpenGL2)
	if err != nil {
		return err
	}
	defer platform.Dispose()

	renderer, err := backend.NewOpenGL2(io)
	if err != nil {
		return err
	}

	theme, err := themes.NewTheme(platform, renderer)
	if err != nil {
		return err
	}

	var waitGroup sync.WaitGroup
	ctx, cancelFunc := context.WithCancel(context.Background())

	stateStore := states.NewStateStore(
		states.Config{
			RPCConfig: rpc.DefaultConfig,
			Theme:     theme,
		},
		platform.WakeFromWaitEvents,
		&waitGroup,
		ctx,
	)

	imgui.CurrentIO().SetConfigFlags(imgui.ConfigFlagsNavEnableKeyboard)
	imgui.CurrentIO().SetIniFilename("")
	imgui.CurrentIO().SetClipboard(backend.NewClipboard(platform))

	err = MainLoop(platform, renderer, theme, stateStore, &waitGroup, cancelFunc)
	if err != nil {
		return err
	}

	return nil
}

func MainLoop(
	platform backend.Platform,
	renderer backend.Renderer,
	theme *themes.Theme,
	stateStore *states.StateStore,
	waitGroup *sync.WaitGroup,
	cancelFunc context.CancelFunc,
) error {
	sizeChanged := false
	sizeChangeCallback := func(width float32, height float32) { sizeChanged = true }
	platform.SetUISizeChangeCallback(sizeChangeCallback)
	defer platform.SetUISizeChangeCallback(nil)
	platform.SetDisplaySizeChangeCallback(sizeChangeCallback)
	defer platform.SetDisplaySizeChangeCallback(nil)
	platform.SetFramebufferSizeChangeCallback(sizeChangeCallback)
	defer platform.SetFramebufferSizeChangeCallback(nil)

	var state states.State
	// Render at about maximum 60fps.
	waitingForBackgroundProcesses := false
	backgroundProcessesFinished := make(chan bool)

	minWait := time.Second / 60
mainLoop:
	for {
		platform.WaitEvents(&minWait, nil)

		state = stateStore.GetLatestState()

		if waitingForBackgroundProcesses {
			state = states.ExitingState{State: state}
		}

		if sizeChanged {
			err := theme.ReloadFonts()
			if err != nil {
				return err
			}
			sizeChanged = false
		}

		platform.NewFrame()
		imgui.NewFrame()
		renderWindow(platform, renderer, theme, state)
		scheduleDrawCall(platform, renderer)

		if _, exiting := state.(states.ExitingState); exiting || platform.ShouldStop() {
			if !waitingForBackgroundProcesses {
				go func() {
					core.DebugLogger.Printf("Signalling background processes to stop what they are doing.")
					cancelFunc()
					core.DebugLogger.Printf("Waiting for background processes to finish...")
					waitGroup.Wait()
					core.DebugLogger.Printf("Background processes finished.")
					backgroundProcessesFinished <- true
				}()
				waitingForBackgroundProcesses = true
			} else {
				select {
				case ok := <-backgroundProcessesFinished:
					if ok {
						break mainLoop
					} else {
						continue
					}
				default:
					continue
				}
			}
		}
	}

	switch v := state.(type) {
	case error:
		return v
	case states.ExitingState:
		if errorState, ok := v.State.(error); ok {
			return errorState
		}
	default:
	}

	return nil
}
