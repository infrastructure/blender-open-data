package states

import (
	"fmt"
	"math"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type downloadSuccessState struct {
	metadataSelectedScenesState
	blenderExecutable core.BlenderExecutable
	benchmarkScript   core.BenchmarkScript
	blendFiles        []core.BlendFile
	devices           []core.Device
}

var (
	_ State = downloadSuccessState{}
)

type deviceSelectingState struct {
	downloadSuccessState
	selectedDeviceIndex int
}

var (
	_ State = deviceSelectingState{}
)

func newDeviceSelectingState(
	state metadataSelectedScenesState,
	blenderExecutable core.BlenderExecutable,
	benchmarkScript core.BenchmarkScript,
	blendFiles []core.BlendFile,
) deviceSelectingState {
	newState := deviceSelectingState{
		downloadSuccessState: downloadSuccessState{
			metadataSelectedScenesState: state,
			blenderExecutable:           blenderExecutable,
			benchmarkScript:             benchmarkScript,
			blendFiles:                  blendFiles,
			devices:                     nil,
		},
		selectedDeviceIndex: 0,
	}

	state.getWaitGroup().Add(1)
	go newState.process()

	return newState
}

func (state deviceSelectingState) benchmark(device core.Device) benchmarkingState {
	return newBenchmarkingState(state.downloadSuccessState, device)
}

func (state deviceSelectingState) fail(err error) deviceSelectingFailedState {
	newState := deviceSelectingFailedState{state, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state *deviceSelectingState) process() {
	defer state.getWaitGroup().Done()

	devices, err := core.ListDevices(state.blenderExecutable, state.benchmarkScript, state.ctx)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	if len(devices) == 0 {
		state.getStore().pushStateNaive(state.fail(&NoSupportedDevices{}))
	}

	state.devices = devices
	state.getStore().pushStateNaive(state)
}

func (state deviceSelectingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Benchmark Device")

	if len(state.devices) == 0 {
		imgui.Text("Listing devices...")
	} else {
		imgui.Text("Select the device you want to benchmark:")

		state.Theme().PushCombo()
		maxTextWidth := 80.0
		for _, device := range state.devices {
			maxTextWidth = math.Max(float64(imgui.CalcTextSize(fmt.Sprintf("%s", device.GetName()), false, 0).X)+40.0, maxTextWidth)
		}
		imgui.PushItemWidth(float32(maxTextWidth))
		if imgui.BeginCombo(
			"##hidelabel",
			fmt.Sprintf("%s", state.devices[state.selectedDeviceIndex].GetName()),
		) {
			for i, item := range state.devices {
				if imgui.SelectableV(fmt.Sprintf("%s", item.GetName()), state.selectedDeviceIndex == i, 0, imgui.Vec2{X: 0, Y: 0}) {
					state.selectedDeviceIndex = i
					state.store.pushStateNaive(state)
				}
				if state.selectedDeviceIndex == i {
					imgui.SetItemDefaultFocus()
				}
			}
			imgui.EndCombo()
		}
		state.Theme().PopCombo()

		state.Theme().PushPrimaryButton()
		size := state.Theme().CalcButtonSize("Start Benchmark")
		imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
		if imgui.ButtonV("Start Benchmark", size) {
			state.store.pushStateNaive(state.benchmark(state.devices[state.selectedDeviceIndex]))
		}
		state.Theme().PopPrimaryButton()
	}
}
