package states

import (
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type submitSuccessState struct {
	benchmarkSuccessState
	benchmarkURL url.URL
}

var (
	_ State = submitSuccessState{}
)

func (state submitSuccessState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Results Submitted")

	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, imgui.Vec2{X: 0, Y: 0})

	imgui.Text("Thank you for submitting your results!")

	imgui.Text("\n")

	imgui.Text("You can view your results by using the buttons below or by ")
	imgui.Text("going to your account area at ")
	imgui.SameLine()
	state.Theme().PushHyperLink()
	if HyperLink("opendata.blender.org/users/benchmarks/") {
		go core.OpenBrowser(config.AccountBenchmarksURL)
	}
	state.Theme().PopHyperLink()
	imgui.PopStyleVarV(1)

	state.Theme().PushPrimaryButton()
	browserSize := state.Theme().CalcButtonSize("View Results in Browser")
	helper := state.Theme().BottomRight(browserSize)
	imgui.SetCursorPos(helper.CursorPos)
	if imgui.ButtonV("View Results in Browser", browserSize) {
		go core.OpenBrowser(state.benchmarkURL)
	}
	state.Theme().PopPrimaryButton()

	state.Theme().PushSecondaryButton()
	copySize := state.Theme().CalcButtonSize("Copy URL")
	imgui.SetCursorPos(helper.Left(copySize))
	if imgui.ButtonV("Copy URL", copySize) {
		platform.SetClipboardText(state.benchmarkURL.String())
	}
	state.Theme().PopSecondaryButton()

	renderLeftExitButton(state)
}
