package states

import (
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type submittingState struct {
	benchmarkSuccessState
	verificationURL *url.URL
}

var (
	_ State = submitSuccessState{}
)

func newSubmittingState(state benchmarkSuccessState) submittingState {
	newState := submittingState{
		benchmarkSuccessState: state,
		verificationURL:       nil,
	}

	newState.getWaitGroup().Add(1)
	go newState.process()
	return newState
}

func (state submittingState) fail(err error) submitFailedState {
	newState := submitFailedState{state.benchmarkSuccessState, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state submittingState) submitted(benchmarkURL url.URL) submitSuccessState {
	return submitSuccessState{
		benchmarkSuccessState: state.benchmarkSuccessState,
		benchmarkURL:          benchmarkURL,
	}
}

func (state submittingState) process() {
	defer state.getWaitGroup().Done()

	token, err := rpc.GetExistingToken()
	if err != nil {
		state.store.pushStateNaive(newAuthenticatingState(state.benchmarkSuccessState))
		return
	}

	benchmarkURL, err := rpc.PostBenchmarkResults(state.config.RPCConfig, *token, state.launcher, state.benchmarkResults)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	state.store.pushStateNaive(state.submitted(*benchmarkURL))
}

func (state submittingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Submitting results")

	imgui.Text("Submitting results...")
}
