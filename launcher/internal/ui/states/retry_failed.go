package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/hashicorp/go-multierror"
	"github.com/inkyblackness/imgui-go/v4"
)

type retryFailedState struct {
	State
	err error
}

var (
	_ ErrorState = retryFailedState{}
)

func newRetryFailedState(state State, err error) retryFailedState {
	return retryFailedState{
		State: state,
		err:   err,
	}
}

func (state retryFailedState) Error() string {
	var errors error = nil
	if errorState, ok := state.State.(error); ok {
		errors = multierror.Append(errors, errorState)
	}

	errors = multierror.Append(errors, state.err)

	return errors.Error()
}

func (state retryFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	imgui.Text(fmt.Sprintf("An unexpected error occured while attempting a retry:\n%s", state.err.Error()))
	renderExitButtonsWithSubmitError(state)
}
