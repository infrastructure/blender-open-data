package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type benchmarkSuccessState struct {
	downloadSuccessState
	benchmarkResults []rpc.BenchmarkResult
}

var (
	_ State = benchmarkSuccessState{}
)

func (state benchmarkSuccessState) submitResults() submittingState {
	return newSubmittingState(state)
}

func (state benchmarkSuccessState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Benchmark Complete!")

	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, imgui.Vec2{X: 0, Y: 0})
	imgui.Text("The benchmark finished successfully. " +
		"Your results and system data that will be submitted " +
		"to the Blender Open Data website are listed below.")

	imgui.Text("Learn more about how data is collected at ")
	state.Theme().PushHyperLink()
	imgui.SameLine()
	if HyperLink("opendata.blender.org/about") {
		go core.OpenBrowser(config.AboutURL)
	}
	state.Theme().PopHyperLink()
	imgui.PopStyleVarV(1)

	imgui.Text("\n")

	imgui.SetCursorPos(imgui.CursorPos().Plus(imgui.Vec2{X: 0, Y: 10}))

	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, imgui.Vec2{X: 0, Y: 0})
	imgui.ColumnsV(3, "###", false)
	imgui.SetColumnWidth(0, 120)
	imgui.SetColumnOffset(0, 0)

	imgui.SetColumnWidth(1, 100)
	imgui.SetColumnOffset(1, 130)

	imgui.SetColumnWidth(2, 350)
	imgui.SetColumnOffset(2, 240)

	imgui.Text("Samples per minute:")
	imgui.NextColumn()

	imgui.NextColumn()

	imgui.Text("System info:")
	imgui.NextColumn()

	systemInfo := state.benchmarkResults[0].RenderInfo.SystemInfo

	numBenchmarkResults := len(state.benchmarkResults)
	numDevices := len(systemInfo.Devices)

	usedDevicesMap := make(map[string]bool)

	rowIndex := 0
	for {
		benchmarkColumnAdded := false
		deviceColumnAdded := false

		if rowIndex < numBenchmarkResults {
			result := state.benchmarkResults[rowIndex]

			imgui.Text(fmt.Sprintf("%s:", result.Scene.Label))
			imgui.NextColumn()

			imgui.Text(fmt.Sprintf("%f", result.RenderInfo.Stats.SamplesPerMinute))
			imgui.NextColumn()

			benchmarkColumnAdded = true
		}

		if rowIndex == 0 {
			if !benchmarkColumnAdded {
				imgui.NextColumn()
				imgui.NextColumn()
			}

			imgui.Text("OS: ")
			imgui.SameLine()
			if systemInfo.DistName == "" {
				imgui.Text(fmt.Sprintf("%s (%s)", systemInfo.System, systemInfo.Machine))
			} else {
				imgui.Text(fmt.Sprintf("%s %s (%s)", systemInfo.DistName, systemInfo.DistVersion, systemInfo.Machine))
			}

			imgui.NextColumn()
			deviceColumnAdded = true
		} else if rowIndex-1 < numDevices {
			device := systemInfo.Devices[rowIndex-1]

			if _, ok := usedDevicesMap[device.GetName()]; !ok {
				if !benchmarkColumnAdded {
					imgui.NextColumn()
					imgui.NextColumn()
				}

				if device.GetType() == core.CPUType {
					imgui.Text("CPU: ")
				} else {
					imgui.Text("GPU: ")
				}
				imgui.SameLine()
				imgui.Text(device.GetName())

				imgui.NextColumn()

				usedDevicesMap[device.GetName()] = true

				deviceColumnAdded = true
			}
		}

		if !benchmarkColumnAdded && !deviceColumnAdded {
			break
		}

		if !deviceColumnAdded {
			imgui.NextColumn()
		}

		rowIndex++
	}

	imgui.PopStyleVarV(1)
	imgui.ColumnsV(1, "###", false)

	state.Theme().PushPrimaryButton()
	size := state.Theme().CalcButtonSize("Submit Results")
	imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
	if imgui.ButtonV("Submit Results", size) {
		state.store.pushStateNaive(state.submitResults())
	}
	state.Theme().PopPrimaryButton()

	renderLeftExitButton(state)
}
