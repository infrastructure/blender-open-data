package states

import (
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type metadataFetchingState struct {
	initialState
}

var (
	_ State = metadataFetchingState{}
)

func newMetadataFetchingState(state initialState) metadataFetchingState {
	newState := metadataFetchingState{
		initialState: state,
	}

	newState.getWaitGroup().Add(1)
	go newState.process()
	return newState
}

func (state metadataFetchingState) fail(err error) metadataFetchFailedState {
	newState := metadataFetchFailedState{state.initialState, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state metadataFetchingState) selectMetadata(launcher rpc.Launcher, blenderVersions []rpc.BlenderVersion) metadataSelectBlenderVersionState {
	return newMetadataSelectBlenderVersionState(state.initialState, launcher, blenderVersions)
}

func (state metadataFetchingState) process() {
	defer state.getWaitGroup().Done()

	var err error

	launchers, blenderVersions, err := rpc.GetMetaData(state.config.RPCConfig)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	launcher, err := rpc.GetCurrentLauncher(launchers)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	supportedBlenderVersions := rpc.FilterSupportedBlenderVersions(blenderVersions)
	if len(supportedBlenderVersions) == 0 {
		state.store.pushStateNaive(state.fail(&NoSupportedBlenderVersionError{}))
		return
	}

	state.store.pushStateNaive(state.selectMetadata(*launcher, supportedBlenderVersions))
}

func (state metadataFetchingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Welcome to the Open Data Benchmark")
	imgui.Text("Fetching metadata...")
}
