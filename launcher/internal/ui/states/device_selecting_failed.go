package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type deviceSelectingFailedState struct {
	deviceSelectingState
	err error
}

var (
	_ retryState = deviceSelectingFailedState{}
)

func (state deviceSelectingFailedState) Error() string {
	return fmt.Sprintf("listing devices failed: %s", state.err.Error())
}

func (state deviceSelectingFailedState) retry() State {
	return newDeviceSelectingState(
		state.metadataSelectedScenesState,
		state.blenderExecutable,
		state.benchmarkScript,
		state.blendFiles,
	)
}

func (state deviceSelectingFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	switch err := state.err.(type) {
	case rpc.BrokenCacheError:
		imgui.Text("It seems like the cache is broken, consider clearing it.")
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	default:
		imgui.Text(fmt.Sprintf("An unexpected error occured while listing devices:\n%s", err.Error()))
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	}
}
