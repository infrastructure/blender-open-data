package states

import (
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type ExitingState struct {
	State
}

var (
	_ State = ExitingState{}
)

func newExitingState(state State) ExitingState {
	return ExitingState{
		State: state,
	}
}

func (state ExitingState) Background() imgui.TextureID {
	return state.Theme().MainBackground
}

func (state ExitingState) BackgroundPath() *string {
	return nil
}

func (state ExitingState) Render(platform backend.Platform) {
	imgui.Text("Quitting...")
}
