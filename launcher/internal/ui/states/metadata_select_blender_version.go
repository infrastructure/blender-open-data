package states

import (
	"math"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type metadataFetchSuccessState struct {
	initialState
	launcher        rpc.Launcher
	blenderVersions []rpc.BlenderVersion
}

var (
	_ State = metadataFetchSuccessState{}
)

type metadataSelectBlenderVersionState struct {
	metadataFetchSuccessState
	selectedBlenderVersionIndex int
}

var (
	_ State = metadataSelectBlenderVersionState{}
)

func newMetadataSelectBlenderVersionState(state initialState, launcher rpc.Launcher, blenderVersions []rpc.BlenderVersion) metadataSelectBlenderVersionState {
	return metadataSelectBlenderVersionState{
		metadataFetchSuccessState: metadataFetchSuccessState{
			initialState:    state,
			launcher:        launcher,
			blenderVersions: blenderVersions,
		},
		selectedBlenderVersionIndex: 0,
	}
}

func (state metadataSelectBlenderVersionState) back() initialState {
	return state.initialState
}

func (state metadataSelectBlenderVersionState) selectScenes(
	selectedBlenderVersion rpc.BlenderVersion,
) metadataSelectScenes {
	return newMetadataSelectScenesState(state.metadataFetchSuccessState, selectedBlenderVersion)
}

func (state metadataSelectBlenderVersionState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Blender Version")

	imgui.Text("Select the Blender version you want to benchmark:")
	state.Theme().PushCombo()
	maxTextWidth := 80.0
	for _, blenderVersion := range state.blenderVersions {
		maxTextWidth = math.Max(float64(imgui.CalcTextSize(string(blenderVersion.Label), false, 0).X)+40.0, maxTextWidth)
	}
	imgui.PushItemWidth(float32(maxTextWidth))
	if imgui.BeginCombo(
		"##hidelabel",
		string(state.blenderVersions[state.selectedBlenderVersionIndex].Label),
	) {
		for i, blenderVersion := range state.blenderVersions {
			if imgui.SelectableV(string(blenderVersion.Label), state.selectedBlenderVersionIndex == i, 0, imgui.Vec2{X: 0, Y: 0}) {
				state.selectedBlenderVersionIndex = i
				state.store.pushStateNaive(state)
			}
			if state.selectedBlenderVersionIndex == i {
				imgui.SetItemDefaultFocus()
			}
		}
		imgui.EndCombo()
	}
	state.Theme().PopCombo()
	imgui.PopItemWidth()

	state.Theme().PushSecondaryButton()
	size := state.Theme().CalcButtonSize("Back")
	imgui.SetCursorPos(state.Theme().BottomLeft(size).CursorPos)
	if imgui.ButtonV("Back", size) {
		state.store.pushStateNaive(state.back())
	}
	state.Theme().PopSecondaryButton()

	state.Theme().PushPrimaryButton()
	size = state.Theme().CalcButtonSize("Next")
	imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
	if imgui.ButtonV("Next", size) {
		state.store.pushStateNaive(state.selectScenes(state.blenderVersions[state.selectedBlenderVersionIndex]))
	}
	state.Theme().PopPrimaryButton()
}
