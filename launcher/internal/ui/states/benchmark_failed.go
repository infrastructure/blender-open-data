package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type benchmarkFailedState struct {
	downloadSuccessState
	err error
}

var (
	_ retryState = benchmarkFailedState{}
)

func (state benchmarkFailedState) Error() string {
	return fmt.Sprintf("benchmark failed: %s", state.err.Error())
}

func (state benchmarkFailedState) retry() State {
	return deviceSelectingState{
		downloadSuccessState: state.downloadSuccessState,
		selectedDeviceIndex:  0,
	}
}

func (state benchmarkFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	imgui.Text(fmt.Sprintf("An unexpected error occured while benchmarking:\n%s", state.err.Error()))
	renderRetryButtonsWithClearCache(state)
	renderExitButtonsWithSubmitError(state)
}
