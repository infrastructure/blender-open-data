package states

type TokenRejectedByRemoteUserError struct{}

func (err *TokenRejectedByRemoteUserError) Error() string {
	return "token was rejected by remote user"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &TokenRejectedByRemoteUserError{}
)

type TokenRejectedByLauncherUserError struct{}

func (err *TokenRejectedByLauncherUserError) Error() string {
	return "token was rejected by launcher user"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &TokenRejectedByLauncherUserError{}
)

type NoSupportedBlenderVersionError struct{}

func (err *NoSupportedBlenderVersionError) Error() string {
	return "no supported Blender version"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &NoSupportedBlenderVersionError{}
)

type NoSupportedDevices struct{}

func (err *NoSupportedDevices) Error() string {
	return "no supported devices found"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &NoSupportedDevices{}
)
