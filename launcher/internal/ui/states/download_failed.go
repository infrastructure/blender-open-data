package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type downloadFailedState struct {
	metadataSelectedScenesState
	err error
}

var (
	_ retryState = downloadFailedState{}
)

func (state downloadFailedState) Error() string {
	return fmt.Sprintf("download failed: %s", state.err.Error())
}

func (state downloadFailedState) retry() State {
	return state.initialState.fetchMetadata()
}

func (state downloadFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	switch err := state.err.(type) {
	case rpc.BrokenCacheError:
		imgui.Text("It seems like the cache is broken, consider clearing it.")
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	case *rpc.DownloadError:
		imgui.Text("Something went wrong while downloading the requested files.")
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	default:
		imgui.Text(fmt.Sprintf("An unexpected error occured while downloading the requested files:\n%s", err.Error()))
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	}
}
