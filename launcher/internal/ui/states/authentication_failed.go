package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type authenticationFailedState struct {
	benchmarkSuccessState
	err error
}

var (
	_ retryState = authenticationFailedState{}
)

func (state authenticationFailedState) Error() string {
	return fmt.Sprintf("authentication failed: %s", state.err.Error())
}

func (state authenticationFailedState) retry() State {
	return newSubmittingState(state.benchmarkSuccessState)
}

func (state authenticationFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	switch err := state.err.(type) {
	case *rpc.CommunicationError:
		imgui.Text("There was a problem communicating with the servers while authenticating.")
		renderRetryButton(state)
		renderExitButtonsWithSubmitError(state)
	case *TokenRejectedByRemoteUserError:
		imgui.Text("The token was rejected by the remote user.")
		renderRetryButton(state)
		renderExitButton(state)
	case *TokenRejectedByLauncherUserError:
		imgui.Text("Do you want to request a new token?")
		renderRetryButton(state)
		renderExitButton(state)
	default:
		imgui.Text(fmt.Sprintf("An unexpected error occured while submitting your results:\n%s", err.Error()))
		renderRetryButton(state)
		renderExitButtonsWithSubmitError(state)
	}
}
