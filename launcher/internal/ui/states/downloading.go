package states

import (
	"context"
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/dustin/go-humanize"
	"github.com/inkyblackness/imgui-go/v4"
)

type metadataSelectedScenesState struct {
	metadataSelectedBlenderVersionState
	selectedScenes []rpc.Scene
}

var (
	_ State = metadataSelectedScenesState{}
)

type downloadingState struct {
	metadataSelectedScenesState
	progress downloadProgress
}

var (
	_ State = downloadingState{}
)

func newDownloadingState(state metadataSelectedBlenderVersionState, selectedScenes []rpc.Scene) downloadingState {
	totalDownloadSize := int64(rpc.TotalDownloadSize(state.selectedBlenderVersion, selectedScenes))

	newState := downloadingState{
		metadataSelectedScenesState: metadataSelectedScenesState{
			metadataSelectedBlenderVersionState: state,
			selectedScenes:                      selectedScenes,
		},
		progress: downloadProgress{
			currentLabel: "Preparing download...",
			currentBytes: 0,
			totalBytes:   totalDownloadSize,
		},
	}

	go newState.process()
	return newState
}

func (state downloadingState) fail(err error) downloadFailedState {
	newState := downloadFailedState{state.metadataSelectedScenesState, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state downloadingState) selectDevices(
	blenderExecutable core.BlenderExecutable,
	benchmarkScript core.BenchmarkScript,
	blendFiles []core.BlendFile,
) deviceSelectingState {
	return newDeviceSelectingState(
		state.metadataSelectedScenesState,
		blenderExecutable,
		benchmarkScript,
		blendFiles,
	)
}

func (state *downloadingState) reportProgress(ctx context.Context, label string, progressChannel chan rpc.DownloadProgress) {
	startBytes := state.progress.currentBytes
	for progressChannel != nil {
		select {
		case <-ctx.Done():
			return
		case progress, ok := <-progressChannel:
			if ok {
				state.progress = downloadProgress{
					currentLabel: label,
					currentBytes: startBytes + progress.CurrentBytes,
					totalBytes:   state.progress.totalBytes,
				}
				state.store.pushStateNaive(state)
			} else {
				progressChannel = nil
			}
		}
	}
}

func (state downloadingState) process() {
	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()

	progressChannel := make(chan rpc.DownloadProgress)
	go state.reportProgress(ctx, fmt.Sprintf("Downloading Blender %s", state.selectedBlenderVersion.Label), progressChannel)
	blenderExecutable, err := rpc.GetBlenderVersion(state.config.RPCConfig, state.selectedBlenderVersion, progressChannel)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	progressChannel = make(chan rpc.DownloadProgress)
	go state.reportProgress(ctx, fmt.Sprintf("Downloading benchmark script %s", state.selectedBlenderVersion.BenchmarkScript.Label), progressChannel)
	benchmarkScript, err := rpc.GetBenchmarkScript(state.config.RPCConfig, state.selectedBlenderVersion.BenchmarkScript, progressChannel)
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	blendFiles := make([]core.BlendFile, 0, len(state.selectedScenes))
	for _, scene := range state.selectedScenes {
		progressChannel = make(chan rpc.DownloadProgress)
		go state.reportProgress(ctx, fmt.Sprintf("Downloading scene %s", scene.Label), progressChannel)
		blendFile, err := rpc.GetScene(state.config.RPCConfig, scene, progressChannel)
		if err != nil {
			state.store.pushStateNaive(state.fail(err))
			return
		}
		blendFiles = append(blendFiles, *blendFile)
	}

	state.store.pushStateNaive(state.selectDevices(*blenderExecutable, *benchmarkScript, blendFiles))
}

func (state downloadingState) Background() imgui.TextureID {
	return state.Theme().ProgressBackground
}

func (state downloadingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Download")

	state.Theme().PushProgressBar()

	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 220})

	var label string
	if state.progress.totalBytes == 0 {
		imgui.ProgressBarV(0.0, imgui.Vec2{X: imgui.WindowWidth(), Y: 40}, "")
		label = "Checking cache..."
	} else {
		imgui.ProgressBarV(
			float32(state.progress.currentBytes)/float32(state.progress.totalBytes),
			imgui.Vec2{X: imgui.WindowWidth(), Y: 40}, "")
		label = fmt.Sprintf("%s [%s/%s]", state.progress.currentLabel, humanize.Bytes(uint64(state.progress.currentBytes)), humanize.Bytes(uint64(state.progress.totalBytes)))
	}
	textSize := imgui.CalcTextSize(label, false, 0)
	imgui.SetCursorPos(imgui.Vec2{X: 0.5 * (imgui.WindowWidth() - textSize.X), Y: 240 - 0.5*textSize.Y})
	imgui.Text(label)

	state.Theme().PopProgressBar()
}
