package states

import (
	"context"
	"sync"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"git.blender.org/blender-open-data/launcher/internal/ui/themes"
	"github.com/inkyblackness/imgui-go/v4"
)

type initialState struct {
	store     *StateStore
	waitGroup *sync.WaitGroup
	ctx       context.Context
	config    Config
}

var (
	_ State = initialState{}
)

func (state initialState) getStore() *StateStore {
	return state.store
}

func (state initialState) getWaitGroup() *sync.WaitGroup {
	return state.waitGroup
}

func (state initialState) getContext() context.Context {
	return state.ctx
}

func (state initialState) getConfig() Config {
	return state.config
}

func (state initialState) retryAfterCacheClear() State {
	return state.fetchMetadata()
}

func (state initialState) fetchMetadata() metadataFetchingState {
	return newMetadataFetchingState(state)
}

func (state initialState) Theme() *themes.Theme {
	return state.config.Theme
}

func (state initialState) Background() imgui.TextureID {
	return state.Theme().MainBackground
}

func (state initialState) BackgroundPath() *string {
	return nil
}

func (state initialState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Welcome to the Blender Benchmark")

	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, imgui.Vec2{X: 0, Y: 0})
	imgui.Text("This benchmark will collect data about your " +
		"device's hardware and operating system. You will be able to choose at " +
		"the end whether or not to publish this data.")
	imgui.Text("\n")
	imgui.Text("To learn more about the data we collect visit ")
	state.Theme().PushHyperLink()
	imgui.SameLine()
	if HyperLink("opendata.blender.org/about") {
		go core.OpenBrowser(config.AboutURL)
	}
	state.Theme().PopHyperLink()
	imgui.PopStyleVarV(1)

	state.Theme().PushPrimaryButton()
	size := state.Theme().CalcButtonSize("Next")
	imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
	if imgui.ButtonV("Next", size) {
		state.store.pushStateNaive(state.fetchMetadata())
	}
	state.Theme().PopPrimaryButton()
}
