package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/dustin/go-humanize"
	"github.com/inkyblackness/imgui-go/v4"
)

type metadataSelectedBlenderVersionState struct {
	metadataFetchSuccessState
	selectedBlenderVersion rpc.BlenderVersion
}

var (
	_ State = metadataSelectedBlenderVersionState{}
)

type metadataSelectScenes struct {
	metadataSelectedBlenderVersionState
	totalDownloadSize uint64
}

var (
	_ State = metadataSelectScenes{}
)

func newMetadataSelectScenesState(state metadataFetchSuccessState, selectedBlenderVersion rpc.BlenderVersion) metadataSelectScenes {
	newState := metadataSelectScenes{
		metadataSelectedBlenderVersionState: metadataSelectedBlenderVersionState{
			metadataFetchSuccessState: state,
			selectedBlenderVersion:    selectedBlenderVersion,
		},
	}
	newState.refreshTotalDownloadSize()

	return newState
}

func (state *metadataSelectScenes) refreshTotalDownloadSize() {
	state.totalDownloadSize = uint64(rpc.TotalDownloadSize(state.selectedBlenderVersion, state.selectedBlenderVersion.AvailableScenes))
}

func (state metadataSelectScenes) back() metadataSelectBlenderVersionState {
	return newMetadataSelectBlenderVersionState(state.initialState, state.launcher, state.blenderVersions)
}

func (state metadataSelectScenes) download(
	selectedScenes []rpc.Scene,
) downloadingState {
	return newDownloadingState(state.metadataSelectedBlenderVersionState, selectedScenes)
}

func (state metadataSelectScenes) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Benchmark Scenes")

	imgui.Text("The benchmark will render the following scenes:")
	imgui.PushStyleVarVec2(imgui.StyleVarItemSpacing, imgui.Vec2{X: 0, Y: 10})
	for _, scene := range state.selectedBlenderVersion.AvailableScenes {
		imgui.Bullet()
		imgui.Text(string(scene.Label))
	}
	imgui.PopStyleVarV(1)

	imgui.ColumnsV(1, "###", false)

	if state.totalDownloadSize == 0 {
		imgui.Text("You already have all files available.")
	} else {
		imgui.Text(fmt.Sprintf("Total download size will be %s.", humanize.Bytes(state.totalDownloadSize)))
	}

	state.Theme().PushSecondaryButton()
	size := state.Theme().CalcButtonSize("Back")
	imgui.SetCursorPos(state.Theme().BottomLeft(size).CursorPos)
	if imgui.ButtonV("Back", size) {
		state.store.pushStateNaive(state.back())
	}
	state.Theme().PopSecondaryButton()

	state.Theme().PushPrimaryButton()
	var pressed bool
	if state.totalDownloadSize == 0 {
		size := state.Theme().CalcButtonSize("Next")
		imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
		pressed = imgui.ButtonV("Next", size)
	} else {
		size := state.Theme().CalcButtonSize("Download Benchmarks")
		imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
		pressed = imgui.ButtonV("Download Benchmarks", size)
	}
	if pressed {
		state.store.pushStateNaive(state.download(state.selectedBlenderVersion.AvailableScenes))
	}
	state.Theme().PopPrimaryButton()
}
