package states

import (
	"context"
	"sync"

	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"git.blender.org/blender-open-data/launcher/internal/ui/themes"
	"github.com/inkyblackness/imgui-go/v4"
)

type Config struct {
	RPCConfig rpc.Config
	Theme     *themes.Theme
}

type State interface {
	BackgroundPath() *string
	Background() imgui.TextureID
	Theme() *themes.Theme
	getStore() *StateStore
	Render(platform backend.Platform)
	getWaitGroup() *sync.WaitGroup
	getContext() context.Context
	getConfig() Config
}

type UIWaker func()
type StateTransform func(oldState State) State
type StateStore struct {
	currentState State
	uiWaker      UIWaker
	transforms   chan StateTransform
}

func NewStateStore(config Config, uiWaker UIWaker, waitGroup *sync.WaitGroup, ctx context.Context) *StateStore {
	store := &StateStore{
		uiWaker:    uiWaker,
		transforms: make(chan StateTransform, 10),
	}
	initialState := initialState{
		store:     store,
		config:    config,
		waitGroup: waitGroup,
		ctx:       ctx,
	}
	store.currentState = initialState
	return store
}

func (store *StateStore) pushStateNaive(state State) {
	store.pushStateTransform(func(oldState State) State {
		return state
	})
}

func (store *StateStore) pushStateTransform(transform StateTransform) {
	store.transforms <- transform
	store.uiWaker()
}

func (store *StateStore) GetLatestState() State {
	last := false
	state := store.currentState
	for !last {
		select {
		case transform := <-store.transforms:
			state = transform(state)
		default:
			last = true
		}
	}
	store.currentState = state
	return state
}

type ErrorState interface {
	State
	error
}

type retryState interface {
	ErrorState
	retry() State
	retryAfterCacheClear() State
}

type benchmarkProgress struct {
	currentScene int
	totalScenes  int
}

type downloadProgress struct {
	currentLabel string
	currentBytes int64
	totalBytes   int64
}
