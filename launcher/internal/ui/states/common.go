package states

import (
	"os"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/inkyblackness/imgui-go/v4"
)

func renderExitButtonsWithSubmitError(state ErrorState) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	state.Theme().PushSecondaryButton()
	exitSize := state.Theme().CalcButtonSize("Quit")
	helper := state.Theme().BottomLeft(exitSize)
	imgui.SetCursorPos(helper.CursorPos)
	if imgui.ButtonV("Quit", exitSize) {
		state.getStore().pushStateNaive(newExitingState(state))
	}
	state.Theme().PopSecondaryButton()

	state.Theme().PushSecondaryButton()
	submitSize := state.Theme().CalcButtonSize("Submit Error")
	imgui.SetCursorPos(helper.Right(submitSize))
	if imgui.ButtonV("Submit Error", submitSize) {
		state.getStore().pushStateNaive(newSubmittingErrorState(state))
	}
	state.Theme().PopSecondaryButton()
}

func renderExitButton(state State) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	state.Theme().PushSecondaryButton()
	size := state.Theme().CalcButtonSize("Quit")
	imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
	if imgui.ButtonV("Quit", size) {
		state.getStore().pushStateNaive(newExitingState(state))
	}
	state.Theme().PopSecondaryButton()
}

func renderLeftExitButton(state State) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	state.Theme().PushSecondaryButton()
	size := state.Theme().CalcButtonSize("Quit")
	imgui.SetCursorPos(state.Theme().BottomLeft(size).CursorPos)
	if imgui.ButtonV("Quit", size) {
		state.getStore().pushStateNaive(newExitingState(state))
	}
	state.Theme().PopSecondaryButton()
}

func renderRetryButtonsWithClearCache(state retryState) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	state.Theme().PushPrimaryButton()
	retrySize := state.Theme().CalcButtonSize("Retry")
	helper := state.Theme().BottomRight(retrySize)
	imgui.SetCursorPos(helper.CursorPos)
	if imgui.ButtonV("Retry", retrySize) {
		state.getStore().pushStateNaive(state.retry())
	}
	state.Theme().PopPrimaryButton()

	state.Theme().PushSecondaryButton()
	clearSize := state.Theme().CalcButtonSize("Clear Cache and Retry")
	imgui.SetCursorPos(helper.Left(clearSize))
	if imgui.ButtonV("Clear Cache and Retry", clearSize) {
		cacheDir, err := core.CacheDirectory()
		if err != nil {
			state.getStore().pushStateNaive(newRetryFailedState(state, err))
		}
		err = os.RemoveAll(cacheDir)
		if err != nil {
			state.getStore().pushStateNaive(newRetryFailedState(state, err))
		}
		configDir, err := core.ConfigDirectory()
		if err != nil {
			state.getStore().pushStateNaive(newRetryFailedState(state, err))
		}
		err = os.RemoveAll(configDir)
		if err != nil {
			state.getStore().pushStateNaive(newRetryFailedState(state, err))
		}
		state.getStore().pushStateNaive(state.retryAfterCacheClear())
	}
	state.Theme().PopSecondaryButton()
}

func renderRetryButton(state retryState) {
	oldCursorPos := imgui.CursorPos()
	defer imgui.SetCursorPos(oldCursorPos)

	state.Theme().PushSecondaryButton()
	size := state.Theme().CalcButtonSize("Retry")
	imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
	if imgui.ButtonV("Retry", size) {
		state.getStore().pushStateNaive(state.retry())
	}
	state.Theme().PopSecondaryButton()
}

func HyperLink(url string) bool {
	imgui.Text(url)
	if imgui.IsItemHovered() {
		imgui.SetMouseCursor(imgui.MouseCursorHand)
		return imgui.IsMouseReleased(0)
	} else {
		return false
	}
}
