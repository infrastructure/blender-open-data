package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type metadataFetchFailedState struct {
	initialState
	err error
}

var (
	_ retryState = metadataFetchFailedState{}
)

func (state metadataFetchFailedState) Error() string {
	return fmt.Sprintf("fetching metadata failed: %s", state.err.Error())
}

func (state metadataFetchFailedState) retry() State {
	return state.initialState.fetchMetadata()
}

func (state metadataFetchFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	switch err := state.err.(type) {
	case *rpc.CommunicationError:
		imgui.Text("There was a problem communicating with the servers while fetching metadata.")
		renderRetryButton(state)
		renderExitButtonsWithSubmitError(state)
	case *rpc.CurrentLauncherNotSupportedError:
		if err.NewLauncherURL != nil {
			imgui.Text("The current launcher version is no longer supported.")
			imgui.Text("Please download the latest version at:")
			state.Theme().PushHyperLink()
			if HyperLink(err.NewLauncherURL.String()) {
				go core.OpenBrowser(*err.NewLauncherURL)
			}
			state.Theme().PopHyperLink()

			state.Theme().PushPrimaryButton()
			size := state.Theme().CalcButtonSize("Open in Browser")
			helper := state.Theme().BottomRight(size)
			imgui.SetCursorPos(helper.CursorPos)
			if imgui.ButtonV("Open in Browser", size) {
				go core.OpenBrowser(*err.NewLauncherURL)
			}
			state.Theme().PopPrimaryButton()

			state.Theme().PushSecondaryButton()
			size = state.Theme().CalcButtonSize("Copy URL")
			imgui.SetCursorPos(helper.Left(size))
			if imgui.ButtonV("Copy URL", size) {
				platform.SetClipboardText(err.NewLauncherURL.String())
			}
			state.Theme().PopSecondaryButton()

		} else {
			imgui.Text("The current launcher version is no longer supported.")
			renderExitButtonsWithSubmitError(state)
		}
	case *NoSupportedBlenderVersionError:
		imgui.Text("There does not seem to be a supported Blender version available for your operating system.")
		renderExitButtonsWithSubmitError(state)
	default:
		imgui.Text(fmt.Sprintf("An unexpected error occured while fetching metadata:\n%s", err.Error()))
		renderRetryButton(state)
		renderExitButtonsWithSubmitError(state)
	}
}
