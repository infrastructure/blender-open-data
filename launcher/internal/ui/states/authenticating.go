package states

import (
	"fmt"
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type authenticatingState struct {
	benchmarkSuccessState
	verificationURL      *url.URL
	token                *rpc.Token
	verificationUserName string
	verificationEmail    string
}

var (
	_ State = authenticatingState{}
)

func newAuthenticatingState(state benchmarkSuccessState) authenticatingState {
	newState := authenticatingState{
		benchmarkSuccessState: state,
		verificationURL:       nil,
	}

	newState.getWaitGroup().Add(1)
	go newState.process()
	return newState
}

func (state authenticatingState) fail(err error) authenticationFailedState {
	newState := authenticationFailedState{state.benchmarkSuccessState, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state authenticatingState) submitResults() submittingState {
	return newSubmittingState(state.benchmarkSuccessState)
}

func (state *authenticatingState) getNewToken() (*rpc.VerifiedResponse, *rpc.Token, error) {
	connection, err := rpc.ConnectToAuthenticator(state.config.RPCConfig)
	if err != nil {
		return nil, nil, err
	}
	token, verificationURL, err := rpc.RequestNewToken(connection)
	if err != nil {
		return nil, nil, err
	}

	state.verificationURL = verificationURL
	state.store.pushStateNaive(state)

	response, verified, err := rpc.WaitForVerification(connection)
	if err != nil {
		return nil, nil, err
	}
	if !verified {
		return nil, nil, &TokenRejectedByRemoteUserError{}
	}

	return response, token, nil
}

func (state authenticatingState) process() {
	defer state.getWaitGroup().Done()

	response, token, err := state.getNewToken()
	if err != nil {
		state.store.pushStateNaive(state.fail(err))
		return
	}

	state.token = token
	state.verificationUserName = response.UserName
	state.verificationEmail = response.Email
	state.store.pushStateNaive(state)
}

func (state authenticatingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Authentication")

	if state.token == nil && state.verificationURL != nil {
		imgui.Text("Please verify the token in the browser.")

		state.Theme().PushPrimaryButton()
		browserSize := state.Theme().CalcButtonSize("Open in Browser")
		helper := state.Theme().BottomRight(browserSize)
		imgui.SetCursorPos(helper.CursorPos)
		if imgui.ButtonV("Open in Browser", browserSize) {
			go core.OpenBrowser(*state.verificationURL)
		}
		state.Theme().PopPrimaryButton()

		state.Theme().PushSecondaryButton()
		copySize := state.Theme().CalcButtonSize("Copy URL")
		imgui.SetCursorPos(helper.Left(copySize))
		if imgui.ButtonV("Copy URL", copySize) {
			platform.SetClipboardText(state.verificationURL.String())
		}
		state.Theme().PopSecondaryButton()
	} else if state.token != nil {
		imgui.Text(fmt.Sprintf("The token was verified by %s <%s>.", state.verificationUserName, state.verificationEmail))

		state.Theme().PushPrimaryButton()
		size := state.Theme().CalcButtonSize("Continue")
		helper := state.Theme().BottomRight(size)
		imgui.SetCursorPos(helper.CursorPos)
		if imgui.ButtonV("Continue", size) {
			err := rpc.SaveToken(*state.token)
			if err != nil {
				state.store.pushStateNaive(state.fail(err))
			} else {
				state.store.pushStateNaive(state.submitResults())
			}
		}
		state.Theme().PopPrimaryButton()

		state.Theme().PushSecondaryButton()
		incorrectSize := state.Theme().CalcButtonSize("This was not me")
		imgui.SetCursorPos(helper.Left(incorrectSize))
		if imgui.ButtonV("This was not me", incorrectSize) {
			state.store.pushStateNaive(state.fail(&TokenRejectedByLauncherUserError{}))
		}
		state.Theme().PopSecondaryButton()
	} else {
		imgui.Text("Requesting a new token...")
	}
}
