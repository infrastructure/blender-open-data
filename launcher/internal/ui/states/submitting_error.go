package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/getsentry/sentry-go"
	"github.com/inkyblackness/imgui-go/v4"
)

type submittingErrorState struct {
	ErrorState
	sentryEventId        *sentry.EventID
	sentryEventSubmitted *bool
}

var (
	_ State = submittingErrorState{}
)

func newSubmittingErrorState(state ErrorState) submittingErrorState {
	newState := submittingErrorState{
		ErrorState:           state,
		sentryEventId:        nil,
		sentryEventSubmitted: nil,
	}

	newState.getWaitGroup().Add(1)
	go newState.process()
	return newState
}

func (state submittingErrorState) process() {
	defer state.getWaitGroup().Done()

	sentry.WithScope(func(scope *sentry.Scope) {
		scope.SetTag("state_type", fmt.Sprintf("%T", state.ErrorState))
		scope.SetLevel(sentry.LevelError)
		stateString := fmt.Sprintf("%#v", state)
		// Make sure we stay under the 200kB limit for Sentry messages.
		if len(stateString) > 50000 {
			stateString = stateString[:50000-3] + "..."
		}
		scope.SetExtra("state", stateString)
		sentryEventId, sentryEventSubmitted := rpc.CaptureWithSentry(state.ErrorState)
		state.sentryEventId = sentryEventId
		state.sentryEventSubmitted = &sentryEventSubmitted
	})

	if state.sentryEventId != nil {
		err := rpc.UploadLogFile(state.getConfig().RPCConfig, *state.sentryEventId)
		if err != nil {
			core.ErrorLogger.Printf("Failed to upload logs: %s", err.Error())
		}
	} else {
		core.DebugLogger.Print("Not uploading logs because we do not have a Sentry Event ID.")
	}

	state.getStore().pushStateNaive(state)
}

func (state submittingErrorState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	if state.sentryEventSubmitted == nil {
		imgui.Text("Submitting errors...")
	} else if *state.sentryEventSubmitted {
		imgui.Text("Thank you for submitting the error.")
		if state.sentryEventId != nil && *state.sentryEventId != "" {
			imgui.Text(fmt.Sprintf("When submitting a bug report please include the following error id:\n%s", *state.sentryEventId))
		}

		state.Theme().PushPrimaryButton()
		size := state.Theme().CalcButtonSize("Quit")
		helper := state.Theme().BottomRight(size)
		imgui.SetCursorPos(helper.CursorPos)
		if imgui.ButtonV("Quit", size) {
			state.getStore().pushStateNaive(newExitingState(state))
		}
		state.Theme().PopSecondaryButton()

		if state.sentryEventId != nil && *state.sentryEventId != "" {
			state.Theme().PushSecondaryButton()
			size = state.Theme().CalcButtonSize("Copy Error ID")
			helper.Left(size)
			imgui.SetCursorPos(helper.CursorPos)
			if imgui.ButtonV("Copy Error ID", size) {
				platform.SetClipboardText(string(*state.sentryEventId))
			}
			state.Theme().PopSecondaryButton()
		}
	} else {
		imgui.Text("Failed to submit the error.")
		renderExitButton(state)
	}
}
