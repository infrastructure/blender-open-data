package states

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type submitFailedState struct {
	benchmarkSuccessState
	err error
}

var (
	_ retryState = submitFailedState{}
)

func (state submitFailedState) Error() string {
	return fmt.Sprintf("submission failed: %s", state.err.Error())
}

func (state submitFailedState) retry() State {
	return state.benchmarkSuccessState.submitResults()
}

func (state submitFailedState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Whoops!")

	switch err := state.err.(type) {
	case *rpc.CommunicationError:
		imgui.Text("There was a problem communicating with the servers while submitting your results.")
		renderRetryButton(state)
		renderExitButtonsWithSubmitError(state)
	case *rpc.CurrentLauncherNotSupportedError:
		if err.NewLauncherURL != nil {
			imgui.Text("The current launcher version is no longer supported.")
			imgui.Text("Please download the latest version at:")
			state.Theme().PushHyperLink()
			if HyperLink(err.NewLauncherURL.String()) {
				go core.OpenBrowser(*err.NewLauncherURL)
			}
			state.Theme().PopHyperLink()

			state.Theme().PushPrimaryButton()
			size := state.Theme().CalcButtonSize("Open in Browser")
			helper := state.Theme().BottomRight(size)
			imgui.SetCursorPos(helper.CursorPos)
			if imgui.ButtonV("Open in Browser", size) {
				go core.OpenBrowser(*err.NewLauncherURL)
			}
			state.Theme().PopPrimaryButton()

			state.Theme().PushSecondaryButton()
			size = state.Theme().CalcButtonSize("Copy URL")
			imgui.SetCursorPos(helper.Left(size))
			if imgui.ButtonV("Copy URL", size) {
				platform.SetClipboardText(err.NewLauncherURL.String())
			}
			state.Theme().PopSecondaryButton()

		} else {
			imgui.Text("The current launcher version is no longer supported.")
			renderExitButtonsWithSubmitError(state)
		}
	case *rpc.InvalidTokenError:
		imgui.Text("The authentication token seems to be broken.")

		state.Theme().PushPrimaryButton()
		size := state.Theme().CalcButtonSize("Request New Token")
		imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
		if imgui.ButtonV("Request New Token", size) {
			state.store.pushStateNaive(newAuthenticatingState(state.benchmarkSuccessState))
		}
		state.Theme().PopPrimaryButton()

		renderLeftExitButton(state)
	case *rpc.NonVerifiedTokenError:
		imgui.Text("The authentication token was revoked.")

		state.Theme().PushPrimaryButton()
		size := state.Theme().CalcButtonSize("Request New Token")
		imgui.SetCursorPos(state.Theme().BottomRight(size).CursorPos)
		if imgui.ButtonV("Request New Token", size) {
			state.store.pushStateNaive(newAuthenticatingState(state.benchmarkSuccessState))
		}
		state.Theme().PopPrimaryButton()

		renderLeftExitButton(state)
	default:
		imgui.Text("An unexpected error occured while submitting your results.")
		renderRetryButtonsWithClearCache(state)
		renderExitButtonsWithSubmitError(state)
	}
}
