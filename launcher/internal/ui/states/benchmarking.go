package states

import (
	"fmt"
	"path/filepath"
	"sync"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"github.com/inkyblackness/imgui-go/v4"
)

type deviceSelectedState struct {
	downloadSuccessState
	device core.Device
}

var (
	_ State = deviceSelectedState{}
)

type benchmarkingState struct {
	deviceSelectedState
	status                       string
	progress                     benchmarkProgress
	currentSceneProgressFraction float64
	background                   *string
}

var (
	_ State = benchmarkingState{}
)

func newBenchmarkingState(state downloadSuccessState, device core.Device) benchmarkingState {
	newState := benchmarkingState{
		deviceSelectedState: deviceSelectedState{
			downloadSuccessState: state,
			device:               device,
		},
		progress: benchmarkProgress{
			currentScene: 0,
			totalScenes:  len(state.selectedScenes),
		},
		currentSceneProgressFraction: 0.0,
		background:                   nil,
	}

	newState.getWaitGroup().Add(1)
	go newState.process()
	return newState
}

func (state benchmarkingState) fail(err error) benchmarkFailedState {
	newState := benchmarkFailedState{state.downloadSuccessState, err}
	core.ErrorLogger.Printf("%s\n", newState.Error())
	return newState
}

func (state benchmarkingState) success(benchmarkResults []rpc.BenchmarkResult) benchmarkSuccessState {
	return benchmarkSuccessState{state.downloadSuccessState, benchmarkResults}
}

func (state *benchmarkingState) runBenchmark(
	blendFile core.BlendFile,
	warmUp core.WarmUp,
) (*core.RenderInfo, error) {
	progressChannel := make(chan core.Progress, 100)

	state.status = "Loading"
	state.store.pushStateNaive(state)

	var progressWaitGroup sync.WaitGroup

	progressWaitGroup.Add(1)

	go func() {
		defer progressWaitGroup.Done()

		did_start_rendering := false
		render_start_time := time.Now()
		time_limit := 0.0

		for progress := range progressChannel {
			if warmUp {
				state.status = "Warming up"
			} else {
				state.status = "Benchmarking"
			}

			if !progress.IsHeartbeat {
				if !did_start_rendering && progress.TimeLimit != 0 {
					did_start_rendering = true
					render_start_time = time.Now()
					time_limit = progress.TimeLimit
				}
			}

			if did_start_rendering {
				render_duration := time.Since(render_start_time).Seconds()
				state.currentSceneProgressFraction = render_duration / time_limit
			}

			state.store.pushStateNaive(state)
		}
	}()

	renderInfo, err := core.RunBenchmark(
		state.blenderExecutable,
		state.benchmarkScript,
		core.RequestedDevice{
			Name: state.device.GetName(),
			Type: state.device.GetType(),
		},
		blendFile,
		warmUp,
		state.ctx,
		progressChannel,
	)

	close(progressChannel)
	progressWaitGroup.Wait()

	// Blender tends to exit with segfaults, so if the RenderInfo is available
	// we consider the run a success.
	if renderInfo != nil {
		return renderInfo, nil
	} else {
		return nil, err
	}
}

func (state benchmarkingState) process() {
	defer state.getWaitGroup().Done()

	benchmarkResults := make([]rpc.BenchmarkResult, 0, len(state.selectedScenes))

	for i, blendFile := range state.blendFiles {
		state.progress.currentScene = i
		state.currentSceneProgressFraction = 0.0

		background := filepath.Join(filepath.Dir(string(blendFile)), "background.png")
		state.background = &background

		_, err := state.runBenchmark(blendFile, true)
		if err != nil {
			state.store.pushStateNaive(state.fail(err))
			return
		}
		state.currentSceneProgressFraction = 0.0

		//noinspection ALL
		renderInfo, err := state.runBenchmark(blendFile, false)
		if err != nil {
			state.store.pushStateNaive(state.fail(err))
			return
		}
		state.currentSceneProgressFraction = 0.0

		benchmarkResults = append(benchmarkResults, rpc.BenchmarkResult{
			Launcher:       state.launcher,
			BlenderVersion: state.selectedBlenderVersion,
			Scene:          state.selectedScenes[i],
			RenderInfo:     *renderInfo,
		})
	}

	state.store.pushStateNaive(state.success(benchmarkResults))
}

func (state benchmarkingState) BackgroundPath() *string {
	return state.background
}

func (state benchmarkingState) Background() imgui.TextureID {
	return state.Theme().ProgressBackground
}

func (state benchmarkingState) Render(platform backend.Platform) {
	state.Theme().HeaderLarge("Benchmarking")

	state.Theme().PushProgressBar()

	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 220})
	imgui.ProgressBarV(float32(state.currentSceneProgressFraction), imgui.Vec2{X: imgui.WindowWidth(), Y: 40}, "")

	label := fmt.Sprintf(
		"%s scene %s [%d/%d]",
		state.status,
		state.selectedScenes[state.progress.currentScene].Label,
		state.progress.currentScene+1,
		state.progress.totalScenes,
	)

	textSize := imgui.CalcTextSize(label, false, 0)
	imgui.SetCursorPos(imgui.Vec2{X: 0.5 * (imgui.WindowWidth() - textSize.X), Y: 240 - 0.5*textSize.Y})
	imgui.Text(label)

	state.Theme().PopProgressBar()
}
