package ui

import (
	"image"
	"os"

	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/ui/backend"
	"git.blender.org/blender-open-data/launcher/internal/ui/states"
	"git.blender.org/blender-open-data/launcher/internal/ui/themes"
	"github.com/inkyblackness/imgui-go/v4"
)

func scheduleDrawCall(platform backend.Platform, renderer backend.Renderer) {
	var clearColor = [4]float32{0.0, 0.0, 0.0, 1.0}
	imgui.Render()
	renderer.PreRender(clearColor)
	renderer.Render(platform.UISize(), platform.FramebufferSize(), imgui.RenderedDrawData())
	platform.PostRender()
}

func renderWindow(platform backend.Platform, renderer backend.Renderer, theme *themes.Theme, state states.State) {
	imgui.SetMouseCursor(imgui.MouseCursorArrow)

	imgui.SetNextWindowPos(imgui.Vec2{X: 0, Y: 0})
	imgui.SetNextWindowSize(imgui.Vec2{X: platform.UISize()[0], Y: platform.UISize()[1]})

	imgui.PushStyleVarFloat(imgui.StyleVarWindowBorderSize, 0.0)
	imgui.PushStyleVarVec2(imgui.StyleVarWindowPadding, imgui.Vec2{X: 0, Y: 0})

	imgui.BeginV("###", nil,
		imgui.WindowFlagsNoTitleBar|
			imgui.WindowFlagsNoResize|
			imgui.WindowFlagsNoMove|
			imgui.WindowFlagsNoScrollbar|
			imgui.WindowFlagsNoScrollWithMouse|
			imgui.WindowFlagsNoCollapse|
			imgui.WindowFlagsNoSavedSettings|
			imgui.WindowFlagsNoBackground)

	renderBackground(platform, renderer, theme, state)

	imgui.SetNextWindowPos(imgui.Vec2{X: 20, Y: 20})
	imgui.BeginChildV("###", imgui.Vec2{X: imgui.WindowWidth() - 2*20, Y: imgui.WindowHeight() - 2*20}, false,
		imgui.WindowFlagsNoTitleBar|
			imgui.WindowFlagsNoResize|
			imgui.WindowFlagsNoMove|
			imgui.WindowFlagsNoScrollbar|
			imgui.WindowFlagsNoScrollWithMouse|
			imgui.WindowFlagsNoCollapse|
			imgui.WindowFlagsNoSavedSettings|
			imgui.WindowFlagsNoBackground)

	imgui.PushTextWrapPos()
	// TODO(sem): Move this into Theme.TopLeft.
	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 150})
	state.Theme().PushDefaults()
	state.Render(platform)
	state.Theme().PopDefaults()
	imgui.PopTextWrapPos()

	imgui.EndChild()

	imgui.End()

	imgui.PopStyleVarV(2)

	platform.SetMouseCursor()
}

var lastBackgroundPath *string = nil
var lastBackgroundPathTextureId *imgui.TextureID = nil

func getBackgroundPathTextureID(renderer backend.Renderer, path *string) *imgui.TextureID {
	if lastBackgroundPath != nil {
		if path != nil && *path == *lastBackgroundPath {
			return lastBackgroundPathTextureId
		} else if lastBackgroundPathTextureId != nil {
			renderer.DestroyTexture(*lastBackgroundPathTextureId)
		}
	}

	lastBackgroundPath = path
	lastBackgroundPathTextureId = nil

	if path != nil {
		file, err := os.Open(*path)
		if err == nil {
			defer file.Close()
			img, _, err := image.Decode(file)
			if err == nil {
				imgNRGBA, ok := img.(*image.NRGBA)
				if ok {
					textureID := renderer.RegisterTexture(*imgNRGBA)
					lastBackgroundPathTextureId = &textureID
				}
			}
		}
	}

	return lastBackgroundPathTextureId
}

func renderBackground(platform backend.Platform, renderer backend.Renderer, theme *themes.Theme, state states.State) {
	background := state.Background()
	backgroundPath := getBackgroundPathTextureID(renderer, state.BackgroundPath())
	if backgroundPath != nil {
		background = *backgroundPath
	}

	oldCursorPos := imgui.CursorPos()
	imgui.SetCursorPos(imgui.Vec2{X: 0, Y: 0})
	imgui.Image(background, imgui.Vec2{
		X: platform.UISize()[0],
		Y: platform.UISize()[1],
	})
	imgui.SetCursorPos(oldCursorPos)

	oldCursorPos = imgui.CursorPos()
	imgui.SetCursorPos(imgui.Vec2{X: 490, Y: 25})
	theme.PushText()
	if states.HyperLink("opendata.blender.org") {
		go core.OpenBrowser(config.OpendataHomeURL)
	}
	theme.PopText()
	imgui.SetCursorPos(oldCursorPos)
}
