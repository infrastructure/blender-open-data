package backend

import (
	"image"

	"github.com/inkyblackness/imgui-go/v4"
)

// Renderer covers rendering imgui draw data.
type Renderer interface {
	// PreRender causes the display buffer to be prepared for new output.
	PreRender(clearColor [4]float32)
	// Render draws the provided imgui draw data.
	Render(uiSize [2]float32, framebufferSize [2]float32, drawData imgui.DrawData)
	// RegisterTexture prepares an image for drawing.
	RegisterTexture(image image.NRGBA) imgui.TextureID
	// RegisterTextureImGUI prepares an image for drawing.
	RegisterTextureImGUI(image imgui.RGBA32Image) imgui.TextureID
	// DestroyTexture deallocates a texture.
	DestroyTexture(texture imgui.TextureID)
}
