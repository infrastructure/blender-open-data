package backend

import (
	"fmt"
	"math"
	"runtime"
	"time"

	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/inkyblackness/imgui-go/v4"
)

// GLFWClientAPI identifies the render system that shall be initialized.
type GLFWClientAPI string

// GLFWClientAPI constants
const (
	GLFWClientAPIOpenGL2 GLFWClientAPI = "OpenGL2"
	GLFWClientAPIOpenGL3 GLFWClientAPI = "OpenGL3"
)

// GLFW implements a platform based on github.com/go-gl/glfw (v3.2).
type GLFW struct {
	imguiIO imgui.IO

	window *glfw.Window

	time             float64
	mouseJustPressed [3]bool

	waitEventsFrameCounter int
	cursors                map[imgui.MouseCursorID]*glfw.Cursor

	uiSizeChangeCallback          SizeChangeCallback
	displaySizeChangeCallback     SizeChangeCallback
	framebufferSizeChangeCallback SizeChangeCallback
}

// NewGLFW attempts to initialize a GLFW context.
func NewGLFW(io imgui.IO, clientAPI GLFWClientAPI) (*GLFW, error) {
	runtime.LockOSThread()

	err := glfw.Init()
	if err != nil {
		return nil, fmt.Errorf("failed to initialize glfw: %v", err)
	}

	switch clientAPI {
	case GLFWClientAPIOpenGL2:
		glfw.WindowHint(glfw.ContextVersionMajor, 2)
		glfw.WindowHint(glfw.ContextVersionMinor, 1)
	case GLFWClientAPIOpenGL3:
		glfw.WindowHint(glfw.ContextVersionMajor, 3)
		glfw.WindowHint(glfw.ContextVersionMinor, 2)
		glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
		glfw.WindowHint(glfw.OpenGLForwardCompatible, 1)
	default:
		glfw.Terminate()
		return nil, fmt.Errorf("unsupported OpenGL ClientAPI: <%s>", clientAPI)
	}

	// Enable HiDPI support.
	glfw.WindowHint(glfw.ScaleToMonitor, glfw.True)
	glfw.WindowHint(glfw.CocoaRetinaFramebuffer, glfw.True)

	glfw.WindowHint(glfw.Resizable, 0)
	window, err := glfw.CreateWindow(640, 480, "Blender Benchmark Launcher", nil, nil)
	if err != nil {
		glfw.Terminate()
		return nil, fmt.Errorf("failed to create window: %v", err)
	}
	window.MakeContextCurrent()
	glfw.SwapInterval(1)

	cursors := make(map[imgui.MouseCursorID]*glfw.Cursor)
	cursors[imgui.MouseCursorArrow] = glfw.CreateStandardCursor(glfw.ArrowCursor)
	cursors[imgui.MouseCursorHand] = glfw.CreateStandardCursor(glfw.HandCursor)

	platform := &GLFW{
		imguiIO:                       io,
		window:                        window,
		waitEventsFrameCounter:        1,
		cursors:                       cursors,
		uiSizeChangeCallback:          nil,
		displaySizeChangeCallback:     nil,
		framebufferSizeChangeCallback: nil,
	}
	platform.setKeyMapping()
	platform.installCallbacks()

	return platform, nil
}

// Dispose cleans up the resources.
func (platform *GLFW) Dispose() {
	for _, cursor := range platform.cursors {
		cursor.Destroy()
	}
	platform.cursors = make(map[imgui.MouseCursorID]*glfw.Cursor)
	platform.window.Destroy()
	glfw.Terminate()
}

// ShouldStop returns true if the window is to be closed.
func (platform *GLFW) ShouldStop() bool {
	return platform.window.ShouldClose()
}

// PollEvents handles all pending window events.
func (platform *GLFW) PollEvents() {
	glfw.PollEvents()
}

// WaitEvents waits for window events and then handles them.
func (platform *GLFW) WaitEvents(minWait *time.Duration, maxWait *time.Duration) {
	if platform.waitEventsFrameCounter > 0 {
		platform.waitEventsFrameCounter -= 1
	} else {
		if maxWait == nil {
			glfw.WaitEvents()
		} else {
			glfw.WaitEventsTimeout(maxWait.Seconds())
		}
		// We need to render an additional three frames to allow ImGUI to render
		// its animations.
		platform.waitEventsFrameCounter = 3
	}

	if minWait != nil {
		currentTime := glfw.GetTime()
		deltaTime := currentTime - platform.time
		if deltaTime < minWait.Seconds() {
			time.Sleep(time.Duration((minWait.Seconds() - deltaTime) * float64(time.Second)))
		}
	}
}

// WakeFromWaitEvents causes WaitEvents() to return.
func (platform *GLFW) WakeFromWaitEvents() {
	glfw.PostEmptyEvent()
}

// UISize returns the dimension of the UI.
func (platform *GLFW) UISize() [2]float32 {
	//w, h := platform.window.GetSize()
	//x, y := platform.window.GetContentScale()
	//return [2]float32{float32(w) / x, float32(h) / y}

	// Since we do support dynamic scaling we just ignore the user scaling setting.
	return [2]float32{640, 480}
}

// DisplaySize returns the dimension of the display.
func (platform *GLFW) DisplaySize() [2]float32 {
	w, h := platform.window.GetSize()
	return [2]float32{float32(w), float32(h)}
}

// FramebufferSize returns the dimension of the framebuffer.
func (platform *GLFW) FramebufferSize() [2]float32 {
	w, h := platform.window.GetFramebufferSize()
	return [2]float32{float32(w), float32(h)}
}

// NewFrame marks the begin of a render pass. It forwards all current state to imgui IO.
func (platform *GLFW) NewFrame() {
	// Setup display size (every frame to accommodate for window resizing)
	uiSize := platform.UISize()
	platform.imguiIO.SetDisplaySize(imgui.Vec2{X: uiSize[0], Y: uiSize[1]})

	// Setup time step
	currentTime := glfw.GetTime()
	if platform.time > 0 {
		platform.imguiIO.SetDeltaTime(float32(currentTime - platform.time))
	}
	platform.time = currentTime

	// Setup inputs
	if platform.window.GetAttrib(glfw.Focused) != 0 && platform.DisplaySize()[0] > 0.0 && platform.DisplaySize()[1] > 0.0 {
		x, y := platform.window.GetCursorPos()
		// The mouse cursor coordinates are in screen space so we need to scale them to UI space.
		scaleX := platform.UISize()[0] / platform.DisplaySize()[0]
		scaleY := platform.UISize()[1] / platform.DisplaySize()[1]
		platform.imguiIO.SetMousePosition(imgui.Vec2{X: float32(x) * scaleX, Y: float32(y) * scaleY})
	} else {
		platform.imguiIO.SetMousePosition(imgui.Vec2{X: -math.MaxFloat32, Y: -math.MaxFloat32})
	}

	for i := 0; i < len(platform.mouseJustPressed); i++ {
		down := platform.mouseJustPressed[i] || (platform.window.GetMouseButton(glfwButtonIDByIndex[i]) == glfw.Press)
		platform.imguiIO.SetMouseButtonDown(i, down)
		platform.mouseJustPressed[i] = false
	}
}

// PostRender performs a buffer swap.
func (platform *GLFW) PostRender() {
	platform.window.SwapBuffers()
}

func (platform *GLFW) setKeyMapping() {
	// Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
	platform.imguiIO.KeyMap(imgui.KeyTab, int(glfw.KeyTab))
	platform.imguiIO.KeyMap(imgui.KeyLeftArrow, int(glfw.KeyLeft))
	platform.imguiIO.KeyMap(imgui.KeyRightArrow, int(glfw.KeyRight))
	platform.imguiIO.KeyMap(imgui.KeyUpArrow, int(glfw.KeyUp))
	platform.imguiIO.KeyMap(imgui.KeyDownArrow, int(glfw.KeyDown))
	platform.imguiIO.KeyMap(imgui.KeyPageUp, int(glfw.KeyPageUp))
	platform.imguiIO.KeyMap(imgui.KeyPageDown, int(glfw.KeyPageDown))
	platform.imguiIO.KeyMap(imgui.KeyHome, int(glfw.KeyHome))
	platform.imguiIO.KeyMap(imgui.KeyEnd, int(glfw.KeyEnd))
	platform.imguiIO.KeyMap(imgui.KeyInsert, int(glfw.KeyInsert))
	platform.imguiIO.KeyMap(imgui.KeyDelete, int(glfw.KeyDelete))
	platform.imguiIO.KeyMap(imgui.KeyBackspace, int(glfw.KeyBackspace))
	platform.imguiIO.KeyMap(imgui.KeySpace, int(glfw.KeyEnter))
	platform.imguiIO.KeyMap(imgui.KeyEnter, int(glfw.KeyEnter))
	platform.imguiIO.KeyMap(imgui.KeyEscape, int(glfw.KeyEscape))
	platform.imguiIO.KeyMap(imgui.KeyA, int(glfw.KeyA))
	platform.imguiIO.KeyMap(imgui.KeyC, int(glfw.KeyC))
	platform.imguiIO.KeyMap(imgui.KeyV, int(glfw.KeyV))
	platform.imguiIO.KeyMap(imgui.KeyX, int(glfw.KeyX))
	platform.imguiIO.KeyMap(imgui.KeyY, int(glfw.KeyY))
	platform.imguiIO.KeyMap(imgui.KeyZ, int(glfw.KeyZ))
}

func (platform *GLFW) installCallbacks() {
	platform.window.SetMouseButtonCallback(platform.mouseButtonChange)
	platform.window.SetScrollCallback(platform.mouseScrollChange)
	platform.window.SetKeyCallback(platform.keyChange)
	platform.window.SetCharCallback(platform.charChange)
	// We fixed the UI size so it will never change.
	// platform.window.SetContentScaleCallback(platform.uiSizeChange)
	platform.window.SetSizeCallback(platform.windowSizeChange)
	platform.window.SetFramebufferSizeCallback(platform.frameBufferSizeChange)
}

var glfwButtonIndexByID = map[glfw.MouseButton]int{
	glfw.MouseButton1: 0,
	glfw.MouseButton2: 1,
	glfw.MouseButton3: 2,
}

var glfwButtonIDByIndex = map[int]glfw.MouseButton{
	0: glfw.MouseButton1,
	1: glfw.MouseButton2,
	2: glfw.MouseButton3,
}

// We fixed the UI size so it will never change.
//func (platform *GLFW) uiSizeChange(window *glfw.Window, x float32, y float32) {
//	if platform.uiSizeChangeCallback != nil {
//		platform.uiSizeChangeCallback(platform.UISize()[0], platform.UISize()[1])
//	}
//}

func (platform *GLFW) SetUISizeChangeCallback(callback SizeChangeCallback) {
	platform.uiSizeChangeCallback = callback
}

func (platform *GLFW) windowSizeChange(window *glfw.Window, width int, height int) {
	if platform.displaySizeChangeCallback != nil {
		platform.displaySizeChangeCallback(platform.DisplaySize()[0], platform.DisplaySize()[1])
	}
}

func (platform *GLFW) SetDisplaySizeChangeCallback(callback SizeChangeCallback) {
	platform.displaySizeChangeCallback = callback
}

func (platform *GLFW) frameBufferSizeChange(window *glfw.Window, width int, height int) {
	if platform.framebufferSizeChangeCallback != nil {
		platform.framebufferSizeChangeCallback(platform.FramebufferSize()[0], platform.FramebufferSize()[1])
	}
}

func (platform *GLFW) SetFramebufferSizeChangeCallback(callback SizeChangeCallback) {
	platform.framebufferSizeChangeCallback = callback
}

func (platform *GLFW) mouseButtonChange(window *glfw.Window, rawButton glfw.MouseButton, action glfw.Action, mods glfw.ModifierKey) {
	buttonIndex, known := glfwButtonIndexByID[rawButton]

	if known && (action == glfw.Press) {
		platform.mouseJustPressed[buttonIndex] = true
	}
}

func (platform *GLFW) mouseScrollChange(window *glfw.Window, x, y float64) {
	platform.imguiIO.AddMouseWheelDelta(float32(x), float32(y))
}

func (platform *GLFW) keyChange(window *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if action == glfw.Press {
		platform.imguiIO.KeyPress(int(key))
	}
	if action == glfw.Release {
		platform.imguiIO.KeyRelease(int(key))
	}

	// Modifiers are not reliable across systems
	platform.imguiIO.KeyCtrl(int(glfw.KeyLeftControl), int(glfw.KeyRightControl))
	platform.imguiIO.KeyShift(int(glfw.KeyLeftShift), int(glfw.KeyRightShift))
	platform.imguiIO.KeyAlt(int(glfw.KeyLeftAlt), int(glfw.KeyRightAlt))
	platform.imguiIO.KeySuper(int(glfw.KeyLeftSuper), int(glfw.KeyRightSuper))
}

func (platform *GLFW) charChange(window *glfw.Window, char rune) {
	platform.imguiIO.AddInputCharacters(string(char))
}

// ClipboardText returns the current clipboard text, if available.
func (platform *GLFW) ClipboardText() (string, error) {
	return platform.window.GetClipboardString(), nil
}

// SetClipboardText sets the text as the current clipboard text.
func (platform *GLFW) SetClipboardText(text string) {
	platform.window.SetClipboardString(text)
}

func (platform *GLFW) SetMouseCursor() {
	switch imgui.MouseCursor() {
	case imgui.MouseCursorHand:
		platform.window.SetCursor(platform.cursors[imgui.MouseCursorHand])
	default:
		platform.window.SetCursor(platform.cursors[imgui.MouseCursorArrow])
	}
}
