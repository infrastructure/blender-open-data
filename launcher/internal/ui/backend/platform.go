package backend

import "time"

type SizeChangeCallback func(width float32, height float32)

// Platform covers mouse/keyboard/gamepad inputs, cursor shape, timing, windowing.
type Platform interface {
	// ShouldStop is regularly called as the abort condition for the program loop.
	ShouldStop() bool
	// PollEvents is called once per render loop to dispatch any pending events.
	PollEvents()
	// WaitEvents is the blocking version of PollEvents.
	// Implementations should not block for 3 consecutive frames after blocking once.
	// This is because ImGUI has animations which need to be rendered.
	WaitEvents(minWait *time.Duration, maxWait *time.Duration)
	// WakeFromWaitEvents can be called from another thread than the main thread to
	// wake the main thread if it is blocking on WaitEvents().
	WakeFromWaitEvents()
	// UISize returns the dimension of the UI.
	UISize() [2]float32
	// SetUISizeCallback sets the callback for a change of the UI size.
	SetUISizeChangeCallback(callback SizeChangeCallback)
	// DisplaySize returns the dimension of the display.
	DisplaySize() [2]float32
	// SetDisplaySizeChangeCallback sets the callback for a change of the display size.
	SetDisplaySizeChangeCallback(callback SizeChangeCallback)
	// FramebufferSize returns the dimension of the framebuffer.
	FramebufferSize() [2]float32
	// SetFramebufferSizeChangeCallback sets the callback for a change of the framebuffer size.
	SetFramebufferSizeChangeCallback(callback SizeChangeCallback)
	// NewFrame marks the begin of a render pass. It must update the imgui IO state according to user input (mouse, keyboard, ...)
	NewFrame()
	// PostRender marks the completion of one render pass. Typically this causes the display buffer to be swapped.
	PostRender()
	// ClipboardText returns the current text of the clipboard, if available.
	ClipboardText() (string, error)
	// SetClipboardText sets the text as the current text of the clipboard.
	SetClipboardText(text string)
	// SetMouseCursor sets the mouse cursor according to the value in ImGUI.
	SetMouseCursor()
}

type Clipboard struct {
	platform Platform
}

func NewClipboard(platform Platform) *Clipboard {
	return &Clipboard{platform: platform}
}

func (board Clipboard) Text() (string, error) {
	return board.platform.ClipboardText()
}

func (board Clipboard) SetText(text string) {
	board.platform.SetClipboardText(text)
}
