package internal

import (
	"os"

	"git.blender.org/blender-open-data/launcher/internal/core"
)

func ExitWithError(err error) {
	core.ErrorLogger.Printf("%s", err.Error())
	os.Exit(1)
}
