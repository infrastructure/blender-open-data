package cli

import (
	"github.com/spf13/cobra"
)

var authenticateCommand = &cobra.Command{
	Use:   "authenticate",
	Short: "Request a new authentication token",
	RunE:  WithErrorLogger(runAuthenticate),
}

func init() {
	authenticateCommand.Flags().BoolVar(&browserFlag, "browser", false, "open the verification URL in the browser")
}

func runAuthenticate(cmd *cobra.Command, args []string) error {
	_, err := getNewToken()
	return err
}
