package cli

import (
	"context"
	"fmt"
	"os"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/AlecAivazis/survey/v2"
	"github.com/cheggaaa/pb"
)

var surveyAskOpt = survey.WithStdio(os.Stdin, os.Stderr, os.Stderr)

var blenderVersionFlag string
var browserFlag = false

func reportDownloadProgress(ctx context.Context, total rpc.SizeInBytes, progressChannel chan rpc.DownloadProgress) {
	progressBar := pb.New(int(total))
	progressBar.Start()
	defer progressBar.Finish()

	for progressChannel != nil {
		select {
		case <-ctx.Done():
			return
		case progress, ok := <-progressChannel:
			if ok {
				progressBar.SetCurrent(progress.CurrentBytes)
				progressBar.SetTotal(progress.TotalBytes)
			} else {
				progressChannel = nil
			}
		}
	}
}

func getNewToken() (*rpc.Token, error) {
	var err error

	connection, err := rpc.ConnectToAuthenticator(rpc.DefaultConfig)
	if err != nil {
		return nil, err
	}
	token, verificationURL, err := rpc.RequestNewToken(connection)
	if err != nil {
		return nil, err
	}

	core.DisplayLogger.Printf("Please verify the token at:\n%s", verificationURL)

	if browserFlag {
		go core.OpenBrowser(*verificationURL)
	}

	core.DisplayLogger.Printf("Waiting for verification...")
	response, verified, err := rpc.WaitForVerification(connection)
	if err != nil {
		return nil, err
	}
	if !verified {
		return nil, &TokenRejectedByRemoteUserError{}
	}

	wasCorrectUser := false
	prompt := &survey.Confirm{
		Message: fmt.Sprintf("The token was verified by %s <%s>, was this you?", response.UserName, response.Email),
		Default: true,
	}
	err = survey.AskOne(prompt, &wasCorrectUser, surveyAskOpt)
	if err != nil {
		return nil, err
	}
	if !wasCorrectUser {
		return nil, &TokenRejectedByLauncherUserError{}
	}

	err = rpc.SaveToken(*token)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func findBlenderVersion(label string, blenderVersions []rpc.BlenderVersion) (*rpc.BlenderVersion, error) {
	for _, b := range blenderVersions {
		if string(b.Label) == label {
			return &b, nil
		}
	}
	return nil, NewUnknownBlenderVersionError(label, blenderVersions)
}

func getMetaData() (*rpc.Launcher, []rpc.BlenderVersion, error) {
	var err error
	launchers, blenderVersions, err := rpc.GetMetaData(rpc.DefaultConfig)
	if err != nil {
		return nil, nil, err
	}
	blenderVersions = rpc.FilterSupportedBlenderVersions(blenderVersions)
	if len(blenderVersions) == 0 {
		return nil, nil, &NoSupportedBlenderVersionError{}
	}

	launcher, err := rpc.GetCurrentLauncher(launchers)
	if err != nil {
		return nil, nil, err
	}

	return launcher, blenderVersions, nil
}
