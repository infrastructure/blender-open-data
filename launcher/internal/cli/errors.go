package cli

import (
	"fmt"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/spf13/cobra"
)

type UserCancelledError interface {
	error
	isUserCancelledError()
}

type UserCancelledDownloadError struct {
}

func (err *UserCancelledDownloadError) Error() string {
	return "the user cancelled the download"
}

func (err *UserCancelledDownloadError) isUserCancelledError() {}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ UserCancelledError = &UserCancelledDownloadError{}
)

type UserCancelledBenchmarkError struct {
}

func (err *UserCancelledBenchmarkError) Error() string {
	return "the user cancelled the benchmark"
}

func (err *UserCancelledBenchmarkError) isUserCancelledError() {}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ UserCancelledError = &UserCancelledBenchmarkError{}
)

type TokenRejectedByRemoteUserError struct{}

func (err *TokenRejectedByRemoteUserError) Error() string {
	return "token was rejected by remote user"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &TokenRejectedByRemoteUserError{}
)

type TokenRejectedByLauncherUserError struct{}

func (err *TokenRejectedByLauncherUserError) Error() string {
	return "token was rejected by launcher user"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &TokenRejectedByLauncherUserError{}
)

type TokenNotFoundError struct {
	err error
}

func (err *TokenNotFoundError) Error() string {
	return fmt.Sprintf("token was not found: %s", err.err.Error())
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &TokenNotFoundError{}
)

type NoSupportedBlenderVersionError struct{}

func (err *NoSupportedBlenderVersionError) Error() string {
	return "no supported Blender version"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &NoSupportedBlenderVersionError{}
)

type NoSupportedDevicesError struct{}

func (err *NoSupportedDevicesError) Error() string {
	return "no supported devices found"
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &NoSupportedDevicesError{}
)

type UnknownBlenderVersionError struct {
	UnknownLabel string
	ValidLabels  []string
}

func NewUnknownBlenderVersionError(unknownLabel string, blenderVersions []rpc.BlenderVersion) *UnknownBlenderVersionError {
	validLabels := make([]string, 0, len(blenderVersions))
	for _, blenderVersion := range blenderVersions {
		validLabels = append(validLabels, string(blenderVersion.Label))
	}
	return &UnknownBlenderVersionError{
		UnknownLabel: unknownLabel,
		ValidLabels:  validLabels,
	}
}

func (err *UnknownBlenderVersionError) Error() string {
	return fmt.Sprintf("unknown Blender version: %s, valid values: %v", err.UnknownLabel, err.ValidLabels)
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &UnknownBlenderVersionError{}
)

type UnknownSceneError struct {
	UnknownLabel string
	ValidLabels  []string
}

func NewUnknownSceneError(unknownLabel string, scenes []rpc.Scene) *UnknownSceneError {
	validLabels := make([]string, 0, len(scenes))
	for _, scene := range scenes {
		validLabels = append(validLabels, string(scene.Label))
	}
	return &UnknownSceneError{
		UnknownLabel: unknownLabel,
		ValidLabels:  validLabels,
	}
}

func (err *UnknownSceneError) Error() string {
	return fmt.Sprintf("unknown scene: %s, valid values: %v", err.UnknownLabel, err.ValidLabels)
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &UnknownSceneError{}
)

type UnknownDeviceType struct {
	UnknownLabel string
}

func (err *UnknownDeviceType) Error() string {
	return fmt.Sprintf("unknown device type: %s, valid valued: %v", err.UnknownLabel,
		[]string{string(core.CPUType), string(core.HIPType), string(core.OptiXType), string(core.CUDAType), string(core.MetalType), string(core.OneAPIType)})
}

//noinspection GoVarAndConstTypeMayBeOmitted
var (
	_ error = &UnknownDeviceType{}
)

func WithErrorLogger(function func(cmd *cobra.Command, args []string) error) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		err := function(cmd, args)
		if err != nil {
			switch v := err.(type) {
			case *UserCancelledDownloadError:
				core.DisplayLogger.Print("ERROR: The download was cancelled.")
			case *UserCancelledBenchmarkError:
				core.DisplayLogger.Print("ERROR: The benchmark was cancelled.")
			case *TokenRejectedByRemoteUserError:
				core.DisplayLogger.Print("ERROR: The token was rejected by the remote user.")
			case *TokenRejectedByLauncherUserError:
				core.DisplayLogger.Print("ERROR: The token was rejected.")
			case *TokenNotFoundError:
				core.DisplayLogger.Print("ERROR: No token found. Create one using the 'authenticate' command.")
			case *rpc.InvalidTokenError:
				core.DisplayLogger.Print("ERROR: The authentication token seems to be broken. Create a new one using the 'authenticate' command.")
			case *rpc.NonVerifiedTokenError:
				core.DisplayLogger.Print("ERROR: The authentication token was revoked. Create a new one using the 'authenticate' command.")
			case *NoSupportedBlenderVersionError:
				core.DisplayLogger.Print("ERROR: No supported Blender versions found for this operating system.")
			case *NoSupportedDevicesError:
				core.DisplayLogger.Print("ERROR: No benchmarkable devices found in this system.")
			case *UnknownBlenderVersionError:
				core.DisplayLogger.Printf("ERROR: Blender version not found: %s. Valid values are: %v", v.UnknownLabel, v.ValidLabels)
			case *UnknownSceneError:
				core.DisplayLogger.Printf("ERROR: Scene not found: %s. Valid values are: %v", v.UnknownLabel, v.ValidLabels)
			case *rpc.CommunicationError:
				core.DisplayLogger.Printf("ERROR: There was an error while communicating with the server.")
			case *rpc.DownloadError:
				core.DisplayLogger.Printf("ERROR: There was an error while downloading the requested files.")
			case *rpc.CurrentLauncherNotSupportedError:
				core.DisplayLogger.Printf("ERROR: The current launcher version is no longer supported.")
				if v.NewLauncherURL != nil {
					core.DisplayLogger.Printf("ERROR: Download the latest launcher at:\n%s", v.NewLauncherURL.String())
				}
			case *rpc.InvalidChecksumError:
				core.DisplayLogger.Printf("ERROR: One of the downloaded files was invalid.")
			case *rpc.BrokenBlenderVersionError:
				core.DisplayLogger.Printf("ERROR: Blender version %s seems to be broken. Consider running the 'clear_cache' command.", string(v.BlenderVersion.Label))
			case *rpc.BrokenBenchmarkScriptError:
				core.DisplayLogger.Printf("ERROR: Benchmark script version %s seems to be broken. Consider running the 'clear_cache' command.", string(v.BenchmarkScript.Label))
			case *rpc.BrokenSceneError:
				core.DisplayLogger.Printf("ERROR: Scene %s seems to be broken. Consider running the 'clear_cache' command.", string(v.Scene.Label))
			case rpc.BrokenCacheError:
				core.DisplayLogger.Print("ERROR: The cache seems to be broken. Consider running the 'clear_cache' command.")
			default:
				core.DisplayLogger.Print("ERROR: An unexpected error occurred. Run with '--verbosity 3' for detailed logs.")
			}
		}
		return err
	}
}
