package cli

import (
	"context"
	"fmt"
	"net/url"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/AlecAivazis/survey/v2"
	"github.com/cheggaaa/pb"
	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"
	"github.com/wzshiming/ctc"
)

var interactiveCommand = &cobra.Command{
	Use:   "interactive",
	Short: "Run the launcher in interactive mode (the default command)",
	RunE:  WithErrorLogger(runInteractive),
}

func runInteractive(cmd *cobra.Command, args []string) error {
	launcher, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}

	blenderVersion, err := selectBlenderVersion(blenderVersions)
	if err != nil {
		return err
	}

	selectedScenes, err := selectScenes(blenderVersion.AvailableScenes)
	if err != nil {
		return err
	}

	blenderExecutable, benchmarkScript, blendFiles, err := downloadAssets(*blenderVersion, selectedScenes)
	if err != nil {
		return err
	}

	device, err := selectDevice(*blenderExecutable, *benchmarkScript)
	if err != nil {
		return err
	}

	requestedDevice := core.RequestedDevice{
		Type: (*device).GetType(),
		Name: (*device).GetName(),
	}

	benchmark := false
	prompt := &survey.Confirm{
		Message: "Start benchmarking?",
		Default: true,
	}
	err = survey.AskOne(prompt, &benchmark, surveyAskOpt)
	if err != nil {
		return err
	}
	if !benchmark {
		return &UserCancelledBenchmarkError{}
	}

	benchmarkResults := make([]rpc.BenchmarkResult, 0, len(selectedScenes))

	for i, scene := range selectedScenes {
		core.DisplayLogger.Printf("Warming up %s", scene.Label)
		_, err := runBenchmark(*blenderExecutable, *benchmarkScript, requestedDevice, blendFiles[i], true)
		if err != nil {
			return err
		}

		core.DisplayLogger.Printf("Benchmarking %s", scene.Label)
		//noinspection ALL
		renderInfo, err := runBenchmark(*blenderExecutable, *benchmarkScript, requestedDevice, blendFiles[i], false)
		if err != nil {
			return err
		}

		benchmarkResults = append(benchmarkResults, rpc.BenchmarkResult{
			Launcher:       *launcher,
			BlenderVersion: *blenderVersion,
			Scene:          scene,
			RenderInfo:     *renderInfo,
		})
	}

	core.DisplayLogger.Print("Benchmark complete:")
	for _, result := range benchmarkResults {
		core.DisplayLogger.Printf("%s: %f samples per minute",
			string(result.Scene.Label),
			result.RenderInfo.Stats.SamplesPerMinute)
	}

	submit := false
	prompt = &survey.Confirm{
		Message: "Do you want to submit your results?",
		Default: true,
	}
	err = survey.AskOne(prompt, &submit, surveyAskOpt)
	if err != nil {
		return err
	}
	if submit {
		core.DisplayLogger.Print("Submitting results")
		benchmarkURL, err := submitResults(*launcher, benchmarkResults)
		if err != nil {
			return err
		}

		core.DisplayLogger.Printf("Submission successful! View your results at:\n%s", benchmarkURL.String())
	}

	return nil
}

func selectBlenderVersion(blenderVersions []rpc.BlenderVersion) (*rpc.BlenderVersion, error) {
	lookup := make(map[string]rpc.BlenderVersion)
	options := make([]string, 0, len(blenderVersions))
	for _, blenderVersion := range blenderVersions {
		options = append(options, string(blenderVersion.Label))
		lookup[string(blenderVersion.Label)] = blenderVersion
	}

	label := ""
	prompt := &survey.Select{
		Message: "Choose a Blender version:",
		Options: options,
	}
	err := survey.AskOne(prompt, &label, surveyAskOpt)
	if err != nil {
		return nil, err
	}

	selectedVersion := lookup[label]
	return &selectedVersion, nil
}

func selectScenes(scenes []rpc.Scene) ([]rpc.Scene, error) {
	scenes_str := ""
	for i, scene := range scenes {
		if i != 0 {
			scenes_str += ", "
		}
		scenes_str += string(scene.Label)
	}

	fmt.Print(ctc.ForegroundBrightGreen)
	fmt.Print("> ")
	fmt.Print(ctc.ForegroundBrightWhite)
	fmt.Print("Will render scenes: ")
	fmt.Print(ctc.ForegroundCyan)
	fmt.Print(scenes_str)
	fmt.Print(ctc.Reset)
	fmt.Println()

	return scenes, nil
}

func reportInteractiveDownloadProgress(ctx context.Context, progressBar *pb.ProgressBar, progressChannel chan rpc.DownloadProgress) {
	start := progressBar.Current()
	for progressChannel != nil {
		select {
		case <-ctx.Done():
			return
		case progress, ok := <-progressChannel:
			if ok {
				progressBar.SetCurrent(start + progress.CurrentBytes)
			} else {
				progressChannel = nil
			}
		}
	}
}

func downloadAssets(blenderVersion rpc.BlenderVersion, scenes []rpc.Scene) (*core.BlenderExecutable, *core.BenchmarkScript, []core.BlendFile, error) {
	totalDownloadSize := rpc.TotalDownloadSize(blenderVersion, scenes)

	var message string
	if totalDownloadSize == 0 {
		message = "No files need to be downloaded, continue?"
	} else {
		message = fmt.Sprintf("Download size will be %s, continue?", humanize.Bytes(uint64(totalDownloadSize)))
	}

	download := false
	prompt := &survey.Confirm{
		Message: message,
		Default: true,
	}
	err := survey.AskOne(prompt, &download, surveyAskOpt)
	if err != nil {
		return nil, nil, nil, err
	}
	if !download {
		return nil, nil, nil, &UserCancelledDownloadError{}
	}

	progressBar := pb.New(int(totalDownloadSize))
	if totalDownloadSize != 0 {
		progressBar.Start()
	}
	defer progressBar.Finish()

	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()

	progressChannel := make(chan rpc.DownloadProgress)
	go reportInteractiveDownloadProgress(ctx, progressBar, progressChannel)
	blenderExecutable, err := rpc.GetBlenderVersion(rpc.DefaultConfig, blenderVersion, progressChannel)
	if err != nil {
		return nil, nil, nil, err
	}

	progressChannel = make(chan rpc.DownloadProgress)
	go reportInteractiveDownloadProgress(ctx, progressBar, progressChannel)
	benchmarkScript, err := rpc.GetBenchmarkScript(rpc.DefaultConfig, blenderVersion.BenchmarkScript, progressChannel)
	if err != nil {
		return nil, nil, nil, err
	}

	var blendFiles []core.BlendFile
	for _, scene := range scenes {
		progressChannel := make(chan rpc.DownloadProgress)
		go reportInteractiveDownloadProgress(ctx, progressBar, progressChannel)
		blendFile, err := rpc.GetScene(rpc.DefaultConfig, scene, progressChannel)
		if err != nil {
			return nil, nil, nil, err
		}
		blendFiles = append(blendFiles, *blendFile)
	}

	return blenderExecutable, benchmarkScript, blendFiles, nil
}

func selectDevice(blenderExecutable core.BlenderExecutable, script core.BenchmarkScript) (*core.Device, error) {
	devices, err := core.ListDevices(blenderExecutable, script, context.Background())
	if err != nil {
		return nil, err
	}
	if len(devices) == 0 {
		return nil, &NoSupportedDevicesError{}
	}

	lookup := make(map[string]core.Device)
	options := make([]string, 0, len(devices))
	for _, device := range devices {
		label := fmt.Sprintf("%s", device.GetName())
		options = append(options, label)
		lookup[label] = device
	}

	label := ""
	prompt := &survey.Select{
		Message: "Choose a device:",
		Options: options,
	}
	err = survey.AskOne(prompt, &label, surveyAskOpt)
	if err != nil {
		return nil, err
	}

	selectedDevice := lookup[label]
	return &selectedDevice, nil
}

func submitResults(launcher rpc.Launcher, benchmarkResults []rpc.BenchmarkResult) (*url.URL, error) {
	var token *rpc.Token
	token, err := rpc.GetExistingToken()
	if err != nil {
		token, err = getNewToken()
		if err != nil {
			return nil, err
		}
	}

	return rpc.PostBenchmarkResults(rpc.DefaultConfig, *token, launcher, benchmarkResults)
}
