package cli

import (
	"git.blender.org/blender-open-data/launcher/internal/config"
	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/spf13/cobra"
)

var Command = &cobra.Command{
	Version: config.Version,
	Use:     "benchmark-launcher-cli",
	Short:   "The Blender Open Data Benchmark launcher command line interface",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		core.InitLogging()
	},
	RunE:          WithErrorLogger(runInteractive),
	SilenceUsage:  true,
	SilenceErrors: true,
}

func init() {
	var defaultVerbosity uint8 = 1
	if //noinspection GoBoolExpressions
	config.Debug {
		defaultVerbosity = 2
	}
	Command.PersistentFlags().Uint8VarP(&core.VerbosityFlag, "verbosity", "v", defaultVerbosity, "control verbosity (0 = no logs, 1 = error logs (default), 2 = debug logs, 3 = dump blender output)")
	Command.Flags().BoolVar(&browserFlag, "browser", false, "open the verification URL in the browser")

	Command.AddCommand(authenticateCommand)
	Command.AddCommand(blenderCommand)
	Command.AddCommand(scenesCommand)
	Command.AddCommand(devicesCommand)
	Command.AddCommand(benchmarkCommand)
	Command.AddCommand(clearCacheCommand)
	Command.AddCommand(interactiveCommand)
}

func Execute() error {
	return Command.Execute()
}
