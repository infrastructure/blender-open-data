package cli

import (
	"context"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/spf13/cobra"
)

var blenderCommand = &cobra.Command{
	Use:   "blender",
	Short: "Commands for managing Blender versions",
}

func init() {
	blenderCommand.AddCommand(blenderListCommand)
	blenderCommand.AddCommand(blenderDownloadCommand)
}

var blenderDownloadCommand = &cobra.Command{
	Use:   "download [blender version, ...]",
	Short: "Download Blender versions",
	RunE:  WithErrorLogger(runBlenderDownload),
	Args:  cobra.MinimumNArgs(1),
}

func runBlenderDownload(cmd *cobra.Command, args []string) error {
	_, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}

	blenderVersionLookup := make(map[string]rpc.BlenderVersion)
	for _, blenderVersion := range blenderVersions {
		blenderVersionLookup[string(blenderVersion.Label)] = blenderVersion
	}

	blenderVersionsToDownload := make([]rpc.BlenderVersion, 0, len(args))
	var totalDownloadSize int64 = 0

	for _, blenderLabel := range args {
		blenderVersion, ok := blenderVersionLookup[blenderLabel]
		if !ok {
			return NewUnknownBlenderVersionError(blenderVersionFlag, blenderVersions)
		}
		totalDownloadSize += int64(blenderVersion.Size)
		blenderVersionsToDownload = append(blenderVersionsToDownload, blenderVersion)
	}

	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()

	for _, blenderVersion := range blenderVersionsToDownload {
		_, err := rpc.FindValidBlenderExecutable(blenderVersion)
		if err == nil {
			core.DisplayLogger.Printf("Blender version already available locally: %s", blenderVersion.Label)
		} else {
			core.DisplayLogger.Printf("Downloading Blender version %s", blenderVersion.Label)
		}

		progressChannel := make(chan rpc.DownloadProgress)
		go reportDownloadProgress(ctx, blenderVersion.Size, progressChannel)
		_, err = rpc.GetBlenderVersion(rpc.DefaultConfig, blenderVersion, progressChannel)
		if err != nil {
			return err
		}

		progressChannel = make(chan rpc.DownloadProgress)
		go reportDownloadProgress(ctx, blenderVersion.Size, progressChannel)
		_, err = rpc.GetBenchmarkScript(rpc.DefaultConfig, blenderVersion.BenchmarkScript, progressChannel)
		if err != nil {
			return err
		}
	}
	return nil
}

var blenderListCommand = &cobra.Command{
	Use:   "list",
	Short: "List downloadable Blender versions",
	RunE:  WithErrorLogger(runBlenderList),
}

func runBlenderList(cmd *cobra.Command, args []string) error {
	_, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}

	for _, blenderVersion := range blenderVersions {
		core.OutputLogger.Printf("%s\t%d", blenderVersion.Label, rpc.DownloadSizeBlenderVersion(blenderVersion))
	}

	return nil
}
