package cli

import (
	"context"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/spf13/cobra"
)

var devicesCommand = &cobra.Command{
	Use:   "devices",
	Short: "List the devices which can be benchmarked",
	RunE:  WithErrorLogger(runDevices),
}

func init() {
	devicesCommand.Flags().StringVarP(&blenderVersionFlag, "blender-version", "b", "", "the Blender version to show devices for")
	err := devicesCommand.MarkFlagRequired("blender-version")
	if err != nil {
		panic(err)
	}
}

func runDevices(cmd *cobra.Command, args []string) error {
	_, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}
	blender, err := findBlenderVersion(blenderVersionFlag, blenderVersions)
	if err != nil {
		return err
	}

	blenderExecutable, err := rpc.FindValidBlenderExecutable(*blender)
	if err != nil {
		return err
	}
	benchmarkScript, err := rpc.FindValidBenchmarkScript(blender.BenchmarkScript)
	if err != nil {
		return err
	}

	devices, err := core.ListDevices(*blenderExecutable, *benchmarkScript, context.Background())
	if err != nil {
		return err
	}

	for _, device := range devices {
		core.OutputLogger.Printf("%s\t%s", device.GetName(), string(device.GetType()))
	}
	return nil
}
