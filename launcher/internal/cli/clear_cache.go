package cli

import (
	"os"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"github.com/spf13/cobra"
)

var clearCacheCommand = &cobra.Command{
	Use:   "clear_cache",
	Short: "Removes all downloaded assets",
	RunE:  WithErrorLogger(runClearCache),
}

func runClearCache(cmd *cobra.Command, args []string) error {
	cacheDir, err := core.CacheDirectory()
	if err != nil {
		return err
	}
	err = os.RemoveAll(cacheDir)
	if err != nil {
		return err
	}
	configDir, err := core.ConfigDirectory()
	if err != nil {
		return err
	}
	err = os.RemoveAll(configDir)
	if err != nil {
		return err
	}

	return nil
}
