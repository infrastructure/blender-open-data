package cli

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/cheggaaa/pb"
	"github.com/spf13/cobra"
)

var benchmarkCommand = &cobra.Command{
	Use:   "benchmark [scenes, ...]",
	Short: "Benchmark scenes",
	RunE:  WithErrorLogger(runBenchmarkCommand),
	Args:  cobra.MinimumNArgs(1),
}

var deviceNameFlag string
var deviceTypeFlag string
var submitFlag bool
var jsonFlag bool

func init() {
	benchmarkCommand.Flags().StringVarP(&blenderVersionFlag, "blender-version", "b", "", "the Blender version to benchmark")
	err := benchmarkCommand.MarkFlagRequired("blender-version")
	if err != nil {
		panic(err)
	}
	benchmarkCommand.Flags().StringVar(&deviceNameFlag, "device-name", "", "the name of the device to benchmark")
	benchmarkCommand.Flags().StringVar(&deviceTypeFlag, "device-type", "", "the type of the device to benchmark")
	benchmarkCommand.Flags().BoolVar(&submitFlag, "submit", false, "submit the benchmark to the Blender Open Data website")
	benchmarkCommand.Flags().BoolVar(&jsonFlag, "json", false, "dump json output to stdout")
}

func runBenchmarkCommand(cmd *cobra.Command, args []string) error {
	launcher, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}

	blender, err := findBlenderVersion(blenderVersionFlag, blenderVersions)
	if err != nil {
		return err
	}

	blenderExecutable, err := rpc.FindValidBlenderExecutable(*blender)
	if err != nil {
		return err
	}
	benchmarkScript, err := rpc.FindValidBenchmarkScript(blender.BenchmarkScript)
	if err != nil {
		return err
	}

	sceneLookup := make(map[string]rpc.Scene)

	for _, scene := range blender.AvailableScenes {
		sceneLookup[string(scene.Label)] = scene
	}

	scenesToRun := make([]rpc.Scene, 0, len(args))
	blendFiles := make(map[rpc.SceneLabel]core.BlendFile)

	for _, sceneLabel := range args {
		scene, ok := sceneLookup[sceneLabel]
		if !ok {
			return NewUnknownSceneError(sceneLabel, blender.AvailableScenes)
		}
		scenesToRun = append(scenesToRun, scene)

		blendFile, err := rpc.FindValidBlendFile(scene)
		if err != nil {
			return err
		}

		blendFiles[scene.Label] = *blendFile
	}

	var token *rpc.Token = nil
	if submitFlag {
		token, err = rpc.GetExistingToken()
		if err != nil {
			return &TokenNotFoundError{err: err}
		}
	}

	var requestedDeviceType core.DeviceType
	switch deviceTypeFlag {
	case string(core.CPUType):
		requestedDeviceType = core.CPUType
	case string(core.HIPType):
		requestedDeviceType = core.HIPType
	case string(core.CUDAType):
		requestedDeviceType = core.CUDAType
	case string(core.OptiXType):
		requestedDeviceType = core.OptiXType
	case string(core.MetalType):
		requestedDeviceType = core.MetalType
	case string(core.OneAPIType):
		requestedDeviceType = core.OneAPIType
	case "":
		requestedDeviceType = core.NotSpecifiedType
	default:
		return &UnknownDeviceType{UnknownLabel: deviceTypeFlag}
	}

	requestedDevice := core.RequestedDevice{
		Type: requestedDeviceType,
		Name: deviceNameFlag,
	}

	benchmarkResults := make([]rpc.BenchmarkResult, 0, len(scenesToRun))

	for _, scene := range scenesToRun {
		core.DisplayLogger.Printf("Warming up %s", scene.Label)
		_, err := runBenchmark(*blenderExecutable, *benchmarkScript, requestedDevice, blendFiles[scene.Label], true)
		if err != nil {
			return err
		}

		core.DisplayLogger.Printf("Benchmarking %s", scene.Label)
		//noinspection ALL
		renderInfo, err := runBenchmark(*blenderExecutable, *benchmarkScript, requestedDevice, blendFiles[scene.Label], false)
		if err != nil {
			return err
		}

		benchmarkResults = append(benchmarkResults, rpc.BenchmarkResult{
			Launcher:       *launcher,
			BlenderVersion: *blender,
			Scene:          scene,
			RenderInfo:     *renderInfo,
		})
	}

	if jsonFlag {
		jsonResults, err := json.MarshalIndent(benchmarkResults, "", "  ")
		if err != nil {
			return err
		}
		core.OutputLogger.Printf("%s", jsonResults)
	}

	if submitFlag {
		core.DisplayLogger.Print("Submitting results")
		benchmarkURL, err := rpc.PostBenchmarkResults(rpc.DefaultConfig, *token, *launcher, benchmarkResults)
		if err != nil {
			return err
		}
		core.DisplayLogger.Printf("Submission successful! View your results at:\n%s", benchmarkURL.String())
	}

	return nil
}

func runBenchmark(
	blenderExecutable core.BlenderExecutable,
	benchmarkScript core.BenchmarkScript,
	requestedDevice core.RequestedDevice,
	blendFile core.BlendFile,
	warmUp core.WarmUp,
) (*core.RenderInfo, error) {
	progressBar := pb.New(0)
	defer progressBar.Finish()

	progressChannel := make(chan core.Progress, 100)

	var progressWaitGroup sync.WaitGroup
	progressWaitGroup.Add(1)

	go func() {
		defer progressWaitGroup.Done()

		did_start_rendering := false
		render_start_time := time.Now()
		time_limit := 0.0

		for progress := range progressChannel {
			if !progress.IsHeartbeat {
				if !did_start_rendering && progress.TimeLimit != 0 {
					did_start_rendering = true
					render_start_time = time.Now()

					time_limit = progress.TimeLimit

					progressBar.Start()
					progressBar.SetCurrent(0)
					progressBar.SetTotal(100)
				}
			}

			if did_start_rendering {
				render_duration := time.Since(render_start_time).Seconds()
				render_progress := render_duration / time_limit * 100
				progressBar.SetCurrent(int64(render_progress))
			}
		}
	}()

	renderInfo, err := core.RunBenchmark(
		blenderExecutable,
		benchmarkScript,
		requestedDevice,
		blendFile,
		warmUp,
		context.Background(),
		progressChannel,
	)

	close(progressChannel)

	progressWaitGroup.Wait()

	// Blender tends to exit with segfaults, so if the RenderInfo is available
	// we consider the run a success.
	if renderInfo != nil {
		return renderInfo, nil
	} else {
		return nil, err
	}
}
