package cli

import (
	"context"

	"git.blender.org/blender-open-data/launcher/internal/core"
	"git.blender.org/blender-open-data/launcher/internal/rpc"
	"github.com/spf13/cobra"
)

var scenesCommand = &cobra.Command{
	Use:   "scenes",
	Short: "Commands for managing scenes",
}

func init() {
	scenesCommand.PersistentFlags().StringVarP(&blenderVersionFlag, "blender-version", "b", "", "the Blender version to manage scenes for")
	err := scenesCommand.MarkPersistentFlagRequired("blender-version")
	if err != nil {
		panic(err)
	}
	scenesCommand.AddCommand(scenesListCommand)
	scenesCommand.AddCommand(scenesDownloadCommand)
}

var scenesDownloadCommand = &cobra.Command{
	Use:   "download [scenes, ...]",
	Short: "Download scenes",
	RunE:  WithErrorLogger(runScenesDownload),
	Args:  cobra.MinimumNArgs(1),
}

func runScenesDownload(cmd *cobra.Command, args []string) error {
	_, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}
	blender, err := findBlenderVersion(blenderVersionFlag, blenderVersions)
	if err != nil {
		return err
	}

	sceneLookup := make(map[string]rpc.Scene)

	for _, scene := range blender.AvailableScenes {
		sceneLookup[string(scene.Label)] = scene
	}

	scenesToDownload := make([]rpc.Scene, 0, len(args))
	var totalDownloadSize int64 = 0

	for _, sceneLabel := range args {
		scene, ok := sceneLookup[sceneLabel]
		if !ok {
			return NewUnknownSceneError(sceneLabel, blender.AvailableScenes)
		}
		totalDownloadSize += int64(scene.Size)
		scenesToDownload = append(scenesToDownload, scene)
	}

	ctx, stopProgress := context.WithCancel(context.Background())
	defer stopProgress()

	for _, scene := range scenesToDownload {
		_, err := rpc.FindValidBlendFile(scene)
		if err == nil {
			core.DisplayLogger.Printf("Scene already available locally: %s", scene.Label)
		} else {
			core.DisplayLogger.Printf("Downloading scene %s", scene.Label)
		}

		progressChannel := make(chan rpc.DownloadProgress)
		go reportDownloadProgress(ctx, scene.Size, progressChannel)
		_, err = rpc.GetScene(rpc.DefaultConfig, scene, progressChannel)
		if err != nil {
			return err
		}
	}

	return nil
}

var scenesListCommand = &cobra.Command{
	Use:   "list",
	Short: "List downloadable scenes",
	RunE:  WithErrorLogger(runScenesList),
}

func runScenesList(cmd *cobra.Command, args []string) error {
	_, blenderVersions, err := getMetaData()
	if err != nil {
		return err
	}
	blender, err := findBlenderVersion(blenderVersionFlag, blenderVersions)
	if err != nil {
		return err
	}

	for _, scene := range blender.AvailableScenes {
		core.OutputLogger.Printf("%s\t%d", scene.Label, rpc.DownloadSizeScene(scene))
	}
	return nil
}
