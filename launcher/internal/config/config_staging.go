//go:build staging
// +build staging

package config

var Version = "0.0.0-staging"
var SentryDSN = ""

const Debug = false

const opendataHomeURL = "https://staging.opendata.blender.org/"
const aboutURL = "https://staging.opendata.blender.org/about/"
const accountBenchmarksURL = "https://staging.opendata.blender.org/users/benchmarks/"
const metadataURL = "https://staging.opendata.blender.org/benchmarks/metadata/"
const submitURL = "https://staging.opendata.blender.org/benchmarks/submit/"
const authenticationURL = "wss://staging.opendata.blender.org/launcher_authenticator/"
const logsURL = "https://staging.opendata.blender.org/benchmarks/logs/"
