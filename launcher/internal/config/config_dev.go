//go:build dev
// +build dev

package config

var Version = "0.0.0-dev"
var SentryDSN = ""

const Debug = true

const opendataHomeURL = "http://opendata.local:8002/"
const aboutURL = "http://opendata.local:8002/about/"
const accountBenchmarksURL = "http://opendata.local:8002/users/benchmarks/"
const metadataURL = "http://opendata.local:8002/benchmarks/metadata/"
const authenticationURL = "ws://opendata.local:8005/"
const submitURL = "http://opendata.local:8002/benchmarks/submit/"
const logsURL = "http://opendata.local:8002/benchmarks/logs/"
