package config

import "net/url"

var CLI = false
var OpendataHomeURL = parseURL(opendataHomeURL)
var AboutURL = parseURL(aboutURL)
var AccountBenchmarksURL = parseURL(accountBenchmarksURL)
var MetadataURL = parseURL(metadataURL)
var AuthenticationURL = parseURL(authenticationURL)
var SubmitURL = parseURL(submitURL)
var LogsURL = parseURL(logsURL)

func parseURL(URL string) url.URL {
	parsedURL, err := url.Parse(URL)
	if err != nil {
		panic(err)
	}
	return *parsedURL
}
