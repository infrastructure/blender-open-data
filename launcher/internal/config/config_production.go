//go:build !dev && !staging
// +build !dev,!staging

package config

var Version = "0.0.0"
var SentryDSN = ""

const Debug = false

const opendataHomeURL = "https://opendata.blender.org/"
const aboutURL = "https://opendata.blender.org/about/"
const accountBenchmarksURL = "https://opendata.blender.org/users/benchmarks/"
const metadataURL = "https://opendata.blender.org/benchmarks/metadata/"
const submitURL = "https://opendata.blender.org/benchmarks/submit/"
const authenticationURL = "wss://opendata.blender.org/launcher_authenticator/"
const logsURL = "https://opendata.blender.org/benchmarks/logs/"
