# Building

## Linux
  - Install `build-essential`, `automake`, `pkg-config` and `libtool`: `apt install build-essential automake pkg-config libtool`,
  - Install [Go](https://golang.org/dl/),
  - Continue with the common build steps.

## Windows
  - Get [MSYS2](https://www.msys2.org/),
  - Update MSYS2: `pacman -Syu`,
  - Install dependencies in MSYS2 (repeat until all packages are up-to-date): `pacman -S --needed base-devel mingw-w64-x86_64-toolchain git zip msys/autoconf msys/libtool msys/automake-wrapper`,
  - Install [Go](https://golang.org/dl/),
  - Inside MSYS2 add mingw64 and Go to `PATH`: `echo 'export PATH=/mingw64/bin:/c/Go/bin:$PATH' >> ~/.bashrc`,
  - Reload the MSYS2 shell to update the `PATH`.
  - Continue with the common build steps.

## macOS
  - Install [Homebrew](https://www.brew.sh/),
  - Install `go`, `automake`, `pkg-config`, `libtool` and `gettext`: `brew install go automake pkg-config libtool gettext`,
  - Continue with the common build steps.

## Common
Copy and adjust `Makeenv.example` to `Makeenv` and build by running `make` in
`/launcher`.
