# Launcher development set up
Before proceeding make sure you read [`DESIGN.md`](DESIGN.md).

## macOS requirements
* `brew install go`
* `brew install autogen`
* `brew install automake`

Make sure the freetype2 submodule is initialized.

Make sure you have [`golangci-lint`](https://github.com/golangci/golangci-lint)
available in `/launcher/test/bin`:
```
cd /launcher
mkdir -p test/bin
curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b test/bin v1.44.1
```
Then start a shell in `/launcher` and:
 1. Copy and adjust `Makeenv.example` to `Makeenv`.
 2. Ensure `./test.sh` succeeds.
 3. `make DEBUG=true` or `make STAGING=true`.
 4. Ensure the launchers in `bin/` work as intended.
