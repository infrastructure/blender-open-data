tell application "Finder"
          tell disk "Benchmark"
               log "applescript: opening [Benchmark]. This will seem to hang with a pop up dialog on applescript permissions for the first run. You have 10 minutes, get on machine now and push that button !!!"
               with timeout of 600 seconds
                    open
                    log "applescript: yay it opened !"
                    log "applescript: setting current view"
                    set current view of container window to icon view
                    set toolbar visible of container window to false
                    set statusbar visible of container window to false
                    set the bounds of container window to {100, 100, 640, 472}
                    set theViewOptions to icon view options of container window
                    set arrangement of theViewOptions to not arranged
                    set icon size of theViewOptions to 128
                    set background picture of theViewOptions to file ".background:background.tif"
                    set position of item " " of container window to {400, 190}
                    set position of item "Blender Benchmark Launcher.app" of container window to {135, 190}
                    log "applescript: updating applications"
                    update without registering applications
                    delay 5
                    log "applescript: closing"
                    close
               end timeout
     end tell
end tell
