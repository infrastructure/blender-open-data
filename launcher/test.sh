#!/usr/bin/env sh
set -eux

cd "$(dirname $0)"
test/bin/golangci-lint run --timeout 4h --enable bodyclose --enable gofmt --enable gosec
