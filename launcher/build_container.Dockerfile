FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -yq update

RUN apt-get -yq install locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8

RUN apt-get -yq install build-essential
RUN apt-get -yq install pkg-config
RUN apt-get -yq install git
RUN apt-get -yq install curl
RUN apt-get -yq install automake libtool
RUN apt-get -yq install \
    libx11-dev \
    libxcursor-dev \
    libxi-dev \
    libxinerama-dev \
    libxrandr-dev \
    libgl1-mesa-dev

WORKDIR /tmp
RUN curl https://dl.google.com/go/go1.13.5.linux-amd64.tar.gz > go.tar.gz
RUN tar -C /usr/local -xzf go.tar.gz
ENV PATH "$PATH:/usr/local/go/bin"

ARG UID
ARG USERNAME
RUN useradd --create-home --uid ${UID:?} ${USERNAME:?}

VOLUME /repo
WORKDIR /repo/launcher
