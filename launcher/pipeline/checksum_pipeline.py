#!/usr/bin/env python3

import argparse
import hashlib
import platform
import subprocess
import tarfile
import tempfile
import zipfile
from pathlib import Path


def sha256_file(filename: Path) -> str:
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()


def checksum_unpacked_dir(archive: Path, dir: Path):
    for filename in Path(dir).iterdir():
        if not filename.name.startswith("benchmark-launcher"):
            continue
        hash = sha256_file(filename)
        size = filename.stat().st_size
        print(f"{archive.name} {hash} {size}")


def checksum_zip(archive: Path):
    with tempfile.TemporaryDirectory() as tmp_dir_str:
        tmp_dir = Path(tmp_dir_str)

        with zipfile.ZipFile(archive, "r") as zip_file:
            zip_file.extractall(path=tmp_dir)

        checksum_unpacked_dir(archive, tmp_dir)


def checksum_zip(archive: Path):
    with tempfile.TemporaryDirectory() as tmp_dir_str:
        tmp_dir = Path(tmp_dir_str)

        with zipfile.ZipFile(archive, "r") as zip_file:
            zip_file.extractall(path=tmp_dir)

        checksum_unpacked_dir(archive, tmp_dir)


def checksum_tar(archive: Path):
    with tempfile.TemporaryDirectory() as tmp_dir_str:
        tmp_dir = Path(tmp_dir_str)

        with tarfile.open(archive, "r") as tar_file:
            tar_file.extractall(path=tmp_dir)

        checksum_unpacked_dir(archive, tmp_dir)


def find_and_checksum_app_bundles_binaries(archive: Path, bundle: Path):
    checksum_unpacked_dir(archive, bundle / "Contents" / "MacOS")


def find_and_checksum_app_bundles(archive: Path, mount: Path):
    for filename in mount.iterdir():
        if filename.name.startswith("."):
            continue
        if filename.suffix != ".app":
            continue
        find_and_checksum_app_bundles_binaries(archive, filename)


def checksum_dmg(archive: Path):
    if platform.system() != "Darwin":
        raise Exception("Unfortunately, DMG can only be handled on mac")

    with tempfile.TemporaryDirectory() as tmp_dir_str:
        tmp_dir = Path(tmp_dir_str)

        output = subprocess.check_output(("hdiutil", "attach", archive))

        disks = []
        mounts = []
        for line in output.decode().split("\n"):
            clean_line = line.strip()
            parts = clean_line.split()
            if len(parts) != 3:
                continue
            if not parts[2].startswith("/Volumes"):
                continue

            disks.append(parts[0])
            mounts.append(parts[2])

        for mount in mounts:
            find_and_checksum_app_bundles(archive, Path(mount))

        for disk in disks:
            subprocess.check_output(("hdiutil", "detach", disk))


################################################################################
# Entry point.


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="OpenData Launcher Codesign Pipeline", add_help=False
    )

    parser.add_argument("dist_dir", type=Path, help="Directory with archives")

    return parser


def run_pipeline():
    parser = create_parser()
    args = parser.parse_args()

    for filename in args.dist_dir.iterdir():
        if not filename.is_file():
            continue

        if filename.suffix == ".zip":
            checksum_zip(filename)
        elif filename.name.endswith(".tar.gz"):
            checksum_tar(filename)
        elif filename.suffix == ".dmg":
            checksum_dmg(filename)


if __name__ == "__main__":
    run_pipeline()
