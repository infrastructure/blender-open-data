//go:build dev
// +build dev

package assets

import (
	"net/http"
	"os"
	"path/filepath"
	"runtime"

	"github.com/shurcooL/httpfs/filter"
)

func currentFilePath() string {
	_, file, _, ok := runtime.Caller(0)
	if !ok {
		panic("Could not find current file path.")
	}
	return file
}

var Assets = filter.Keep(http.Dir(filepath.Dir(currentFilePath())), func(path string, fi os.FileInfo) bool {
	if path == "/" || path == "/Heebo-Regular.ttf" || path == "/background.png" || path == "/progress.png" {
		return true
	} else {
		return false
	}
})
