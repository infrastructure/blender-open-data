# Blender Open Data

Blender Open Data is a platform to collect and display the results of hardware 
and software performance tests. In the future, analytics, telemetry and more 
might also be made available. Currently, the platform consists of four parts:

  - `/website`: the portal for viewing, querying, and managing data samples.
  - `/launcher`: the benchmark launcher for downloading Blender versions and 
    scenes and coordinating benchmarks against them.
  - `/launcher_authenticator`: a websocket service that handles authentication
    of the benchmark launcher using tokens.
  - `/benchmark_script`: the Python script that is run within Blender and 
    gathers system information and runs benchmarks.

For information on how these parts work internally see:
  - [`/launcher/DESIGN.md`](launcher/DESIGN.md)
  - [`/launcher_authenticator/DESIGN.md`](launcher_authenticator/DESIGN.md)

For information on how these parts are tied together see 
[`ARCHITECTURE.md`](ARCHITECTURE.md).
