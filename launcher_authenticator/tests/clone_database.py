import abc
import unittest
from contextlib import closing

import psycopg2
import psycopg2.errors
from psycopg2 import sql
from psycopg2.errorcodes import DUPLICATE_DATABASE, OBJECT_IN_USE

from tests.config import (
    TestConfig,
    TestDatabaseAlreadyExistsStrategy,
    UnclosedConnectionsToTestDatabaseStrategy,
)


class CloneDatabaseMixin(unittest.TestCase, abc.ABC):
    test_config: TestConfig

    def _create_database(self, cursor: psycopg2._psycopg.cursor) -> None:
        database_to_clone = self.test_config.database_to_clone
        postgres = self.test_config.config.postgres

        cursor.execute(
            sql.SQL('CREATE DATABASE {database} TEMPLATE {database_to_clone};').format(
                database=sql.Identifier(postgres.database),
                database_to_clone=sql.Identifier(database_to_clone),
            )
        )

    def _drop_database(self, cursor: psycopg2._psycopg.cursor) -> None:
        postgres = self.test_config.config.postgres

        cursor.execute(
            sql.SQL('DROP DATABASE {database};').format(database=sql.Identifier(postgres.database))
        )

    def _kill_connections(self, cursor: psycopg2._psycopg.cursor) -> None:
        postgres = self.test_config.config.postgres

        cursor.execute(
            sql.SQL(
                '''
                SELECT pg_terminate_backend(pg_stat_activity.pid)
                FROM pg_stat_activity
                WHERE pg_stat_activity.datname = %(database)s
                  AND pid <> pg_backend_pid();
                '''
            ),
            {'database': postgres.database},
        )

    def setUp(self) -> None:
        super().setUp()
        postgres = self.test_config.config.postgres

        with closing(
            psycopg2.connect(
                host=postgres.host,
                port=postgres.port,
                user=postgres.user,
                password=postgres.password,
            )
        ) as connection, connection.cursor() as cursor:
            connection.autocommit = True
            try:
                self._create_database(cursor)
            except psycopg2.errors.lookup(DUPLICATE_DATABASE):
                if (
                    self.test_config.test_database_already_exists_strategy
                    == TestDatabaseAlreadyExistsStrategy.ask
                ):
                    drop = input(
                        f'Database {postgres.database} already exists,'
                        f' type "drop" to DROP it:\n'
                    )

                    if drop == 'drop':
                        self._drop_database(cursor)
                        self._create_database(cursor)
                    else:
                        raise
                elif (
                    self.test_config.test_database_already_exists_strategy
                    == TestDatabaseAlreadyExistsStrategy.drop
                ):
                    self._drop_database(cursor)
                    self._create_database(cursor)
                else:
                    raise

    def tearDown(self) -> None:
        postgres = self.test_config.config.postgres

        with closing(
            psycopg2.connect(
                host=postgres.host,
                port=postgres.port,
                user=postgres.user,
                password=postgres.password,
            )
        ) as connection, connection.cursor() as cursor:
            connection.autocommit = True
            try:
                self._drop_database(cursor)
            except psycopg2.errors.lookup(OBJECT_IN_USE) as e:
                if (
                    self.test_config.unclosed_connections_to_test_database_strategy
                    == UnclosedConnectionsToTestDatabaseStrategy.ask
                ):
                    kill = input(
                        f'There are unclosed connections to {postgres.database},'
                        f' type "kill" to KILL them:\n'
                    )

                    if kill == 'kill':
                        self._kill_connections(cursor)
                        self._drop_database(cursor)
                    else:
                        raise
                elif (
                    self.test_config.unclosed_connections_to_test_database_strategy
                    == UnclosedConnectionsToTestDatabaseStrategy.kill
                ):
                    self._kill_connections(cursor)
                    self._drop_database(cursor)
                else:
                    raise

                raise Exception(
                    'There were still connections open to the test database, '
                    'make sure you close your connections!'
                ) from e

        super().tearDown()
