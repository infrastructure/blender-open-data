import aiopg

from authenticator.errors import SQLError
from authenticator.parsing import parse_field
from authenticator.sql import fetch_one


async def create_user(cursor: aiopg.Cursor, username: str, email: str) -> int:
    async with cursor.begin_nested():
        await cursor.execute(
            '''
            INSERT INTO auth_user (
                password,
                last_login,
                is_superuser,
                username,
                first_name,
                last_name,
                email,
                is_staff,
                is_active,
                date_joined
            )
            VALUES (
                'blabla',
                '2019-11-08 13:08:51.870575',
                false,
                %(username)s,
                '',
                '',
                %(email)s,
                false,
                true,
                '2019-11-08 13:08:51.859999'
            )
            RETURNING auth_user.id;
            ''',
            {'username': username, 'email': email},
        )

        sql_dict = await fetch_one(cursor)

        try:
            return parse_field(sql_dict, 'id', int)
        except Exception as e:
            raise SQLError(query=cursor.query, message='could not parse row') from e
