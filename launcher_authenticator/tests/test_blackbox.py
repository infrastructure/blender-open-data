import asyncio
import dataclasses as dc
import json
import signal
import socket
import unittest
import uuid
from asyncio import Future

import aiopg
import websockets
from psycopg2 import sql
from yarl import URL

from authenticator.config import PostgresConfig
from authenticator.listener import Listener
from authenticator.loggers import get_logger
from authenticator.server import Server
from authenticator.sql import get_pool
from authenticator.state import State
from config import test_config
from tests.clone_database import CloneDatabaseMixin
from tests.common import create_user


class BlackBoxTest(CloneDatabaseMixin, unittest.TestCase):
    test_config = test_config

    def setUp(self) -> None:
        super().setUp()

        async def _create_user(postgres: PostgresConfig) -> int:
            async with aiopg.connect(
                host=postgres.host,
                port=postgres.port,
                database=postgres.database,
                user=postgres.user,
                password=postgres.password,
            ) as connection, connection.cursor() as cursor:
                return await create_user(cursor, username='test', email='test@test.local')

        self.user_id: int = asyncio.run(_create_user(self.test_config.config.postgres))

    def test_blackbox(self) -> None:
        config = dc.replace(
            self.test_config.config,
            verification_url_function=lambda t: URL('http://test.local/verify').with_query(
                {'id': str(t.id)}
            ),
        )

        async def run() -> None:
            loop = asyncio.get_running_loop()
            server_socket: Future[socket.socket] = loop.create_future()
            stop: Future[None] = loop.create_future()

            async def server() -> None:
                loop = asyncio.get_running_loop()

                logger = get_logger(config).bind(service='launcher_authenticator')
                state = State()

                async with get_pool(config) as pool:
                    listener = Listener(config, logger, state, pool)
                    loop.add_signal_handler(signal.SIGTERM, listener.stop, None)

                    server = Server(config, logger, state, pool)
                    loop.add_signal_handler(signal.SIGTERM, server.stop, None)

                    async def sockets_waiter() -> None:
                        server_socket.set_result((await server.sockets)[0])
                        return

                    async def stop_waiter() -> None:
                        await stop
                        listener.stop()
                        server.stop()
                        return

                    await asyncio.gather(
                        server.run(), listener.run(), sockets_waiter(), stop_waiter()
                    )

                return

            async def launcher(verify: bool) -> None:
                hostname = 'test_hostname'

                socket = await server_socket
                host, port = socket.getsockname()
                url = URL.build(scheme='ws', host=host, port=port)
                async with websockets.connect(str(url)) as websocket:
                    await asyncio.wait_for(
                        websocket.send(
                            json.dumps(
                                {
                                    'message_type': 'authentication_request',
                                    'message': {'hostname': hostname},
                                }
                            )
                        ),
                        timeout=1,
                    )

                    response = json.loads(await asyncio.wait_for(websocket.recv(), timeout=1))
                    assert response['message_type'] == 'token_response'
                    verification_url = URL(response['message']['verification_url'])
                    assert verification_url.scheme == 'http'
                    assert verification_url.host == 'test.local'
                    assert verification_url.path == '/verify'
                    token_id = uuid.UUID(verification_url.query['id'])
                    token_uuid = uuid.UUID(hex=response['message']['token'])
                    print(f'{token_uuid=}')

                    async with aiopg.connect(
                        host=config.postgres.host,
                        port=config.postgres.port,
                        database=config.postgres.database,
                        user=config.postgres.user,
                        password=config.postgres.password,
                    ) as connection, connection.cursor() as cursor:
                        await cursor.execute(
                            '''
                            UPDATE
                                opendata_main_launcherauthenticationtoken
                            SET
                                verified = %(verified)s,
                                user_id = %(user_id)s
                            WHERE
                                opendata_main_launcherauthenticationtoken.id = %(id)s
                            ;
                            ''',
                            {'id': token_id, 'user_id': self.user_id, 'verified': verify},
                        )
                        await cursor.execute(
                            sql.SQL(
                                '''
                                NOTIFY
                                    {channel},
                                    %(payload)s
                                ;
                                '''
                            ).format(channel=sql.Identifier(config.notification_channel)),
                            {'payload': str(token_id)},
                        )

                    if verify:
                        response = json.loads(await asyncio.wait_for(websocket.recv(), timeout=1))
                        assert response['message_type'] == 'verified_response'
                        assert response['message'] == {
                            'user_id': self.user_id,
                            'username': 'test',
                            'email': 'test@test.local',
                        }
                    else:
                        response = json.loads(await asyncio.wait_for(websocket.recv(), timeout=1))
                        assert response['message_type'] == 'rejected_response'
                        assert response['message'] == {}
                return

            async def launchers() -> None:
                try:
                    await launcher(verify=True)
                    await launcher(verify=False)
                finally:
                    stop.set_result(None)

            await asyncio.gather(server(), launchers())

        asyncio.run(run())
