import dataclasses as dc
import enum

from authenticator.config import Config


class TestDatabaseAlreadyExistsStrategy(enum.Enum):
    ask = 'ask'  # Note that you need to run `nosetests` with `--nocapture` for this to work.
    drop = 'drop'


class UnclosedConnectionsToTestDatabaseStrategy(enum.Enum):
    ask = 'ask'  # Note that you need to run `nosetests` with `--nocapture` for this to work.
    kill = 'kill'


@dc.dataclass
class TestConfig:
    config: Config
    database_to_clone: str
    test_database_already_exists_strategy: TestDatabaseAlreadyExistsStrategy
    unclosed_connections_to_test_database_strategy: UnclosedConnectionsToTestDatabaseStrategy
