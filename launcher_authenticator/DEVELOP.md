# Launcher authenticator development set up
Before proceeding make sure you read [`DESIGN.md`](DESIGN.md).

Start a shell in `/launcher_authenticator` and do the following:

 0. Create a `virtualenv` with Python 3.10
 1. `pip install poetry==1.4.2`
 2. `poetry install`
 3. Copy `config.example.py` to `config.py`
    * Make sure the PostgreSQL settings are correct.
    * Make sure all URLs/hostnames point to the correct location.
 4. Ensure `./test.sh` succeeds.
