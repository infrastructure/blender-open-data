#!/usr/bin/env python3
import asyncio
import importlib
import importlib.util
import signal
import types

import click

from authenticator import sql, sentry
from authenticator.config import Config
from authenticator.listener import Listener
from authenticator.loggers import get_logger
from authenticator.server import Server
from authenticator.state import State


async def run(config: Config) -> None:
    loop = asyncio.get_running_loop()

    sentry.initialize_sentry(config)
    logger = get_logger(config).bind(service='launcher_authenticator')
    state = State()

    async with sql.get_pool(config) as pool:
        listener = Listener(config, logger, state, pool)

        server = Server(config, logger, state, pool)

        def terminate() -> None:
            listener.stop()
            server.stop()

        loop.add_signal_handler(signal.SIGTERM, terminate)

        done, pending = await asyncio.wait(
            (server.run(), listener.run()), return_when=asyncio.FIRST_COMPLETED
        )
        logger.info(f'terminating, done={done}')
        terminate()


def get_config(config_module: str) -> Config:
    module: types.ModuleType = importlib.import_module(config_module)

    if not hasattr(module, 'config'):
        raise AttributeError(f'config module {config_module} has no `config` ' f'variable.')

    config: object = getattr(module, 'config')

    if not isinstance(config, Config):
        raise ValueError(
            f'the variable `config` in config module {config_module} should be of'
            f' type `Config` but found {type(config)}'
        )

    return config


@click.command()
@click.option(
    '--config-module', default='config', help='The config module to import', type=click.STRING,
)
def main(config_module: str) -> None:
    asyncio.run(run(get_config(config_module)))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
