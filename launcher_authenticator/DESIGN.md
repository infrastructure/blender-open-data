# Design

The launcher authenticator is responsible for providing tokens for the launcher.
If you are interested in the general data flow between the launcher and
authenticator take a look at
[`ACHITECTURE.md`](../ARCHITECTURE.md#Launcher%20authentication%20flow)
instead.

The authenticator is implemented using `asyncio`. **MAKE SURE YOU READ THE
ASYNCHRONICITY SECTION**. Apart from that the authenticator is relatively
simple and consists of three main parts:

 1. the `Server` defined in [`authenticator/server.py`](authenticator/server.py),
 2. the `Listener` defined in [`authenticator/listener.py`](authenticator/listener.py),
 3. the `State` defined in [`authenticator/state.py`](authenticator/state.py).

## Asynchronicity

The main difference between the authenticator and a contemporary Django app is
that the authenticator is asynchronous (whereas Django apps are synchronous).
This is also the reason `/launcher_authenticator` is separate from `/website`.

In general synchronous web servers are easier to understand. That is why most
simple web apps out there are synchronous. However, the number of requests
they can handle concurrently is bounded by the number of instances of the web
server running in parallel which in turn is bounded by the system resources.

Since the `/launcher` has to wait for the token to be verified we have long
lived connections which are mostly idle. Therefore, we have a high number of
concurrent connections. To be able to handle that without wasting system
resources the launcher is implemented in an asynchronous fashion using `asyncio`.

The downside to using `asyncio` is that all code must cooperate in handling the
connections concurrently. This is done by deferring to handling another
connection whenever the equivalent synchronous code would block. **This means
that we cannot use libraries like `requests` which would block instead of
deferring; every library needs to be specifically designed with `asyncio`
in mind. To reiterate: DO NOT USE BLOCKING SYNCHRONOUS CODE.** If you must
use blocking synchronous code: spawn a thread and await that thread. Luckily,
there are `asyncio` compatible libraries for almost all common web server
operations like `websockets` for WebSockets and `aiopg` for PostgreSQL.
And not all is bad: due to the fact that `asyncio` is a quite recent addition
to Python these libraries often have the added advantage of being written in
modern Python i.e. with type annotations and no dynamic `dict`-soup. 

## Server

The `Server` is responsible for communicating with the `/launcher`. Whenever
the `/launcher` requests a new `LauncherAuthenticationToken` the `Server` adds
an entry to `State.pending_authentication_tokens` consisting of the token ID
and an empty Future. It then waits for this future to be completed by the
`Listener`. As soon as it is completed it checks if the token was verified and, 
if so, forwards the verification to the `/launcher`.

## Listener

The `Listener` is responsible for communicating with PostgreSQL. It does this
by opening a (single) connection to PostgreSQL and issuing
`NOTIFY <Config.notification_channel>;`. It then waits until a message
is received from PostgreSQL. After a message is received it looks up the
token ID in the message body and completes the future in
`State.pending_authentication_tokens`, if any.
