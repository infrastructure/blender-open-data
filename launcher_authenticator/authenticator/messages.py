from __future__ import annotations

import abc
import dataclasses as dc
import enum
import json
import uuid
from typing import Dict, Type, cast

import yarl

from authenticator.errors import ParseError
from authenticator.parsing import parse_field


class Message(abc.ABC):
    pass


class Response(Message, abc.ABC):
    @abc.abstractmethod
    def to_json_dict(self) -> Dict[str, object]:
        pass


class Request(Message, abc.ABC):
    @classmethod
    @abc.abstractmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> Request:
        pass


@dc.dataclass
class TokenResponse(Response):
    verification_url: yarl.URL
    token: uuid.UUID

    def to_json_dict(self) -> Dict[str, object]:
        return {'verification_url': str(self.verification_url), 'token': self.token.hex}


@dc.dataclass
class VerifiedResponse(Response):
    user_id: int
    username: str
    email: str

    def to_json_dict(self) -> Dict[str, object]:
        return {'user_id': self.user_id, 'username': self.username, 'email': self.email}


@dc.dataclass
class RejectedResponse(Response):
    def to_json_dict(self) -> Dict[str, object]:
        return {}


@dc.dataclass
class VerificationTimedOutResponse(Response):
    def to_json_dict(self) -> Dict[str, object]:
        return {}


class ErrorResponse(Response, abc.ABC):
    message: str

    def to_json_dict(self) -> Dict[str, object]:
        return {'message': self.message}


@dc.dataclass
class ParseErrorResponse(ErrorResponse):
    message: str

    @classmethod
    def from_parse_error(cls, parse_error: ParseError) -> ParseErrorResponse:
        return ParseErrorResponse(message=parse_error.message)


@dc.dataclass
class UnhandledRequestResponse(ErrorResponse):
    message: str
    message_type: str

    @classmethod
    def from_request(cls, request: Request) -> UnhandledRequestResponse:
        return UnhandledRequestResponse(
            message='unhandled request type', message_type=MessageType.from_message(request).name
        )


@dc.dataclass
class AuthenticationRequest(Request):
    hostname: str

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> AuthenticationRequest:
        hostname: str = parse_field(json_dict, 'hostname', str)
        return AuthenticationRequest(hostname=hostname)


class MessageType(enum.Enum):
    @classmethod
    def from_message(cls, message: Message) -> MessageType:
        try:
            return ResponseType(type(message))
        except KeyError:
            try:
                return RequestType(type(message))
            except KeyError:
                raise TypeError(message)


class ResponseType(MessageType):
    parse_error_response: Type[ParseErrorResponse] = ParseErrorResponse
    unhandled_request_response: Type[UnhandledRequestResponse] = UnhandledRequestResponse
    verification_timed_out_response: Type[
        VerificationTimedOutResponse
    ] = VerificationTimedOutResponse
    token_response: Type[TokenResponse] = TokenResponse
    verified_response: Type[VerifiedResponse] = VerifiedResponse
    rejected_response: Type[RejectedResponse] = RejectedResponse


class RequestType(MessageType):
    authentication_request: Type[AuthenticationRequest] = AuthenticationRequest


class Envelope(abc.ABC):
    message_type: MessageType
    message: Message

    @classmethod
    def from_response(cls, response: Response) -> ResponseEnvelope:
        return ResponseEnvelope(message_type=ResponseType(type(response)), message=response)

    @classmethod
    def deserialize(cls, request_envelope: str) -> RequestEnvelope:
        return RequestEnvelope.from_json_dict(json.loads(request_envelope))


@dc.dataclass
class ResponseEnvelope(Envelope):
    message_type: ResponseType
    message: Response

    def to_json_dict(self) -> Dict[str, object]:
        return {'message_type': self.message_type.name, 'message': self.message.to_json_dict()}

    def serialize(self) -> str:
        return json.dumps(self.to_json_dict())


@dc.dataclass
class RequestEnvelope(Envelope):
    message_type: RequestType
    message: Request

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> RequestEnvelope:
        message_type = parse_field(json_dict, 'message_type', str)
        if message_type not in RequestType.__members__:
            raise ParseError(
                f'message_type should be one of {[n for n in RequestType.__members__.keys()]} '
                f'but found {message_type}'
            )
        request_type = RequestType[message_type]
        message_class: Type[Request] = cast(Type[Request], request_type.value)

        message = parse_field(json_dict, 'message', dict)

        return RequestEnvelope(
            message_type=request_type, message=message_class.from_json_dict(message)
        )
