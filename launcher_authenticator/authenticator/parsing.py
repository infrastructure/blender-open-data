import uuid
from typing import TypeVar, overload, Dict, Type, Optional, Callable, Union

from authenticator.errors import ParseError

LiteralType = TypeVar('LiteralType', bound=Union[uuid.UUID, str, int, None])


@overload
def parse_field(
    dct: Dict[str, object], field: str, type_: Type[Dict[object, object]]
) -> Dict[str, object]:
    ...


@overload
def parse_field(dct: Dict[str, object], field: str, type_: Type[LiteralType]) -> LiteralType:
    ...


def parse_field(dct: Dict[str, object], field: str, type_: Type[object]) -> object:
    value: Optional[object] = dct.get(field)
    if isinstance(value, type_):
        return value
    else:
        raise ParseError(f'{field} should be of type {type_} but found {type(value)}')


@overload
def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[Dict[object, object]]
) -> Optional[Dict[str, object]]:
    ...


@overload
def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[LiteralType]
) -> Optional[LiteralType]:
    ...


def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[object]
) -> Optional[object]:
    value: Optional[object] = dct.get(field)
    if value is None:
        return None
    elif isinstance(value, type_):
        return value
    else:
        raise ParseError(f'{field} should be of type {type_} but found {type(value)}')


A = TypeVar('A')
B = TypeVar('B')


def map_optional(value: Optional[A], f: Callable[[A], B]) -> Optional[B]:
    if value is None:
        return None
    else:
        return f(value)
