from __future__ import annotations

import asyncio
import dataclasses as dc
import socket
from asyncio import Future
from typing import Optional, List

import aiopg
import websockets
import websockets.exceptions
from websockets.server import WebSocketServerProtocol

from authenticator.config import Config
from authenticator.errors import ParseError, SQLError
from authenticator.loggers import Logger
from authenticator.messages import (
    Response,
    TokenResponse,
    Request,
    ParseErrorResponse,
    UnhandledRequestResponse,
    AuthenticationRequest,
    Envelope,
    VerifiedResponse,
    RejectedResponse,
    VerificationTimedOutResponse,
    MessageType,
)
from authenticator.sql import (
    get_authentication_token,
    create_authentication_token,
    delete_authentication_token,
    get_user,
    User,
)
from authenticator.state import State


@dc.dataclass
class Server:
    def __init__(self, config: Config, logger: Logger, state: State, pool: aiopg.Pool):
        self.config: Config = config
        self.logger: Logger = logger.bind(module='server')
        self.state: State = state
        self.pool: aiopg.Pool = pool
        self._stop: Optional[Future[None]] = None
        self.sockets: Future[List[socket.socket]] = asyncio.get_running_loop().create_future()

    async def run(self) -> None:
        if self._stop is not None:
            raise Exception('server already running')

        self._stop = asyncio.get_running_loop().create_future()
        async with websockets.server.serve(
            self.serve, self.config.hostnames, self.config.port
        ) as server:
            assert server.sockets
            for sock in server.sockets:
                self.logger.info(
                    'server bound',
                    socket_addres=sock.getsockname()[0],
                    socket_port=sock.getsockname()[1],
                )
            self.sockets.set_result(server.sockets)

            await self._stop

        self.sockets = asyncio.get_running_loop().create_future()
        self.logger.info('server stopped')

    def stop(self) -> None:
        if self._stop is None:
            raise Exception('server not running')

        self._stop.set_result(None)

    async def receive_request(
        self, logger: Logger, websocket: WebSocketServerProtocol
    ) -> Optional[Request]:
        request_text = await websocket.recv()
        if not isinstance(request_text, str):
            logger.info('received a non-text frame')
            await self.send_response(
                logger, websocket, ParseErrorResponse('received a non-text frame')
            )
            return None

        try:
            request_envelope = Envelope.deserialize(request_text)
        except ParseError as e:
            logger.info('received an invalid message', exc_info=self.config.debug)
            await self.send_response(logger, websocket, ParseErrorResponse.from_parse_error(e))
            return None

        logger.info('received message', message_type=request_envelope.message_type.name)
        return request_envelope.message

    async def send_response(
        self, logger: Logger, websocket: WebSocketServerProtocol, response: Response
    ) -> None:
        await websocket.send(Envelope.from_response(response).serialize())
        logger.info('sent response', message_type=MessageType.from_message(response).name)

    async def serve(self, websocket: WebSocketServerProtocol, path: str) -> None:
        client_address, client_port = websocket.remote_address
        server_address, server_port = websocket.local_address
        logger = self.logger.bind(
            client_address=client_address,
            client_port=client_port,
            server_address=server_address,
            server_port=server_port,
        )
        try:
            while True:
                request = await self.receive_request(logger, websocket)
                if request is not None:
                    await self.dispatch(logger, websocket, request)
        except websockets.exceptions.ConnectionClosed:
            logger.info('connection closed')
        except Exception:
            logger.exception('unexpected error')

    async def dispatch(
        self, logger: Logger, websocket: WebSocketServerProtocol, request: Request
    ) -> None:
        if isinstance(request, AuthenticationRequest):
            await self.handle_authentication_request(logger, websocket, request)
        else:
            await self.send_response(
                logger, websocket, UnhandledRequestResponse.from_request(request)
            )

    async def handle_authentication_request(
        self, logger: Logger, websocket: WebSocketServerProtocol, request: AuthenticationRequest
    ) -> None:
        # Create a row in the token table which the user can verify.
        async with self.pool.acquire() as connection, connection.cursor() as cursor:
            token = await create_authentication_token(cursor, hostname=request.hostname)

        logger = logger.bind(authentication_token_id=token.id)

        # As soon as the user verifies or rejects the token the Listener will
        # complete the future.
        user_responded: Future[None] = asyncio.get_running_loop().create_future()

        self.state.pending_authentication_tokens[token.id] = user_responded

        # Send a URL to the launcher where the user can verify the token.
        await self.send_response(
            logger,
            websocket,
            TokenResponse(
                verification_url=self.config.verification_url_function(token), token=token.token
            ),
        )
        logger.info('sent URL for user approval')

        try:
            # Wait for the user to verify the token with a timeout, or until the
            # connection is closed.
            done, pending = await asyncio.wait(
                (user_responded, websocket.connection_lost_waiter),
                timeout=self.config.verification_timeout,
                return_when=asyncio.FIRST_COMPLETED,
            )

            if not done:
                # The timeout was reached so the user was not quick enough to
                # verify the token.
                logger.info('verification timed out; deleting token')
                async with self.pool.acquire() as connection, connection.cursor() as cursor:
                    try:
                        await delete_authentication_token(cursor, token.id)
                    except SQLError:
                        logger.exception('error while deleting token')
                await self.send_response(logger, websocket, VerificationTimedOutResponse())
                return

            elif websocket.connection_lost_waiter in done:
                logger.info('launcher disconnected; deleting token')
                async with self.pool.acquire() as connection, connection.cursor() as cursor:
                    await delete_authentication_token(cursor, token.id)
                    return

            assert user_responded in done

            logger.info('user responded')

            # Get the updated token.
            async with self.pool.acquire() as connection, connection.cursor() as cursor:
                maybe_verified_token = await get_authentication_token(cursor, token.id)

                if maybe_verified_token.user_id is not None:
                    verification_user: Optional[User] = await get_user(
                        cursor, maybe_verified_token.user_id
                    )
                else:
                    verification_user = None

            # Check if the token is verified.
            if maybe_verified_token.verified and verification_user is not None:
                logger.info('token verified')

                # The token was verified.
                await self.send_response(
                    logger,
                    websocket,
                    VerifiedResponse(
                        user_id=verification_user.id,
                        username=verification_user.username,
                        email=verification_user.email,
                    ),
                )
            else:
                logger.info('token rejected')

                # The user rejected the token.
                async with self.pool.acquire() as connection, connection.cursor() as cursor:
                    await delete_authentication_token(cursor, token.id)
                await self.send_response(logger, websocket, RejectedResponse())
        except Exception:
            # If anything goes wrong we delete the token.
            logger.info('there was an error; deleting token')
            async with self.pool.acquire() as connection, connection.cursor() as cursor:
                try:
                    await delete_authentication_token(cursor, token.id)
                except SQLError:
                    logger.exception('error while deleting token')
            raise
