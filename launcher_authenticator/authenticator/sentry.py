import sentry_sdk
from sentry_sdk.integrations.argv import ArgvIntegration
from sentry_sdk.integrations.atexit import AtexitIntegration
from sentry_sdk.integrations.dedupe import DedupeIntegration
from sentry_sdk.integrations.excepthook import ExcepthookIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.modules import ModulesIntegration
from sentry_sdk.integrations.stdlib import StdlibIntegration
from sentry_sdk.integrations.threading import ThreadingIntegration

from authenticator.config import Config

_sentry_initialized: bool = False


def initialize_sentry(config: Config) -> None:
    global _sentry_initialized

    if _sentry_initialized:
        raise Exception('trying to initialize Sentry twice')
    elif not config.debug:
        # TODO(sem): Test this.
        if config.sentry:
            sentry_sdk.hub.init(
                dsn=str(config.sentry.dsn),
                with_locals=False,  # We do not want to send all this sensitive data to the USA.
                default_integrations=False,
                integrations=[
                    LoggingIntegration(
                        event_level=None  # We disable this because we handle this using `structlog_sentry`.
                    ),
                    StdlibIntegration(),
                    ExcepthookIntegration(),
                    DedupeIntegration(),
                    AtexitIntegration(),
                    ModulesIntegration(),
                    ArgvIntegration(),
                    ThreadingIntegration(),
                ],
            )

        _sentry_initialized = True
