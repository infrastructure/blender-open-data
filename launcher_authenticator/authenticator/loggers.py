from __future__ import annotations

from typing import Optional, cast

import structlog
import structlog_sentry

from authenticator.config import Config


class Logger(structlog.stdlib.BoundLogger):
    def debug(self, event: Optional[str], *args: object, **kwargs: object) -> None:
        super().debug(event, *args, **kwargs)

    def info(self, event: Optional[str], *args: object, **kwargs: object) -> None:
        super().info(event, *args, **kwargs)

    def warning(self, event: Optional[str], *args: object, **kwargs: object) -> None:
        super().warning(event, *args, **kwargs)

    def error(self, event: Optional[str], *args: object, **kwargs: object) -> None:
        super().error(event, *args, **kwargs)

    def bind(self, **kwargs: object) -> Logger:
        return cast(Logger, super().bind(**kwargs))


def get_logger(config: Config) -> Logger:
    processors = [
        structlog.processors.StackInfoRenderer(),
        structlog.dev.set_exc_info,
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(),
        structlog.stdlib.add_log_level,
    ]
    if config.sentry is not None:
        processors.append(structlog_sentry.SentryProcessor())
    processors += [
        structlog.dev.ConsoleRenderer(),
    ]

    return Logger(logger=structlog.PrintLogger(), processors=processors, context={},)
