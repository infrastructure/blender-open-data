import dataclasses as dc
from typing import Sequence, NewType, TYPE_CHECKING, Protocol, Optional

import yarl

if TYPE_CHECKING:
    from authenticator.tokens import AuthenticationToken


@dc.dataclass
class PostgresConfig:
    host: str
    port: int
    database: str
    user: str
    password: str
    minimum_number_of_connections: int
    maximum_number_of_connections: int


@dc.dataclass
class SentryConfig:
    dsn: yarl.URL


Seconds = NewType('Seconds', float)


class VerificationURLFunction(Protocol):
    def __call__(self, token: 'AuthenticationToken') -> yarl.URL:
        ...


@dc.dataclass
class Config:
    debug: bool
    hostnames: Sequence[str]
    port: int
    verification_timeout: Seconds
    notification_channel: str
    verification_url_function: VerificationURLFunction
    postgres: PostgresConfig
    sentry: Optional[SentryConfig]
