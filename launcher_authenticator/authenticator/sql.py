from __future__ import annotations

import dataclasses as dc
import datetime
import uuid
from typing import Dict, Sequence, TypeVar, Union

import aiopg
import aiopg.utils

from authenticator.config import Config
from authenticator.errors import SQLError, ParseError
from authenticator.parsing import parse_field
from authenticator.tokens import AuthenticationTokenId, AuthenticationToken, UserId


def get_pool(config: Config) -> aiopg.utils._PoolContextManager:
    assert (
        config.postgres.minimum_number_of_connections >= 2
    ), 'we need at least two connections for: one for the Listener, and one for the Server'

    return aiopg.create_pool(
        host=config.postgres.host,
        port=config.postgres.port,
        database=config.postgres.database,
        user=config.postgres.user,
        password=config.postgres.password,
        minsize=config.postgres.minimum_number_of_connections,
        maxsize=config.postgres.maximum_number_of_connections,
    )


def row_as_dict(names: Sequence[str], row: Sequence[object]) -> Dict[str, object]:
    return {name: value for name, value in zip(names, row)}


async def fetch_one(cursor: aiopg.Cursor) -> Dict[str, object]:
    if cursor.rowcount != 1:
        raise SQLError(
            query=cursor.query, message=f'expected a single row but found {cursor.rowcount}'
        )
    else:
        return row_as_dict(names=[d[0] for d in cursor.description], row=await cursor.fetchone())


LiteralType = TypeVar('LiteralType', bound=Union[uuid.UUID, str, int, None])


async def get_authentication_token(
    cursor: aiopg.Cursor, id_: AuthenticationTokenId
) -> AuthenticationToken:
    await cursor.execute(
        '''
            SELECT
                opendata_main_launcherauthenticationtoken.id,
                opendata_main_launcherauthenticationtoken.user_id,
                opendata_main_launcherauthenticationtoken.token,
                opendata_main_launcherauthenticationtoken.hostname,
                opendata_main_launcherauthenticationtoken.verified
            FROM
                opendata_main_launcherauthenticationtoken
            WHERE
                opendata_main_launcherauthenticationtoken.id = %(id)s
            ;
            ''',
        {'id': id_},
    )

    sql_dict = await fetch_one(cursor)
    try:
        return AuthenticationToken.from_sql_dict(sql_dict)
    except ParseError as e:
        raise SQLError(query=cursor.query, message='could not parse row') from e


async def create_authentication_token(cursor: aiopg.Cursor, hostname: str) -> AuthenticationToken:
    async with cursor.begin_nested():
        await cursor.execute(
            '''
                INSERT INTO
                    opendata_main_launcherauthenticationtoken (
                        id,
                        updated_at,
                        created_at,
                        hostname,
                        token
                    )
                VALUES (
                    %(id)s,
                    %(created_at)s,
                    %(created_at)s,
                    %(hostname)s,
                    %(token)s
                )
                RETURNING
                    opendata_main_launcherauthenticationtoken.id,
                    opendata_main_launcherauthenticationtoken.user_id,
                    opendata_main_launcherauthenticationtoken.token,
                    opendata_main_launcherauthenticationtoken.hostname,
                    opendata_main_launcherauthenticationtoken.verified
                ;
                ''',
            {
                'id': uuid.uuid4(),
                'created_at': datetime.datetime.now(datetime.timezone.utc),
                'hostname': hostname,
                'token': uuid.uuid4(),
            },
        )

        sql_dict = await fetch_one(cursor)

        try:
            return AuthenticationToken.from_sql_dict(sql_dict)
        except ParseError as e:
            raise SQLError(query=cursor.query, message='could not parse row') from e


async def delete_authentication_token(cursor: aiopg.Cursor, id_: AuthenticationTokenId) -> None:
    async with cursor.begin_nested():
        await cursor.execute(
            '''
                DELETE FROM
                    opendata_main_launcherauthenticationtoken
                WHERE
                    opendata_main_launcherauthenticationtoken.id = %(id)s
                ;
                ''',
            {'id': id_},
        )

        if not cursor.rowcount == 1:
            raise SQLError(
                query=cursor.query, message=f'tried to delete one row but found {cursor.rowcount}'
            )


@dc.dataclass
class User:
    id: UserId
    username: str
    email: str

    @classmethod
    def from_sql_dict(cls, sql_dict: Dict[str, object]) -> User:
        return User(
            id=UserId(parse_field(sql_dict, 'id', int)),
            username=parse_field(sql_dict, 'username', str),
            email=parse_field(sql_dict, 'email', str),
        )


async def get_user(cursor: aiopg.Cursor, id_: UserId) -> User:
    await cursor.execute(
        '''
            SELECT
                auth_user.id,
                auth_user.username,
                auth_user.email
            FROM
                auth_user
            WHERE
                auth_user.id = %(id)s
            ;
            ''',
        {'id': id_},
    )

    sql_dict = await fetch_one(cursor)
    try:
        return User.from_sql_dict(sql_dict)
    except ParseError as e:
        raise SQLError(query=cursor.query, message='could not parse row') from e
