import asyncio
import uuid
from asyncio import Future
from typing import Optional

import aiopg
import psycopg2
from psycopg2 import sql
from psycopg2._psycopg import Notify

from authenticator.config import Config
from authenticator.loggers import Logger
from authenticator.state import State
from authenticator.tokens import AuthenticationTokenId


class Listener:
    def __init__(self, config: Config, logger: Logger, state: State, pool: aiopg.Pool):
        self.config: Config = config
        self.logger: Logger = logger.bind(module='listener')
        self.state: State = state
        self.pool: aiopg.Pool = pool
        self._stop: Optional[Future[None]] = None

    async def run(self) -> None:
        if self._stop is not None:
            raise Exception('listener already running')

        self._stop = asyncio.get_running_loop().create_future()

        MAX_ATTEMPTS = 3
        attempts = 0
        while True:
            try:
                async with self.pool.acquire() as connection, connection.cursor() as cursor:
                    await cursor.execute(
                        sql.SQL('LISTEN {channel};').format(
                            channel=sql.Identifier(self.config.notification_channel)
                        )
                    )
                    self.logger.info('listener listening', channel=self.config.notification_channel)
                    # reset attempts after a successful connect
                    attempts = 0
                    while True:
                        notification: Future[Notify] = asyncio.ensure_future(
                            connection.notifies.get()
                        )
                        done, pending = await asyncio.wait(
                            (self._stop, notification), return_when=asyncio.FIRST_COMPLETED
                        )
                        if self._stop in done:
                            self.logger.info('listener stopped')
                            return
                        elif notification in done:
                            await self.handle(notification.result())
            except (psycopg2.Error, psycopg2.OperationalError):
                self.logger.exception('failed to handle notification')
                if attempts < MAX_ATTEMPTS:
                    attempts += 1
                    self.logger.info(
                        f'will try to reconnect after a short delay, attempt nr={attempts}'
                    )
                    await asyncio.sleep(2 ** attempts)
                else:
                    self.logger.error(f'exhausted MAX_ATTEMPTS={MAX_ATTEMPTS} budget, giving up')
                    return

    def stop(self) -> None:
        if self._stop is None:
            raise Exception('listener not running')

        self._stop.set_result(None)

    async def handle(self, notification: Notify) -> None:
        authentication_token_id_string: str = notification.payload
        try:
            authentication_token_id = AuthenticationTokenId(
                uuid.UUID(hex=authentication_token_id_string)
            )
        except ValueError:
            self.logger.exception(
                'notification payload was not an integer',
                authentication_token_id=authentication_token_id_string,
                pid=notification.pid,
            )
            return

        self.logger.info(
            'received notification',
            authentication_token_id=authentication_token_id,
            pid=notification.pid,
        )

        if authentication_token_id in self.state.pending_authentication_tokens:
            self.state.pending_authentication_tokens[authentication_token_id].set_result(None)
            del self.state.pending_authentication_tokens[authentication_token_id]
