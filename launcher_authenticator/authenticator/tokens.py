from __future__ import annotations

import dataclasses as dc
import uuid
from typing import NewType, Optional, Dict

from authenticator.parsing import parse_field, parse_field_optional, map_optional

AuthenticationTokenId = NewType('AuthenticationTokenId', uuid.UUID)
UserId = NewType('UserId', int)


@dc.dataclass
class AuthenticationToken:
    id: AuthenticationTokenId
    user_id: Optional[UserId]
    token: uuid.UUID
    hostname: str
    verified: Optional[bool]

    @classmethod
    def from_sql_dict(cls, sql_dict: Dict[str, object]) -> AuthenticationToken:
        return AuthenticationToken(
            id=AuthenticationTokenId(parse_field(sql_dict, 'id', uuid.UUID)),
            user_id=map_optional(parse_field_optional(sql_dict, 'user_id', int), UserId),
            token=parse_field(sql_dict, 'token', uuid.UUID),
            hostname=parse_field(sql_dict, 'hostname', str),
            verified=parse_field_optional(sql_dict, 'verified', bool),
        )
