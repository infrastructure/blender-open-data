from __future__ import annotations

import dataclasses as dc
from asyncio import Future
from weakref import WeakValueDictionary

from authenticator.tokens import AuthenticationTokenId


@dc.dataclass
class State:
    # We use a `WeakValueDictionary` to guarantee that we do not leak Futures.
    pending_authentication_tokens: 'WeakValueDictionary[AuthenticationTokenId, Future[None]]' = dc.field(
        default_factory=WeakValueDictionary
    )
