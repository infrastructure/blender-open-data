import dataclasses as dc


class Error(Exception):
    pass


@dc.dataclass
class ParseError(Error):
    message: str


@dc.dataclass
class SQLError(Error):
    query: str
    message: str
