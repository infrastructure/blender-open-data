import yarl

from authenticator.config import Config, Seconds, PostgresConfig
from tests.config import (
    TestConfig,
    TestDatabaseAlreadyExistsStrategy,
    UnclosedConnectionsToTestDatabaseStrategy,
)

config = Config(
    debug=True,
    hostnames=['opendata.local'],
    port=8005,
    verification_timeout=Seconds(10 * 60),
    notification_channel='opendata_main_launcherauthenticationtoken_user_response',
    verification_url_function=(
        lambda t: yarl.URL('http://opendata.local:8002/benchmarks/tokens/') / str(t.id) / 'verify/'
    ),
    postgres=PostgresConfig(
        host='127.0.0.1',
        port=5432,
        database='opendata',
        user='postgres',
        password='postgres',
        minimum_number_of_connections=2,
        maximum_number_of_connections=10,
    ),
    sentry=None,
)

test_config = TestConfig(
    config=Config(
        debug=True,
        hostnames=['opendata.local'],
        port=0,  # Use a random port.
        verification_timeout=Seconds(10 * 60),
        notification_channel='opendata_main_launcherauthenticationtoken_user_response',
        verification_url_function=(
            lambda t: yarl.URL('http://opendata.local:8002/benchmarks/tokens/')
            / str(t.id)
            / 'verify/'
        ),
        postgres=PostgresConfig(
            host='127.0.0.1',
            port=5432,
            database='opendata_test',
            user='postgres',
            password='postgres',
            minimum_number_of_connections=2,
            maximum_number_of_connections=10,
        ),
        sentry=None,
    ),
    database_to_clone='opendata',
    test_database_already_exists_strategy=TestDatabaseAlreadyExistsStrategy.drop,
    unclosed_connections_to_test_database_strategy=UnclosedConnectionsToTestDatabaseStrategy.kill,
)
