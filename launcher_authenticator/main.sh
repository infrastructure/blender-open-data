#!/usr/bin/env sh
set -eux

cd "$(dirname $0)"
pyenv exec poetry run ./main.py "$@"
