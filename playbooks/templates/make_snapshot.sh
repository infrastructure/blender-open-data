#!/bin/bash

# This script triggers a dump of the database as used by
# Open Data (https://opendata.blender.org/). The dump is automatically
# placed in {{ dir.snapshots }} and served at
# https://{{ domain }}/snapshots/
set -eu
TMP_DIR=$(mktemp -d -t blender-open-data-snapshot-XXXXXXXXX)
FILE_NAME="opendata-$(date '+%Y-%m-%d-%H%M%S%z')"
JSONL_FILE="${TMP_DIR:?}/${FILE_NAME:?}.jsonl"
ZIP_FILE="${TMP_DIR:?}/snapshot.zip"

cp -r {{ dir.source }}/playbooks/snapshot_template/. "${TMP_DIR:?}"
{{ dir.source }}/website/.venv/bin/python {{ dir.source }}/website/manage.py dump_snapshot > "${JSONL_FILE:?}"
cd "${TMP_DIR:?}" && zip -q -r "${ZIP_FILE:?}" .
cd {{ dir.snapshots }} && mv "${ZIP_FILE:?}" {{ dir.snapshots }}/opendata-latest.zip

rm -rf "${TMP_DIR:?}"
