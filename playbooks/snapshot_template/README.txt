# Blender Open Data

This archive contains a direct dump of [Blender Open
Data](https://opendata.blender.org/). The data itself is contained in the
`xxxx.jsonl` file. This file contains a JSON-formatted document on each line.


## License

The data in this archive is licensed under the *Creative Commons 0* license.
For more details and legal text refer to `LICENSE.txt`.

