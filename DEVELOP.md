# Development set up

Start a shell in the root of the repo and then issue:

 1. `git submodule init`
 2. `git submodule foreach --recursive 'git fetch --tags'`
 3. `git submodule update --recursive`

Next set up the services in the following order:

 1. [`/website`](website/DEVELOP.md)
 2. [`/launcher_authenticator`](launcher_authenticator/DEVELOP.md)
 3. [`/benchmark_script`](benchmark_script/DEVELOP.md)
 4. [`/launcher`](launcher/DEVELOP.md)
