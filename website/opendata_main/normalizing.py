"""Parse RawBenchmarks and return indexable versions."""
import datetime
import logging
from dataclasses import dataclass
from typing import Optional

from opendata_main.models.benchmarks import RawBenchmark, RawBenchmarkOwnership
from opendata_main.models.user_settings import UserSettings


log = logging.getLogger(__name__)


@dataclass
class NormalizedBenchmark:
    """Benchmark representation that is used in BenchmarkDetailView, as
    well as for indexing. An index record is populate by passing this
    class asdict().
    """

    raw_benchmark: RawBenchmark
    score: float
    score_scenes: dict
    device_name: str
    device_type: str
    compute_type: str
    operating_system: str
    blender_version: str
    submission_date: Optional[datetime.datetime] = None
    username: Optional[str] = None

    def __post_init__(self):
        # Format device_name
        self.device_name = DeviceNameFormatter.format(self.device_name)

        # Format OS
        if self.operating_system == 'Darwin':
            self.operating_system = 'macOS'

        # Assign username
        try:
            if not self.raw_benchmark.ownership.get().user.settings.benchmarks_anonymous:
                self.username = self.raw_benchmark.ownership.get().user.settings.benchmarks_name
        except RawBenchmarkOwnership.DoesNotExist:
            pass
        except UserSettings.DoesNotExist:
            pass

        # Assign submission_date
        if not self.submission_date:
            self.submission_date = self.raw_benchmark.created_at


class DeviceNameFormatter:
    @staticmethod
    def strip_tm_and_r(s):
        for char in {'(R)', '(TM)', '®', '™'}:
            s = s.replace(char, '')
        return s

    @staticmethod
    def prepend_nvidia(s):
        """Ensure NVIDIA before GeForce or Quadro."""
        for label in {'geforce', 'quadro', 'tesla'}:
            if label in s.lower() and not s.lower().startswith('nvidia'):
                s = f'NVIDIA {s}'
        return s

    @staticmethod
    def prepend_amd(s):
        """Ensure AMD before Radeon"""
        for label in {'radeon'}:
            if label in s.lower() and not s.lower().startswith('amd'):
                s = f'AMD {s}'
        return s

    @staticmethod
    def remove_intel_gen(s):
        """Remove 11th Gen and 12th Gen from name."""
        for char in {'11th Gen ', '12th Gen '}:
            if s.startswith(char):
                s = s.replace(char, '')
        return s

    @staticmethod
    def format(s):
        s = DeviceNameFormatter.strip_tm_and_r(s)
        s = DeviceNameFormatter.prepend_nvidia(s)
        s = DeviceNameFormatter.prepend_amd(s)
        s = DeviceNameFormatter.remove_intel_gen(s)
        return s


class Normalizer:
    @staticmethod
    def _render_time_to_samples_per_minute(scene_name, render_time_no_sync) -> int:
        """Generates samples per minutes. Used for v3 and earlier schemas."""
        scene_render_samples_map = {
            'barbershop_interior': 32,
            'classroom': 300,
            'bmw27': 1225,
            'pavillon_barcelona': 1000,
            'koro': 500,
            'fishy_cat': 128,
            'victor': 600,
        }
        if scene_name not in scene_render_samples_map:
            return 0

        return scene_render_samples_map[scene_name] / render_time_no_sync * 60

    @staticmethod
    def _normalize_v1(raw_benchmark: RawBenchmark) -> NormalizedBenchmark:
        """Generate a NormalizedBenchmark for schema v1."""
        data = raw_benchmark.data
        # Compute Score
        score_scenes = {}
        scores = []
        for s in data['scenes']:
            score = Normalizer._render_time_to_samples_per_minute(
                s['name'], s['stats']['render_time_no_sync']
            )
            score_scenes[s['name']] = score
            scores.append(score)
        score = sum(scores)
        # Fetch device type
        device_type = 'CPU' if data['device_info']['device_type'] == 'CPU' else 'GPU'
        # Fetch and parse device name
        device_name = data['system_info']['devices'][0]['name']
        # Handle multi-core CPUs
        if device_type == 'CPU' and data['system_info']['num_cpu_sockets'] > 1:
            device_name = f"{data['system_info']['num_cpu_sockets']}X {device_name}"

        return NormalizedBenchmark(
            raw_benchmark=raw_benchmark,
            score=score,
            score_scenes=score_scenes,
            device_name=device_name,
            device_type=device_type,
            compute_type=data['device_info']['device_type'],
            blender_version=data['blender_version']['version'],
            operating_system=data['system_info']['system'],
        )

    @staticmethod
    def _normalize_v2(raw_benchmark: RawBenchmark) -> NormalizedBenchmark:
        """Generate a NormalizedBenchmark for schema v2."""
        data = raw_benchmark.data
        # Compute Score
        score_scenes = {}
        scores = []
        for s in data['scenes']:
            score = Normalizer._render_time_to_samples_per_minute(
                s['name'], s['stats']['render_time_no_sync']
            )
            score_scenes[s['name']] = score
            scores.append(score)
        score = sum(scores)
        # Fetch device type
        device_type = 'CPU' if data['device_info']['device_type'] == 'CPU' else 'GPU'
        # Fetch and parse device name
        device_name = data['device_info']['compute_devices'][0]['name']
        # Handle multi-core CPUs
        if device_type == 'CPU' and data['system_info']['num_cpu_sockets'] > 1:
            device_name = f"{data['system_info']['num_cpu_sockets']}X {device_name}"

        return NormalizedBenchmark(
            raw_benchmark=raw_benchmark,
            score=score,
            score_scenes=score_scenes,
            device_name=device_name,
            device_type=device_type,
            compute_type=data['device_info']['device_type'],
            blender_version=data['blender_version']['version'],
            operating_system=data['system_info']['system'],
        )

    @staticmethod
    def _normalize_v3(raw_benchmark: RawBenchmark) -> NormalizedBenchmark:
        """Generate a NormalizedBenchmark for schema v3."""
        data = raw_benchmark.data
        first_scene = raw_benchmark.data[0]
        # Compute Score
        score_scenes = {}
        scores = []
        for s in data:
            score = Normalizer._render_time_to_samples_per_minute(
                s['scene']['label'], s['stats']['render_time_no_sync']
            )
            score_scenes[s['scene']['label']] = score
            scores.append(score)
        score = sum(scores)
        # Fetch device type
        device_type = 'CPU' if first_scene['device_info']['device_type'] == 'CPU' else 'GPU'
        # Fetch and parse device name
        device_name = first_scene['device_info']['compute_devices'][0]['name']
        # Handle multi-core CPUs
        if device_type == 'CPU' and first_scene['system_info']['num_cpu_sockets'] > 1:
            device_name = f"{first_scene['system_info']['num_cpu_sockets']}X {device_name}"

        return NormalizedBenchmark(
            raw_benchmark=raw_benchmark,
            score=score,
            score_scenes=score_scenes,
            device_name=device_name,
            device_type=device_type,
            compute_type=first_scene['device_info']['device_type'],
            blender_version=first_scene['blender_version']['label'],
            operating_system=first_scene['system_info']['system'],
        )

    @staticmethod
    def _normalize_v4(raw_benchmark: RawBenchmark) -> NormalizedBenchmark:
        data = raw_benchmark.data
        first_scene = raw_benchmark.data[0]
        # Fetch device type
        device_type = 'CPU' if first_scene['device_info']['device_type'] == 'CPU' else 'GPU'
        # Fetch and parse device name
        device_name = first_scene['device_info']['compute_devices'][0]['name']
        # Handle multi-core CPUs
        if device_type == 'CPU' and first_scene['system_info']['num_cpu_sockets'] > 1:
            device_name = f"{first_scene['system_info']['num_cpu_sockets']}X {device_name}"
        score_scenes = {}
        scores = []
        for s in data:
            score_scene = s['stats']['samples_per_minute']
            score_scenes[s['scene']['label']] = score_scene
            scores.append(score_scene)
        score = sum(scores)

        return NormalizedBenchmark(
            raw_benchmark=raw_benchmark,
            score=score,
            score_scenes=score_scenes,
            device_name=device_name,
            device_type=device_type,
            compute_type=first_scene['device_info']['device_type'],
            blender_version=first_scene['blender_version']['label'],
            operating_system=first_scene['system_info']['system'],
        )

    @staticmethod
    def normalize(raw_benchmark: RawBenchmark):
        if raw_benchmark.schema_version == 'v1':
            return Normalizer._normalize_v1(raw_benchmark)
        elif raw_benchmark.schema_version == 'v2':
            return Normalizer._normalize_v2(raw_benchmark)
        elif raw_benchmark.schema_version == 'v3':
            return Normalizer._normalize_v3(raw_benchmark)
        elif raw_benchmark.schema_version == 'v4':
            return Normalizer._normalize_v4(raw_benchmark)
        else:
            return NormalizedBenchmark(
                raw_benchmark=raw_benchmark,
                score=0,
                device_type='Unknown',
                device_name='Unknown',
                compute_type='Unknown',
                blender_version='Unknown',
                operating_system='Unknown',
                score_scenes={},
            )
