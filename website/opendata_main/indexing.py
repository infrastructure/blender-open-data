import logging
import dataclasses
from opendata_main.models.benchmarks import RawBenchmark, BenchmarkIndex, RawBenchmarkOwnership
from opendata_main.normalizing import Normalizer

log = logging.getLogger(__name__)

SUPPORTED_SCHEMAS = {'v4'}


class BenchmarkIndexer:
    @staticmethod
    def reindex_all(force=False, drop_index=False):
        if drop_index:
            log.info('Dropping all indexed benchmarks before reindexing')
            BenchmarkIndex.objects.all().delete()
        for raw_benchmark in RawBenchmark.objects.exclude(is_valid=False).filter(
            schema_version__in=SUPPORTED_SCHEMAS
        ):
            BenchmarkIndexer.index(raw_benchmark, force=force)

    @staticmethod
    def index(raw_benchmark: RawBenchmark, force=False):
        if force:
            BenchmarkIndexer.drop(raw_benchmark)

        # Skip unsupported schemas
        if raw_benchmark.schema_version not in SUPPORTED_SCHEMAS:
            return

        # Skip invalid benchmark
        if not raw_benchmark.is_valid:
            return

        # Skip existing version, unless force is true
        if BenchmarkIndex.objects.filter(raw_benchmark=raw_benchmark) and not force:
            log.debug('Skipping indexed benchmark')
            return

        # Index the benchmark
        serialized_benchmark = Normalizer.normalize(raw_benchmark)
        BenchmarkIndex.objects.create(**dataclasses.asdict(serialized_benchmark))

    @staticmethod
    def drop(raw_benchmark: RawBenchmark):
        """Drop indexed benchmark."""
        try:
            raw_benchmark.indexed_benchmark.delete()
            log.debug('Dropped indexed benchmark %s before reindexing' % raw_benchmark.id)
        except BenchmarkIndex.DoesNotExist:
            pass
