from typing import Dict, List, Tuple

import psycopg2
from django.db import connection
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView
from django.shortcuts import reverse
from django.conf import settings
from yarl import URL

from opendata_main.decorators import Seconds, cache
from opendata_main.models.metadata import Launcher
from opendata_main.sql import fetch_all, fetch_one


class HomeView(TemplateView):
    template_name = 'opendata_main/home.pug'

    def _query_fastest_total_median_score(
        self,
        cursor: psycopg2._psycopg.cursor,
        device_types: Tuple[str, ...],
        minimum_number_of_samples_per_benchmark: int,
        device_name_blacklist: Tuple[str, ...],
        blender_version: str,
    ) -> List[Dict[str, object]]:
        cursor.execute(
            '''
            SELECT 
                device_name, 
                percentile_cont(0.5) WITHIN GROUP (ORDER BY opendata_main_benchmarkindex.score) AS median_score 
            FROM opendata_main_benchmarkindex
            WHERE opendata_main_benchmarkindex.device_type IN %(device_types)s
                AND opendata_main_benchmarkindex.device_name NOT IN %(device_name_blacklist)s
                AND opendata_main_benchmarkindex.blender_version = %(blender_version)s
            GROUP BY 
                opendata_main_benchmarkindex.device_name
            HAVING count(*) >= %(minimum_number_of_samples_per_benchmark)s
            ORDER BY median_score DESC
            LIMIT 10;
            ''',
            {
                'device_types': device_types,
                'minimum_number_of_samples_per_benchmark': minimum_number_of_samples_per_benchmark,
                'device_name_blacklist': device_name_blacklist,
                'blender_version': blender_version,
            },
        )
        return fetch_all(cursor)

    @cache('homepage_queries', timeout=Seconds(5 * 60))
    def _get_query_results(self) -> Dict[str, object]:
        minimum_number_of_samples_per_benchmark = settings.MINIMUM_NUMBER_OF_SAMPLES_PER_BENCHMARK
        device_name_blacklist = ('Graphics Device',)

        with connection.cursor() as cursor:
            cursor.execute(
                '''
                SELECT COUNT(*) AS count
                FROM opendata_main_benchmarkindex;
                '''
            )
            total_number_of_benchmarks = fetch_one(cursor)

            cursor.execute(
                '''
                SELECT COUNT(*)                                  AS count,
                       opendata_main_benchmarkindex.compute_type AS compute_type
                FROM opendata_main_benchmarkindex
                GROUP BY opendata_main_benchmarkindex.compute_type
                ORDER BY count DESC;
                '''
            )
            compute_type_distribution = fetch_all(cursor)

            cursor.execute(
                '''
                SELECT COUNT(*)                                      AS count,
                       opendata_main_benchmarkindex.operating_system AS os
                FROM opendata_main_benchmarkindex
                GROUP BY opendata_main_benchmarkindex.operating_system
                ORDER BY count DESC;
                '''
            )
            operating_system_distribution = fetch_all(cursor)

            fastest_total_median_score_cpus = self._query_fastest_total_median_score(
                cursor,
                device_types=('CPU',),
                minimum_number_of_samples_per_benchmark=minimum_number_of_samples_per_benchmark,
                device_name_blacklist=device_name_blacklist,
                blender_version=settings.DEFAULT_BLENDER_VERSION,
            )

            fastest_total_median_score_gpus = self._query_fastest_total_median_score(
                cursor,
                device_types=('GPU',),
                minimum_number_of_samples_per_benchmark=minimum_number_of_samples_per_benchmark,
                device_name_blacklist=device_name_blacklist,
                blender_version=settings.DEFAULT_BLENDER_VERSION,
            )

            cursor.execute(
                '''
                SELECT d.date AS date,
                       count(*) AS count
                FROM (
                    SELECT d::date AS date
                    FROM generate_series(((now() - interval '31 days') AT TIME ZONE 'UTC')::date,
                                         (now() AT TIME ZONE 'UTC')::date, '1 day') d
                ) d
                LEFT JOIN opendata_main_benchmarkindex
                ON (opendata_main_benchmarkindex.submission_date AT TIME ZONE 'UTC')::date = d.date
                GROUP BY d.date;
                '''
            )
            number_of_benchmarks_over_time = fetch_all(cursor)

        return {
            'total_number_of_benchmarks': total_number_of_benchmarks,
            'compute_type_distribution': compute_type_distribution,
            'operating_system_distribution': operating_system_distribution,
            'fastest_total_median_score_cpus': fastest_total_median_score_cpus,
            'fastest_total_median_score_gpus': fastest_total_median_score_gpus,
            'number_of_benchmarks_over_time': number_of_benchmarks_over_time,
        }

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:

        # Build URLs for Top CPU and GPU
        group_by_device_name = ('group_by', 'device_name')
        default_blender_version = ('blender_version', settings.DEFAULT_BLENDER_VERSION)
        url_top_cpus = URL(reverse('benchmarks/query')).with_query(
            [
                ('compute_type', 'CPU'),
                group_by_device_name,
                default_blender_version,
            ]
        )

        url_top_gpus = URL(reverse('benchmarks/query')).with_query(
            [
                ('compute_type', 'OPTIX'),
                ('compute_type', 'CUDA'),
                ('compute_type', 'HIP'),
                ('compute_type', 'METAL'),
                ('compute_type', 'ONEAPI'),
                group_by_device_name,
                default_blender_version,
            ]
        )

        return self.render_to_response(
            {
                **self._get_query_results(),
                'latest_launchers': [
                    {
                        'operating_system': launcher.operating_system,
                        'url': launcher.url,
                        'size': launcher.size,
                        'cli': launcher.cli,
                    }
                    for launcher in Launcher.objects.filter(latest=True)
                ],
                'url_top_cpus': url_top_cpus,
                'url_top_gpus': url_top_gpus,
            }
        )
