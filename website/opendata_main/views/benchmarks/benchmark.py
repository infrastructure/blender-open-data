from typing import List, Dict, cast, SupportsFloat
from dataclasses import dataclass

# TODO(sem): We can probably replace our relatively minor usage of NumPy with
#            native Python; `np.linspace` could be replaced easily and
#            `np.interp` could be replaced using the `bisect` module in the
#            standard library.
import numpy as np
from django.db import connection
from django.views.generic.detail import DetailView

from opendata_main.models.benchmarks import RawBenchmark
from opendata_main.sql import fetch_one
from opendata_main.normalizing import Normalizer


@dataclass
class BenchmarkCommonProps:
    """Properties shared across benchmark scenes."""

    blender_version: str
    os: str
    device_name: str
    username: str
    verified: bool
    device_type: str


class BenchmarkDetailView(DetailView):
    model = RawBenchmark
    template_name = 'opendata_main/benchmarks/benchmark.pug'
    context_object_name = 'raw_benchmark'

    def _get_comparison_data(self, total_score: float) -> Dict[str, object]:
        distribution_device_name_sample_points = 100
        distribution_device_name_percentiles = np.linspace(
            0.0, 1.0, num=distribution_device_name_sample_points
        )

        distribution_graph_sample_points = 400
        distribution_graph_percentiles = np.linspace(0.0, 1.0, num=distribution_graph_sample_points)

        with connection.cursor() as cursor:
            cursor.execute(
                '''
WITH device_samples AS (
         WITH per_device AS (
             SELECT sum(per_device_per_scene.score) AS total_median_score,
                    per_device_per_scene.device_name       AS device_name
             FROM (
                      SELECT avg(opendata_main_benchmarkindex.score) AS score, 
                        opendata_main_benchmarkindex.device_name AS device_name
                      FROM opendata_main_benchmarkindex
                      WHERE opendata_main_benchmarkindex.blender_version = %(blender_version)s
                      GROUP BY opendata_main_benchmarkindex.device_name
                      HAVING count(*) >= 5 -- we require at least five samples per (device_name, scene) combination.
                  ) per_device_per_scene
             GROUP BY per_device_per_scene.device_name
         )
         SELECT DISTINCT ON (per_device.total_median_score)
            per_device.total_median_score   AS total_median_score,
            per_device.device_name          AS device_name
         FROM per_device
                  RIGHT JOIN
              (
                  SELECT unnest(percentile_disc(%(distribution_device_name_percentiles)s)
                                WITHIN GROUP ( ORDER BY per_device.total_median_score))
                  FROM per_device
              ) percentiles(p) ON per_device.total_median_score = percentiles.p
        ORDER BY per_device.total_median_score
     )
SELECT (
           SELECT percentile_cont(%(distribution_graph_percentiles)s)
                  WITHIN GROUP ( ORDER BY opendata_main_benchmarkindex.score DESC )
           FROM opendata_main_benchmarkindex
           WHERE opendata_main_benchmarkindex.blender_version = %(blender_version)s
       ) AS distribution_graph_total_median_score,
       (
           SELECT count(*)
           FROM opendata_main_benchmarkindex
       ) AS number_of_benchmarks,
       (
           SELECT jsonb_agg(json_build_object(
                   'total_median_score',
                   device_samples.total_median_score,
                   'device_name',
                   device_samples.device_name))::json
           FROM device_samples
       ) AS distribution_device_samples
                ''',
                {
                    'distribution_device_name_percentiles': distribution_device_name_percentiles.tolist(),
                    'distribution_graph_percentiles': distribution_graph_percentiles.tolist(),
                    'total_score': total_score,
                    'blender_version': '3.0.1',
                },
            )
            sql_dict = fetch_one(cursor)

        distribution_graph = np.stack(
            (
                distribution_graph_percentiles,
                np.array(cast(List[float], sql_dict['distribution_graph_total_median_score'])),
            )
        )

        reversed_distribution_graph = (
            distribution_graph[0][::-1],
            distribution_graph[1][::-1],
        )

        rank = np.interp(
            total_score,
            reversed_distribution_graph[1],
            reversed_distribution_graph[0],
        )

        current_benchmark = {
            'rank': rank,
            'total_score': total_score,
        }

        return {
            'number_of_benchmarks': sql_dict['number_of_benchmarks'],
            'current_benchmark': current_benchmark,
            'device_samples': [
                {
                    **sample,
                    'rank': np.interp(
                        float(cast(SupportsFloat, sample['total_median_score'])),
                        reversed_distribution_graph[1],
                        reversed_distribution_graph[0],
                    ),
                }
                for sample in cast(
                    List[Dict[str, object]],
                    []
                    if sql_dict['distribution_device_samples'] is None
                    else sql_dict['distribution_device_samples'],
                )
            ],
            'distribution_graph': distribution_graph.T.tolist(),
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # If benchmark is indexed
        context['is_indexed'] = self.object.is_indexed
        context['benchmark'] = Normalizer.normalize(self.object)
        if context['is_indexed']:
            context['comparison_data'] = self._get_comparison_data(
                self.object.indexed_benchmark.score
            )
        return context

