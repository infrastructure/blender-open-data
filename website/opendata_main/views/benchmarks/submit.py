from __future__ import annotations

import json
import logging

import yarl
from django import urls
from django.http.request import HttpRequest
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

from opendata_main.errors import ParseError, SchemaValidationError, InvalidTokenError
from opendata_main.models.benchmarks import RawBenchmarkOwnership
from opendata_main.models.metadata import Launcher
from opendata_main.models.tokens import LauncherAuthenticationToken
from opendata_main.schemas import benchmarks
from opendata_main.views.benchmarks.messages import (
    ResponseEnvelope,
    ParseErrorResponse,
    RequestEnvelope,
    InvalidTokenErrorResponse,
    SubmissionRequest,
    UnhandledRequestResponse,
    InvalidLauncherErrorResponse,
    Response,
    NonVerifiedTokenErrorResponse,
    SchemaValidationErrorResponse,
    SubmissionSuccessfulResponse,
)

logger = logging.getLogger(__name__)


@method_decorator(csrf_exempt, name='dispatch')
class BenchmarkSubmitView(View):
    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        message = self._post(request)
        return ResponseEnvelope.from_response(message).to_json_response()

    def _post(self, request: HttpRequest) -> Response:
        try:
            json_dict = json.loads(request.body)
        except json.JSONDecodeError:
            return ParseErrorResponse('invalid JSON')
        if not isinstance(json_dict, dict):
            return ParseErrorResponse('JSON not a dict')

        try:
            envelope = RequestEnvelope.from_json_dict(json_dict)
        except InvalidTokenError as e:
            return InvalidTokenErrorResponse.from_invalid_token_error(e)
        except ParseError as e:
            return ParseErrorResponse.from_parse_error(e)

        if not isinstance(envelope.message, SubmissionRequest):
            return UnhandledRequestResponse.from_request(envelope.message)

        submission_request = envelope.message

        if not Launcher.objects.filter(
            checksum=submission_request.launcher_checksum, supported=True
        ).exists():
            return InvalidLauncherErrorResponse()

        try:
            token: LauncherAuthenticationToken = LauncherAuthenticationToken.objects.get(
                token=submission_request.token
            )
        except LauncherAuthenticationToken.DoesNotExist:
            return NonVerifiedTokenErrorResponse()

        if not token.verified or token.user is None:
            return NonVerifiedTokenErrorResponse()

        try:
            benchmarks.validate(submission_request.benchmark)
        except SchemaValidationError:
            return SchemaValidationErrorResponse()

        submission_request.benchmark.save()
        RawBenchmarkOwnership.objects.create(
            user=token.user, benchmark=submission_request.benchmark
        )

        return SubmissionSuccessfulResponse(
            benchmark_url=yarl.URL(
                request.build_absolute_uri(
                    urls.reverse(
                        'benchmarks/benchmark', kwargs={'pk': submission_request.benchmark.id}
                    )
                )
            )
        )
