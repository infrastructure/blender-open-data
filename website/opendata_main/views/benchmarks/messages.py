from __future__ import annotations

import abc
import dataclasses as dc
import enum
import json
import uuid
from typing import Dict, Type, cast, NewType, ClassVar

import yarl
from django.http.response import JsonResponse

from opendata_main.errors import ParseError, InvalidTokenError
from opendata_main.models.benchmarks import RawBenchmark
from opendata_main.parsing import parse_field


class Message(abc.ABC):
    pass


class Response(Message, abc.ABC):
    status_code: ClassVar[int]

    @abc.abstractmethod
    def to_json_dict(self) -> Dict[str, object]:
        pass


class Request(Message, abc.ABC):
    @classmethod
    @abc.abstractmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> Request:
        pass


class ErrorResponse(Response, abc.ABC):
    status_code: ClassVar[int] = 400
    message: str

    def to_json_dict(self) -> Dict[str, object]:
        return {'message': self.message}


@dc.dataclass
class ParseErrorResponse(ErrorResponse):
    status_code: ClassVar[int] = 400
    message: str

    @classmethod
    def from_parse_error(cls, parse_error: ParseError) -> ParseErrorResponse:
        return ParseErrorResponse(message=parse_error.message)


@dc.dataclass
class UnhandledRequestResponse(ErrorResponse):
    status_code: ClassVar[int] = 400
    message: str
    message_type: str

    @classmethod
    def from_request(cls, request: Request) -> UnhandledRequestResponse:
        return UnhandledRequestResponse(
            message='unhandled request type', message_type=MessageType.from_message(request).name
        )


SHA256Hex = NewType('SHA256Hex', str)


@dc.dataclass
class SubmissionRequest(Request):
    token: uuid.UUID
    benchmark: RawBenchmark
    launcher_checksum: SHA256Hex

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> SubmissionRequest:
        token_string = parse_field(json_dict, 'token', str)
        try:
            token = uuid.UUID(hex=token_string)
        except ValueError as e:
            raise InvalidTokenError() from e

        return SubmissionRequest(
            token=token,
            benchmark=RawBenchmark.from_json_dict(parse_field(json_dict, 'benchmark', dict)),
            launcher_checksum=SHA256Hex(parse_field(json_dict, 'launcher_checksum', str)),
        )


@dc.dataclass
class SubmissionSuccessfulResponse(Response):
    status_code: ClassVar[int] = 201
    benchmark_url: yarl.URL

    def to_json_dict(self) -> Dict[str, object]:
        return {'benchmark_url': str(self.benchmark_url)}


@dc.dataclass
class InvalidLauncherErrorResponse(ErrorResponse):
    status_code: ClassVar[int] = 400
    message: str = dc.field(default='invalid launcher')


@dc.dataclass
class InvalidTokenErrorResponse(ErrorResponse):
    status_code: ClassVar[int] = 400
    message: str = dc.field(default='invalid token')

    @classmethod
    def from_invalid_token_error(
        cls, invalid_token_error: InvalidTokenError
    ) -> InvalidTokenErrorResponse:
        return InvalidTokenErrorResponse(message=invalid_token_error.message)


@dc.dataclass
class NonVerifiedTokenErrorResponse(ErrorResponse):
    status_code: ClassVar[int] = 403
    message: str = dc.field(default='token not verified')


@dc.dataclass
class SchemaValidationErrorResponse(ErrorResponse):
    status_code: ClassVar[int] = 400
    message: str = dc.field(default='benchmark does not match schema')


class MessageType(enum.Enum):
    @classmethod
    def from_message(cls, message: Message) -> MessageType:
        try:
            return ResponseType(type(message))
        except KeyError:
            try:
                return RequestType(type(message))
            except KeyError:
                raise TypeError(message)


class ResponseType(MessageType):
    parse_error_response: Type[ParseErrorResponse] = ParseErrorResponse
    unhandled_request_response: Type[UnhandledRequestResponse] = UnhandledRequestResponse
    invalid_launcher_error_response: Type[
        InvalidLauncherErrorResponse
    ] = InvalidLauncherErrorResponse
    invalid_token_error_response: Type[InvalidTokenErrorResponse] = InvalidTokenErrorResponse
    non_verified_token_error_response: Type[
        NonVerifiedTokenErrorResponse
    ] = NonVerifiedTokenErrorResponse
    schema_validation_error_response: Type[
        SchemaValidationErrorResponse
    ] = SchemaValidationErrorResponse
    submission_successful_response: Type[
        SubmissionSuccessfulResponse
    ] = SubmissionSuccessfulResponse


class RequestType(MessageType):
    submission_request: Type[SubmissionRequest] = SubmissionRequest


class Envelope(abc.ABC):
    message_type: MessageType
    message: Message


@dc.dataclass
class ResponseEnvelope(Envelope):
    message_type: ResponseType
    message: Response

    @classmethod
    def from_response(cls, response: Response) -> ResponseEnvelope:
        return ResponseEnvelope(message_type=ResponseType(type(response)), message=response)

    def to_json_dict(self) -> Dict[str, object]:
        return {'message_type': self.message_type.name, 'message': self.message.to_json_dict()}

    def serialize(self) -> str:
        return json.dumps(self.to_json_dict())

    def to_json_response(self) -> JsonResponse:
        return JsonResponse(self.to_json_dict(), status=self.message.status_code)


@dc.dataclass
class RequestEnvelope(Envelope):
    message_type: RequestType
    message: Request

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> RequestEnvelope:
        message_type = parse_field(json_dict, 'message_type', str)
        if message_type not in RequestType.__members__:
            raise ParseError(
                f'message_type should be one of {[n for n in RequestType.__members__.keys()]} '
                f'but found {message_type}'
            )
        request_type = RequestType[message_type]
        message_class: Type[Request] = cast(Type[Request], request_type.value)

        message = parse_field(json_dict, 'message', dict)

        return RequestEnvelope(
            message_type=request_type, message=message_class.from_json_dict(message)
        )

    @classmethod
    def deserialize(cls, request_envelope: str) -> RequestEnvelope:
        return RequestEnvelope.from_json_dict(json.loads(request_envelope))
