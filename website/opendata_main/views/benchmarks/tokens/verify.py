from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.db import connection
from django.http import HttpRequest
from django.http.response import HttpResponseBase
from django.views import View
from django.views.generic.base import TemplateResponseMixin
from psycopg2 import sql

from opendata import settings
from opendata_main.models.tokens import LauncherAuthenticationToken
from opendata_main.views.mixins import SingleObjectStoreMixin


class TokenVerifyView(
    LoginRequiredMixin,
    SingleObjectStoreMixin[LauncherAuthenticationToken],
    PermissionRequiredMixin,
    TemplateResponseMixin,
    View,
):
    template_name = 'opendata_main/tokens/verify.pug'
    model = LauncherAuthenticationToken
    context_object_name = 'token'

    def has_permission(self) -> bool:
        """
        Check if the user has permission to verify or reject the token. Which is
        the case iff. the token has not been verified by anyone.

        :return: A boolean indicating if the user has permission to verify or
                 reject the token.
        """
        return self.object.user_id is None and self.object.verified is None

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseBase:
        return self.render_to_response({'token': self.object})

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseBase:
        verify = request.POST.get('verify')
        verified = verify == 'true'

        self.object.verified = verified
        self.object.user = request.user
        self.object.save()

        with connection.cursor() as cursor:
            cursor.execute(
                sql.SQL('NOTIFY {channel}, %(id)s').format(
                    channel=sql.Identifier(
                        settings.LAUNCHER_AUTHENTICATOR_NOTIFICATION_CHANNEL  # type: ignore
                    )
                ),
                {'id': str(self.object.id)},
            )

        return self.render_to_response({'token': self.object})
