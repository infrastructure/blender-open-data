from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import HttpRequest, HttpResponseRedirect
from django.urls.base import reverse
from django.views import View

from opendata_main.models.tokens import LauncherAuthenticationToken
from opendata_main.views.mixins import SingleObjectStoreMixin


class TokenRevokeView(
    LoginRequiredMixin,
    SingleObjectStoreMixin[LauncherAuthenticationToken],
    PermissionRequiredMixin,
    View,
):
    model = LauncherAuthenticationToken

    def has_permission(self) -> bool:
        """
        Check if the user has permission to revoke the token. Which is the case
        iff. he owns the token.

        :return: A boolean indicating if the user has permission to revoke the
                 token.
        """
        return self.object.user == self.request.user

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:
        self.object.delete()
        return HttpResponseRedirect(reverse('users/tokens'))
