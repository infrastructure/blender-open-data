import enum
from typing import Optional, Dict, List

from django.core.exceptions import ValidationError
from django.conf import settings
from django.db import connection
from django.http import Http404, HttpRequest, StreamingHttpResponse
from django.http.response import HttpResponseBase, JsonResponse
from django.shortcuts import render, redirect, reverse
from django.utils.safestring import mark_safe
from django.views.generic.base import View
from simplejson import JSONEncoder
from yarl import URL

from opendata_main.parsing import parse_enum, ParseError
from opendata_main.query_builder import queries
from opendata_main.query_builder.queries import Columns


def parse_group_by(group_by_qs: List[str]) -> List[Columns]:
    group_by: List[Columns] = []

    for g_qs in group_by_qs:
        g = parse_enum(Columns, 'group_by', g_qs)
        if not g.value.groupable:
            raise ValidationError(
                f'Invalid value for group_by "{g_qs}",' f' column is not groupable.'
            )
        group_by.append(g)

    return group_by


class ResponseType(enum.Enum):
    html: str = 'html'
    datatables: str = 'datatables'


def parse_response_type(response_type_qs: Optional[str]) -> ResponseType:
    if response_type_qs:
        return parse_enum(ResponseType, 'response_type', response_type_qs)
    else:
        return ResponseType.html


class BenchmarkQueryView(View):
    def get(self, request: HttpRequest, *args: object, **kwargs: object) -> HttpResponseBase:
        filters: Dict[Columns, List[str]] = {
            c: request.GET.getlist(c.name, []) for c in Columns if c.value.filterable
        }

        try:
            group_by = parse_group_by([qs for qs in request.GET.getlist('group_by', []) if qs])
        except (ParseError, ValidationError) as e:
            raise Http404(str(e))

        if not request.GET:
            # If no query args are present in the request, we force grouping by device_name.
            # This is done through a redirect so that the natural language form is properly
            # populated.
            url_query_default = URL(reverse('benchmarks/query')).with_query(
                [('group_by', 'device_name'), ('blender_version', settings.DEFAULT_BLENDER_VERSION)]
            )
            return redirect(str(url_query_default))

        response_type = parse_response_type(request.GET.get('response_type'))

        if response_type == ResponseType.html:
            results_url = URL(request.get_full_path()).with_query(
                [(c.name, e) for c, es in filters.items() for e in es]
            )
            if group_by:
                results_url = results_url.update_query([('group_by', g.name) for g in group_by])

            results_url = results_url.update_query({'response_type': 'datatables'})

            return render(
                request,
                'opendata_main/benchmarks/query.pug',
                {
                    'groupable_columns': [
                        {
                            'value': c.name,
                            'label': c.value.display_name,
                        }
                        for c in Columns
                        if c.value.groupable
                    ],
                    'filters': {c: e for c, e in filters.items()},
                    'group_by': group_by,
                    'results_datatables_url': mark_safe(results_url),
                },
            )
        else:
            bindings, query = queries.query(
                filters=filters,
                group_by=group_by,
            )

            with connection.cursor() as cursor:
                cursor.execute(query, bindings.parameters)
                columns = [Columns[c[0]] for c in cursor.description]

                if response_type == ResponseType.datatables:
                    response = StreamingHttpResponse(
                        JSONEncoder(iterable_as_array=True).iterencode(  # type: ignore
                            {
                                'columns': [
                                    {
                                        'display_name': c.value.display_name,
                                        'sortable': c.value.sortable,
                                        'filterable': c.value.filterable,
                                    }
                                    for c in columns
                                ],
                                'rows': cursor.fetchall(),
                            }
                        ),
                        content_type='application/json',
                    )
                    return response
                else:
                    raise ValueError(response_type)


class BenchmarkQuerySuggestView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        column = parse_enum(Columns, 'column', request.GET.get('column', ''))
        expression = request.GET.get('expression', '')

        bindings, query = queries.suggest(column.value, expression, limit=10)

        with connection.cursor() as cursor:
            cursor.execute(query, bindings.parameters)
            suggestions = [{'label': row[0], 'value': row[0]} for row in cursor.fetchall()]

        return JsonResponse(suggestions, safe=False)
