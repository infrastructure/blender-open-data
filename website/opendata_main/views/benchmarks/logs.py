import gzip
from pathlib import Path
from typing import cast

from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

from opendata import settings


@method_decorator(csrf_exempt, name='dispatch')
class BenchmarkLogsView(View):
    def post(self, request: HttpRequest, *args: object, **kwargs: object) -> HttpResponse:
        sentry_event_id = cast(str, kwargs['sentry_event_id'])

        launcher_logs_directory = Path(settings.LAUNCHER_LOGS_DIRECTORY)
        launcher_logs_directory.mkdir(parents=True, exist_ok=True)

        log_path = launcher_logs_directory / f'{sentry_event_id}.gz'

        if log_path.exists():
            return JsonResponse({'error': 'Log already exists'}, status=400)

        log_size: int = 0
        with gzip.GzipFile(log_path, mode='wb') as log_file:
            for chunk in request:
                log_size += len(chunk)
                if log_size > settings.LAUNCHER_LOGS_MAX_BYTES:
                    return JsonResponse({'error': 'Maximum log size exceeded'}, status=400)

                log_file.write(chunk)

        return JsonResponse({}, status=201)
