from django.http.request import HttpRequest
from django.http.response import JsonResponse
from django.views.generic.base import View

from opendata_main.models.metadata import BlenderVersion, Launcher


class MetadataView(View):
    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        return JsonResponse(
            {
                'message_type': 'metadata_response',
                'message': {
                    'launchers': [
                        {
                            'label': launcher.label,
                            'url': launcher.url,
                            'checksum': launcher.checksum,
                            'size': launcher.size,
                            'operating_system': launcher.operating_system,
                            'supported': launcher.supported,
                            'latest': launcher.latest,
                            'cli': launcher.cli,
                        }
                        for launcher in Launcher.objects.filter(supported=True)
                    ],
                    'blender_versions': [
                        {
                            'label': blender_version.label,
                            'url': blender_version.url,
                            'checksum': blender_version.checksum,
                            'size': blender_version.size,
                            'operating_system': blender_version.operating_system,
                            'architecture': blender_version.architecture,
                            'benchmark_script': {
                                'label': blender_version.benchmark_script.label,
                                'url': blender_version.benchmark_script.url,
                                'checksum': blender_version.benchmark_script.checksum,
                                'size': blender_version.benchmark_script.size,
                            },
                            'available_scenes': [
                                {
                                    'label': scene.label,
                                    'url': scene.url,
                                    'checksum': scene.checksum,
                                    'size': scene.size,
                                }
                                for scene in blender_version.scenes.all()
                            ],
                        }
                        for blender_version in BlenderVersion.objects.filter(is_supported=True).order_by('-label')
                        .all()
                        .prefetch_related('benchmark_script', 'scenes')
                    ],
                },
            }
        )
