from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView

from opendata_main.models.tokens import LauncherAuthenticationToken


class UserTokensView(LoginRequiredMixin, TemplateView):
    template_name = 'opendata_main/users/tokens.pug'

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        tokens = LauncherAuthenticationToken.objects.filter(user=request.user).order_by(
            '-created_at'
        )
        return self.render_to_response({'tokens': tokens})
