from typing import List, Dict

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView

from opendata_main.models.benchmarks import RawBenchmark, BenchmarkIndex
from opendata_main.normalizing import Normalizer


class UserBenchmarksView(LoginRequiredMixin, TemplateView):
    template_name = 'opendata_main/users/benchmarks.pug'
    raw_benchmarks: List[Dict[str, object]]

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:

        raw_benchmarks = (
            RawBenchmark.objects.filter(ownership__user=self.request.user)
            .prefetch_related('indexed_benchmark')
            .order_by('-created_at')
        )
        benchmarks = []
        for raw_benchmark in raw_benchmarks:
            benchmarks.append(Normalizer.normalize(raw_benchmark))

        return self.render_to_response({'benchmarks': benchmarks})
