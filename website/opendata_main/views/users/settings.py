from typing import Optional

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import QuerySet
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView

from opendata_main.models.user_settings import UserSettings


class UserSettingsForm(forms.Form):
    benchmarks_public = forms.BooleanField(required=False)
    benchmarks_name = forms.CharField()


class UserSettingsView(LoginRequiredMixin, TemplateView):
    model = UserSettings
    fields = ['benchmarks_anonymous', 'benchmarks_name']
    template_name = 'opendata_main/users/settings.pug'
    context_object_name = 'settings'

    def get_object(self, queryset: 'Optional[QuerySet[UserSettings]]' = None) -> UserSettings:
        return UserSettings.get_or_create(user=self.request.user)

    def get(self, request: HttpRequest, *args: object, **kwargs: object) -> HttpResponse:
        settings = self.get_object()
        return self.render_to_response(
            {
                'settings': {
                    'benchmarks_public': not settings.benchmarks_anonymous,
                    'benchmarks_name': settings.benchmarks_name,
                }
            }
        )

    def post(self, request: HttpRequest, *args: object, **kwargs: object) -> HttpResponse:
        settings = self.get_object()

        form = UserSettingsForm(request.POST)

        if form.is_valid():
            settings.benchmarks_name = form.cleaned_data['benchmarks_name']
            settings.benchmarks_anonymous = not form.cleaned_data['benchmarks_public']
            settings.save()

        return self.render_to_response(
            {
                'settings': {
                    'benchmarks_public': not settings.benchmarks_anonymous,
                    'benchmarks_name': settings.benchmarks_name,
                }
            }
        )
