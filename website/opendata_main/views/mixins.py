from typing import Generic, TypeVar, cast

from django.db import models
from django.http.request import HttpRequest
from django.views import View
from django.views.generic.detail import SingleObjectMixin

ModelType = TypeVar('ModelType', bound=models.Model)


class SingleObjectStoreMixin(SingleObjectMixin, Generic[ModelType], View):
    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)  # type: ignore
        self.object: ModelType = cast(ModelType, self.get_object())
