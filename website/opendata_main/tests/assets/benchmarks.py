import functools
import json
from pathlib import Path
from typing import Dict, cast


@functools.lru_cache(maxsize=1)
def v1() -> Dict[str, object]:
    with open(Path(__file__).parent / 'benchmark-v1.json') as f:
        return cast(Dict[str, object], json.load(f))


@functools.lru_cache(maxsize=1)
def v2() -> Dict[str, object]:
    with open(Path(__file__).parent / 'benchmark-v2.json') as f:
        return cast(Dict[str, object], json.load(f))


@functools.lru_cache(maxsize=1)
def v3() -> Dict[str, object]:
    with open(Path(__file__).parent / 'benchmark-v3.json') as f:
        return cast(Dict[str, object], json.load(f))


@functools.lru_cache(maxsize=1)
def v4() -> Dict[str, object]:
    with open(Path(__file__).parent / 'benchmark-v4.json') as f:
        return cast(Dict[str, object], json.load(f))
