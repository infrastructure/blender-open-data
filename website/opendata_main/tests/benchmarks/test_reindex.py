from decimal import Decimal
from typing import List, Dict

from django import test
from django.contrib.auth.models import User
from django.db import connection

from opendata_main.models.benchmarks import (
    RawBenchmark,
    BenchmarkSchemaVersion,
    RawBenchmarkOwnership,
)
from opendata_main.sql import fetch_all
from opendata_main.tests import assets


class ReindexTest(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.owner_user_settings = self.owner_user.settings  # type: ignore

        self.owner_user_settings.benchmarks_verified = True
        self.owner_user_settings.benchmarks_anonymous = False
        self.owner_user_settings.benchmarks_name = 'benchmark_hero_007'
        self.owner_user_settings.save()

    def _get_benchmarks(self) -> List[Dict[str, object]]:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmark;')
            return fetch_all(cursor)

    def test_reindex_v1(self) -> None:
        data = assets.benchmarks.v1()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v1.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        self.benchmarks = [
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 0,
                'render_time': Decimal('116.766'),
                'peak_memory_usage': Decimal('142.13'),
                'device_type': 'CPU',
                'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                'os': 'Linux',
                'benchmark': 'barbershop_interior',
                'blender_version': '2.79',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('16.60698531319736364120'),
            },
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 1,
                'render_time': Decimal('116.766'),
                'peak_memory_usage': Decimal('142.13'),
                'device_type': 'CPU',
                'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                'os': 'Linux',
                'benchmark': 'victor',
                'blender_version': '2.79',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('0'),
            },
        ]

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE opendata_main_benchmark;')

        self.assertCountEqual([], self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('SELECT opendata_main_reindex_benchmarks();')

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

    def test_reindex_v2(self) -> None:
        data = assets.benchmarks.v2()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v2.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        self.benchmarks = [
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 0,
                'render_time': Decimal('116.766'),
                'peak_memory_usage': Decimal('142.13'),
                'device_type': 'CPU',
                'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                'os': 'Linux',
                'benchmark': 'barbershop_interior',
                'blender_version': '2.79',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('16.60698531319736364120'),
            },
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 1,
                'render_time': Decimal('116.766'),
                'peak_memory_usage': Decimal('142.13'),
                'device_type': 'CPU',
                'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                'os': 'Linux',
                'benchmark': 'victor',
                'blender_version': '2.79',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('0'),
            },
        ]

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE opendata_main_benchmark;')

        self.assertCountEqual([], self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('SELECT opendata_main_reindex_benchmarks();')

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

    def test_reindex_v3(self) -> None:
        data = assets.benchmarks.v3()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v3.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        self.benchmarks = [
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 0,
                'render_time': Decimal('2.98291'),
                'peak_memory_usage': Decimal('78.25'),
                'device_type': 'CPU',
                'device_name': 'AMD Ryzen 7 1800X Eight-Core Processor',
                'os': 'Linux',
                'benchmark': 'victor',
                'blender_version': '2.80',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('0'),
            },
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 1,
                'render_time': Decimal('2.98291'),
                'peak_memory_usage': Decimal('78.25'),
                'device_type': 'CPU',
                'device_name': 'AMD BLA Ryzen 7 1800X Eight-Core Processor',
                'os': 'Linux',
                'benchmark': 'not victor',
                'blender_version': '2.80',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('0'),
            },
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 2,
                'render_time': Decimal('116.766'),
                'peak_memory_usage': Decimal('142.13'),
                'device_type': 'CPU',
                'device_name': 'AMD Ryzen 7 1800X Eight-Core Processor',
                'os': 'Linux',
                'benchmark': 'barbershop_interior',
                'blender_version': '2.80',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('16.60698531319736364120'),
            },
        ]

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE opendata_main_benchmark;')

        self.assertCountEqual([], self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('SELECT opendata_main_reindex_benchmarks();')

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

    def test_reindex_v4(self) -> None:
        data = assets.benchmarks.v4()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v4.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        self.benchmarks = [
            {
                'benchmark_id': self.raw_benchmark.id,
                'benchmark_sample_index': 0,
                'render_time': Decimal('32.7422'),
                'peak_memory_usage': Decimal('147.05'),
                'device_type': 'CPU',
                'device_name': 'Intel(R) Core(TM) i7-8700B CPU @ 3.20GHz',
                'os': 'Darwin',
                'benchmark': 'bmw27',
                'blender_version': '3.0.1',
                'verified': True,
                'username': 'benchmark_hero_007',
                'score': Decimal('150.23445'),
            },
        ]

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE opendata_main_benchmark;')

        self.assertCountEqual([], self._get_benchmarks())

        with connection.cursor() as cursor:
            cursor.execute('SELECT opendata_main_reindex_benchmarks();')

        self.assertCountEqual(self.benchmarks, self._get_benchmarks())
