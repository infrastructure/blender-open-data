from typing import List, Dict

from django import test
from django.contrib.auth.models import User
from django.db import connection

from opendata_main.models.benchmarks import (
    RawBenchmark,
    BenchmarkSchemaVersion,
    RawBenchmarkOwnership,
)
from opendata_main.indexing import BenchmarkIndexer
from opendata_main.sql import fetch_all
from opendata_main.tests import assets


class ReindexTest(test.TestCase):

    maxDiff = None

    def setUp(self) -> None:
        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.owner_user_settings = self.owner_user.settings  # type: ignore

        self.owner_user_settings.benchmarks_anonymous = False
        self.owner_user_settings.benchmarks_name = 'benchmark_hero_007'
        self.owner_user_settings.save()

    def _get_benchmarks(self) -> List[Dict[str, object]]:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmarkindex;')
            return fetch_all(cursor)

    def test_reindex_v1(self) -> None:
        # Do not index v1
        data = assets.benchmarks.v1()
        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v1.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )
        self.assertCountEqual([], self._get_benchmarks())

    def test_reindex_v3(self) -> None:
        # Do not index v3
        data = assets.benchmarks.v3()
        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v3.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )
        self.assertCountEqual([], self._get_benchmarks())

    def test_reindex_v4(self) -> None:
        data = assets.benchmarks.v4()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v4.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        self.expected_benchmarks = [
            {
                'raw_benchmark_id': self.raw_benchmark.id,
                'score': 150.23445,
                'device_name': 'Intel Core i7-8700B CPU @ 3.20GHz',
                'device_type': 'CPU',
                'compute_type': 'CPU',
                'operating_system': 'macOS',
                'blender_version': '3.0.1',
                'is_verified': False,
                'username': 'benchmark_hero_007',
                'submission_date': self.raw_benchmark.created_at,
            },
        ]

        self.assertCountEqual(self.expected_benchmarks, self._get_benchmarks())

    def test_reindex_v4_marked_invalid(self) -> None:
        data = assets.benchmarks.v4()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data,
            schema_version=BenchmarkSchemaVersion.v4.name,
        )

        self.assertEqual(1, len(self._get_benchmarks()))
        self.raw_benchmark.is_valid = False
        self.raw_benchmark.save()
        self.raw_benchmark.refresh_from_db()

        self.assertCountEqual([], self._get_benchmarks())

        self.raw_benchmark.is_valid = True
        self.raw_benchmark.save()

        self.assertEqual(1, len(self._get_benchmarks()))


class IndexerLegacyBenchmarkTest(test.TestCase):

    maxDiff = None

    def setUp(self) -> None:
        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.owner_user_settings = self.owner_user.settings  # type: ignore

        self.owner_user_settings.benchmarks_anonymous = False
        self.owner_user_settings.benchmarks_name = 'benchmark_hero_007'
        self.owner_user_settings.save()

    def test_get_index_v3(self) -> None:
        data = assets.benchmarks.v3()
        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v3.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )
        b = BenchmarkIndexer._get_index_v3(self.raw_benchmark)
        print(b.score)
