import sys
from io import StringIO
from tempfile import NamedTemporaryFile

from django import test
from django.contrib.auth.models import User
from django.core import management
from django.db import connection

from opendata_main.models.benchmarks import (
    RawBenchmark,
    BenchmarkSchemaVersion,
)
from opendata_main.tests import assets


class SnapshotTest(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.raw_benchmarks = [
            RawBenchmark.objects.create(
                data=assets.benchmarks.v1(), schema_version=BenchmarkSchemaVersion.v1.name
            ),
            RawBenchmark.objects.create(
                data=assets.benchmarks.v2(), schema_version=BenchmarkSchemaVersion.v2.name
            ),
            RawBenchmark.objects.create(
                data=assets.benchmarks.v3(), schema_version=BenchmarkSchemaVersion.v3.name
            ),
        ]

    def test_snapshot(self) -> None:
        self.assertCountEqual(self.raw_benchmarks, RawBenchmark.objects.all())

        snapshot = StringIO()
        management.call_command('dump_snapshot', stdout=snapshot)

        with connection.cursor() as cursor:
            cursor.execute('TRUNCATE opendata_main_rawbenchmark CASCADE;')

        self.assertCountEqual([], RawBenchmark.objects.all())

        with NamedTemporaryFile(mode='w+', encoding='utf-8') as f:
            snapshot.seek(0)
            f.write(snapshot.read())
            f.flush()
            management.call_command('import_snapshot', f.name)

        self.assertCountEqual(self.raw_benchmarks, RawBenchmark.objects.all())
