from decimal import Decimal
from typing import List, Dict

from django import test
from django.contrib.auth.models import User
from django.db import connection

from opendata_main.models.benchmarks import (
    RawBenchmark,
    BenchmarkSchemaVersion,
    RawBenchmarkOwnership,
)
from opendata_main.sql import fetch_all
from opendata_main.tests import assets


class VerifiedUsersTest(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.owner_user_settings = self.owner_user.settings  # type: ignore

        self.other_user: User = User.objects.create_user(
            username='other_user',
            email='test-other@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.other_user_settings = self.other_user.settings  # type: ignore

        data = assets.benchmarks.v4()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v4.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

    def test_verified_user(self) -> None:

        self.other_user_settings.benchmarks_verified = False
        self.other_user_settings.save()

        self.raw_benchmark.refresh_from_db()
        self.assertFalse(self.raw_benchmark.indexed_benchmark.is_verified)

        self.owner_user_settings.benchmarks_verified = True
        self.owner_user_settings.save()
        self.raw_benchmark.refresh_from_db()
        self.assertTrue(self.raw_benchmark.indexed_benchmark.is_verified)

    def test_username_vs_anonymous_display(self) -> None:

        # Updating public name reflects on indexed benchmarks
        self.owner_user_settings.benchmarks_name = 'koro'
        self.owner_user_settings.benchmarks_anonymous = False
        self.owner_user_settings.save()
        rb = RawBenchmark.objects.first()
        self.assertEqual('koro', rb.indexed_benchmark.username)

        # Setting benchmarks to anonymous removes name from indexed
        self.owner_user_settings.benchmarks_anonymous = True
        self.owner_user_settings.save()
        rb.refresh_from_db()
        self.assertEqual('', rb.indexed_benchmark.username)
