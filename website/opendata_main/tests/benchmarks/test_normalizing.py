from decimal import Decimal
from django import test
from django.contrib.auth.models import User

from opendata_main.normalizing import DeviceNameFormatter
from opendata_main.tests import assets
from opendata_main.models.benchmarks import (
    RawBenchmark,
    BenchmarkSchemaVersion,
    RawBenchmarkOwnership,
)
from opendata_main.normalizing import Normalizer, NormalizedBenchmark


class DeviceNameFormatterTest(test.TestCase):
    def test_strip_tm_and_r(self):
        device_names = {
            'Intel(R) Core(TM) i7-8700B CPU @ 3.20GHz': 'Intel Core i7-8700B CPU @ 3.20GHz',
            'Intel® Core™ i7-8700B CPU @ 3.20GHz': 'Intel Core i7-8700B CPU @ 3.20GHz',
        }
        for k, v in device_names.items():
            self.assertEqual(v, DeviceNameFormatter.strip_tm_and_r(k))
            self.assertEqual(v, DeviceNameFormatter.format(k))

    def test_prepend_nvidia(self):
        device_names = {
            'Quadro RTX 8000': 'NVIDIA Quadro RTX 8000',
            'NVIDIA Quadro RTX 8000': 'NVIDIA Quadro RTX 8000',
            'GeForce RTX 3090': 'NVIDIA GeForce RTX 3090',
            'Tesla K20Xm': 'NVIDIA Tesla K20Xm',
        }
        for k, v in device_names.items():
            self.assertEqual(v, DeviceNameFormatter.prepend_nvidia(k))
            self.assertEqual(v, DeviceNameFormatter.format(k))

    def test_prepend_amd(self):
        device_names = {
            'Radeon RX 5500 XT': 'AMD Radeon RX 5500 XT',
        }
        for k, v in device_names.items():
            self.assertEqual(v, DeviceNameFormatter.prepend_amd(k))
            self.assertEqual(v, DeviceNameFormatter.format(k))

    def test_remove_intel_gen(self):
        device_names = {
            '11th Gen Intel Core i9-11900K @ 3.50GHz': 'Intel Core i9-11900K @ 3.50GHz',
            '12th Gen Intel Core i9-12900K @ 3.50GHz': 'Intel Core i9-12900K @ 3.50GHz',
        }
        for k, v in device_names.items():
            self.assertEqual(v, DeviceNameFormatter.remove_intel_gen(k))
            self.assertEqual(v, DeviceNameFormatter.format(k))


class SerializerTest(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.owner_user_settings = self.owner_user.settings  # type: ignore

        self.owner_user_settings.benchmarks_verified = True
        self.owner_user_settings.benchmarks_anonymous = False
        self.owner_user_settings.benchmarks_name = 'benchmark_hero_007'
        self.owner_user_settings.save()

    def test_v2(self):
        data = assets.benchmarks.v2()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v2.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        serialized_benchmark = Normalizer.normalize(self.raw_benchmark)
        self.assertEqual('Linux', serialized_benchmark.operating_system)
        self.assertEqual(644153.1679627256, serialized_benchmark.score)
        self.assertIn('barbershop_interior', serialized_benchmark.score_scenes)

    def test_v3(self):
        data = assets.benchmarks.v3()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v3.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        serialized_benchmark = Normalizer.normalize(self.raw_benchmark)
        self.assertEqual('Linux', serialized_benchmark.operating_system)
        self.assertEqual(14073.83552461096, serialized_benchmark.score)

    def test_v4(self):
        data = assets.benchmarks.v4()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v4.name
        )
        self.raw_benchmark_ownership = RawBenchmarkOwnership.objects.create(
            user=self.owner_user, benchmark=self.raw_benchmark
        )

        serialized_benchmark = Normalizer.normalize(self.raw_benchmark)
        self.assertEqual('macOS', serialized_benchmark.operating_system)
        self.assertEqual(150.23445, serialized_benchmark.score)
