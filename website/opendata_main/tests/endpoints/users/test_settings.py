import yarl
from django import test
from django.contrib.auth.models import User
from django.urls import reverse


class UsersSettingsTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.client.force_login(self.user)

        self.user_settings = self.user.settings  # type: ignore

        self.url = reverse('users/settings')

    def test_get(self) -> None:
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_needs_login(self) -> None:
        self.client.logout()

        response = self.client.post(self.url, {'verify': 'true'})
        self.assertRedirects(
            response,
            str(yarl.URL(reverse('oauth:login')).with_query(next=self.url)),
            target_status_code=302,
        )

    def test_cannot_set_verified(self) -> None:
        self.user_settings.benchmarks_verified = False
        self.user_settings.save()

        response = self.client.post(
            self.url, data={'benchmarks_verified': 'on', 'benchmarks_name': 'dfgdf'}
        )
        self.assertEqual(200, response.status_code)

        self.user_settings.refresh_from_db()
        self.assertFalse(self.user_settings.benchmarks_verified)

    def test_set_not_anonymous(self) -> None:
        self.user_settings.benchmarks_anonymous = True
        self.user_settings.save()

        response = self.client.post(
            self.url, data={'benchmarks_public': True, 'benchmarks_name': 'fgfd'}
        )
        self.assertEqual(200, response.status_code)

        self.user_settings.refresh_from_db()
        self.assertFalse(self.user_settings.benchmarks_anonymous)

    def test_set_anonymous(self) -> None:
        self.user_settings.benchmarks_anonymous = False
        self.user_settings.save()

        response = self.client.post(self.url, data={'benchmarks_name': 'fgfd'})
        self.assertEqual(200, response.status_code)

        self.user_settings.refresh_from_db()
        self.assertTrue(self.user_settings.benchmarks_anonymous)
