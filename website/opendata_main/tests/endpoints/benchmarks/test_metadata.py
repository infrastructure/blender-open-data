from django import test
from django.urls import reverse

from opendata_main.models.metadata import Launcher, BenchmarkScript, Scene, BlenderVersion


class BenchmarksMetadataTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.supported_launcher: Launcher = Launcher.objects.create(
            label='launcher 2.80',
            operating_system='linux',
            url='http://opendata.local/launcher_supported.exe',
            checksum='legitchecksum0',
            size=100,
            supported=True,
            latest=True,
            cli=False,
        )

        self.unsupported_launcher: Launcher = Launcher.objects.create(
            label='launcher 2.80 (unsupported)',
            operating_system='linux',
            url='http://opendata.local/launcher_unsupported.exe',
            checksum='legitchecksum1',
            size=100,
            supported=False,
            latest=False,
            cli=True,
        )

        self.benchmark_script: BenchmarkScript = BenchmarkScript.objects.create(
            label='script 2.80',
            url='http://opendata.local/benchmark_script.tar.gz',
            checksum='legitchecksum2',
            size=101,
        )

        self.available_scene: Scene = Scene.objects.create(
            label='scene 1',
            url='http://opendata.local/scene_1.tar.gz',
            checksum='legitchecksum3',
            size=102,
        )

        self.unavailable_scene: Scene = Scene.objects.create(
            label='scene 2',
            url='http://opendata.local/scene_2.tar.gz',
            checksum='legitchecksum4',
            size=103,
        )

        self.blender_version: BlenderVersion = BlenderVersion.objects.create(
            label='blender 2.80',
            operating_system='linux',
            url='http://opendata.local/blender_2_80_linux.tar.gz',
            checksum='legitchecksum5',
            size=104,
            benchmark_script=self.benchmark_script,
        )
        self.blender_version.scenes.add(self.available_scene)
        self.blender_version.save()

        self.url = reverse('benchmarks/metadata')

    def test_get(self) -> None:
        response = self.client.get(self.url, {'verify': 'true'})
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            {
                'message_type': 'metadata_response',
                'message': {
                    'launchers': [
                        {
                            'label': 'launcher 2.80',
                            'url': 'http://opendata.local/launcher_supported.exe',
                            'checksum': 'legitchecksum0',
                            'size': 100,
                            'operating_system': 'linux',
                            'supported': True,
                            'latest': True,
                            'cli': False,
                        }
                    ],
                    'blender_versions': [
                        {
                            'label': 'blender 2.80',
                            'url': 'http://opendata.local/blender_2_80_linux.tar.gz',
                            'checksum': 'legitchecksum5',
                            'size': 104,
                            'operating_system': 'linux',
                            'benchmark_script': {
                                'label': 'script 2.80',
                                'url': 'http://opendata.local/benchmark_script.tar.gz',
                                'checksum': 'legitchecksum2',
                                'size': 101,
                            },
                            'available_scenes': [
                                {
                                    'label': 'scene 1',
                                    'url': 'http://opendata.local/scene_1.tar.gz',
                                    'checksum': 'legitchecksum3',
                                    'size': 102,
                                }
                            ],
                        }
                    ],
                },
            },
            response.json(),
        )
