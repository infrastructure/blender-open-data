import json

from django import test
from django.http import StreamingHttpResponse
from django.urls.base import reverse
from django.utils.datastructures import MultiValueDict

from opendata_main.models.benchmarks import RawBenchmark, BenchmarkSchemaVersion
from opendata_main.tests import assets


class BenchmarksQueryTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        data = assets.benchmarks.v1()

        self.raw_benchmark = RawBenchmark.objects.create(
            data=data, schema_version=BenchmarkSchemaVersion.v1.name
        )

    def test_get(self) -> None:
        response = self.client.get(reverse('benchmarks/query'))
        self.assertEqual(200, response.status_code)

        response = self.client.get(
            reverse('benchmarks/query'), data={'response_type': 'datatables'},
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Device Type', 'sortable': True, 'filterable': True},
                    {'display_name': 'Operating System', 'sortable': True, 'filterable': True},
                    {'display_name': 'Scene', 'sortable': True, 'filterable': True},
                    {'display_name': 'Blender Version', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'barbershop_interior',
                        '2.79',
                        116.766,
                        1,
                    ],
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'victor',
                        '2.79',
                        116.766,
                        1,
                    ],
                ],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )

        response = self.client.get(
            reverse('benchmarks/query'),
            data={'response_type': 'datatables', 'group_by': 'device_name'},
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [['Intel Xeon CPU E5-2699 v4 @ 2.20GHz', 116.766, 2]],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )

        response = self.client.get(
            reverse('benchmarks/query'),
            data={'response_type': 'datatables', 'benchmark': ['barbershop']},
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Device Type', 'sortable': True, 'filterable': True},
                    {'display_name': 'Operating System', 'sortable': True, 'filterable': True},
                    {'display_name': 'Scene', 'sortable': True, 'filterable': True},
                    {'display_name': 'Blender Version', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'barbershop_interior',
                        '2.79',
                        116.766,
                        1,
                    ],
                ],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )

        response = self.client.get(
            reverse('benchmarks/query'),
            data={'response_type': 'datatables', 'benchmark': ['barbershop', 'victor']},
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Device Type', 'sortable': True, 'filterable': True},
                    {'display_name': 'Operating System', 'sortable': True, 'filterable': True},
                    {'display_name': 'Scene', 'sortable': True, 'filterable': True},
                    {'display_name': 'Blender Version', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'barbershop_interior',
                        '2.79',
                        116.766,
                        1,
                    ],
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'victor',
                        '2.79',
                        116.766,
                        1,
                    ],
                ],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )

        response = self.client.get(
            reverse('benchmarks/query'),
            data={
                'response_type': 'datatables',
                'benchmark': ['barbershop', 'victor'],
                'device_type': ['CPU'],
            },
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Device Type', 'sortable': True, 'filterable': True},
                    {'display_name': 'Operating System', 'sortable': True, 'filterable': True},
                    {'display_name': 'Scene', 'sortable': True, 'filterable': True},
                    {'display_name': 'Blender Version', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'barbershop_interior',
                        '2.79',
                        116.766,
                        1,
                    ],
                    [
                        'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                        'CPU',
                        'Linux',
                        'victor',
                        '2.79',
                        116.766,
                        1,
                    ],
                ],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )

        response = self.client.get(
            reverse('benchmarks/query'),
            data=MultiValueDict(
                {'response_type': 'datatables', 'group_by': ['device_name', 'benchmark']}
            ),
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.streaming)
        assert isinstance(response, StreamingHttpResponse)
        self.assertEqual(
            {
                'columns': [
                    {'display_name': 'Device Name', 'sortable': True, 'filterable': True},
                    {'display_name': 'Scene', 'sortable': True, 'filterable': True},
                    {'display_name': 'Median Render Time', 'sortable': True, 'filterable': False},
                    {'display_name': 'Number of Benchmarks', 'sortable': True, 'filterable': False},
                ],
                'rows': [
                    ['Intel Xeon CPU E5-2699 v4 @ 2.20GHz', 'barbershop_interior', 116.766, 1],
                    ['Intel Xeon CPU E5-2699 v4 @ 2.20GHz', 'victor', 116.766, 1],
                ],
            },
            json.loads(b''.join(response.streaming_content).decode('utf-8')),
        )
