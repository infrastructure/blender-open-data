import json
import uuid
from decimal import Decimal
from typing import List, Dict

import yarl
from django import test, urls
from django.contrib.auth.models import User
from django.db import connection
from django.urls import reverse

from opendata_main.models.benchmarks import RawBenchmark, RawBenchmarkOwnership
from opendata_main.models.metadata import Launcher
from opendata_main.models.tokens import LauncherAuthenticationToken
from opendata_main.sql import fetch_all
from opendata_main.tests import assets


class BenchmarksSubmitTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.token: LauncherAuthenticationToken = LauncherAuthenticationToken.objects.create(
            user=self.owner_user, hostname='test', verified=True
        )

        self.launcher: Launcher = Launcher.objects.create(
            label='2.80',
            operating_system='linux',
            url='http://opendata.local:8002/launcher.exe',
            checksum='verylegitchecksum',
            size='1000',
            supported=True,
            latest=True,
            cli=False,
        )

        self.url = reverse('benchmarks/submit')

        self.data_v1 = assets.benchmarks.v1()
        self.data_v2 = assets.benchmarks.v2()
        self.data_v3 = assets.benchmarks.v3()
        self.data_v4 = assets.benchmarks.v4()

        # Authentication happens using the token so users should not have to be logged in.
        self.client.logout()

    def test_submit_unsupported_launcher(self) -> None:
        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': 'wrong_checksum',
                        'benchmark': {'schema_version': 'v1', 'data': self.data_v1},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            {
                'message_type': 'invalid_launcher_error_response',
                'message': {'message': 'invalid launcher'},
            },
            response.json(),
        )

        self.launcher.supported = False
        self.launcher.latest = False
        self.launcher.save()

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v1', 'data': self.data_v1},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            {
                'message_type': 'invalid_launcher_error_response',
                'message': {'message': 'invalid launcher'},
            },
            response.json(),
        )

    def test_submit_invalid_token(self) -> None:
        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': 'invalid',
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v1', 'data': self.data_v1},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            {
                'message_type': 'invalid_token_error_response',
                'message': {'message': 'token is invalid'},
            },
            response.json(),
        )

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(uuid.uuid4()),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v1', 'data': self.data_v1},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(403, response.status_code)
        self.assertEqual(
            {
                'message_type': 'non_verified_token_error_response',
                'message': {'message': 'token not verified'},
            },
            response.json(),
        )

    def test_submit_rejected_token(self) -> None:
        def test() -> None:
            response = self.client.post(
                self.url,
                json.dumps(
                    {
                        'message_type': 'submission_request',
                        'message': {
                            'token': str(self.token.token),
                            'launcher_checksum': self.launcher.checksum,
                            'benchmark': {'schema_version': 'v1', 'data': self.data_v1},
                        },
                    }
                ),
                content_type='application/json',
            )
            self.assertEqual(403, response.status_code)
            self.assertEqual(
                {
                    'message_type': 'non_verified_token_error_response',
                    'message': {'message': 'token not verified'},
                },
                response.json(),
            )

        self.token.verified = False
        self.token.save()
        test()

        self.token.verified = True
        self.token.user = None
        self.token.save()
        test()

        self.token.verified = False
        self.token.user = None
        self.token.save()
        test()

    def test_submit_invalid_data(self) -> None:
        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {
                            'schema_version': 'v1',
                            'data': {'this': 'does not match the schema'},
                        },
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            {
                'message_type': 'schema_validation_error_response',
                'message': {'message': 'benchmark does not match schema'},
            },
            response.json(),
        )

    def test_submit_v1(self) -> None:
        data = self.data_v1

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v1', 'data': data},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(201, response.status_code)
        json_dict = response.json()
        self.assertEqual('submission_successful_response', json_dict.get('message_type'))
        self.assertIn('message', json_dict)
        message = json_dict['message']
        self.assertIsInstance(message, dict)
        self.assertIn('benchmark_url', message)
        func, args, kwargs = urls.resolve(yarl.URL(message['benchmark_url']).path)
        self.assertIn('pk', kwargs)
        raw_benchmark_id = kwargs['pk']

        raw_benchmark: RawBenchmark = RawBenchmark.objects.get()
        self.assertEqual('v1', raw_benchmark.schema_version)
        self.assertEqual(data, raw_benchmark.data)

        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmark;')
            benchmarks: List[Dict[str, object]] = fetch_all(cursor)

        self.assertCountEqual(
            [
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 0,
                    'render_time': Decimal('116.766'),
                    'peak_memory_usage': Decimal('142.13'),
                    'device_type': 'CPU',
                    'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                    'os': 'Linux',
                    'benchmark': 'barbershop_interior',
                    'blender_version': '2.79',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('16.60698531319736364120')
                },
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 1,
                    'render_time': Decimal('116.766'),
                    'peak_memory_usage': Decimal('142.13'),
                    'device_type': 'CPU',
                    'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                    'os': 'Linux',
                    'benchmark': 'victor',
                    'blender_version': '2.79',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('0')
                },
            ],
            benchmarks,
        )

        ownership: RawBenchmarkOwnership = RawBenchmarkOwnership.objects.get()
        self.assertEqual(raw_benchmark, ownership.benchmark)
        self.assertEqual(self.owner_user, ownership.user)

    def test_submit_v2(self) -> None:
        data = self.data_v2

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v2', 'data': data},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(201, response.status_code)
        json_dict = response.json()
        self.assertEqual('submission_successful_response', json_dict.get('message_type'))
        self.assertIn('message', json_dict)
        message = json_dict['message']
        self.assertIsInstance(message, dict)
        self.assertIn('benchmark_url', message)
        func, args, kwargs = urls.resolve(yarl.URL(message['benchmark_url']).path)
        self.assertIn('pk', kwargs)
        raw_benchmark_id = kwargs['pk']

        raw_benchmark: RawBenchmark = RawBenchmark.objects.get()
        self.assertEqual('v2', raw_benchmark.schema_version)
        self.assertEqual(data, raw_benchmark.data)

        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmark;')
            benchmarks: List[Dict[str, object]] = fetch_all(cursor)

        self.assertCountEqual(
            [
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 0,
                    'render_time': Decimal('116.766'),
                    'peak_memory_usage': Decimal('142.13'),
                    'device_type': 'CPU',
                    'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                    'os': 'Linux',
                    'benchmark': 'barbershop_interior',
                    'blender_version': '2.79',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('16.60698531319736364120')
                },
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 1,
                    'render_time': Decimal('116.766'),
                    'peak_memory_usage': Decimal('142.13'),
                    'device_type': 'CPU',
                    'device_name': 'Intel Xeon CPU E5-2699 v4 @ 2.20GHz',
                    'os': 'Linux',
                    'benchmark': 'victor',
                    'blender_version': '2.79',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('0')
                },
            ],
            benchmarks,
        )

        ownership: RawBenchmarkOwnership = RawBenchmarkOwnership.objects.get()
        self.assertEqual(raw_benchmark, ownership.benchmark)
        self.assertEqual(self.owner_user, ownership.user)

    def test_submit_v3(self) -> None:
        data = self.data_v3

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v3', 'data': data},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(201, response.status_code)
        json_dict = response.json()
        self.assertEqual('submission_successful_response', json_dict.get('message_type'))
        self.assertIn('message', json_dict)
        message = json_dict['message']
        self.assertIsInstance(message, dict)
        self.assertIn('benchmark_url', message)
        func, args, kwargs = urls.resolve(yarl.URL(message['benchmark_url']).path)
        self.assertIn('pk', kwargs)
        raw_benchmark_id = kwargs['pk']

        raw_benchmark: RawBenchmark = RawBenchmark.objects.get()
        self.assertEqual('v3', raw_benchmark.schema_version)
        self.assertEqual(data, raw_benchmark.data)

        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmark;')
            benchmarks: List[Dict[str, object]] = fetch_all(cursor)

        self.assertCountEqual(
            [
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 0,
                    'render_time': Decimal('2.98291'),
                    'peak_memory_usage': Decimal('78.25'),
                    'device_type': 'CPU',
                    'device_name': 'AMD Ryzen 7 1800X Eight-Core Processor',
                    'os': 'Linux',
                    'benchmark': 'victor',
                    'blender_version': '2.80',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('0'),
                },
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 1,
                    'render_time': Decimal('2.98291'),
                    'peak_memory_usage': Decimal('78.25'),
                    'device_type': 'CPU',
                    'device_name': 'AMD BLA Ryzen 7 1800X Eight-Core Processor',
                    'os': 'Linux',
                    'benchmark': 'not victor',
                    'blender_version': '2.80',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('0'),
                },
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 2,
                    'render_time': Decimal('116.766'),
                    'peak_memory_usage': Decimal('142.13'),
                    'device_type': 'CPU',
                    'device_name': 'AMD Ryzen 7 1800X Eight-Core Processor',
                    'os': 'Linux',
                    'benchmark': 'barbershop_interior',
                    'blender_version': '2.80',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('16.60698531319736364120'),
                },
            ],
            benchmarks,
        )

        ownership: RawBenchmarkOwnership = RawBenchmarkOwnership.objects.get()
        self.assertEqual(raw_benchmark, ownership.benchmark)
        self.assertEqual(self.owner_user, ownership.user)

    def test_submit_v4(self) -> None:
        data = self.data_v4

        response = self.client.post(
            self.url,
            json.dumps(
                {
                    'message_type': 'submission_request',
                    'message': {
                        'token': str(self.token.token),
                        'launcher_checksum': self.launcher.checksum,
                        'benchmark': {'schema_version': 'v4', 'data': data},
                    },
                }
            ),
            content_type='application/json',
        )
        self.assertEqual(201, response.status_code)
        json_dict = response.json()
        self.assertEqual('submission_successful_response', json_dict.get('message_type'))
        self.assertIn('message', json_dict)
        message = json_dict['message']
        self.assertIsInstance(message, dict)
        self.assertIn('benchmark_url', message)
        func, args, kwargs = urls.resolve(yarl.URL(message['benchmark_url']).path)
        self.assertIn('pk', kwargs)
        raw_benchmark_id = kwargs['pk']

        raw_benchmark: RawBenchmark = RawBenchmark.objects.get()
        self.assertEqual('v4', raw_benchmark.schema_version)
        self.assertEqual(data, raw_benchmark.data)

        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM opendata_main_benchmark;')
            benchmarks: List[Dict[str, object]] = fetch_all(cursor)

        self.assertCountEqual(
            [
                {
                    'benchmark_id': raw_benchmark_id,
                    'benchmark_sample_index': 0,
                    'render_time': Decimal('32.7422'),
                    'peak_memory_usage': Decimal('147.05'),
                    'device_type': 'CPU',
                    'device_name': 'Intel(R) Core(TM) i7-8700B CPU @ 3.20GHz',
                    'os': 'Darwin',
                    'benchmark': 'bmw27',
                    'blender_version': '3.0.1',
                    'verified': False,
                    'username': None,
                    'samples_per_minute': Decimal('150.23445'),
                },
            ],
            benchmarks,
        )

        ownership: RawBenchmarkOwnership = RawBenchmarkOwnership.objects.get()
        self.assertEqual(raw_benchmark, ownership.benchmark)
        self.assertEqual(self.owner_user, ownership.user)
