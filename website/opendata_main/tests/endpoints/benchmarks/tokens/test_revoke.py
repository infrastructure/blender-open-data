import yarl
from django import test
from django.contrib.auth.models import User
from django.urls import reverse

from opendata_main.models.tokens import LauncherAuthenticationToken


class TokensRevokeTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.other_user: User = User.objects.create_user(
            username='other_user',
            email='test_other@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.token: LauncherAuthenticationToken = LauncherAuthenticationToken.objects.create(
            user=self.owner_user, hostname='test', verified=True
        )

        self.client.force_login(self.owner_user)

        self.url = reverse('benchmarks/tokens/revoke', kwargs={'pk': self.token.id})

    def test_revoke(self) -> None:
        response = self.client.post(self.url)
        self.assertRedirects(
            response, reverse('users/tokens'), target_status_code=200,
        )

    def test_needs_login(self) -> None:
        self.client.logout()

        response = self.client.post(self.url)
        self.assertRedirects(
            response,
            str(yarl.URL(reverse('oauth:login')).with_query(next=self.url)),
            target_status_code=302,
        )

    def test_needs_to_be_owner(self) -> None:
        self.client.force_login(self.other_user)
        response = self.client.post(self.url)
        self.assertEqual(403, response.status_code)
