import yarl
from django import test
from django.contrib.auth.models import User
from django.db import connection
from django.test.utils import CaptureQueriesContext
from django.urls import reverse

from opendata import settings
from opendata_main.models.tokens import LauncherAuthenticationToken


class TokensVerifyTests(test.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.owner_user: User = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )

        self.token: LauncherAuthenticationToken = LauncherAuthenticationToken.objects.create(
            user=None, hostname='test', verified=None
        )

        self.client.force_login(self.owner_user)

        self.url = reverse('benchmarks/tokens/verify', kwargs={'pk': self.token.id})

        self.channel: str = settings.LAUNCHER_AUTHENTICATOR_NOTIFICATION_CHANNEL  # type: ignore

    def test_get(self) -> None:
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_verify(self) -> None:
        with CaptureQueriesContext(connection) as capturer:
            response = self.client.post(self.url, {'verify': 'true'})
            self.assertEqual(200, response.status_code)

        self.assertIn(
            f'NOTIFY "{self.channel}", \'{str(self.token.id)}\'',
            [q['sql'] for q in capturer.captured_queries],
        )

        self.token.refresh_from_db()
        self.assertEqual(self.owner_user, self.token.user)
        self.assertEqual(True, self.token.verified)

    def test_reject(self) -> None:
        with CaptureQueriesContext(connection) as capturer:
            response = self.client.post(self.url, {'verify': 'false'})
            self.assertEqual(200, response.status_code)

        self.assertIn(
            f'NOTIFY "{self.channel}", \'{str(self.token.id)}\'',
            [q['sql'] for q in capturer.captured_queries],
        )

        self.token.refresh_from_db()
        self.assertEqual(self.owner_user, self.token.user)
        self.assertEqual(False, self.token.verified)

    def test_cannot_verify_already_verified_token(self) -> None:
        def test() -> None:
            response = self.client.post(self.url, {'verify': 'true'})
            self.assertEqual(403, response.status_code)

        self.token.verified = True
        self.token.user = self.owner_user
        self.token.save()
        test()

        self.token.verified = True
        self.token.user = None
        self.token.save()
        test()

        self.token.verified = None
        self.token.user = self.owner_user
        self.token.save()
        test()

    def test_needs_login(self) -> None:
        self.client.logout()

        response = self.client.post(self.url, {'verify': 'true'})
        self.assertRedirects(
            response,
            str(yarl.URL(reverse('oauth:login')).with_query(next=self.url)),
            target_status_code=302,
        )
