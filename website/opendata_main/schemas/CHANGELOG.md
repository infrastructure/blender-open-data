# Benchmark Schema Changelog

This file describes the changes between the different benchmark schema versions.

The schema version is stored in the top-level `schema_version` key in the benchmark JSON.
If this key doesn't exist, it is implied to be version 1.


## Version 0

There is no more support for version 0 JSON documents; the minimum supported version is 1.


## Version 1

This version introduces compatibility with the Benchmark Bundle code running in Blender itself,
and was used since Blender Benchmark Client version 1.0-beta1.

- The build and commit timestamps of Blender are reported in separate fields, and no longer
  as ISO 8601 timestamp. Blender doesn't report the timezone, so the ISO 8601 timestamp
  was deceptively incorrect anyway.
- The type of `data.scenes.stats.result` is changed from boolean to string.
  `true` becomes `'OK'`, and `false` becomes `'CRASH'`.


## Version 2

This version introduces better reporting of render/compute devices from Blender.

- Instead of having `(Display)` in the name, `data.system_info.devices` is now a list of
  dictionaries: `{name: 'GeForce GTX 970', is_display: True}`
- The same applies to `data.device_info.compute_devices`.
