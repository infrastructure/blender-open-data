import functools
import json
from pathlib import Path
from typing import Dict, cast

import jsonschema

from opendata_main.errors import SchemaValidationError
from opendata_main.models.benchmarks import BenchmarkSchemaVersion, RawBenchmark


@functools.lru_cache(maxsize=None)
def _get_schema(version: BenchmarkSchemaVersion) -> Dict[str, object]:
    with open(Path(__file__).absolute().parent / f'benchmark-v{version.value}.json') as f:
        schema = json.load(f)
    assert isinstance(schema, dict)
    return cast(Dict[str, object], schema)


def validate(benchmark: RawBenchmark) -> None:
    """
    Validate against the benchmark schema.

    :raise SchemaValidationError: if the data doesn't match the schema.
    """
    try:
        jsonschema.validate(
            benchmark.data, _get_schema(BenchmarkSchemaVersion[benchmark.schema_version])
        )
    except jsonschema.ValidationError as e:
        raise SchemaValidationError() from e
