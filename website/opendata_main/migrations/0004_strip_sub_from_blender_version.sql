-- This function normalizes all possible schemas to the schema of
-- `opendata_main_benchmark`.
CREATE OR REPLACE FUNCTION opendata_main_rawbenchmark_normalize(raw opendata_main_rawbenchmark)
    RETURNS TABLE
            (
                benchmark_id           uuid,
                benchmark_sample_index bigint,
                render_time            numeric,
                peak_memory_usage      numeric,
                device_type            text,
                device_name            text,
                os                     text,
                benchmark              text,
                blender_version        text
            )
AS
$$
BEGIN
    CASE raw.schema_version
        WHEN 'v1' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   replace(
                                           raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                           ' (Display)', ''
                                       )
                           ELSE
                               replace(raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                       ' (Display)', ''
                                   )
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       substring(raw.data -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v2' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       substring(raw.data -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v3' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       scene -> 'device_info' ->> 'device_type'             AS device_type,
                       CASE
                           WHEN scene -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (scene -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (scene -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       scene -> 'system_info' ->> 'system'                  AS os,
                       scene -> 'scene' ->> 'label'                         AS benchmark,
                       substring(scene -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version
                FROM jsonb_array_elements(raw.data) WITH ORDINALITY AS scenes(scene, sample_index);
        ELSE
            RAISE EXCEPTION 'Unimplemented schema version: %', raw.schema_version USING HINT =
                    'Implement this schema version in the `opendata_main_rawbenchmark_normalize` function';
    END CASE;
END;
$$ LANGUAGE plpgsql;

SELECT *
FROM opendata_main_reindex_benchmarks();
