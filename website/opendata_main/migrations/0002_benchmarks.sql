-- We create a separate table for the indexed benchmarks s.t. we have a
-- normalized and flat view of all available data. The reason we did not use
-- a VIEW is that we need indices, and the reason we did not use a
-- MATERIALIZED VIEW is that we need the benchmarks to be indexed as soon as
-- they are submitted (because the user is redirected to their indexed results
-- after submission).
CREATE TABLE opendata_main_benchmark
(
    PRIMARY KEY (benchmark_id, benchmark_sample_index),
    benchmark_id           uuid   NOT NULL
        CONSTRAINT opendata_main_benchmark_fkey_opendata_main_rawbenchmark REFERENCES opendata_main_rawbenchmark ON DELETE CASCADE,
    benchmark_sample_index bigint NOT NULL,
    render_time            numeric,
    peak_memory_usage      numeric,
    device_type            text,
    device_name            text,
    os                     text,
    benchmark              text,
    blender_version        text,
    verified               bool   NOT NULL DEFAULT FALSE,
    username               text            DEFAULT NULL
);

-- We use trigrams to enable fast ILIKE searches for the /benchmarks/query page.
CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE INDEX opendata_main_benchmark_render_time_idx
    ON opendata_main_benchmark (render_time);

CREATE INDEX opendata_main_benchmark_peak_memory_usage_idx
    ON opendata_main_benchmark (peak_memory_usage);

CREATE INDEX opendata_main_benchmark_device_type_idx_trgm
    ON opendata_main_benchmark USING gin (device_type gin_trgm_ops);
CREATE INDEX opendata_main_benchmark_device_type_idx
    ON opendata_main_benchmark (device_type);

CREATE INDEX opendata_main_benchmark_device_name_idx_trgm
    ON opendata_main_benchmark USING gin (device_name gin_trgm_ops);
CREATE INDEX opendata_main_benchmark_device_name_idx
    ON opendata_main_benchmark (device_name);

CREATE INDEX opendata_main_benchmark_os_idx_trgm
    ON opendata_main_benchmark USING gin (os gin_trgm_ops);
CREATE INDEX opendata_main_benchmark_os_idx
    ON opendata_main_benchmark (os);

CREATE INDEX opendata_main_benchmark_benchmark_idx_trgm
    ON opendata_main_benchmark USING gin (benchmark gin_trgm_ops);
CREATE INDEX opendata_main_benchmark_benchmark_idx
    ON opendata_main_benchmark (benchmark);

CREATE INDEX opendata_main_benchmark_blender_version_idx_trgm
    ON opendata_main_benchmark USING gin (blender_version gin_trgm_ops);
CREATE INDEX opendata_main_benchmark_blender_version_idx
    ON opendata_main_benchmark (blender_version);

CREATE INDEX opendata_main_benchmark_benchmark_id_idx
    ON opendata_main_benchmark (benchmark_id);

-- This function normalizes all possible schemas to the schema of
-- `opendata_main_benchmark`.
CREATE FUNCTION opendata_main_rawbenchmark_normalize(raw opendata_main_rawbenchmark)
    RETURNS TABLE
            (
                benchmark_id           uuid,
                benchmark_sample_index bigint,
                render_time            numeric,
                peak_memory_usage      numeric,
                device_type            text,
                device_name            text,
                os                     text,
                benchmark              text,
                blender_version        text
            )
AS
$$
BEGIN
    CASE raw.schema_version
        WHEN 'v1' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   replace(
                                           raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                           ' (Display)', ''
                                       )
                           ELSE
                               replace(raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                       ' (Display)', ''
                                   )
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       raw.data -> 'blender_version' ->> 'version'          AS blender_version
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v2' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       raw.data -> 'blender_version' ->> 'version'          AS blender_version
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v3' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       scene -> 'device_info' ->> 'device_type'             AS device_type,
                       CASE
                           WHEN scene -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (scene -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (scene -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       scene -> 'system_info' ->> 'system'                  AS os,
                       scene -> 'scene' ->> 'label'                         AS benchmark,
                       scene -> 'blender_version' ->> 'version'             AS blender_version
                FROM jsonb_array_elements(raw.data) WITH ORDINALITY AS scenes(scene, sample_index);
        ELSE
            RAISE EXCEPTION 'Unimplemented schema version: %', raw.schema_version USING HINT =
                    'Implement this schema version in the `opendata_main_rawbenchmark_normalize` function';
    END CASE;
END;
$$ LANGUAGE plpgsql;

-- This trigger function creates a row in `opendata_main_benchmark` for every
-- sample in every row in `opendata_main_rawbenchmarks`.
CREATE FUNCTION opendata_main_rawbenchmark_update_benchmark()
    RETURNS trigger
AS
$$
BEGIN
    CASE TG_OP
        WHEN 'DELETE' THEN
            DELETE FROM opendata_main_benchmark WHERE opendata_main_benchmark.benchmark_id = OLD.id;
        WHEN 'UPDATE' THEN
            PERFORM *
            FROM opendata_main_benchmark
            WHERE opendata_main_benchmark.benchmark_id = OLD.id FOR UPDATE;

            DELETE FROM opendata_main_benchmark WHERE opendata_main_benchmark.benchmark_id = OLD.id;

            INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                                 peak_memory_usage,
                                                 device_type, device_name, os, benchmark,
                                                 blender_version)
            SELECT *
            FROM opendata_main_rawbenchmark_normalize(NEW) AS normalized;

            UPDATE opendata_main_benchmark
            SET verified = verified_and_username.verified,
                username = verified_and_username.username
            FROM opendata_main_rawbenchmarkownership,
                 LATERAL opendata_main_rawbenchmarkownership_verified_and_username(
                         opendata_main_rawbenchmarkownership) AS verified_and_username
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.benchmark_id = NEW.id;
        WHEN 'INSERT' THEN
            INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                                 peak_memory_usage,
                                                 device_type, device_name, os, benchmark,
                                                 blender_version)
            SELECT *
            FROM opendata_main_rawbenchmark_normalize(NEW) AS normalized;
    END CASE;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER opendata_main_rawbenchmark_trigger_update_benchmark
    AFTER INSERT OR DELETE OR UPDATE
    ON opendata_main_rawbenchmark
    FOR EACH ROW
EXECUTE PROCEDURE opendata_main_rawbenchmark_update_benchmark();

-- This function computes the verified an username fields given a row of
-- `opendata_main_rawbenchmarkownership`.
CREATE FUNCTION opendata_main_rawbenchmarkownership_verified_and_username(ownership opendata_main_rawbenchmarkownership)
    RETURNS TABLE
            (
                verified bool,
                username text
            )
AS
$$
BEGIN
    RETURN QUERY SELECT opendata_main_usersettings.benchmarks_verified AS verified,
                        (CASE
                             WHEN opendata_main_usersettings.benchmarks_anonymous = true
                                 OR opendata_main_usersettings.benchmarks_name = '' THEN
                                 null
                             ELSE opendata_main_usersettings.benchmarks_name
                         END)                                          AS username
                 FROM opendata_main_usersettings
                 WHERE opendata_main_usersettings.user_id = ownership.user_id;
END;
$$ LANGUAGE plpgsql;

-- This trigger function updates the rows in `opendata_main_benchmark` for
-- updated rows in `opendata_main_rawbenchmarkownership`.
CREATE FUNCTION opendata_main_rawbenchmarkownership_update_benchmark()
    RETURNS trigger
AS
$$
BEGIN
    CASE TG_OP
        WHEN 'DELETE' THEN
            UPDATE opendata_main_benchmark
            SET verified = false,
                username = null
            WHERE opendata_main_benchmark.benchmark_id = OLD.benchmark_id;
        WHEN 'UPDATE' THEN
            PERFORM *
            FROM opendata_main_benchmark
            WHERE opendata_main_benchmark.benchmark_id = OLD.benchmark_id
               OR opendata_main_benchmark.benchmark_id = NEW.benchmark_id FOR UPDATE;

            UPDATE opendata_main_benchmark
            SET verified = false,
                username = null
            WHERE opendata_main_benchmark.benchmark_id = OLD.benchmark_id;

            UPDATE opendata_main_benchmark
            SET verified = verified_and_username.verified,
                username = verified_and_username.username
            FROM opendata_main_rawbenchmarkownership_verified_and_username(
                         NEW) AS verified_and_username
            WHERE opendata_main_benchmark.benchmark_id = NEW.benchmark_id;
        WHEN 'INSERT' THEN
            UPDATE opendata_main_benchmark
            SET verified = verified_and_username.verified,
                username = verified_and_username.username
            FROM opendata_main_rawbenchmarkownership_verified_and_username(
                         NEW) AS verified_and_username
            WHERE opendata_main_benchmark.benchmark_id = NEW.benchmark_id;
    END CASE;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER opendata_main_rawbenchmarkownership_trigger_update_benchmark
    AFTER INSERT OR DELETE OR UPDATE
    ON opendata_main_rawbenchmarkownership
    FOR EACH ROW
EXECUTE PROCEDURE opendata_main_rawbenchmarkownership_update_benchmark();

-- This trigger function updates the rows in `opendata_main_benchmark` for
-- updated rows in `opendata_main_usersettings`.
CREATE FUNCTION opendata_main_usersettings_update_benchmark()
    RETURNS trigger
AS
$$
BEGIN
    CASE TG_OP
        WHEN 'DELETE' THEN
            UPDATE opendata_main_benchmark
            SET verified = false,
                username = null
            FROM opendata_main_rawbenchmarkownership
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.user_id = OLD.user_id;
        WHEN 'UPDATE' THEN
            PERFORM *
            FROM opendata_main_benchmark
                     LEFT JOIN opendata_main_rawbenchmarkownership
                               ON opendata_main_benchmark.benchmark_id =
                                  opendata_main_rawbenchmarkownership.benchmark_id
            WHERE opendata_main_rawbenchmarkownership.user_id = OLD.user_id
               OR opendata_main_rawbenchmarkownership.user_id = NEW.user_id
                FOR UPDATE OF opendata_main_benchmark;

            UPDATE opendata_main_benchmark
            SET verified = false,
                username = null
            FROM opendata_main_rawbenchmarkownership
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.user_id = OLD.user_id;

            UPDATE opendata_main_benchmark
            SET verified = NEW.benchmarks_verified,
                username = CASE
                               WHEN NEW.benchmarks_anonymous = true
                                   OR NEW.benchmarks_name = '' THEN
                                   null
                               ELSE NEW.benchmarks_name
                           END
            FROM opendata_main_rawbenchmarkownership
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.user_id = NEW.user_id;
        WHEN 'INSERT' THEN
            UPDATE opendata_main_benchmark
            SET verified = NEW.benchmarks_verified,
                username = CASE
                               WHEN NEW.benchmarks_anonymous = true
                                   OR NEW.benchmarks_name = '' THEN
                                   null
                               ELSE NEW.benchmarks_name
                           END
            FROM opendata_main_rawbenchmarkownership
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.user_id = NEW.user_id;
    END CASE;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER opendata_main_usersettings_trigger_update_benchmark
    AFTER INSERT OR DELETE OR UPDATE
    ON opendata_main_usersettings
    FOR EACH ROW
EXECUTE PROCEDURE opendata_main_usersettings_update_benchmark();

-- This function can be used to recreate the whole `opendata_main_benchmark
-- table`. Note that this can be **VERY EXPENSIVE** in a busy environment due to
-- the ACCESS EXCLUSIVE lock on `opendata_main_benchmark`.
CREATE FUNCTION opendata_main_reindex_benchmarks()
    RETURNS bigint
AS
$$
DECLARE
    number_of_inserted_rows bigint;
BEGIN
    LOCK TABLE opendata_main_benchmark IN ACCESS EXCLUSIVE MODE;
    TRUNCATE opendata_main_benchmark;

    INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                         peak_memory_usage,
                                         device_type, device_name, os, benchmark,
                                         blender_version)
    SELECT normalized.*
    FROM opendata_main_rawbenchmark,
         LATERAL opendata_main_rawbenchmark_normalize(opendata_main_rawbenchmark) AS normalized;

    GET DIAGNOSTICS number_of_inserted_rows = ROW_COUNT;

    UPDATE opendata_main_benchmark
    SET verified = verified_and_username.verified,
        username = verified_and_username.username
    FROM opendata_main_rawbenchmarkownership,
         LATERAL opendata_main_rawbenchmarkownership_verified_and_username(
                 opendata_main_rawbenchmarkownership) AS verified_and_username
    WHERE opendata_main_benchmark.benchmark_id = opendata_main_rawbenchmarkownership.benchmark_id;

    RETURN number_of_inserted_rows;
END;
$$ LANGUAGE plpgsql;
