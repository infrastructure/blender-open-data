from pathlib import Path

from django.db import migrations, models


def _get_sql() -> str:
    with open(Path(__file__).parent / '0009_samples_per_minute_to_score.sql') as f:
        return f.read()


def _get_reverse_sql() -> str:
    with open(Path(__file__).parent / '0009_samples_per_minute_to_score_reverse.sql') as f:
        return f.read()


class Migration(migrations.Migration):
    dependencies = [
        ('opendata_main', '0008_add_scene_deprecation'),
    ]

    operations = [
        migrations.RunSQL(sql=_get_sql(), reverse_sql=_get_reverse_sql(),),
    ]
