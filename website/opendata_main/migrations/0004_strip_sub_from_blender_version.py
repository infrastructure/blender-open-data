from pathlib import Path

from django.db import migrations


def _get_sql() -> str:
    with open(Path(__file__).parent / '0004_strip_sub_from_blender_version.sql') as f:
        return f.read()


def _get_reverse_sql() -> str:
    with open(Path(__file__).parent / '0004_strip_sub_from_blender_version_reverse.sql') as f:
        return f.read()


class Migration(migrations.Migration):
    dependencies = [
        ('opendata_main', '0003_fix_one_latest_launcher_per_os_constraint'),
    ]

    operations = [
        migrations.RunSQL(sql=_get_sql(), reverse_sql=_get_reverse_sql(),),
    ]
