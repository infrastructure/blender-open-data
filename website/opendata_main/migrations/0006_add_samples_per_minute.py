from pathlib import Path

from django.db import migrations, models


def _get_sql() -> str:
    with open(Path(__file__).parent / '0006_add_samples_per_minute.sql') as f:
        return f.read()


def _get_reverse_sql() -> str:
    with open(Path(__file__).parent / '0006_add_samples_per_minute_reverse.sql') as f:
        return f.read()


class Migration(migrations.Migration):
    """
    Abandon all hope
    """
    dependencies = [
        ('opendata_main', '0005_add_schema_v4'),
    ]

    operations = [
        migrations.RunSQL(sql=_get_sql(), reverse_sql=_get_reverse_sql(),),
    ]
