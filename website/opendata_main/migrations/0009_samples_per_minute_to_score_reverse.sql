-- Rename score to samples_per_minute
ALTER TABLE opendata_main_benchmark
    RENAME COLUMN score TO samples_per_minute;

-- This function normalizes all possible schemas to the schema of
-- `opendata_main_benchmark`.
DROP FUNCTION opendata_main_rawbenchmark_normalize RESTRICT;
CREATE FUNCTION opendata_main_rawbenchmark_normalize(raw opendata_main_rawbenchmark)
    RETURNS TABLE
            (
                benchmark_id           uuid,
                benchmark_sample_index bigint,
                render_time            numeric,
                peak_memory_usage      numeric,
                device_type            text,
                device_name            text,
                os                     text,
                benchmark              text,
                blender_version        text,
                samples_per_minute     numeric
            )
AS
$$
BEGIN
    CASE raw.schema_version
        WHEN 'v1' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   replace(
                                           raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                           ' (Display)', ''
                                       )
                           ELSE
                               replace(raw.data -> 'device_info' -> 'compute_devices' ->> 0,
                                       ' (Display)', ''
                                   )
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       substring(raw.data -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version,
                       CASE scene ->> 'name'
                           WHEN 'barbershop_interior' THEN
                               (32 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'classroom' THEN
                               (300 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'bmw27' THEN
                               (1225 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'pavillon_barcelona' THEN
                               (1000 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'koro' THEN
                               (500 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'fishy_cat' THEN
                               (128 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'victor' THEN
                               (600 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           ELSE
                               0
                       END                                                  AS samples_per_minute
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v2' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       raw.data -> 'device_info' ->> 'device_type'          AS device_type,
                       CASE
                           WHEN raw.data -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (raw.data -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (raw.data -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               raw.data -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       raw.data -> 'system_info' ->> 'system'               AS os,
                       scene ->> 'name'                                     AS benchmark,
                       substring(raw.data -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version,
                       CASE scene ->> 'name'
                           WHEN 'barbershop_interior' THEN
                               (32 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'classroom' THEN
                               (300 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'bmw27' THEN
                               (1225 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'pavillon_barcelona' THEN
                               (1000 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'koro' THEN
                               (500 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'fishy_cat' THEN
                               (128 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'victor' THEN
                               (600 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           ELSE
                               0
                       END                                                  AS samples_per_minute
                FROM jsonb_array_elements(raw.data -> 'scenes') WITH ORDINALITY AS scenes(scene, sample_index)
                WHERE scene -> 'stats' ->> 'result' = 'OK';
        WHEN 'v3' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       scene -> 'device_info' ->> 'device_type'             AS device_type,
                       CASE
                           WHEN scene -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (scene -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (scene -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       scene -> 'system_info' ->> 'system'                  AS os,
                       scene -> 'scene' ->> 'label'                         AS benchmark,
                       substring(scene -> 'blender_version' ->> 'version' from
                                 '^\d.\d\d[a-z]*')                          AS blender_version,
                       CASE scene -> 'scene' ->> 'label'
                           WHEN 'barbershop_interior' THEN
                               (32 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'classroom' THEN
                               (300 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'bmw27' THEN
                               (1225 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'pavillon_barcelona' THEN
                               (1000 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'koro' THEN
                               (500 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'fishy_cat' THEN
                               (128 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           WHEN 'victor' THEN
                               (600 / (scene -> 'stats' ->> 'render_time_no_sync')::numeric * 60)
                           ELSE
                               0
                       END                                                  AS samples_per_minute
                FROM jsonb_array_elements(raw.data) WITH ORDINALITY AS scenes(scene, sample_index);
        WHEN 'v4' THEN
            RETURN QUERY
                SELECT raw.id                                               AS benchmark_id,
                       sample_index - 1                                     as benchmark_sample_index,
                       (scene -> 'stats' ->> 'total_render_time')::numeric  AS render_time,
                       (scene -> 'stats' ->> 'device_peak_memory')::numeric AS peak_memory_usage,
                       scene -> 'device_info' ->> 'device_type'             AS device_type,
                       CASE
                           WHEN scene -> 'device_info' ->> 'device_type' = 'CPU' AND
                                (scene -> 'system_info' ->> 'num_cpu_sockets') :: int > 1 THEN
                                   (scene -> 'system_info' ->> 'num_cpu_sockets') || 'x ' ||
                                   (scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name')
                           ELSE
                               scene -> 'device_info' -> 'compute_devices' -> 0 ->> 'name'
                       END                                                  AS device_name,
                       scene -> 'system_info' ->> 'system'                  AS os,
                       scene -> 'scene' ->> 'label'                         AS benchmark,
                       substring(scene -> 'blender_version' ->> 'version' from
                                 '^\d.\d.\d')                               AS blender_version,
                       (scene -> 'stats' ->> 'samples_per_minute')::numeric AS samples_per_minute
                FROM jsonb_array_elements(raw.data) WITH ORDINALITY AS scenes(scene, sample_index);
        ELSE
            RAISE EXCEPTION 'Unimplemented schema version: %', raw.schema_version USING HINT =
                    'Implement this schema version in the `opendata_main_rawbenchmark_normalize` function';
    END CASE;
END;
$$ LANGUAGE plpgsql;


-- This function can be used to recreate the whole `opendata_main_benchmark
-- table`. Note that this can be **VERY EXPENSIVE** in a busy environment due to
-- the ACCESS EXCLUSIVE lock on `opendata_main_benchmark`.
CREATE OR REPLACE FUNCTION opendata_main_reindex_benchmarks()
    RETURNS bigint
AS
$$
DECLARE
    number_of_inserted_rows bigint;
BEGIN
    LOCK TABLE opendata_main_benchmark IN ACCESS EXCLUSIVE MODE;
    TRUNCATE opendata_main_benchmark;

    INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                         peak_memory_usage,
                                         device_type, device_name, os, benchmark,
                                         blender_version,
                                         samples_per_minute)
    SELECT normalized.*
    FROM opendata_main_rawbenchmark,
         LATERAL opendata_main_rawbenchmark_normalize(opendata_main_rawbenchmark) AS normalized;

    GET DIAGNOSTICS number_of_inserted_rows = ROW_COUNT;

    UPDATE opendata_main_benchmark
    SET verified = verified_and_username.verified,
        username = verified_and_username.username
    FROM opendata_main_rawbenchmarkownership,
         LATERAL opendata_main_rawbenchmarkownership_verified_and_username(
                 opendata_main_rawbenchmarkownership) AS verified_and_username
    WHERE opendata_main_benchmark.benchmark_id = opendata_main_rawbenchmarkownership.benchmark_id;

    RETURN number_of_inserted_rows;
END;
$$ LANGUAGE plpgsql;


-- This trigger function creates a row in `opendata_main_benchmark` for every
-- sample in every row in `opendata_main_rawbenchmarks`.
CREATE OR REPLACE FUNCTION opendata_main_rawbenchmark_update_benchmark()
    RETURNS trigger
AS
$$
BEGIN
    CASE TG_OP
        WHEN 'DELETE' THEN
            DELETE FROM opendata_main_benchmark WHERE opendata_main_benchmark.benchmark_id = OLD.id;
        WHEN 'UPDATE' THEN
            PERFORM *
            FROM opendata_main_benchmark
            WHERE opendata_main_benchmark.benchmark_id = OLD.id FOR UPDATE;

            DELETE FROM opendata_main_benchmark WHERE opendata_main_benchmark.benchmark_id = OLD.id;

            INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                                 peak_memory_usage,
                                                 device_type, device_name, os, benchmark,
                                                 blender_version, samples_per_minute)
            SELECT *
            FROM opendata_main_rawbenchmark_normalize(NEW) AS normalized;

            UPDATE opendata_main_benchmark
            SET verified = verified_and_username.verified,
                username = verified_and_username.username
            FROM opendata_main_rawbenchmarkownership,
                 LATERAL opendata_main_rawbenchmarkownership_verified_and_username(
                         opendata_main_rawbenchmarkownership) AS verified_and_username
            WHERE opendata_main_benchmark.benchmark_id =
                  opendata_main_rawbenchmarkownership.benchmark_id
              AND opendata_main_rawbenchmarkownership.benchmark_id = NEW.id;
        WHEN 'INSERT' THEN
            INSERT INTO opendata_main_benchmark (benchmark_id, benchmark_sample_index, render_time,
                                                 peak_memory_usage,
                                                 device_type, device_name, os, benchmark,
                                                 blender_version, samples_per_minute)
            SELECT *
            FROM opendata_main_rawbenchmark_normalize(NEW) AS normalized;
    END CASE;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

