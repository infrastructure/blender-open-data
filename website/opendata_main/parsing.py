import enum
from typing import TypeVar, overload, Dict, Type, Optional, Callable, Union, cast, Any

from opendata_main.errors import ParseError

LiteralType = TypeVar('LiteralType', bound=Union[str, int, None, enum.Enum])


@overload
def parse_field(
    dct: Dict[str, object], field: str, type_: Type[Dict[object, object]]
) -> Dict[str, object]:
    ...


@overload
def parse_field(dct: Dict[str, object], field: str, type_: Type[LiteralType]) -> LiteralType:
    ...


def parse_field(dct: Dict[str, object], field: str, type_: Type[object]) -> object:
    value: Optional[object] = dct.get(field)
    if issubclass(type_, enum.Enum):
        if isinstance(value, str):
            try:
                return type_[value]
            except KeyError:
                raise ParseError(
                    f'{field} not one of the allowed values {list(type_.__members__.keys())}'
                )
        else:
            raise ParseError(f'{field} should be of type {type_} but found {type(value)}')

    elif isinstance(value, type_):
        return value
    else:
        raise ParseError(f'{field} should be of type {type_} but found {type(value)}')


@overload
def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[Dict[object, object]]
) -> Optional[Dict[str, object]]:
    ...


@overload
def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[LiteralType]
) -> Optional[LiteralType]:
    ...


def parse_field_optional(
    dct: Dict[str, object], field: str, type_: Type[object]
) -> Optional[object]:
    value: Optional[object] = dct.get(field)
    if value is None:
        return None
    else:
        return cast(object, parse_field(dct, field, cast(Any, type_)))


A = TypeVar('A')
B = TypeVar('B')


def map_optional(value: Optional[A], f: Callable[[A], B]) -> Optional[B]:
    if value is None:
        return None
    else:
        return f(value)


EnumType = TypeVar('EnumType', bound=enum.Enum)


def parse_enum(enum_class: Type[EnumType], field_name: str, value: str) -> EnumType:
    if value not in enum_class.__members__:
        valid_names = ', '.join(f'"{k}"' for k in enum_class.__members__.keys())
        raise ParseError(
            f'Invalid value for {field_name}: "{value}", should be one of {valid_names}.'
        )
    else:
        return enum_class[value]
