from django.apps import AppConfig


class OpendataMainConfig(AppConfig):
    name = 'opendata_main'
    verbose_name = 'Open Data'

    def ready(self):
        import opendata_main.signals  # noqa
