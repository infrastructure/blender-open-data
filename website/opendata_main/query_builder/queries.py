import dataclasses as dc
import enum
from typing import Mapping, Tuple, Optional, Iterable, List, Dict

from psycopg2 import sql


class QuerySort(enum.Enum):
    ALPHABETIC_ASC = 1
    ALPHABETIC_DESC = 2
    POPULARITY = 3


@dc.dataclass(frozen=True)
class Column:
    display_name: str
    parameter_prefix: str
    identifier: Optional[sql.Identifier]
    groupable: bool
    sortable: bool
    filterable: bool
    query_sort: QuerySort


class Columns(enum.Enum):
    number_of_samples: Column = Column(
        display_name='Number of Benchmarks',
        parameter_prefix='number_of_samples',
        identifier=sql.Identifier('number_of_samples'),
        groupable=False,
        sortable=True,
        filterable=False,
        query_sort=QuerySort.ALPHABETIC_ASC,
    )
    score: Column = Column(
        display_name='Median Score',
        parameter_prefix='score',
        identifier=sql.Identifier('score'),
        groupable=False,
        sortable=True,
        filterable=False,
        query_sort=QuerySort.ALPHABETIC_ASC,
    )
    compute_type: Column = Column(
        display_name='Compute Type',
        parameter_prefix='compute_type',
        identifier=sql.Identifier('compute_type'),
        groupable=True,
        sortable=True,
        filterable=True,
        query_sort=QuerySort.ALPHABETIC_ASC,
    )
    device_name: Column = Column(
        display_name='Device Name',
        parameter_prefix='device_name',
        identifier=sql.Identifier('device_name'),
        groupable=True,
        sortable=True,
        filterable=True,
        query_sort=QuerySort.POPULARITY,
    )
    operating_system: Column = Column(
        display_name='OS',
        parameter_prefix='operating_system',
        identifier=sql.Identifier('operating_system'),
        groupable=True,
        sortable=True,
        filterable=True,
        query_sort=QuerySort.ALPHABETIC_ASC,
    )
    blender_version: Column = Column(
        display_name='Blender Version',
        parameter_prefix='blender_version',
        identifier=sql.Identifier('blender_version'),
        groupable=True,
        sortable=True,
        filterable=True,
        query_sort=QuerySort.ALPHABETIC_DESC,
    )


@dc.dataclass(frozen=True)
class Bindings:
    parameters: Dict[str, object]

    def bind(self, prefix: str, value: str) -> Tuple['Bindings', sql.Placeholder]:
        next_free_name: str = f'{prefix}_param_{len(self.parameters)}'
        new_bindings = dc.replace(self, parameters={**self.parameters, next_free_name: value})
        placeholder = sql.Placeholder(name=next_free_name)
        return new_bindings, placeholder


def compile_expression(
    bindings: Bindings, parameter_prefix: str, identifier: sql.Identifier, expression: str
) -> Tuple[Bindings, Optional[sql.Composable]]:
    if expression:
        escaped_string = expression.replace('\\', '\\\\').replace('%', r'\%').replace('_', r'\_')
        ilike_pattern = f'%{escaped_string}%'

        bindings, place_holder = bindings.bind(parameter_prefix, ilike_pattern)
        return bindings, sql.SQL('{} ILIKE {}').format(identifier, place_holder)
    else:
        return bindings, None


def compile_filters(filters: Mapping[Columns, List[str]]) -> Tuple[Bindings, sql.Composable]:
    bindings = Bindings(parameters={})

    sql_column_expressions: List[List[sql.Composable]] = []
    for column, expressions in filters.items():
        sql_expressions: List[sql.Composable] = []

        for expression in expressions:
            bindings, sql_expression = compile_expression(
                bindings, column.value.parameter_prefix, column.value.identifier, expression
            )
            if sql_expression:
                sql_expressions.append(sql_expression)

        if sql_expressions:
            sql_column_expressions.append(sql_expressions)

    if sql_column_expressions:
        sql_expression = sql.SQL(' AND ').join(
            sql.SQL('({})').format(sql.SQL(' OR ').join(sql.SQL('({})').format(e) for e in es))
            for es in sql_column_expressions
        )
        return bindings, sql_expression
    else:
        return bindings, sql.SQL('TRUE')


def query(
    filters: Mapping[Columns, List[str]], group_by: List[Columns]
) -> Tuple[Bindings, sql.Composable]:
    bindings, where_clause = compile_filters(filters)

    aggregate_columns: sql.SQL
    select_columns: Iterable[Columns]
    if not group_by:
        aggregate_columns = sql.SQL(
            '''
            score AS "score",
            1     AS "number_of_samples"
        '''
        )
        select_columns = [
            Columns.device_name,
            Columns.compute_type,
            Columns.operating_system,
            Columns.blender_version,
        ]
    else:
        aggregate_columns = sql.SQL(
            '''
            percentile_cont(0.5) WITHIN GROUP (ORDER BY score) AS "score",
            count(*)                                           AS "number_of_samples"
        '''
        )
        select_columns = group_by

    sql_statement = sql.SQL(
        '''
        SELECT
            {select_columns},
            {aggregate_columns}
        FROM
            opendata_main_benchmarkindex
        WHERE
            ({where_clause})
        {group_by_clause};
    '''
    ).format(
        aggregate_columns=aggregate_columns,
        select_columns=sql.SQL(',').join(
            sql.SQL('{} AS {}').format(c.value.identifier, c.value.identifier)
            for c in select_columns
        ),
        where_clause=where_clause,
        group_by_clause=(
            sql.SQL('GROUP BY {}').format(sql.SQL(', ').join(g.value.identifier for g in group_by))
            if group_by
            else sql.SQL('')
        ),
    )

    return bindings, sql_statement


def suggest(column: Column, expression: str, limit: int) -> Tuple[Bindings, sql.Composable]:
    bindings = Bindings(parameters={'limit': limit})

    bindings, sql_expression = compile_expression(
        bindings, column.parameter_prefix, column.identifier, expression
    )

    if sql_expression is None:
        sql_expression = sql.SQL('TRUE')

    if column.query_sort == QuerySort.POPULARITY:
        order_by = sql.SQL(f'count({column.identifier.string}) DESC')
    elif column.query_sort == QuerySort.ALPHABETIC_DESC:
        order_by = sql.SQL(f'lower({column.identifier.string}) DESC')
    else:
        order_by = sql.SQL(f'lower({column.identifier.string}) ASC')

    sql_statement = sql.SQL(
        '''
        SELECT
            {identifier}
        FROM
            opendata_main_benchmarkindex
        WHERE
            {where_clause}
        GROUP BY {identifier}
        ORDER BY {order_by}
        LIMIT %(limit)s;
    '''
    ).format(
        identifier=column.identifier,
        where_clause=sql_expression,
        order_by=order_by,
    )

    return bindings, sql_statement
