from typing import Sequence, Dict, List

import psycopg2

from opendata_main.errors import SQLError


def row_as_dict(names: Sequence[str], row: Sequence[object]) -> Dict[str, object]:
    return {name: value for name, value in zip(names, row)}


def fetch_one(cursor: psycopg2._psycopg.cursor) -> Dict[str, object]:
    if cursor.rowcount != 1:
        raise SQLError(
            query=cursor.query, message=f'expected a single row but found {cursor.rowcount}'
        )
    else:
        return row_as_dict(names=[d[0] for d in cursor.description], row=cursor.fetchone())


def fetch_all(cursor: psycopg2._psycopg.cursor) -> List[Dict[str, object]]:
    names = [d[0] for d in cursor.description]
    return [row_as_dict(names=names, row=row) for row in cursor.fetchall()]
