import dataclasses as dc


class Error(Exception):
    pass


@dc.dataclass
class ParseError(Error):
    message: str


@dc.dataclass
class SchemaValidationError(Error):
    pass


@dc.dataclass
class SQLError(Error):
    query: str
    message: str


@dc.dataclass
class InvalidTokenError(Error):
    message: str = dc.field(default='token is invalid')
