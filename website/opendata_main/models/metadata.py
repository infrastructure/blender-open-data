import enum

from django.db import models
from django.db.models.query_utils import Q


class OperatingSystem(enum.Enum):
    linux = 'Linux'
    darwin = 'macOS'
    windows = 'Windows'


class Architecture(enum.Enum):
    x64 = 'x64'
    arm64 = 'arm64'


class Launcher(models.Model):
    class Meta:
        indexes = [models.Index(fields=['checksum'], name='launcher_index_checksum')]

        constraints = [
            models.UniqueConstraint(
                fields=['label', 'operating_system', 'cli'],
                name='launcher_unique_label_operating_system_cli',
            ),
            models.UniqueConstraint(
                fields=['operating_system', 'cli'],
                condition=Q(latest=True),
                name='launcher_one_latest_per_operating_system',
            ),
            models.CheckConstraint(
                check=Q(latest=False) | Q(supported=True), name='launcher_latest_implies_supported'
            ),
        ]

    label = models.CharField(max_length=128)
    operating_system = models.TextField(choices=[(os.name, os.value) for os in OperatingSystem])
    url = models.URLField(unique=True)
    checksum = models.CharField(
        max_length=128, unique=True, help_text='hex encoded sha256 checksum'
    )
    size = models.IntegerField(help_text='size in bytes')
    cli = models.BooleanField(null=False)

    # A Launcher is supported iff. we allow submissions using this version of the launcher.
    supported = models.BooleanField(null=False)

    # A Launcher is the latest iff. users are able to download this version of the launcher.
    latest = models.BooleanField(null=False)

    def __str__(self) -> str:
        if self.cli:
            return f'{self.label} ({OperatingSystem[self.operating_system].value}, CLI)'
        else:
            return f'{self.label} ({OperatingSystem[self.operating_system].value}, GUI)'


class BenchmarkScript(models.Model):
    label = models.CharField(max_length=128, unique=True)
    url = models.URLField(unique=True)
    checksum = models.CharField(
        max_length=128, unique=True, help_text='hex encoded sha256 checksum'
    )
    size = models.IntegerField(help_text='size in bytes')

    def __str__(self) -> str:
        return self.label


class Scene(models.Model):
    label = models.CharField(max_length=128, unique=True)
    url = models.URLField(unique=True)
    checksum = models.CharField(
        max_length=128, unique=True, help_text='hex encoded sha256 checksum'
    )
    size = models.IntegerField(help_text='size in bytes')
    is_deprecated = models.BooleanField(
        help_text='Scene will not be linked to new BlenderVersions', default=False
    )

    def __str__(self) -> str:
        return self.label


class BlenderVersion(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['label', 'operating_system', 'architecture'],
                name='blender_version_unique_label_operating_system_architecture',
            ),
            models.UniqueConstraint(fields=['checksum'], name='scene_unique_checksum'),
        ]

    label = models.CharField(max_length=128)
    operating_system = models.TextField(choices=[(os.name, os.value) for os in OperatingSystem])
    url = models.URLField(unique=True)
    checksum = models.CharField(
        max_length=128, unique=True, help_text='hex encoded sha256 checksum'
    )
    size = models.IntegerField(help_text='size in bytes')
    architecture = models.TextField(
        choices=[(a.name, a.value) for a in Architecture], default=Architecture.x64.value
    )

    benchmark_script = models.ForeignKey(
        BenchmarkScript, null=False, on_delete=models.PROTECT, related_name='blender_versions'
    )
    scenes = models.ManyToManyField(Scene, related_name='blender_versions')
    is_supported = models.BooleanField(
        help_text='BlenderVersion will be available in the metadata', default=True
    )

    def __str__(self) -> str:
        return f'{self.label} ({OperatingSystem[self.operating_system].value} - {Architecture[self.architecture].value})'
