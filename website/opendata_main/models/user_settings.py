from __future__ import annotations

from django.contrib.auth.models import User
from django.db import models


class UserSettings(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='settings')
    benchmarks_name = models.TextField(null=False, blank=True, default="")
    benchmarks_anonymous = models.BooleanField(null=False, default=True)
    benchmarks_verified = models.BooleanField(null=False, default=False)

    @classmethod
    def get_or_create(cls, user: User) -> UserSettings:
        try:
            return UserSettings.objects.get(user=user)
        except UserSettings.DoesNotExist:
            return UserSettings.objects.create(user=user)

    def __str__(self) -> str:
        return f'{self.user.username} <{self.user.email}>'
