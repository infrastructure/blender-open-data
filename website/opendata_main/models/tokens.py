import uuid

from django.contrib.auth.models import User
from django.db import models

from opendata_main.models.mixins import CreatedUpdatedMixin


class LauncherAuthenticationToken(CreatedUpdatedMixin, models.Model):
    class Meta:
        indexes = [models.Index(fields=['token'])]

    # We use an UUID as id to ensure that third parties cannot enumerate authentication tokens.
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    # The token is the secret value that authenticates the launcher as the user.
    token = models.UUIDField(unique=True, default=uuid.uuid4)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name='launcher_authentication_tokens'
    )
    hostname = models.TextField()
    verified = models.BooleanField(null=True)
