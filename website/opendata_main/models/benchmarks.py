from __future__ import annotations

import enum
import uuid
from typing import Dict

from django.contrib.auth.models import User
from django.db.models import JSONField
from django.db import models
from django.urls import reverse

from opendata_main.errors import ParseError
from opendata_main.models.mixins import CreatedUpdatedMixin
from opendata_main.parsing import parse_field


class BenchmarkSchemaVersion(enum.IntEnum):
    v1 = 1
    v2 = 2
    v3 = 3
    v4 = 4


class RawBenchmark(CreatedUpdatedMixin, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data = JSONField(editable=False)
    schema_version = models.TextField(
        editable=False, choices=[(version.name, version.name) for version in BenchmarkSchemaVersion]
    )
    is_valid = models.BooleanField(null=False, default=True)

    @classmethod
    def from_json_dict(cls, json_dict: Dict[str, object]) -> RawBenchmark:
        data = json_dict.get('data')
        if not isinstance(data, (dict, list)):
            raise ParseError(f'data should be of type dict or list but found {type(data)}')
        return RawBenchmark(
            data=data,
            schema_version=parse_field(json_dict, 'schema_version', BenchmarkSchemaVersion).name,
        )

    @property
    def is_indexed(self):
        return BenchmarkIndex.objects.filter(raw_benchmark=self).exists()

    def get_absolute_url(self):
        return reverse('benchmarks/benchmark', kwargs={'pk': self.pk})


class RawBenchmarkOwnership(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=('user', 'benchmark'), name='raw_benchmark_ownership_unique'
            )
        ]

        indexes = [
            models.Index(fields=['user'], name='raw_benchmark_user_index'),
            models.Index(fields=['benchmark'], name='raw_benchmark_benchmark_index'),
        ]

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='raw_benchmark_ownership')
    benchmark = models.ForeignKey(RawBenchmark, on_delete=models.CASCADE, related_name='ownership')


class BenchmarkIndex(models.Model):
    """Indexed benchmarks table.

    This table is mostly read-only and populated via Signals.
    """

    raw_benchmark = models.OneToOneField(
        RawBenchmark,
        on_delete=models.CASCADE,
        related_name='indexed_benchmark',
        primary_key=True,
    )
    score = models.FloatField()
    device_name = models.CharField(max_length=128)
    # CPU, GPU
    device_type = models.CharField(max_length=8)
    # OPTIX, CUDA, OPENCL, METAL
    compute_type = models.CharField(max_length=16)
    # Linux, Windows, macOS
    operating_system = models.CharField(max_length=16)
    blender_version = models.CharField(max_length=16)
    is_verified = models.BooleanField(default=False)
    username = models.CharField(max_length=128, null=True, blank=True)
    submission_date = models.DateTimeField('submission date')
    score_scenes = JSONField(null=True)  # Key value with scene_name: score

    class Meta:
        indexes = [
            models.Index(fields=['submission_date']),
        ]
        ordering = ['-submission_date']
        verbose_name_plural = "benchmark index"

    def get_absolute_url(self):
        return reverse('benchmarks/benchmark', kwargs={'pk': self.raw_benchmark.pk})
