from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from opendata_main.models.benchmarks import BenchmarkIndex
from opendata_main.models.user_settings import UserSettings


@receiver(post_save, sender=UserSettings)
def update_settings(
    sender: object, instance: UserSettings, created: bool, **kwargs: object
) -> None:
    """Whenever settings are updated, update related indexed benchmarks.

    For example, if a user enables name display in the settings, update all indexed
    benchmarks belonging to them with their name.
    """
    username = ''
    if not instance.benchmarks_anonymous:
        username = instance.benchmarks_name
    BenchmarkIndex.objects.filter(raw_benchmark__ownership__user=instance.user).update(
        username=username,
        is_verified=instance.benchmarks_verified,
    )


@receiver(pre_delete, sender=UserSettings)
def anonymize_benchmarks(
    sender: object, instance: UserSettings, created: bool, **kwargs: object
) -> None:
    """Whenever settings are updated, update related indexed benchmarks.

    For example, if a user enables name display in the settings, update all indexed
    benchmarks belonging to them with their name.
    """
    username = ''
    if not instance.benchmarks_anonymous:
        username = instance.benchmarks_name
    BenchmarkIndex.objects.filter(raw_benchmark__ownership__user=instance.user).update(
        username=username,
        is_verified=instance.benchmarks_verified,
    )
