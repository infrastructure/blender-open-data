from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from opendata_main.models.user_settings import UserSettings


@receiver(post_save, sender=User)
def create_profile(sender: object, instance: User, created: bool, **kwargs: object) -> None:
    if created:
        UserSettings.objects.create(user=instance)
