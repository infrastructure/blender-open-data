from django.db.models.signals import post_save
from django.dispatch import receiver

from opendata_main.models.benchmarks import RawBenchmark, RawBenchmarkOwnership
from opendata_main.indexing import BenchmarkIndexer


@receiver(post_save, sender=RawBenchmark)
def index_benchmark_after_save(
    sender: object, instance: RawBenchmark, created: bool, **kwargs: object
) -> None:
    """Reindex after RawBenchmark is created, or "is_valid" is updated."""
    if created:
        BenchmarkIndexer.index(instance)
    elif instance.is_valid:
        BenchmarkIndexer.index(instance, force=True)
    elif not instance.is_valid:
        BenchmarkIndexer.drop(instance)


@receiver(post_save, sender=RawBenchmarkOwnership)
def reindex_benchmark_after_ownership_save(
    sender: object, instance: RawBenchmarkOwnership, created: bool, **kwargs: object
) -> None:
    if created:
        BenchmarkIndexer.index(instance.benchmark, force=True)
