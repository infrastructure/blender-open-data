import functools
import uuid
from typing import NewType, Callable, TypeVar, Any, cast

from django.core.cache import cache as django_cache

from opendata_main.types import sentinel

Seconds = NewType('Seconds', float)

FuncType = TypeVar('FuncType', bound=Callable[..., object])


def cache(namespace: str, timeout: Seconds) -> Callable[[FuncType], FuncType]:
    key = f'{namespace}.{uuid.uuid4()}'

    def decorator(function: FuncType) -> FuncType:
        @functools.wraps(function)
        def wrapper(*args: object, **kwargs: object) -> Any:
            cached = django_cache.get(key, default=sentinel)
            if cached is sentinel:
                value = function(*args, **kwargs)
                django_cache.set(key, value, timeout)
                return value
            else:
                return cached

        return cast(FuncType, wrapper)

    return decorator
