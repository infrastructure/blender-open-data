from django.contrib import admin

import opendata_main.models.metadata

admin.site.register(opendata_main.models.metadata.BenchmarkScript)


@admin.register(opendata_main.models.metadata.Launcher)
class LauncherAdmin(admin.ModelAdmin):
    def make_launcher_unsupported(self, request, queryset):
        rows_updated = queryset.update(is_supported=False)
        if rows_updated == 1:
            message_bit = "1 launcher was"
        else:
            message_bit = "%s launchers were" % rows_updated
        self.message_user(request, "%s successfully marked as unsupported." % message_bit)

    def make_launcher_supported(self, request, queryset):
        rows_updated = queryset.update(is_supported=True)
        if rows_updated == 1:
            message_bit = "1 launcher was"
        else:
            message_bit = "%s launchers were" % rows_updated
        self.message_user(request, "%s successfully marked as supported." % message_bit)

    make_launcher_unsupported.short_description = 'Mark selected launchers as unsupported'
    make_launcher_supported.short_description = 'Mark selected launchers as supported'

    def make_launcher_latest(self, request, queryset):
        for launcher in queryset:
            opendata_main.models.metadata.Launcher.objects.filter(
                operating_system=launcher.operating_system,
                cli=launcher.cli,
                supported=True,
                latest=True,
            ).update(latest=False)
            launcher.latest = True
            launcher.save()

    make_launcher_latest.short_description = 'Mark selected launchers as latest'

    list_display = [
        'label',
        'operating_system',
        'supported',
        'cli',
        'latest',
    ]
    list_filter = [
        'operating_system',
        'label',
        'supported',
    ]
    actions = [
        make_launcher_unsupported,
        make_launcher_supported,
        make_launcher_latest,
    ]


@admin.register(opendata_main.models.metadata.Scene)
class SceneAdmin(admin.ModelAdmin):
    list_display = [
        'label',
        'is_deprecated',
    ]
    list_filter = [
        'is_deprecated',
    ]


@admin.register(opendata_main.models.metadata.BlenderVersion)
class BlenderVersionAdmin(admin.ModelAdmin):
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "scenes":
            kwargs["queryset"] = opendata_main.models.metadata.Scene.objects.filter(
                is_deprecated=False
            )
        return super(BlenderVersionAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )

    def make_blender_version_unsupported(self, request, queryset):
        queryset.update(is_supported=False)

    def make_blender_version_supported(self, request, queryset):
        queryset.update(is_supported=True)

    make_blender_version_unsupported.short_description = 'Mark selected versions as unsupported'
    make_blender_version_supported.short_description = 'Mark selected versions as supported'

    list_display = [
        'label',
        'operating_system',
        'architecture',
        'is_supported',
    ]
    list_filter = [
        'operating_system',
        'architecture',
        'label',
        'is_supported',
    ]
    actions = [
        make_blender_version_unsupported,
        make_blender_version_supported,
    ]
