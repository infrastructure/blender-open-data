from django.contrib import admin
from django.urls.base import reverse
from django.utils.html import format_html

import opendata_main.models.benchmarks


@admin.register(opendata_main.models.benchmarks.RawBenchmark)
class RawBenchmarkAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'schema_version',
        'is_valid',
    ]
    list_filter = [
        'schema_version',
        'is_valid',
    ]

    readonly_fields = ['schema_version', 'data']


@admin.register(opendata_main.models.benchmarks.BenchmarkIndex)
class BenchmarkIndexAdmin(admin.ModelAdmin):
    def link_to_raw(self, obj):
        link = reverse("admin:opendata_main_rawbenchmark_change", args=[obj.raw_benchmark_id])
        return format_html('<a href="{}">Edit Raw</a>', link)

    link_to_raw.short_description = 'Raw'

    def delete_raw(self, request, queryset):
        for b in queryset:
            b.raw_benchmark.delete()

        rows_deleted = queryset.count()
        if rows_deleted == 1:
            message_bit = "1 Raw benchmark was"
        else:
            message_bit = "%s Raw benchmarks were" % rows_deleted
        self.message_user(request, "%s successfully deleted." % message_bit)

    def get_trimmed_id(self, obj):
        return f"{str(obj.raw_benchmark_id)[:5]}..."

    get_trimmed_id.short_description = 'Raw Id'

    def get_username(self, obj):
        return obj.raw_benchmark.ownership.get().user.username

    get_username.short_description = 'Username'

    def get_score_round(self, obj):
        return int(obj.score)

    get_score_round.short_description = 'Score'
    get_score_round.admin_order_field = 'score'

    list_display = [
        'get_trimmed_id',
        'get_username',
        'device_name',
        'device_type',
        'compute_type',
        'operating_system',
        'get_score_round',
        'submission_date',
        'link_to_raw',
    ]
    list_filter = [
        'device_type',
        'compute_type',
        'operating_system',
        'blender_version',
    ]

    readonly_fields = [
        'raw_benchmark',
    ]

    search_fields = [
        'device_name',
    ]

    actions = [delete_raw]
