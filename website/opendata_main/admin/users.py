from django.contrib import admin

import opendata_main.models.benchmarks

import opendata_main.models.tokens
import opendata_main.models.user_settings

admin.site.register(opendata_main.models.user_settings.UserSettings)

admin.site.register(opendata_main.models.tokens.LauncherAuthenticationToken)
