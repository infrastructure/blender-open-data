function drawDeviceTypeDistribution() {
  let jsonData = JSON.parse(
    document.getElementById('compute-type-distribution-json').innerHTML
  );

  let context = document.getElementById('compute-type-distribution-chart').getContext('2d');
  createPieChart(context,
    jsonData.map(e => e['count']),
    jsonData.map(e => e['compute_type'])
  )
}

function drawOperatingSystemDistribution() {
  let jsonData = JSON.parse(
    document.getElementById('operating-system-distribution-json').innerHTML
  );

  let context = document.getElementById('operating-system-distribution-chart').getContext('2d');
  createPieChart(context,
    jsonData.map(e => e['count']),
    jsonData.map(e => e['os']),
  );
}

function createDeviceBarChart(context, labels, scores, maxRenderTime) {

  return new Chart(context, {
    type: 'horizontalBar',
    data: {
      // datasets: [{
      // 	data: data,
      // 	backgroundColor: data.map((_, i) => getColor(i))
      // }],
      datasets: [{
        label: 'Cycles Benchmark',
        data: scores,
        backgroundColor: getColor(0),
      }],
      labels: labels,
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        mode: 'point',
        cornerRadius: 3,
        footerFontSize: 10,
        footerFontStyle: 'normal',
        callbacks: {
          label: function (tooltipItem, data) {
            const benchmark = data.datasets[tooltipItem.datasetIndex].label;
            return benchmark + ' (Score: ' + tooltipItem.xLabel.toFixed(2) + ')';
          },

        }
      },
      legend: {display: false},
      plugins: {
        datalabels: {
          align: 'end',
          anchor: 'start',
          offset: 10,
          formatter: function (value, context) {
            if (context.datasetIndex === 0) {
              return context.chart.data.labels[context.dataIndex];
            } else {
              return null;
            }
          },
          backgroundColor: 'rgba(255, 255, 255, 0.8)',
          borderRadius: 3,
          color: '#001f3f',
        }
      },
      scales: {
        yAxes: [{
          stacked: true,
          ticks: {
            display: false,
          }
        }],
        xAxes: [{
          stacked: true,
          scaleLabel: {
            display: true,
            labelString: 'Benchmark Score'
          },
          ticks: {
            beginAtZero: true,
            suggestedMax: maxRenderTime,
          }
        }]
      },
      animation: {
        duration: 0 // general animation time
      },
      hover: {
        animationDuration: 0 // duration of animations when hovering an item
      },
      responsiveAnimationDuration: 0 // animation duration after a resize
    }
  });
}

function drawFastestTotalMedianScore() {
  const cpuJsonData = JSON.parse(
    document.getElementById('fastest-total-median-score-cpus-json').innerHTML
  );
  const gpuJsonData = JSON.parse(
    document.getElementById('fastest-total-median-score-gpus-json').innerHTML
  );
  const maxRenderTimeCPUs = Math.max(
    ...cpuJsonData.map(e => e['median_score'])
  );
  const maxRenderTimeGPUs = Math.max(
    ...gpuJsonData.map(e => e['median_score'])
  );
  drawFastestTotalMedianRenderTimeCPUs(cpuJsonData, maxRenderTimeCPUs);
  drawFastestTotalMedianRenderTimeGPUs(gpuJsonData, maxRenderTimeGPUs);
}

function drawFastestTotalMedianRenderTimeCPUs(jsonData, maxRenderTime) {
  if (jsonData.length === 0) return;
  const context = document.getElementById('fastest-total-median-score-cpus-chart').getContext('2d');
  createDeviceBarChart(context,
    jsonData.map(e => e['device_name']),
    jsonData.map(e => e['median_score']),
    maxRenderTime,
  )
}

function drawFastestTotalMedianRenderTimeGPUs(jsonData, maxRenderTime) {
  if (jsonData.length === 0) return;
  let context = document.getElementById('fastest-total-median-score-gpus-chart').getContext('2d');
  createDeviceBarChart(context,
    jsonData.map(e => e['device_name']),
    jsonData.map(e => e['median_score']),
    maxRenderTime,
  )
}


function drawNumberOfBenchmarksOverTime() {
  let jsonData = JSON.parse(
    document.getElementById('number-of-benchmarks-over-time-json').innerHTML
  );

  const canvas = document.getElementById('number-of-benchmarks-over-time-chart');
  // Hardcode chart height. Needs maintainAspectRatio: false
  canvas.parentNode.style.height = '250px';
  let context = canvas.getContext('2d');

  createNumberOfBenchmarksOverTimeChart(context,
    jsonData.map(e => ({x: e['date'], y: e['count']}))
  )
}

function createNumberOfBenchmarksOverTimeChart(context, data) {
  return new Chart(context, {
    type: 'bar',
    data: {
      datasets: [
        {
          data: data,
          backgroundColor: getColor(0)
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            return tooltipItem.yLabel;
          }
        }
      },
      legend: {display: false},
      plugins: {
        datalabels: {
          display: false,
        },
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              day: 'D MMM'
            }
          }
        }]
      },
      animation: {
        duration: 0 // general animation time
      },
      hover: {
        animationDuration: 0 // duration of animations when hovering an item
      },
      responsiveAnimationDuration: 0 // animation duration after a resize
    }
  });
}

function getOS() {
  const platform = window.navigator.platform;
  const macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
  const windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'];

  if (macosPlatforms.indexOf(platform) !== -1) {
    return 'darwin';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    return 'windows';
  } else if (/Linux/.test(platform)) {
    return 'linux';
  } else {
    return null;
  }
}

function getReadableFileSizeString(fileSizeInBytes) {
  let i = -1;
  let byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
  do {
    fileSizeInBytes = fileSizeInBytes / 1024;
    i++;
  } while (fileSizeInBytes > 1024);

  return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
}

function downloadButtonTextGenerator(label, size, url) {
  const downloadButton = $('#benchmark_download');
  downloadButton.html('<i class="i-' + label + '"></i> Download Benchmark for ' + label);
  downloadButton.attr('title', 'Download Benchmark for ' + label + ' (' + getReadableFileSizeString(size) + ')');
  downloadButton.attr('href', url);
}

function dropdownButtonHTMLGenerator(label, cli, size, url, cssClass) {
  $('.download-other-list').append('<li class="os ' + cssClass + '"><a href="' + url + '" title="Download Benchmark for ' + label + '" class="js-download"><span class="name">' + label + cli + '</span><span class="size">' + getReadableFileSizeString(size) + '</span></a></li>');
}

function setupDropdownDownload() {
  let latestLauncherRaw = JSON.parse(
    document.getElementById('latest-launcher-json').innerHTML
  );


  let os = getOS();
  $.each(latestLauncherRaw, function (index, value) {
    if (value['operating_system'] !== os || value['cli'] == true) {
      let cli = null;
      if (value.cli == true) {
        cli = ' CLI'
      } else {
        cli = ''
      }
      switch (value.operating_system) {
        case "darwin":
          dropdownButtonHTMLGenerator('macOS', cli, value.size, value.url, 'macos');
          break;
        case 'windows':
          dropdownButtonHTMLGenerator('Windows', cli, value.size, value.url, 'windows');
          break;
        case "linux":
          dropdownButtonHTMLGenerator('Linux', cli, value.size, value.url, 'linux');
          break;
        default:
      }
    }
  });
}


function setDownloadButton() {
  let latestLauncherRaw = JSON.parse(
    document.getElementById('latest-launcher-json').innerHTML
  );

  let latestLauncherNoCli = latestLauncherRaw.filter(latestLauncherNoCli => latestLauncherNoCli.cli == false)

  let latestLauncher = {};
  latestLauncherNoCli.forEach(element => {
    latestLauncher[element['operating_system']] = element;
  });

  let os = getOS();
  switch (os) {
    case 'darwin':
      downloadButtonTextGenerator('macOS', latestLauncher['darwin'].size, latestLauncher['darwin'].url);
      break;
    case 'windows':
      downloadButtonTextGenerator('Windows', latestLauncher['windows'].size, latestLauncher['windows'].url);
      break;
    case 'linux':
      downloadButtonTextGenerator('Linux', latestLauncher['linux'].size, latestLauncher['linux'].url);
      break;
    default:
      downloadButtonTextGenerator('Windows', latestLauncher['windows'].size, latestLauncher['windows'].url);
  }
}

$(document).ready(function () {
  drawDeviceTypeDistribution();
  drawOperatingSystemDistribution();
  drawFastestTotalMedianScore();
  drawNumberOfBenchmarksOverTime();
  setDownloadButton();
  setupDropdownDownload();
});
