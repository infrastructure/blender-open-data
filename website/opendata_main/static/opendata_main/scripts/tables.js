function createTable(element, rows, columns, orderedColumnIndex) {
  if (orderedColumnIndex === -1 ) {orderedColumnIndex = 0}
  $(element).DataTable({
    data: rows,
    columns: columns,
    pageLength: 25,
    responsive: true,
    order: [[ orderedColumnIndex, 'desc' ]],
    lengthMenu: [10, 25, 50, 100],
    // insert <div> with class 'top' and then include rtip (p[r]ocessing unit, [t]able, [i] stats, [p]agination).
    dom: "<'datatable-topbar'<'datatable-bar-filters'fl><'datatable-bar-buttons'<'top'>>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row datatable-bottombar'<'datatable-stats col-sm-12 col-md-4'i><'datatable-pagination col-sm-12 col-md-8'p>>",
    buttons: [
      {
        extend: 'csv',
        text: '<i class="i-download"></i> CSV',
      },
      {
        text: '<i class="i-download"></i> JSON',
        action: function ( e, dt, button, config ) {
          var data = dt.buttons.exportData();

          $.fn.dataTable.fileSave(
            new Blob( [ JSON.stringify( data ) ] ),
            'Export.json'
          );
        }
      }
    ],

    columnDefs: [
      // Show all columns on all devices
      { className: 'all', targets: '_all'},
    ],

  })

  $("#" + element.id + "_wrapper .top").append('<div class="search-bar"><div class="js-download-data search-bar-actions"></div></div>');

  const table = $(element).DataTable();

  //When typing or X click (search.dt is an event from DataTables for this) update the table using .draw()
  $(' .search-input').on( 'keyup search.dt', function () {
    table.search( this.value ).draw()
  } );

  //On change event of the length dropdown (how many results to show) re-draw table using .draw()
  $('.datatables-length-dropdrown ').on('change', function(){
    table.page.len( this.value ).draw();
  })

  //Append the download buttons into the .top div.
  $(element).DataTable().buttons().container().appendTo( $('.js-download-data', table.table().container() ) );

  // Remove certain Bootstrap classes from input and select fields.
  $('.dataTables_wrapper .dataTables_filter input, .dataTables_wrapper .dataTables_length select').removeClass('form-control-sm custom-select custom-select-sm');
}
