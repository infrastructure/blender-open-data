function secondsToHms(d) {
	d = Number(d);
	let h = Math.floor(d / 3600);
	let m = Math.floor(d % 3600 / 60);
	let s = Math.floor(d % 3600 % 60);

	let hDisplay = h > 0 ? h + "h " : "";
	let mDisplay = m > 0 ? m + "m " : "";
	let sDisplay = s > 0 ? s + "s" : "";

	return hDisplay + mDisplay + sDisplay;
}

function formatScore(d) {
	d = Number(d);
	return `Score: ${Math.round((d + Number.EPSILON) * 100) / 100}`
}


function createAreaScatterChart(context, distribution, benchmarkData, samplesLabels, samplesData, maximumScore) {
	return new Chart(context, {
		type: 'line',
		data: {
			datasets: [{
				label: 'Total Score Distribution',
				data: distribution,
				backgroundColor: '#f7d67d',
				borderColor: '#f7d67d',
				pointRadius: 0,
				pointHoverRadius: 0,

				order: 2,
				fill: false,
			}, {
				label: 'This Benchmark',
				data: benchmarkData,
				backgroundColor: 'rgb(255, 89, 102)',
				borderColor: 'rgb(255, 89, 102)',
				pointRadius: 7,
				pointHoverRadius: 7,
				// borderWidth: 0,
				order: 0
			}, {
				label: 'Other Devices',
				labels: samplesLabels,
				data: samplesData,
				backgroundColor: 'rgb(255, 189, 14)',
				borderColor: 'rgb(255, 189, 14)',
				pointRadius: 4,
				// borderWidth: 0,
				fill: false,
				showLine: false,
				order: 1,
			}],
		},
		options: {
			scales: {
				xAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'Rank (Top %)'
					},
					type: 'linear',
					position: 'bottom',
					ticks: {
						min: 0.0,
						max: 1.0,
						beginAtZero: true,
						callback: function (value) {
							return Math.floor(value * 100) + '%';
						},
						stepSize: 0.1
					},
				}],
				yAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'Score'
					},
					ticks: {
						max: maximumScore,
						beginAtZero: false,
						maxTicksLimit: 15,
						stepSize: 100
					}
				}]
			},
			plugins: {
				datalabels: {
					display: false,
				}
			},
			tooltips: {
				mode: 'x',
				intersect: false,
				filter: function (t) {
					return t.datasetIndex !== 0;
				},
				callbacks: {
					title: function () {
					},
					label: function (tooltipItem, data) {
						if (tooltipItem.datasetIndex === 2) {
							return data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index] + ' (' + formatScore(tooltipItem.yLabel) + ')';
						} else if (tooltipItem.datasetIndex === 1) {
							return 'This Benchmark (' + formatScore(tooltipItem.yLabel) + ')';
						}
					}
				}
			},
			elements: {
				line: {
					tension: 0 // disables bezier curves
				}
			},
			animation: {
				duration: 0 // general animation time
			},
			hover: {
				animationDuration: 0 // duration of animations when hovering an item
			},
			responsiveAnimationDuration: 0 // animation duration after a resize
		}
	});
}

function drawBenchmark(jsonData) {
	let distributionGraph = jsonData['distribution_graph'];

	let currentBenchmark = jsonData['current_benchmark'];
	let currentBenchmarkData = [{
		x: currentBenchmark['rank'],
		y: currentBenchmark['total_score']
	}];
	let currentBenchmarkRank = currentBenchmark['rank'];
	let minimumRank = Math.min(0.05, Math.max(0.0, currentBenchmarkRank - 0.1));
	let maximumScore = distributionGraph.find(e => e[0] >= minimumRank)[1];

	let deviceSamples = jsonData['device_samples'].filter(
		e => e['rank'] >= minimumRank && Math.abs(e['rank'] - currentBenchmarkRank) > 0.01
	);

	let deviceSamplesLabels = deviceSamples.map(
		e => e['device_name']
	);

	let deviceSamplesData = deviceSamples.map(e => ({
		x: e['rank'],
		y: e['total_median_score']
	}));


	let context = document.getElementById('benchmark-chart').getContext('2d');

	createAreaScatterChart(
		context,
		distributionGraph.map(e => ({x: e[0], y: e[1]})),
		currentBenchmarkData,
		deviceSamplesLabels,
		deviceSamplesData,
		maximumScore,
	);
}

function setRanking(jsonData) {
	let ranking = jsonData['current_benchmark']['rank'];
	$('#ranking').html(Math.ceil(ranking * 100) + '%');
}

$(document).ready(function () {
	let jsonData = JSON.parse(
		document.getElementById('comparison-data-json').innerHTML
	);

	drawBenchmark(jsonData);
	setRanking(jsonData);
});
