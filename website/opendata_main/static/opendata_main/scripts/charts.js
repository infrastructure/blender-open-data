function getColor(i) {
	let colors = [
		'rgb(255, 89, 102)', // reddish
		'rgb(255, 189, 14)', // orange
		'rgb(75, 202, 192)', // green-cyan
		'rgb(75, 255, 192)', // green-bright
		'rgb(114, 212, 255)', // blue
		'rgb(233, 152, 255)', // pink
		'rgb(171, 173, 227)', // purple
		'rgb(255, 245, 86)', // yellow
		'rgb(255, 139, 112)', // red
	];
	return colors[i % colors.length];
}

function createPieChart(context, data, labels) {
	return new Chart(context, {
		type: 'pie',
		data: {
			datasets: [{
				data: data,
				backgroundColor: data.map((_, i) => getColor(i))
			}],
			labels: labels
		},
		options: {
			legend: {display: false},
			plugins: {
				datalabels: {
					formatter: function (value, context) {
						return context.chart.data.labels[context.dataIndex];
					},
					backgroundColor: 'rgba(255, 255, 255, 0.8)',
					borderRadius: 4,
					color: '#001f3f',
				}
			},
			animation: {
				duration: 0 // general animation time
			},
			hover: {
				animationDuration: 0 // duration of animations when hovering an item
			},
			responsiveAnimationDuration: 0 // animation duration after a resize
		}
	});
}

function createBarChart(context, data, labels) {
	return new Chart(context, {
		type: 'horizontalBar',
		data: {
			datasets: [{
				data: data,
				backgroundColor: data.map((_, i) => getColor(i))
			}],
			labels: labels,
		},
		options: {
			tooltips: {
				callbacks: {
					label: function (tooltipItem, data) {
						var label = data.datasets[tooltipItem.datasetIndex].label || '';

						if (label) {
							label += ': ';
						}
						label += tooltipItem.xLabel.toFixed(2);
						return label;
					}
				}
			},
			legend: {display: false},
			plugins: {
				datalabels: {
					align: 'end',
					anchor: 'start',
					offset: 10,
					formatter: function (value, context) {
						return context.chart.data.labels[context.dataIndex];
					},
					backgroundColor: 'rgba(255, 255, 255, 0.8)',
					borderRadius: 3,
					color: '#001f3f',
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						display: false,
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			},
			animation: {
				duration: 0 // general animation time
			},
			hover: {
				animationDuration: 0 // duration of animations when hovering an item
			},
			responsiveAnimationDuration: 0 // animation duration after a resize
		}
	});
}
