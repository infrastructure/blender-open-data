import requests
from typing import Optional
from dataclasses import dataclass

from django.core.management.base import BaseCommand, CommandError, CommandParser

from opendata_main.models.metadata import BlenderVersion, BenchmarkScript, Scene


@dataclass
class BlenderVersionNumber:
    version_str: str
    version_list: Optional[list] = None

    @property
    def stripped(self):
        return '.'.join(self.version_list[:-1])

    @property
    def full(self):
        return '.'.join(self.version_list)

    def __post_init__(self):
        self.version_list = [n for n in self.version_str.split('.')]
        if len(self.version_list) != 3:
            raise CommandError("Version %s is not valid, use x.x.x" % self.version_str)
        if [x for x in self.version_list if x == '']:
            raise CommandError("Version %s is not valid, use x.x.x" % self.version_str)
        if int(self.version_list[0]) < 2:
            raise CommandError(
                "Version %s is not valid. Only 3 and up are supported" % self.version_str
            )


class Command(BaseCommand):
    help = 'Adds a BlenderVersion to Open Data'

    url_base = 'https://download.blender.org/release/'
    url_cdn = 'https://opendata.blender.org/cdn/'

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument('blender_version', type=str)

    def get_versions_sha256(self, blender_version: BlenderVersionNumber):
        """Scrape download.blender.org and fetch the sha256 hashes.

        Returns a dictionary of filename: checksums for each Blender version.
        """
        url = f"{self.url_base}Blender{blender_version.stripped}/blender-{blender_version.full}.sha256"
        checksums = {}
        r = requests.get(url, stream=True)
        for line in r.iter_lines():
            if line:
                decoded_line = line.decode("utf-8")
                checksum, name = decoded_line.split()
                checksums[name] = checksum
        return checksums

    def handle(self, *args: object, **options: object) -> None:
        blender_version = BlenderVersionNumber(version_str=options['blender_version'])
        checksums = self.get_versions_sha256(blender_version)
        # Iterate of the 'bundled' versions of Blender that should be offered by the launcher
        for variant in ['linux-x64.tar.xz', 'macos-x64.dmg', 'macos-arm64.dmg', 'windows-x64.zip']:
            file_version = f"blender-{blender_version.full}-{variant}"
            operating_system = variant.split('-')[0]
            architecture = variant.split('-')[1].split('.')[0]
            if operating_system == 'macos':
                operating_system = 'darwin'
            # Get file size for the Blender version
            r = requests.head(f"{self.url_base}Blender{blender_version.stripped}/{file_version}")
            r_size = r.headers['Content-Length']
            b, _ = BlenderVersion.objects.get_or_create(
                label=blender_version.full,
                operating_system=operating_system,
                architecture=architecture,
                url=f"{self.url_cdn}Blender{blender_version.stripped}/{file_version}",
                checksum=checksums[file_version],
                size=r_size,
                benchmark_script=BenchmarkScript.objects.last(),
            )
            supported_scenes = Scene.objects.filter(is_deprecated=False)
            b.scenes.add(*supported_scenes)

            self.stderr.write(self.style.SUCCESS(f'Added {file_version}'))
