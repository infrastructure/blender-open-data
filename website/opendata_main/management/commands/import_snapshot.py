import argparse
import datetime
import json
import uuid
from io import TextIOBase
from typing import Dict, List

from django.core.management.base import BaseCommand, CommandParser

from opendata_main.models.benchmarks import RawBenchmark, BenchmarkSchemaVersion
from opendata_main.parsing import parse_field
from opendata_main.schemas import benchmarks


class Command(BaseCommand):
    help = 'Imports an Open Data snapshot'

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--legacy-format',
            action='store_true',
            default=False,
            help='assume the dump is in legacy format',
        )
        parser.add_argument('snapshot', type=argparse.FileType('r'), help='a .jsonl file to import')

    def handle(self, *args: object, **options: object) -> None:
        snapshot = options['snapshot']
        assert isinstance(snapshot, TextIOBase)

        if options['legacy_format']:
            self._import_legacy(snapshot)
        else:
            self._import(snapshot)

    def _import(self, snapshot: TextIOBase) -> None:
        number_of_samples = sum(1 for line in snapshot)
        snapshot.seek(0)

        with snapshot:
            for i, sample in enumerate(snapshot):
                if i % 100 == 0:
                    self.stderr.write(f'Importing {i + 1}/{number_of_samples}\n')
                json_dict = json.loads(sample)
                assert isinstance(json_dict, dict)
                data = json_dict.get('data')
                if not isinstance(data, (dict, list)):
                    raise Exception(f'data should be of type dict or list but found {type(data)}')

                benchmark = RawBenchmark(
                    id=uuid.UUID(hex=parse_field(json_dict, 'id', str)),
                    data=data,
                    schema_version=parse_field(
                        json_dict, 'schema_version', BenchmarkSchemaVersion
                    ).name,
                )
                if RawBenchmark.objects.filter(id=benchmark.id).exists():
                    continue
                benchmarks.validate(benchmark)
                benchmark.save()
                benchmark.created_at = datetime.datetime.fromisoformat(
                    parse_field(json_dict, 'created_at', str)
                )
                benchmark.save()

        self.stderr.write(self.style.SUCCESS(f'Importing complete!\n'))

    def _import_legacy(self, snapshot: TextIOBase) -> None:
        benchmarks_data: Dict[uuid.UUID, List[Dict[str, object]]] = {}
        benchmarks_created_at: Dict[uuid.UUID, datetime.datetime] = {}

        number_of_samples = sum(1 for line in snapshot)
        snapshot.seek(0)

        with snapshot:
            for i, sample in enumerate(snapshot):
                if i % 100 == 0:
                    self.stderr.write(f'Parsing {i + 1}/{number_of_samples}\n')
                json_dict = json.loads(sample)
                assert isinstance(json_dict, dict)
                _source = parse_field(json_dict, '_source', dict)
                created_at = datetime.datetime.fromisoformat(
                    parse_field(_source, 'date_created', str)
                )
                benchmark_id = uuid.UUID(hex=parse_field(_source, 'benchmark_id', str))
                data = parse_field(_source, 'data', dict)
                benchmarks_data.setdefault(benchmark_id, []).append(data)
                benchmarks_created_at[benchmark_id] = created_at

        number_of_benchmarks = len(benchmarks_data)

        for i, (benchmark_id, datas) in enumerate(benchmarks_data.items()):
            if i % 100 == 0:
                self.stderr.write(f'Inserting {i + 1}/{number_of_benchmarks}\n')

            data = datas[0].copy()
            data['scenes'] = [data['scene'] for data in datas]
            del data['scene']

            schema_version = BenchmarkSchemaVersion.v2.name

            benchmark = RawBenchmark(id=benchmark_id, data=data, schema_version=schema_version)
            benchmarks.validate(benchmark)
            benchmark.created_at = benchmarks_created_at[benchmark_id]
            benchmark.save()

        self.stderr.write(self.style.SUCCESS(f'Importing complete!\n'))
