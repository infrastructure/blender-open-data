import json
from typing import Dict

from django.core.management.base import BaseCommand

from opendata_main.models.benchmarks import RawBenchmark


class Command(BaseCommand):
    help = 'Dumps an Open Data snapshot to stdout'

    def _benchmark_to_json_dict(self, benchmark: RawBenchmark) -> Dict[str, object]:
        return {
            'id': str(benchmark.id),
            'created_at': benchmark.created_at.isoformat(),
            'schema_version': benchmark.schema_version,
            'data': benchmark.data,
        }

    def handle(self, *args: object, **options: object) -> None:
        for i, benchmark in enumerate(RawBenchmark.objects.all()):
            self.stdout.write(
                json.dumps(self._benchmark_to_json_dict(benchmark), sort_keys=True) + '\n'
            )
