from django.core.management.base import BaseCommand, CommandParser

from opendata_main.indexing import BenchmarkIndexer


class Command(BaseCommand):
    help = 'Reindex benchmarks'

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--force',
            action='store_true',
            default=False,
            help='Drop benchmark just before reindexing',
        )
        parser.add_argument(
            '--drop-index',
            action='store_true',
            default=False,
            help='Drop all benchmarks before stargin to index indexing',
        )

    def handle(self, *args: object, **options: object) -> None:
        BenchmarkIndexer.reindex_all(options['force'], options['drop_index'])
        self.stderr.write(self.style.SUCCESS(f'Indexing complete!\n'))
