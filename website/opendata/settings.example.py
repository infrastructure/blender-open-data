import os

# noinspection PyUnresolvedReferences
from typing import List

from opendata.settings_common import *

# Enable to use OAuth without https during local development
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

SECRET_KEY = '7ijgr7*_l=x^f0!((j!**xfbf3vcz0=#!5aai85$h3ck6l9m3c'
DEBUG = True
ALLOWED_HOSTS: List[str] = ['opendata.local']

BLENDER_ID = {
    # MUST end in a slash:
    "BASE_URL": "http://id.local:8000/",
    "OAUTH_CLIENT": "ocE3W8wQKwaKeMnzxs9UW9c1J0JhEEn46QSOi5Wn",
    "OAUTH_SECRET": "1LLEtHlc2SA1ZEm7aD4OxBQXDdrq6WIR9n6ybkMysGnKZzLeH6UmZNEuwytXWB2vEAQTCeBJ1vqsVRRmRC4FlIUjDwBMgKXPPDpyxPUUCxX4I48Czn8V7syGCQzJPykT",
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'opendata',
        'USER': 'opendata',
        'PASSWORD': 'opendata',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

LAUNCHER_LOGS_DIRECTORY = '/tmp/launcher_logs'
LAUNCHER_LOGS_MAX_SIZE = 10 * 1024 * 1024

# import sentry_sdk
# from sentry_sdk.integrations.django import DjangoIntegration
#
# sentry_sdk.init(
#     dsn="<snip>",
#     integrations=[DjangoIntegration()]
# )

if os.environ.get('ADMINS') is not None:
    # Expects the following format:
    #   ADMINS='J Doe: jane@example.com, John Dee: john@example.com'
    ADMINS = [[_.strip() for _ in adm.split(':')] for adm in os.environ.get('ADMINS').split(',')]
    EMAIL_SUBJECT_PREFIX = f'[{ALLOWED_HOSTS[0]}]'
    SERVER_EMAIL = f'django@{ALLOWED_HOSTS[0]}'
