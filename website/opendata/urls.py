from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView


from opendata_main.views.benchmarks.benchmark import BenchmarkDetailView
from opendata_main.views.benchmarks.logs import BenchmarkLogsView
from opendata_main.views.benchmarks.metadata import MetadataView
from opendata_main.views.benchmarks.query import BenchmarkQueryView, BenchmarkQuerySuggestView
from opendata_main.views.benchmarks.submit import BenchmarkSubmitView
from opendata_main.views.benchmarks.tokens.revoke import TokenRevokeView
from opendata_main.views.benchmarks.tokens.verify import TokenVerifyView
from opendata_main.views.home import HomeView
from opendata_main.views.users.benchmarks import UserBenchmarksView
from opendata_main.views.users.settings import UserSettingsView
from opendata_main.views.users.tokens import UserTokensView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('oauth/', include('blender_id_oauth_client.urls')),
    path('', HomeView.as_view(), name='home'),
    path('about/', TemplateView.as_view(template_name='opendata_main/about.pug'), name='about'),
    path(
        'download/',
        TemplateView.as_view(template_name='opendata_main/download.pug'),
        name='download',
    ),
    # Redirect for legacy URL
    path('raw-data/', RedirectView.as_view(url='download', permanent=True), name='raw_data'),
    path(
        'updates/', TemplateView.as_view(template_name='opendata_main/updates.pug'), name='updates'
    ),
    path(
        'benchmarks/<uuid:pk>/',
        BenchmarkDetailView.as_view(),
        name='benchmarks/benchmark',
    ),
    path(
        'benchmarks/tokens/<uuid:pk>/verify/',
        TokenVerifyView.as_view(),
        name='benchmarks/tokens/verify',
    ),
    path(
        'benchmarks/tokens/<uuid:pk>/revoke/',
        TokenRevokeView.as_view(),
        name='benchmarks/tokens/revoke',
    ),
    path('benchmarks/submit/', BenchmarkSubmitView.as_view(), name='benchmarks/submit'),
    path(
        'benchmarks/logs/<slug:sentry_event_id>/',
        BenchmarkLogsView.as_view(),
        name='benchmarks/logs',
    ),
    path('benchmarks/metadata/', MetadataView.as_view(), name='benchmarks/metadata'),
    path('benchmarks/query/', BenchmarkQueryView.as_view(), name='benchmarks/query'),
    path(
        'benchmarks/query/suggest/',
        BenchmarkQuerySuggestView.as_view(),
        name='benchmarks/query/suggest',
    ),
    path('users/settings/', UserSettingsView.as_view(), name='users/settings'),
    path('users/benchmarks/', UserBenchmarksView.as_view(), name='users/benchmarks'),
    path('users/tokens/', UserTokensView.as_view(), name='users/tokens'),
]
