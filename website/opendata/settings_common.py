import pathlib

BASE_DIR = pathlib.Path(__file__).absolute().parent.parent

# Application definition

INSTALLED_APPS = [
    'opendata_main',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'pipeline',
    'blender_id_oauth_client',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'opendata.urls'

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = BASE_DIR / 'public/static'
MEDIA_ROOT = BASE_DIR / 'public/media'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(BASE_DIR / 'templates'), str(BASE_DIR / 'blender-web-assets/src/templates')],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            # PyPugJS:
            'loaders': [
                (
                    'pypugjs.ext.django.Loader',
                    (
                        'django.template.loaders.filesystem.Loader',
                        'django.template.loaders.app_directories.Loader',
                    ),
                )
            ],
            'builtins': ['pypugjs.ext.django.templatetags'],
        },
    }
]

WSGI_APPLICATION = 'opendata.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# Blender ID login with Blender ID OAuth client

LOGIN_URL = '/oauth/login'
LOGOUT_URL = '/oauth/logout'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

USE_I18N = True

USE_L10N = True

USE_TZ = True

PIPELINE = {
    # 'PIPELINE_ENABLED': False,
    'JS_COMPRESSOR': 'pipeline.compressors.jsmin.JSMinCompressor',
    'CSS_COMPRESSOR': 'pipeline.compressors.NoopCompressor',
    'JAVASCRIPT': {
        'vendor': {
            'source_filenames': (
                'opendata_main/scripts/vendor/jquery-3.6.0.min.js',
                'opendata_main/scripts/vendor/moment.js',
                'opendata_main/scripts/vendor/Chart.js',
                'opendata_main/scripts/vendor/chartjs-plugin-datalabels.js',
                'opendata_main/scripts/vendor/datatables.js',
                'opendata_main/scripts/vendor/jquery-ui.js',
                'opendata_main/scripts/vendor/jquery.json-viewer.js',
            ),
            'output_filename': 'js/vendor.js',
        },
        'charts': {
            'source_filenames': ('opendata_main/scripts/charts.js',),
            'output_filename': 'js/charts.js',
        },
        'tables': {
            'source_filenames': ('opendata_main/scripts/tables.js',),
            'output_filename': 'js/tables.js',
        },
        'naturalLanguageForm': {
            'source_filenames': ('opendata_main/scripts/naturalLanguageForm.js',),
            'output_filename': 'js/naturalLanguageForm.js',
        },
        'home': {
            'source_filenames': ('opendata_main/scripts/home.js',),
            'output_filename': 'js/home.js',
        },
        'benchmark': {
            'source_filenames': ('opendata_main/scripts/benchmark.js',),
            'output_filename': 'js/benchmark.js',
        },
        'about': {
            'source_filenames': ('opendata_main/scripts/about.js',),
            'output_filename': 'js/about.js',
        },
        'web-assets': {
            'source_filenames': ('tutti/10_navbar.js',),
            'output_filename': 'js/web-assets.js',
        },
        'tutti': {
            'source_filenames': (),
            'output_filename': 'js/tutti.js',
        },
    },
    'STYLESHEETS': {
        'main': {
            'source_filenames': ('opendata_main/styles/main.sass',),
            'output_filename': 'css/main.css',
            'extra_context': {'media': 'screen,projection'},
        },
    },
    'COMPILERS': ('libsasscompiler.LibSassCompiler',),
    'DISABLE_WRAPPER': True,
}

STATICFILES_STORAGE = 'pipeline.storage.PipelineManifestStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
)

STATICFILES_DIRS = [
    str(BASE_DIR / 'blender-web-assets/assets/'),
    str(BASE_DIR / 'blender-web-assets/src/scripts/'),
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'},
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {'opendata': {'level': 'DEBUG'}, 'opendata_main': {'level': 'DEBUG'}},
    'root': {'level': 'WARNING', 'handlers': ['console', 'mail_admins']},
}

LAUNCHER_AUTHENTICATOR_NOTIFICATION_CHANNEL = (
    'opendata_main_launcherauthenticationtoken_user_response'
)


# Used as default setting for various queries (home.py, query.py)
DEFAULT_BLENDER_VERSION = '4.3.0'
MINIMUM_NUMBER_OF_SAMPLES_PER_BENCHMARK = 5
