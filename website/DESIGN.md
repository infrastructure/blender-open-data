# Design

The website is mostly a simple Django app except for the fact that we have the
`Benchmark` model being handled by PostgreSQL directly. The original
functionality is defined in
[`opendata_main/migrations/0003_benchmarks.sql`](opendata_main/migrations/0002_benchmarks.sql),
and it has been partially updated to support Blender 3.0 in 
[`opendata_main/migrations/0006_add_samples_per_minute.sql`](opendata_main/migrations/0006_add_samples_per_minute.sql).
