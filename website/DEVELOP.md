# Website development set up

Start a shell in `/website` and do the following:

 0. Create a `virtualenv` with Python 3.10
 1. `pip install poetry==1.4.2`
 2. `poetry install`
 3. Initialise submodules included into this repo:
   ```git submodule update --init```
 4. Create a PostgreSQL user named `opendata`:
    ```sudo -u postgres createuser -d -l -P opendata```
 5. Create a database named `opendata`:
     ```sudo -u postgres createdb -O opendata opendata```
 6. Add `opendata.local` to `/etc/hosts` as an alias of 127.0.0.1:
    ```
    127.0.0.1    localhost opendata.local  # studio.local can be added on the same line as localhost
     ...
    ```
 7. Copy `opendata/settings.example.py` to `opendata/settings.py`
    * Obtain an OAuth client and secret key from Blender ID and set the
      `BLENDER_ID.OAUTH_CLIENT` and `BLENDER_ID.OAUTH_SECRET` properties.
    * Make sure your PostgreSQL settings are correct.
 8. `./manage.sh migrate`
 9. Create a superuser `echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@example.com', 'password')" | python manage.py shell`
 10. `yarn install`
 11. Ensure `./test.sh` succeeds.

Optionally, you can import a [snapshot](https://opendata.blender.org/download/) of the benchmarks by using:
`./manage.sh import_snapshot snapshot.jsonl`
